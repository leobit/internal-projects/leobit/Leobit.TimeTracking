﻿namespace Leobit.TimeTracking.BL.Infrastructure.Settings
{
    public class ProjectCleanupSettings
    {
        public double CleanupIntervalInDays { get; set; }
    }
}
