﻿namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
    public class ProjectReportingReopenRequestSettings
    {
        public double ExpirationTimeInHours { get; set; }
        public double ApprovedExpirationTimeInHours { get; set; }
        public double CleanupIntervalInMinutes { get; set; }
    }
}
