﻿namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
    public class ProjectSettingsBackupSettings
    {
        public double ExpirationTimeInHours { get; set; }
        public double CleanupIntervalInMinutes { get; set; }
    }
}
