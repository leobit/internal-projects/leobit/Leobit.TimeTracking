﻿namespace Leobit.TimeTracking.BL.Infrastructure.Settings
{
    public class SlackReopenRequestMessagesSettings
    {
        public double UpdateMessageIntervalInMinutes { get; set; }
    }
}
