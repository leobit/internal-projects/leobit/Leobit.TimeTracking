using AutoMapper;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Models;
using System;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class ProjectSettingsBackupProfile : Profile
    {
        public ProjectSettingsBackupProfile()
        {
            CreateMap<ProjectSettingsBackup, ProjectSettingsBackupDTO>()
                .ForMember(
                    pDTO => pDTO.ExpirationTime,
                    options => options.MapFrom(p =>
                        new DateTime(p.ExpirationTime.Year, p.ExpirationTime.Month, p.ExpirationTime.Day, p.ExpirationTime.Hour, p.ExpirationTime.Minute, 0, DateTimeKind.Utc)));
            CreateMap<ProjectSettingsBackupDTO, ProjectSettingsBackup>();
        }
    }
}
