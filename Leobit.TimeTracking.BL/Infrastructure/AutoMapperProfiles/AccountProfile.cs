using System.Linq;
using AutoMapper;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<Account, AccountDTO>()
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => !src.Project.Any() || src.Project.Any(p => !p.IsClosed)));

            CreateMap<AccountDTO, Account>();

            CreateMap<Account, AccountShortDTO>();
        }
    }
}
