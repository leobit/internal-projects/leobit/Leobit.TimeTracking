﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class SentSlackMessageProfile : Profile
    {
        public SentSlackMessageProfile()
        {
            CreateMap<SentSlackMessageResponseDTO, SentSlackMessageDTO>()
                .ForMember(d => d.TimeStamp, options => options.MapFrom(s => s.SlackSendResponse.TimeStamp))
                .ForMember(d => d.ChannelId, options => options.MapFrom(s => s.SlackSendResponse.ChannelId));
        }
    }
}
