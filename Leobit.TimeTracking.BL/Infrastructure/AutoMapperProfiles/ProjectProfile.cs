using AutoMapper;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Models;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<Project, ProjectDTO>()
                .ForMember(pDTO => pDTO.ProjectSettingsBackup, options => options.MapFrom((p, pDTO, i, context) => 
                    context.Mapper.Map<ProjectSettingsBackupDTO>(p.ProjectSettingsBackup)))
                .ForMember(pDTO => pDTO.ProjectReopenRequests, options => options.MapFrom(p => p.ProjectReportingReopenRequests));

            CreateMap<ProjectDTO, Project>()
                .ForMember(p => p.AccountId, options => options.MapFrom(pDTO => pDTO.Account.Id))
                .ForMember(p => p.Account, options => options.Ignore())
                .ForMember(p => p.CategoryId, options => options.MapFrom(pDTO => pDTO.Category.Id))
                .ForMember(p => p.Category, options => options.Ignore())
                .ForMember(p => p.ProjectSettingsBackup, options => options.Ignore())
                .ForMember(p => p.ProjectReportingReopenRequests, options => options.Ignore());

            CreateMap<Project, ProjectShortDTO>()
                .ForMember(psDTO => psDTO.Restriction, options => options.Ignore());

            CreateMap<Project, ProjectClosingSettingsDTO>();
        }
    }
}