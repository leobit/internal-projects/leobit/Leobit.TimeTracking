using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Common;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using System;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class TimeReportProfile : Profile
    {
        public TimeReportProfile()
        {
            CreateMap<TimeReport, TimeReportDTO>()
                .ForMember(trDTO => trDTO.InternalHours, options => options.MapFrom(tr => (double)tr.InternalHours))
                .ForMember(trDTO => trDTO.ExternalHours, options => options.MapFrom(tr => (double)tr.ExternalHours))
                .ForMember(trDTO => trDTO.Account, options => options.MapFrom(tr => tr.Project.Account))
                .ForMember(trDTO => trDTO.ReportingDate, options => options.MapFrom(tr => new DateTime(tr.ReportingDate.Year, tr.ReportingDate.Month, tr.ReportingDate.Day, 0, 0, 0, DateTimeKind.Utc)))
                .ForMember(
                    trDTO => trDTO.StartTime, 
                    options => options.MapFrom(tr => 
                        tr.StartTime.HasValue 
                            ? (DateTime?)new DateTime(tr.ReportingDate.Year, tr.ReportingDate.Month, tr.ReportingDate.Day, tr.StartTime.Value.Hour, tr.StartTime.Value.Minute, 0, DateTimeKind.Utc) 
                            : null));

            CreateMap<TimeReportDTO, TimeReport>()
                .ForMember(tr => tr.UserId, options => options.MapFrom(trDTO => trDTO.User.Id))
                .ForMember(tr => tr.User, options => options.Ignore())
                .ForMember(tr => tr.ProjectId, options => options.MapFrom(trDTO => trDTO.Project.Id))
                .ForMember(tr => tr.Project, options => options.Ignore())
                .ForMember(tr => tr.ReportingDate, options => options.MapFrom(trDTO => trDTO.ReportingDate.Date))
                .ForMember(tr => tr.ProjectPrioritySnapshot, options => options.MapFrom(trDTO => trDTO.Project.ProjectPriority));

            CreateMap<TimeReportHistory, TimeReportDTO>()
                .ForMember(trDTO => trDTO.Id, options => options.MapFrom(tr => tr.TimeReportId))
                .ForMember(trDTO => trDTO.InternalHours, options => options.MapFrom(tr => (double)tr.InternalHours))
                .ForMember(trDTO => trDTO.ExternalHours, options => options.MapFrom(tr => (double)tr.ExternalHours))
                .ForMember(trDTO => trDTO.Account, options => options.MapFrom(tr => tr.Project.Account))
                .ForMember(trDTO => trDTO.ReportingDate, options => options.MapFrom(tr => new DateTime(tr.ReportingDate.Year, tr.ReportingDate.Month, tr.ReportingDate.Day, 0, 0, 0, DateTimeKind.Utc)));

            CreateMap<TimeReportDTO, TimeReportHistory>()
                .ForMember(tr => tr.TimeReportId, options => options.MapFrom(trDTO => trDTO.Id))
                .ForMember(tr => tr.UserId, options => options.MapFrom(trDTO => trDTO.User.Id))
                .ForMember(tr => tr.User, options => options.Ignore())
                .ForMember(tr => tr.ProjectId, options => options.MapFrom(trDTO => trDTO.Project.Id))
                .ForMember(tr => tr.ProjectPrioritySnapshot, options => options.MapFrom(trDTO => trDTO.Project.ProjectPriority))
                .ForMember(tr => tr.Project, options => options.Ignore())
                .ForMember(tr => tr.ReportingDate, options => options.MapFrom(trDTO => trDTO.ReportingDate.Date));

            BackupOptions editOptions = null;
            CreateMap<TimeReport, TimeReportHistory>()
                .ConvertUsing(tr => tr.ToHistory(editOptions));

            CreateMap<TimeReportHistory, TimeReport>()
                .ConvertUsing(trh => trh.FromHistory());

            CreateMap<TimeReport, SpreadSheetTimeReportDTO>()
                .ForMember(tr => tr.UserId, options => options.MapFrom(tr => tr.User.Id))
                .ForMember(tr => tr.Username, options => options.MapFrom(tr => tr.User.FirstName + " " + tr.User.LastName))
                .ForMember(tr => tr.ProjectId, options => options.MapFrom(tr => tr.Project.Id))
                .ForMember(tr => tr.Project, options => options.MapFrom(tr => tr.Project.Name))
                .ForMember(tr => tr.Division, options => options.MapFrom(tr => tr.Project.Category.OrganizationUnit.Division.Name))
                .ForMember(tr => tr.DivisionId, options => options.MapFrom(tr => tr.Project.Category.OrganizationUnit.Division.Id))
                .ForMember(tr => tr.OrganizationUnit, options => options.MapFrom(tr => tr.Project.Category.OrganizationUnit.Name))
                .ForMember(tr => tr.OrganizationUnitId, options => options.MapFrom(tr => tr.Project.Category.OrganizationUnit.Id))
                .ForMember(tr => tr.Category, options => options.MapFrom(tr => tr.Project.Category.Name))
                .ForMember(tr => tr.CategoryId, options => options.MapFrom(tr => tr.Project.Category.Id))
                .ForMember(tr => tr.Account, options => options.MapFrom(tr => tr.Project.Account.Name))
                .ForMember(tr => tr.BudgetOwnerId, options => options.MapFrom(tr => tr.Project.Account.BudgetOwnerId))
                .ForMember(tr => tr.AccountId, options => options.MapFrom(tr => tr.Project.AccountId))
                .ForMember(tr => tr.ReportingDate, options => options.MapFrom(tr => tr.ReportingDate.Date))
                .ForMember(tr => tr.ReportingMonth, options => options.MapFrom(tr => tr.ReportingDate.ToString("yyyy-MM")))
                .ForMember(tr => tr.ReportingYear, options => options.MapFrom(tr => tr.ReportingDate.Year))
                .ForMember(tr => tr.ProjectPriority, options => options.MapFrom(tr => tr.Project.ProjectPriority))
                .ForMember(tr => tr.ProjectType, options => options.MapFrom(tr => tr.Project.ProjectType));
        }
    }
}