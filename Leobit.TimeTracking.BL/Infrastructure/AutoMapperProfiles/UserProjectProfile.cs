using AutoMapper;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class UserProjectProfile : Profile
    {
        public UserProjectProfile()
        {
            CreateMap<UserProject, UserProjectDTO>();
            CreateMap<UserProjectDTO, UserProject>()
                .ForMember(up => up.UserId, options => options.MapFrom(upDTO => upDTO.User.Id))
                .ForMember(up => up.User, options => options.Ignore())
                .ForMember(up => up.ProjectId, options => options.MapFrom(upDTO => upDTO.Project.Id))
                .ForMember(up => up.Project, options => options.Ignore());
        }
    }
}