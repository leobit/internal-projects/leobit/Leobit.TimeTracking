﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class PermissionProfile : Profile
    {
        public PermissionProfile()
        {
            CreateMap<Permission, PermissionDTO>();
        }
    }
}
