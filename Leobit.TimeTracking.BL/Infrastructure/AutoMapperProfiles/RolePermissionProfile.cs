﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class RolePermissionProfile : Profile
    {
        public RolePermissionProfile()
        {
            CreateMap<RolePermission, RolePermissionDTO>();

            CreateMap<RolePermissionDTO, RolePermission>()
                .ForMember(rp => rp.PermissionId, options => options.MapFrom(rpDTO => rpDTO.Permission.Id))
                .ForMember(rp => rp.Permission, options => options.Ignore());
        }
    }
}
