﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class OrganizationUnitProfile : Profile
    {
        public OrganizationUnitProfile()
        {
            CreateMap<OrganizationUnit, OrganizationUnitDTO>();

            CreateMap<OrganizationUnitDTO, OrganizationUnit>()
                .ForMember(o => o.DivisionId, options => options.MapFrom(oDTO => oDTO.Division.Id))
                .ForMember(o => o.Division, options => options.Ignore());

            CreateMap<OrganizationUnit, OrganizationUnitShortDTO>();
            CreateMap<OrganizationUnitShortDTO, OrganizationUnit>();
        }
    }
}
