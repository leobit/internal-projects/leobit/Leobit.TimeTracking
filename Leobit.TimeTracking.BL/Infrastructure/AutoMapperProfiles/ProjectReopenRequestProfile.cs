using AutoMapper;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Models;
using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class ProjectReportingReopenRequestProfile : Profile
    {
        public ProjectReportingReopenRequestProfile()
        {
            CreateMap<ProjectReportingReopenRequest, ProjectReopenRequestDetailsDTO>()
                .ForMember(pDTO => pDTO.SenderFullName, options =>
                    options.MapFrom(p => $"{p.Sender.FirstName} {p.Sender.LastName}"))
                .ForMember(pDTO => pDTO.ProjectManagerApprovedFullName, options =>
                    options.MapFrom(p => p.ProjectManagerApproved != null ? $"{p.ProjectManagerApproved.FirstName} {p.ProjectManagerApproved.LastName}" : null))
                .ForMember(pDTO => pDTO.ExpirationTime, options => 
                    options.MapFrom(p => new DateTime(p.ExpirationTime.Year, p.ExpirationTime.Month, p.ExpirationTime.Day, p.ExpirationTime.Hour, p.ExpirationTime.Minute, p.ExpirationTime.Second, DateTimeKind.Utc)));

            CreateMap<ProjectReportingReopenRequest, ProjectReopenRequestDTO>()
                .ForMember(dest => dest.SenderFullName, opt => opt.MapFrom(src => $"{src.Sender.FirstName} {src.Sender.LastName}"))
                .ForMember(dest => dest.ProjectName, opt => opt.MapFrom(src => src.Project.Name))
                .ForMember(dest => dest.AccountName, opt => opt.MapFrom(src => src.Project.Account.Name))
                .ForMember(dest => dest.SentMessages, opt => opt.MapFrom(src => JsonConvert.DeserializeObject<ICollection<SentSlackMessageDTO>>(src.SentMessages)));

        }
    }
}
