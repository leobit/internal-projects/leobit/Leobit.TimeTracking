﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class RoleProfile : Profile
    {
        public RoleProfile()
        {
            CreateMap<Role, RoleShortDTO>();

            CreateMap<Role, RoleDTO>();
            CreateMap<RoleDTO, Role>().AfterMap(UpdateRolePermissions);
        }

        private void UpdateRolePermissions(RoleDTO roleDTO, Role role)
        {
            foreach (var rolePermission in role.RolePermission)
            {
                rolePermission.RoleId = role.Id;
            }
        }
    }
}
