﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class TimeOffProfile : Profile
    {
        public TimeOffProfile()
        {
            CreateMap<TransferredVacationBalance, TransferredVacationBalanceDTO>().ReverseMap();

            CreateMap<CustomDayOff, CustomDayOffDTO>()
                .ForMember(d => d.User, o => o.MapFrom(s => s.User))
                .ForMember(d => d.LastEditor, o => o.MapFrom(s => s.LastEditor));

            CreateMap<CustomDayOffDTO, CustomDayOff>()
                .ForMember(cdo => cdo.User, options => options.Ignore())
                .ForMember(cdo => cdo.LastEditor, options => options.Ignore());
        }
    }
}