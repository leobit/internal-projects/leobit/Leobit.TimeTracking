﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class SettingsProfile : Profile
    {
        public SettingsProfile()
        {
            CreateMap<DAL.Entities.Settings, SettingsDTO>();

            CreateMap<SettingsDTO, DAL.Entities.Settings>()
                .ForMember(s => s.Uniqueness, options => options.MapFrom(sDTO => true));
        }
    }
}
