﻿using AutoMapper;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<User, UserShortDTO>();
            CreateMap<UserShortDTO, User>();
        }
    }
}
