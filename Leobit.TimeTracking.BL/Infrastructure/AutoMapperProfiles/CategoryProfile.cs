﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class CategoryProfile : Profile
    {
        public CategoryProfile()
        {
            CreateMap<Category, CategoryDTO>();

            CreateMap<CategoryDTO, Category>()
                .ForMember(c => c.OrganizationUnitId, options => options.MapFrom(cDTO => cDTO.OrganizationUnit.Id))
                .ForMember(c => c.OrganizationUnit, options => options.Ignore());

            CreateMap<Category, CategoryShortDTO>();
            CreateMap<CategoryShortDTO, Category>();
            CreateMap<CategoryInputDTO, Category>()
                .ForMember(c => c.OrganizationUnitId, options => options.MapFrom(cDTO => cDTO.OrganizationUnit.Id))
                .ForMember(c => c.OrganizationUnit, options => options.Ignore()); 
        }
    }
}
