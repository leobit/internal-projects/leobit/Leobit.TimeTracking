﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class EmployeeProfile : Profile
    {
        public EmployeeProfile()
        {
            CreateMap<DAL.Entities.Employee, UserSlackInfoDTO>()
                .ForMember(tr => tr.UserId, options => options.MapFrom(trDTO => trDTO.Id)).ReverseMap();

            CreateMap<DAL.Entities.Employee, EmployeeBaseDTO>().ReverseMap();

            CreateMap<DAL.Entities.EmployeeHistory, EmployeeHistoryDTO>().ReverseMap();
        }
    }
}