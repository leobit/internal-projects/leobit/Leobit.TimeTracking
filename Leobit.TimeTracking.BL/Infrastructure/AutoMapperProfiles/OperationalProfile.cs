﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using System;

namespace Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles
{
    public class OperationalProfile : Profile
    {
        public OperationalProfile()
        {
            CreateMap<Holiday, HolidayDTO>()
                .ForMember(d => d.Date, opt => opt.MapFrom(s => new DateTime(s.Year, s.Month, s.Day)));

            CreateMap<HolidayDTO, Holiday>()
                .ForMember(d => d.Year, opt => opt.MapFrom(s => s.Date.Year))
                .ForMember(d => d.Month, opt => opt.MapFrom(s => s.Date.Month))
                .ForMember(d => d.Day, opt => opt.MapFrom(s => s.Date.Day));
        }
    }
}
