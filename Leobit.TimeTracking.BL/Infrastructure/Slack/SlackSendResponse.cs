﻿using System.Text.Json.Serialization;

namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
    public class SlackSendResponse
    {
        [JsonPropertyName("ok")]
        public bool IsSuccess { get; set; }

        [JsonPropertyName("ts")]
        public string TimeStamp { get; set; }

        [JsonPropertyName("channel")]
        public string ChannelId { get; set; }
    }

    public class SlackDeleteResponse
    {
        [JsonPropertyName("ok")]
        public bool IsSuccess { get; set; }
    }
}
