﻿using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
    public class SlackMessageHistoryResponse
    {
        public bool Ok { get; set; }
        public List<Message> Messages { get; set; }
    }

    public class Message
    {
        public string Text { get; set; }
        public string Ts { get; set; }
    }
}