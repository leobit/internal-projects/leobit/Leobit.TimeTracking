﻿namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
    public class SlackSettings
    {
        public string SlackHostUrl { get; set; }
        public string SlackBotToken { get; set; }
        public string TimeTrackingHostUrl { get; set; }
    }
}