﻿using System;

namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
	public static class SlackMessageReopenProjectBuilder
	{
		public static string CreateRequestMessagePreview(
			string senderUserName,
			string projectName,
			string accountName)
			=> $"{senderUserName} requests to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}";

        public static string CreateExpiredMessage(
            string receiverUserName,
            string senderUserName,
            string projectName,
            string accountName,
            DateTime requestedDateRangeStart,
            DateTime requestedDateRangeEnd)
            => $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}}, 
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":""Request to re-open reporting from {senderUserName} for project {GetProjectAccountDescription(projectName, accountName)} expired""			
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
					}}
			}}
		]";

        public static string CreateRequestForApprovalMessageBody(
			string receiverUserName,
			string senderUserName,
			string projectName,
			string accountName,
			DateTime requestedDateRangeStart,
			DateTime requestedDateRangeEnd,
			string approveRequestLinkUrl)
			=> $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}}, 
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":""{senderUserName} requests to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}""			
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":""Would you like to approve?""
					}},
				""accessory"":{{
					""type"":""button"",
					""text"":{{
						""type"":""plain_text"",
						""text"":"":white_check_mark: Approve"",
						""emoji"":true
						}},
					""value"":""approve_123"",
					""url"":""{approveRequestLinkUrl}"",
					""style"": ""primary"",
					""action_id"":""button-action""
				}}
			}}
		]";

        public static string CreateRequestForNotificationMessageBody(
			string receiverUserName,
			string senderUserName,
			string projectName,
			string accountName,
			DateTime requestedDateRangeStart,
			DateTime requestedDateRangeEnd)
			=> $@"[
					{{
						""type"":""header"",
						""text"":{{
							""type"":""plain_text"",
							""text"":""Hey {receiverUserName},"",
							""emoji"":true
							}}
					}}, 
					{{
						""type"":""section"",
						""text"":{{
							""type"":""mrkdwn"",
							""text"":""{senderUserName} requests to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}""			
							}}
					}},
					{{
						""type"":""section"",
						""text"":{{
							""type"":""mrkdwn"",
							""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
							}}
					}}
				]";

        public static string CreateDeniedResponseMessagePreview(string projectName, string accountName)
            => $"The request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)} was denied";

        public static string CreateResponseMessageOnDeniedRequest(
            string receiverUserName,
            string projectName,
            string accountName,
            DateTime requestedDateRangeStart,
            DateTime requestedDateRangeEnd)
            => $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":"":x: The request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)} was *denied*""
					}}
			}},
            {{
	            ""type"":""section"",
	            ""text"":{{
		            ""type"":""mrkdwn"",
		            ""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
		            }}
            }}
		]";

        public static string CreateApprovedResponseMessagePreview(string projectName, string accountName)
            => $"The request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)} was approved";

        public static string CreateResponseMessageOnApprovedRequest(
            string receiverUserName,
            string projectManagerName,
            string projectName,
			string accountName,
            DateTime requestedDateRangeStart,
            DateTime requestedDateRangeEnd)
            => $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":"":white_check_mark: The request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)} was *approved* by {projectManagerName}""
					}}
			}},
            {{
	            ""type"":""section"",
	            ""text"":{{
		            ""type"":""mrkdwn"",
		            ""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
		            }}
            }}
		]";

        public static string CreateResponseMessageOnApprovedRequestForApprover(
           string receiverUserName,
           string projectName,
		   string accountName,
           DateTime requestedDateRangeStart,
           DateTime requestedDateRangeEnd)
           => $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":"":white_check_mark: You *approved* the request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}""	
					}}
			}},
            {{
	            ""type"":""section"",
	            ""text"":{{
		            ""type"":""mrkdwn"",
		            ""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
		            }}
            }}
		]";

        public static string CreateResponseMessageOnApprovedRequestForSender(
            string receiverUserName,
            string projectManagerName,
            string projectName,
			string accountName,
            DateTime requestedDateRangeStart,
            DateTime requestedDateRangeEnd)
            => $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":"":white_check_mark: Response to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}""	
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"": ""*Approved* by {projectManagerName}""
					}}
			}},
            {{
	            ""type"":""section"",
	            ""text"":{{
		            ""type"":""mrkdwn"",
		            ""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
		            }}
            }},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"": ""You have *1 hour* from now to proceed with reporting""
					}}
			}}
		]";

        public static string CreateRequestMessagePreviewForSender(
            string projectName,
			string accountName)
            => $"You request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}";

        public static string CreateRequestMessageBodyForSender(
            string receiverUserName,
            string projectName,
			string accountName,
            DateTime requestedDateRangeStart,
            DateTime requestedDateRangeEnd)
            => $@"[
			{{
				""type"":""header"",
				""text"":{{
					""type"":""plain_text"",
					""text"":""Hey {receiverUserName},"",
					""emoji"":true
					}}
			}}, 
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"":""You request to re-open reporting for project {GetProjectAccountDescription(projectName, accountName)}""			
					}}
			}},
			{{
				""type"":""section"",
				""text"":{{
					""type"":""mrkdwn"",
					""text"": ""Requested date range: *{requestedDateRangeStart:dd.MM.yyyy}* — *{requestedDateRangeEnd:dd.MM.yyyy}*""			
					}}
			}}
		]";

        private static string GetProjectAccountDescription(string projectName, string accountName) => $"{accountName} - {projectName}";
    }
}