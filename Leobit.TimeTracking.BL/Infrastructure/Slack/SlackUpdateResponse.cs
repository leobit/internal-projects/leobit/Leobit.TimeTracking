﻿using System.Text.Json.Serialization;

namespace Leobit.TimeTracking.BL.Infrastructure.Slack
{
    public class SlackUpdateResponse
    {
        [JsonPropertyName("ok")]
        public bool IsSuccess { get; set; }

        [JsonPropertyName("ts")]
        public string TimeStamp { get; set; }
    }
}
