﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL
{
    public enum AccountType : int
    {
        External = 0,
        Internal = 1,
        Operational= 2
    }
}
