﻿using System;

namespace Leobit.TimeTracking.BL.Types
{
    public struct DateRange
    {
        public DateTime StartDate { get; private set; }
        public DateTime EndDate { get; private set; }

        public DateRange(DateTime startDate, DateTime endDate)
        {
            AssertStartDateFollowsEndDate(startDate, endDate);

            StartDate = startDate;
            EndDate = endDate;
        }

        public readonly void Deconstruct(out DateTime startDate, out DateTime endDate)
        {
            startDate = StartDate;
            endDate = EndDate;
        }

        private static void AssertStartDateFollowsEndDate(DateTime startDate, DateTime endDate)
        {
            if (endDate < startDate)
            {
                throw new InvalidOperationException("Start Date must be less than or equal to End Date");
            }
        }
    }
}
