﻿namespace Leobit.TimeTracking.BL
{
    public enum ProjectType
    {
        External = 0,
        Internal = 1,
        Vacation = 2,
        SickLeaveWithoutCertificate = 3,
        SickLeaveWithCertificate = 4,
        VacationUnpaid = 5,
        Reserve = 6,
        Onboarding = 7
    }
}
