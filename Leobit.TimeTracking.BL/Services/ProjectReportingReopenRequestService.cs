using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Extensions;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Infrastructure.Settings;
using Leobit.TimeTracking.BL.Infrastructure.Slack;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Interfaces;

namespace Leobit.TimeTracking.BL.Services
{
    public class ProjectReportingReopenRequestService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;
        private readonly ProjectManagerService _projectManagerService;
        private readonly IOperationalService _operationalService;
        private readonly IVacationService _vacationService;
        private readonly SlackService _slackService;
        private readonly UkrainianDateTimeProvider _ukrainianDateTimeProvider;
        private readonly ILogger<ProjectReportingReopenRequest> _logger;
        private readonly ProjectSettingsBackupSettings _projectSettingsBackupSettings;
        private readonly ProjectReportingReopenRequestSettings _projectReportingReopenRequestSettings;
        private readonly SlackReopenRequestMessagesSettings _slackReopenRequestMessagesSettings;

        private const int MiddleOfMonthDay = 15;

        public ProjectReportingReopenRequestService(
            IMapper mapper,
            LeobitContext dbContext,
            ProjectManagerService projectManagerService,
            IOperationalService operationalService,
            SlackService slackService,
            UkrainianDateTimeProvider ukrainianDateTimeProvider,
            ILogger<ProjectReportingReopenRequest> logger,
            IOptions<ProjectSettingsBackupSettings> projectSettingsBackupSettingsOptions,
            IOptions<ProjectReportingReopenRequestSettings> projectReportingReopenRequestSettingsOptions,
            IOptions<SlackReopenRequestMessagesSettings> slackReopenRequestMessagesSettingsOptions,
            IVacationService vacationService)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _projectManagerService = projectManagerService;
            _operationalService = operationalService;
            _slackService = slackService;
            _ukrainianDateTimeProvider = ukrainianDateTimeProvider;
            _logger = logger;
            _projectSettingsBackupSettings = projectSettingsBackupSettingsOptions.Value;
            _projectReportingReopenRequestSettings = projectReportingReopenRequestSettingsOptions.Value;
            _slackReopenRequestMessagesSettings = slackReopenRequestMessagesSettingsOptions.Value;
            _vacationService = vacationService;
        }

        public Task<bool> ProjectReportingReopenRequestExists(int id)
        {
            return _dbContext.ProjectReportingReopenRequest
                .NotDeleted()
                .AnyAsync(p => p.Id == id);
        }

        public async Task<bool> IsProjectReportingReopenRequestApproved(int id)
        {
            var requestReopenProject = await GetProjectReportingReopenRequest(id);
            return requestReopenProject?.ProjectManagerApprovedId != null;
        }

        public async Task<ICollection<ReopenReportingApprovalAuthorityDTO>> GetReopenReportingAllowance(int projectId, string requestorUserId, CancellationToken cancellationToken = default)
        {
            var users = new List<UserShortDTO>();

            var budgetOwner = await GetReopenProjectAvailableBudgetOwnersAsync(projectId, requestorUserId);
            var projectManagers = await GetReopenProjectAvailableProjectManagersAsync(projectId);

            users.AddRange(projectManagers);
            if (budgetOwner is not null) users.Add(budgetOwner);

            return users.Select(u => new ReopenReportingApprovalAuthorityDTO()
            {
                FirstName = u.FirstName,
                LastName = u.LastName,
                ReopenAllowedFromDate = GetValidReopenReportingFromDate(_ukrainianDateTimeProvider.Now),
                ReopenAllowedEndDate = DateTime.Now.AddDays(-1)
            }).ToList();
        }

        public async Task<IEnumerable<string>> GetReopenRequestNotificationRecipients(ProjectReopenRequestDTO projectReopenRequestInfoDTO)
        {
            var recipientsDetails = await GetReopenRequestRecipientDetails(projectReopenRequestInfoDTO);

            return recipientsDetails.Select(r => r.FullName);
        }

        public async Task<bool> RequestReopenProjectReporting(ProjectReopenRequestDTO projectReopenRequestDTO)
        {
            var project = await LoadActiveProjectNoTrackingAsync(projectReopenRequestDTO.ProjectId)
                ?? throw new ConflictException("Active project not found");

            projectReopenRequestDTO.CreationTimestamp = _ukrainianDateTimeProvider.UtcNow;

            await ValidateProjectReopenRequestDTO(projectReopenRequestDTO, project);

            var projectReopenRequest = await CreateProjectReportingReopenRequest(projectReopenRequestDTO);
            projectReopenRequestDTO.ProjectName = project.Name;
            projectReopenRequestDTO.AccountName = project.Account.Name;
            projectReopenRequestDTO.SenderFullName = GetEmployeeFullName(projectReopenRequest.SenderId);
            projectReopenRequestDTO.ProjectReportingReopenRequestId = projectReopenRequest.Id;

            var responses = await SendReopenRequestMessages(projectReopenRequestDTO);
            var sentMessages = _mapper.Map<SentSlackMessageDTO[]>(
                responses.Where(r => r.SlackSendResponse.IsSuccess));

            await EditProjectReopenRequestSentMessages(projectReopenRequest, sentMessages);

            return responses.All(r => r.SlackSendResponse.IsSuccess);
        }

        public async Task<bool> ApproveRequestReopenProjectReporting(int id, string projectManagerId)
        {
            var projectReopenRequest = await GetProjectReportingReopenRequest(id);

            var currentProject = await LoadActiveProjectNoTrackingAsync(projectReopenRequest.ProjectId)
                ?? throw new ConflictException("Active project not found");

            await EditProjectReopenRequestApproved(projectReopenRequest, projectManagerId);

            var responses = await SendApprovalMessages(
                ConvertProjectReopenRequestToDTO(projectReopenRequest, currentProject, projectManagerId));

            var sentMessages = _mapper.Map<SentSlackMessageDTO[]>(
                responses.Where(r => r.SlackSendResponse.IsSuccess));

            await EditProjectReopenRequestSentMessages(projectReopenRequest, sentMessages);

            return responses.All(r => r.SlackSendResponse.IsSuccess);
        }

        public async Task<bool> DeclineRequestReopenProjectReporting(int id)
        {
            var projectReopenRequest = await GetProjectReportingReopenRequest(id);

            var currentProject = await LoadActiveProjectNoTrackingAsync(projectReopenRequest.ProjectId);

            await SoftDeleteProjectReopenRequests(new List<int> { id });

            var respones = await SendDenialMessages(
                ConvertProjectReopenRequestToDTO(projectReopenRequest, currentProject, null));

            var sentMessages = _mapper.Map<SentSlackMessageDTO[]>(
                respones.Where(r => r.SlackSendResponse.IsSuccess));

            await EditProjectReopenRequestSentMessages(projectReopenRequest, sentMessages);

            return respones.All(r => r.SlackSendResponse.IsSuccess);
        }

        public async Task OverrideProjectSettingsBackup(ProjectDTO projectDTO, Project existingProject)
        {
            var incomingProject = await LoadProjectNoTrackingAsync(projectDTO.Id);
            var projectSettings = await GetProjectSettingsBackup(projectDTO.Id);

            if (projectDTO.RestoreClosingSettingsLater)
            {
                await CreateProjectSettingsBackup(incomingProject, projectSettings);
            }
            else if (projectSettings != null)
            {
                await DeleteProjectSettingsBackup(projectSettings, existingProject);
            }
        }

        public async Task CleanUpExpiredProjectSettingsBackups(DateTime expirationTime)
        {
            var projectSettingsBackups = await _dbContext.ProjectSettingsBackup.Where(r => r.ExpirationTime <= expirationTime).ToListAsync();
            if (projectSettingsBackups.Any())
            {
                var projects = await RestoreOriginalConfigurationSetting(projectSettingsBackups);
                _dbContext.Project.UpdateRange(projects);
                _dbContext.ProjectSettingsBackup.RemoveRange(projectSettingsBackups);
                await _dbContext.SaveChangesAsync();
            }
        }

        public async Task CleanUpExpiredProjectReopenRequests(DateTime expirationTime)
        {
            var projectReopenRequestIds = await _dbContext.ProjectReportingReopenRequest
                .Where(p => p.ExpirationTime <= expirationTime && !p.IsDeleted)
                .Select(p => p.Id)
                .ToListAsync();

            await SoftDeleteProjectReopenRequests(projectReopenRequestIds, expirationTime);
        }

        public async Task UpdateProjectReopenRequestSentMessages(DateTime expirationTime)
        {
            var fifteenMinutesAgo = expirationTime.AddMinutes(-(_slackReopenRequestMessagesSettings.UpdateMessageIntervalInMinutes));

            var projectReopenRequests = await _dbContext.ProjectReportingReopenRequest
                .Where(p => p.ExpirationTime > fifteenMinutesAgo && p.ExpirationTime <= expirationTime)
                .Include(p => p.Sender)
                .Include(p => p.ProjectManagerApproved)
                .Include(p => p.Project)
                .ThenInclude(p => p.Account)
                .ToListAsync();

            foreach (var projectReopenRequest in projectReopenRequests)
            {
                var projectReopenRequestDTO = _mapper.Map<ProjectReopenRequestDTO>(projectReopenRequest);
                var userIds = projectReopenRequestDTO.SentMessages.Select(sm => sm.UserId).ToList();

                var recipientsSlackInfos = await GetUsersSlackInfo(userIds);

                var slackInfoDict = recipientsSlackInfos.ToDictionary(info => info.UserId, info => info);

                var tasks = new List<Task>();

                foreach (var sentMessage in projectReopenRequestDTO.SentMessages)
                {
                    if (slackInfoDict.TryGetValue(sentMessage.UserId, out var slackInfo))
                    {
                        var projectReopenRequestSlackMessageDTO = new ProjectReopenRequestSlackMessageDTO
                        {
                            ReceiverSlackInfo = slackInfo,
                            ProjectReopenRequestInfo = projectReopenRequestDTO
                        };

                        var task = _slackService.UpdateReopenProjectMessageOnExpiration(projectReopenRequestSlackMessageDTO, sentMessage.TimeStamp, sentMessage.ChannelId);
                        tasks.Add(task);
                    }
                }
                await Task.WhenAll(tasks);
            }

        }

        private async Task<List<Project>> RestoreOriginalConfigurationSetting(List<ProjectSettingsBackup> requestReopenProject)
        {
            var projects = new List<Project>();
            foreach (var request in requestReopenProject)
            {
                var projectConfigurationSnapshot = JsonConvert.DeserializeObject<ProjectClosingSettingsDTO>(request.ConfigurationSnapshot);
                var project = await _dbContext.Project.NotClosed().FirstOrDefaultAsync(p => p.Id == request.ProjectId);
                if (project != null)
                {
                    project.OverrideClosingSettings = projectConfigurationSnapshot.OverrideClosingSettings;
                    project.AfterClosing = projectConfigurationSnapshot.AfterClosing;
                    project.BeforeClosing = projectConfigurationSnapshot.BeforeClosing;

                    projects.Add(project);
                }
            }
            return projects;
        }

        private async Task<SentSlackMessageResponseDTO[]> SendReopenRequestMessages(ProjectReopenRequestDTO projectReopenRequestInfoDTO)
        {
            var recipientDetails = await GetReopenRequestRecipientDetails(projectReopenRequestInfoDTO);

            if (!recipientDetails.Any())
            {
                throw new ConflictException("Recipients of project reopen request could not be resolved");
            }

            List<Task<SentSlackMessageResponseDTO>> slackSendTasks = recipientDetails.Select(recipient =>
            {
                var slackMessageDTO = new ProjectReopenRequestSlackMessageDTO
                {
                    ReceiverSlackInfo = recipient.UserSlackInfo,
                    ProjectReopenRequestInfo = projectReopenRequestInfoDTO
                };

                Task<SlackSendResponse> sendTask = recipient.Type == SlackReopenRequestMessageType.ForApproval
                    ? _slackService.SendReopenProjectRequestMessageForApprovalAsync(slackMessageDTO)
                    : _slackService.SendReopenProjectRequestMessageForNotificationAsync(slackMessageDTO);

                return sendTask.ContinueWith(task => new SentSlackMessageResponseDTO
                {
                    UserId = recipient.UserSlackInfo.UserId,
                    SlackSendResponse = task.Result
                }, TaskContinuationOptions.OnlyOnRanToCompletion);
            }).ToList();

            slackSendTasks.Add(SendReopenRequestMessageToSender(projectReopenRequestInfoDTO));

            return await Task.WhenAll(slackSendTasks);
        }

        private async Task<List<string>> GetReopenRequestRecipients(ProjectReopenRequestDTO projectReopenRequestDTO)
        {
            var budgetOwner = await LoadBudgetOwnerByProjectId(projectReopenRequestDTO.ProjectId);

            if (budgetOwner is null)
            {
                _logger.LogError("Cannot process project reopen request by id = {RequestId}. Budget owner is unknown",
                    projectReopenRequestDTO.ProjectReportingReopenRequestId);

                return new List<string>();
            }

            if (projectReopenRequestDTO.SenderId == budgetOwner.Id)
            {
                var budgetOwnerPrimaryManagerId = await _projectManagerService.GetManagerId(budgetOwner.Id);

                if (budgetOwnerPrimaryManagerId is null)
                {
                    _logger.LogError("Cannot process project reopen request by id = {RequestId}. Manager of budget owner is unknown. Budget owner id = {BudgetOwnerId}",
                        projectReopenRequestDTO.ProjectReportingReopenRequestId, budgetOwner.Id);

                    return new List<string>();
                }

                if (await _vacationService.IsEmployeeOnVacation(budgetOwnerPrimaryManagerId, DateTime.UtcNow))
                {
                    return new List<string> { await _projectManagerService.GetManagerId(budgetOwnerPrimaryManagerId) };
                }

                return new List<string> { budgetOwnerPrimaryManagerId };
            }

            var requestCreationDate = _ukrainianDateTimeProvider.ConvertUtcToLocalTime(projectReopenRequestDTO.StartDate).Date;

            if (requestCreationDate < GetFirstDateOfLastWeek())
            {
                if (await _vacationService.IsEmployeeOnVacation(budgetOwner.Id, DateTime.UtcNow))
                {
                    return new List<string> { await _projectManagerService.GetManagerId(budgetOwner.Id) };
                }

                return new List<string> { budgetOwner.Id };
            }

            var projectManagersIds = await LoadProjectManagerIds(projectReopenRequestDTO.ProjectId);

            var userVacationStatuses = await _vacationService.AreEmployeesOnVacation(projectManagersIds, DateTime.UtcNow);

            var availablePMs = projectManagersIds
                .Where(pmId => userVacationStatuses.TryGetValue(pmId, out var isOnVacation) && !isOnVacation)
                .ToList();

            if (!availablePMs.Any())
            {
                return new List<string> { budgetOwner.Id };
            }

            return availablePMs;
        }

        private async Task<IEnumerable<SlackReopenRequestMessageDto>> GetReopenRequestRecipientDetails(ProjectReopenRequestDTO projectReopenRequestDTO)
        {
            var budgetOwner = await GetReopenProjectAvailableBudgetOwnersAsync(projectReopenRequestDTO.ProjectId, projectReopenRequestDTO.SenderId);
            var availablePms = await GetReopenProjectAvailableProjectManagersAsync(projectReopenRequestDTO.ProjectId);
            var isLastWeekProjectReopenRequest = IsLastWeekProjectReopenRequest(projectReopenRequestDTO);
            var recipientDetails = new List<SlackReopenRequestMessageDto>();

            recipientDetails.AddRange(availablePms.Select(pm => new SlackReopenRequestMessageDto
            {
                FullName = $"{pm.FirstName} {pm.LastName}",
                Type = isLastWeekProjectReopenRequest 
                        ? SlackReopenRequestMessageType.ForApproval
                        : SlackReopenRequestMessageType.ForNotification,
                UserId = pm.Id
            }));

            if (!string.IsNullOrWhiteSpace(budgetOwner.Id) && (!isLastWeekProjectReopenRequest || !availablePms.Any()))
            {
                recipientDetails.Add(new SlackReopenRequestMessageDto
                {
                    UserId = budgetOwner.Id,
                    FullName = $"{budgetOwner.FirstName} {budgetOwner.LastName}",
                    Type = SlackReopenRequestMessageType.ForApproval,
                });
            }

            var recipientsSlackInfos = await GetUsersSlackInfo(recipientDetails.Select(x => x.UserId));
            foreach (var recipient in recipientDetails)
            {
                recipient.UserSlackInfo = recipientsSlackInfos.FirstOrDefault(x => x.UserId == recipient.UserId);
            }

            return recipientDetails;
        }

        private async Task<UserShortDTO> GetReopenProjectAvailableBudgetOwnersAsync(int projectId, string senderId)
        {
            var budgetOwner = await LoadBudgetOwnerByProjectId(projectId);

            if (budgetOwner is null)
            {
                return null;
            }

            if (senderId == budgetOwner.Id || await _vacationService.IsEmployeeOnVacation(budgetOwner.Id, DateTime.UtcNow))
            {
                var budgetOwnerPrimaryManager = await _projectManagerService.GetEmployeePrimaryManager(budgetOwner.Id);

                if (budgetOwnerPrimaryManager is null)
                {
                    return null;
                }

                if (await _vacationService.IsEmployeeOnVacation(budgetOwnerPrimaryManager.Id, DateTime.UtcNow))
                {
                    return await _projectManagerService.GetEmployeePrimaryManager(budgetOwnerPrimaryManager.Id);
                }

                return budgetOwnerPrimaryManager;
            }

            return budgetOwner;
        }

        private async Task<IEnumerable<UserShortDTO>> GetReopenProjectAvailableProjectManagersAsync(int projectId)
        {
            var projectManagers = await LoadProjectManagersAsync(projectId);
            var userVacationStatuses = await _vacationService.AreEmployeesOnVacation(projectManagers.Select(pm => pm.Id).ToList(), DateTime.UtcNow);

            var availablePMs = projectManagers
                .Where(pm => userVacationStatuses.TryGetValue(pm.Id, out var isOnVacation) && !isOnVacation)
                .ToList();

            return availablePMs;
        }

        private bool IsLastWeekProjectReopenRequest(ProjectReopenRequestDTO projectReopenRequestInfoDTO)
        {
            var requestCreationDate = _ukrainianDateTimeProvider.ConvertUtcToLocalTime(projectReopenRequestInfoDTO.StartDate).Date;
            return requestCreationDate >= GetFirstDateOfLastWeek();
        }

        private async Task<SentSlackMessageResponseDTO> SendReopenRequestMessageToSender(ProjectReopenRequestDTO projectReopenRequestDTO)
        {
            var senderSlackInfo = (await GetUsersSlackInfo(new[] { projectReopenRequestDTO.SenderId }))
               .FirstOrDefault();

            var slackMessageReopenProject = new ProjectReopenRequestSlackMessageDTO
            {
                ReceiverSlackInfo = senderSlackInfo,
                ProjectReopenRequestInfo = projectReopenRequestDTO
            };

            return new SentSlackMessageResponseDTO
            {
                UserId = projectReopenRequestDTO.SenderId,
                SlackSendResponse = await _slackService.SendReopenProjectRequestMessageForUserAsync(slackMessageReopenProject)
            };
        }

        private async Task<SentSlackMessageResponseDTO[]> SendApprovalMessages(ProjectReopenRequestDTO projectReopenRequestInfoDTO)
        {
            var recipientIds = await GetReopenRequestRecipients(projectReopenRequestInfoDTO);

            var recipientSlackInfos = await GetUsersSlackInfo(recipientIds);
            var requestApproverSlackInfo = (await GetUsersSlackInfo(new[] { projectReopenRequestInfoDTO.ProjectManagerApprovedId }))
               .FirstOrDefault();

            List<Task<SentSlackMessageResponseDTO>> slackSendTasks = recipientSlackInfos.Select(recipientSlackInfo =>
            {
                var slackMessageDTO = new ProjectReopenRequestSlackMessageDTO
                {
                    ReceiverSlackInfo = recipientSlackInfo,
                    ProjectReopenRequestInfo = projectReopenRequestInfoDTO
                };

                Task<SlackSendResponse> sendTask = recipientSlackInfo.UserId == projectReopenRequestInfoDTO.ProjectManagerApprovedId
                    ? _slackService.SendApprovedRequestForApproverAsync(slackMessageDTO)
                    : _slackService.SendApprovedRequestAsync(slackMessageDTO, requestApproverSlackInfo);

                return sendTask.ContinueWith(task => new SentSlackMessageResponseDTO
                {
                    UserId = recipientSlackInfo.UserId,
                    SlackSendResponse = task.Result
                }, TaskContinuationOptions.OnlyOnRanToCompletion);
            }).ToList();

            slackSendTasks.Add(SendApprovalMessageToSender(projectReopenRequestInfoDTO, requestApproverSlackInfo));

            return await Task.WhenAll(slackSendTasks);
        }

        private async Task<SentSlackMessageResponseDTO[]> SendDenialMessages(ProjectReopenRequestDTO projectReopenRequestInfoDTO)
        {
            var recipientIds = await GetReopenRequestRecipients(projectReopenRequestInfoDTO);
            var recipientSlackInfos = await GetUsersSlackInfo(recipientIds);

            List<Task<SlackSendResponse>> slackSendResponseTasks = recipientSlackInfos.Select(recipientSlackInfo =>
            {
                return _slackService.SendDeniedRequestAsync(new ProjectReopenRequestSlackMessageDTO
                {
                    ReceiverSlackInfo = recipientSlackInfo,
                    ProjectReopenRequestInfo = projectReopenRequestInfoDTO
                });
            }).ToList();

            var slackSendResponses = await Task.WhenAll(slackSendResponseTasks);

            var sentMessageResponses =  slackSendResponses
                .Select(slackSendResponse => new SentSlackMessageResponseDTO
                {
                    UserId = projectReopenRequestInfoDTO.SenderId,
                    SlackSendResponse = slackSendResponse
                }).ToArray();

            return sentMessageResponses;
        }

        private async Task<SentSlackMessageResponseDTO> SendApprovalMessageToSender(ProjectReopenRequestDTO projectReopenRequestDTO, UserSlackInfoDTO requestApproverSlackInfo)
        {
            var senderUserSlackInfo = (await GetUsersSlackInfo(new[] { projectReopenRequestDTO.SenderId })).First();

            var slackMessageReopenProject = new ProjectReopenRequestSlackMessageDTO
            {
                ReceiverSlackInfo = senderUserSlackInfo,
                ProjectReopenRequestInfo = projectReopenRequestDTO
            };

            return new SentSlackMessageResponseDTO
            {
                UserId = projectReopenRequestDTO.SenderId,
                SlackSendResponse = await _slackService.SendReopenProjectResponseMessageOnApprovedRequestForSenderAsync(
                    slackMessageReopenProject, requestApproverSlackInfo)
            };
        }

        private DateTime GetFirstDateOfLastWeek() => _ukrainianDateTimeProvider.Now.Date.AddWeeks(-1).FirstDateOfWeek();

        private static DateTime GetValidReopenReportingFromDate(DateTime requestDate)
        {
            var date = requestDate.Date;

            return date.Day switch
            {
                <= MiddleOfMonthDay => date.AddMonths(-1).FirstDateTimeOfMonth(), // previous month is allowed

                _ => date.FirstDateTimeOfMonth() // only current month is allowed
            };
        }

        private async Task ValidateProjectReopenRequestDTO(ProjectReopenRequestDTO projectReopenRequestDTO, Project project)
        {
            if (ProjectService.IsOperationalProjectType(project.ProjectType))
            {
                throw new ConflictException("You cannot make reporting re-open request for operational projects");
            }

            var validReopenFromDate = GetValidReopenReportingFromDate(
                _ukrainianDateTimeProvider.ConvertUtcToLocalTime(projectReopenRequestDTO.CreationTimestamp));

            if (projectReopenRequestDTO.StartDate < validReopenFromDate)
            {
                throw new ConflictException("The requested reporting range is not allowed");
            }

            var globalSettings = await _dbContext.Settings.AsNoTracking().SingleAsync();

            var (beforeClosing, afterClosing) = ProjectService.CalculateReopenDateRange(
                project,
                globalSettings,
                projectReopenRequestDTO.StartDate,
                projectReopenRequestDTO.EndDate,
                DateTime.UtcNow);

            if (project.BeforeClosing >= beforeClosing && project.AfterClosing >= afterClosing)
            {
                throw new ConflictException("The requested reporting range is already fully available");
            }

            var holidays = await _operationalService.GetHolidaysInRange(projectReopenRequestDTO.StartDate,
                projectReopenRequestDTO.EndDate);

            var anyBusinessDays = true;
            var date = projectReopenRequestDTO.StartDate;

            while (date <= projectReopenRequestDTO.EndDate)
            {
                if (!date.IsWeekend() && !holidays.Any(h => h.Date == date))
                {
                    anyBusinessDays = false;
                    break;
                }
                date = date.AddDays(1);
            }

            if (anyBusinessDays)
            {
                throw new ConflictException("The requested reporting range fully consists of weekends and/or holidays");
            }
        }

        private ProjectReopenRequestDTO ConvertProjectReopenRequestToDTO(
            ProjectReportingReopenRequest projectReopenRequest, Project project, string projectManagerId)
        {
            return new ProjectReopenRequestDTO
            {
                SenderId = projectReopenRequest.SenderId,
                SenderFullName = GetEmployeeFullName(projectReopenRequest.SenderId),
                ProjectId = projectReopenRequest.ProjectId,
                ProjectName = project.Name,
                AccountName = project.Account.Name,
                StartDate = projectReopenRequest.StartDate,
                EndDate = projectReopenRequest.EndDate,
                ProjectManagerApprovedId = projectManagerId,
                SentMessages = JsonConvert.DeserializeObject<ICollection<SentSlackMessageDTO>>(projectReopenRequest.SentMessages)
            };
        }

        private Task<List<UserSlackInfoDTO>> GetUsersSlackInfo(IEnumerable<string> employeeIds)
        {
            return _dbContext.Employee
                .AsNoTracking()
                .Where(e => employeeIds.Contains(e.Id))
                .ProjectTo<UserSlackInfoDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        private string GetEmployeeFullName(string userId)
        {
            return _dbContext.Employee.AsNoTracking().First(e => e.Id == userId).FullName;
        }

        private Task<Project> LoadProjectNoTrackingAsync(int projectId)
        {
            return _dbContext.Project.AsNoTracking()
                .FirstOrDefaultAsync(p => p.Id == projectId);
        }

        private Task<Project> LoadActiveProjectNoTrackingAsync(int projectId)
        {
            return _dbContext.Project.AsNoTracking()
                .NotClosed()
                .Include(p => p.Account)
                .FirstOrDefaultAsync(p => p.Id == projectId);
        }

        private Task<ProjectSettingsBackup> GetProjectSettingsBackup(int projectId)
        {
            return _dbContext.ProjectSettingsBackup.FirstOrDefaultAsync(p => p.ProjectId == projectId);
        }

        private async Task<ProjectSettingsBackup> CreateProjectSettingsBackup(
            Project currentProject,
            ProjectSettingsBackup projectBackup)
        {
            var currentDateTime = DateTime.UtcNow;

            string snapshot;
            if (projectBackup != null)
            {
                snapshot = projectBackup.ConfigurationSnapshot;
                await DeleteProjectSettingsBackup(projectBackup, currentProject);
            }
            else
            {
                snapshot = JsonConvert.SerializeObject(_mapper.Map<ProjectClosingSettingsDTO>(currentProject));
            }

            var projectBackupToCreate = new ProjectSettingsBackup
            {
                ProjectId = currentProject.Id,
                TimeCreated = currentDateTime,
                ExpirationTime = currentDateTime.AddHours(_projectSettingsBackupSettings.ExpirationTimeInHours),
                ConfigurationSnapshot = snapshot
            };

            _dbContext.ProjectSettingsBackup.Add(projectBackupToCreate);
            _dbContext.SaveChanges();

            return projectBackupToCreate;
        }

        private async Task DeleteProjectSettingsBackup(ProjectSettingsBackup projectSettingsBackup, Project project)
        {
            _dbContext.ProjectSettingsBackup.Remove(projectSettingsBackup);
            await _dbContext.SaveChangesAsync();
        }

        private Task<ProjectReportingReopenRequest> GetProjectReportingReopenRequest(int id)
        {
            return _dbContext.ProjectReportingReopenRequest
                .NotDeleted()
                .FirstOrDefaultAsync(p => p.Id == id);
        }

        private Task<List<ProjectReportingReopenRequest>> GetProjectReportingReopenRequests(List<int> ids)
        {
            return _dbContext.ProjectReportingReopenRequest
                .NotDeleted()
                .Where(p => ids.Contains(p.Id))
                .ToListAsync();
        }

        private async Task<ProjectReportingReopenRequest> CreateProjectReportingReopenRequest(ProjectReopenRequestDTO projectReopenRequestDTO)
        {
            var projectReportingReopenRequest = new ProjectReportingReopenRequest
            {
                ProjectId = projectReopenRequestDTO.ProjectId,
                StartDate = projectReopenRequestDTO.StartDate,
                EndDate = projectReopenRequestDTO.EndDate,
                CreationTimestamp = projectReopenRequestDTO.CreationTimestamp,
                ExpirationTime = projectReopenRequestDTO.CreationTimestamp.AddHours(_projectReportingReopenRequestSettings.ExpirationTimeInHours),
                SenderId = projectReopenRequestDTO.SenderId
            };
            _dbContext.ProjectReportingReopenRequest.Add(projectReportingReopenRequest);

            await _dbContext.SaveChangesAsync();
            return projectReportingReopenRequest;
        }

        private async Task EditProjectReopenRequestSentMessages(
            ProjectReportingReopenRequest projectReopenRequest,
            SentSlackMessageDTO[] reportingRequestMessagesDTO)
        {
            projectReopenRequest.SentMessages = JsonConvert.SerializeObject(reportingRequestMessagesDTO);
            _dbContext.ProjectReportingReopenRequest.Update(projectReopenRequest);
            await _dbContext.SaveChangesAsync();
        }

        private async Task EditProjectReopenRequestApproved(ProjectReportingReopenRequest projectReopenRequest, string projectManagerId)
        {
            projectReopenRequest.ProjectManagerApprovedId = projectManagerId;
            projectReopenRequest.ApprovalTimestamp = DateTime.UtcNow;
            projectReopenRequest.ExpirationTime = DateTime.UtcNow.AddHours(_projectReportingReopenRequestSettings.ApprovedExpirationTimeInHours);
            _dbContext.ProjectReportingReopenRequest.Update(projectReopenRequest);
            await _dbContext.SaveChangesAsync();
        }

        private async Task SoftDeleteProjectReopenRequests(List<int> projectReopenRequestIds, DateTime? deletionTimestamp = null)
        {
            var projectReportingReopenRequests = await GetProjectReportingReopenRequests(projectReopenRequestIds);
            foreach (var projectReportingReopenRequest in projectReportingReopenRequests)
            {
                projectReportingReopenRequest.IsDeleted = true;
                projectReportingReopenRequest.DeletionTimestamp = deletionTimestamp ?? DateTime.UtcNow;
            }

            _dbContext.ProjectReportingReopenRequest.UpdateRange(projectReportingReopenRequests);
            await _dbContext.SaveChangesAsync();
        }

        private Task<UserShortDTO> LoadBudgetOwnerByProjectId(int projectId, CancellationToken cancellationToken = default)
        {
            return _dbContext.Project
                .ById(projectId)
                .Include(p => p.Account)
                .ThenInclude(a => a.BudgetOwner)
                .Select(p => p.Account.BudgetOwner)
                .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(cancellationToken);
        }

        private Task<bool> IsReopenReportingRequestPending(ProjectReopenRequestDTO projectReopenRequestDTO)
        {
            var now = DateTime.UtcNow;
            return _dbContext.ProjectReportingReopenRequest
                .NotDeleted()
                .AnyAsync(p => p.ProjectId == projectReopenRequestDTO.ProjectId
                               && p.SenderId == projectReopenRequestDTO.SenderId && p.ExpirationTime >= now);
        }

        private Task<List<string>> LoadProjectManagerIds(int projectId)
        {
            return _dbContext.ProjectManager
                .Where(p => p.ProjectId == projectId)
                .Select(x => x.UserId)
                .ToListAsync();
        }

        private Task<UserShortDTO[]> LoadProjectManagersAsync(int projectId)
        {
            return _dbContext.ProjectManager
                .Where(p => p.ProjectId == projectId)
                .Select(p => p.User)
                .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
                .ToArrayAsync();
        }
    }
}
