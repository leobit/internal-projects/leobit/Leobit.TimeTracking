using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;

namespace Leobit.TimeTracking.BL.Services
{
    public class AccountService
    {
        private const string DefaultBudgetOwnerId = "vlysovych";
        private const string EligibleBudgetOwnersCacheKey = "eligibleBudgetOwners";

        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;
        private readonly IMemoryCache _memoryCache;

        public AccountService(IMapper mapper, LeobitContext dbContext, IMemoryCache memoryCache)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _memoryCache = memoryCache;
        }

        public async Task<List<AccountDTO>> GetAccountsFromProjects(List<int> projectIds)
        {
            return await _dbContext.Account
                .GroupJoin(_dbContext.Project,
                    a => a.Id,
                    pa => pa.AccountId,
                    (a, pas) => new { a, pas })
                .SelectMany(
                    ap => ap.pas.DefaultIfEmpty(),
                    (ap, pa) => new { Account = ap.a, Project = pa }
                )
                .Where(j => j.Project == null || projectIds.Contains(j.Project.Id))
                .Select(j => j.Account)
                .Distinct()
                .OrderByDescending(p => p.Id)
                .ProjectTo<AccountDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<AccountDTO> CreateAccount(AccountDTO accountDto, CancellationToken cancellationToken = default)
        {
            await AssertBudgetOwnerIsEligible(accountDto.BudgetOwnerId, cancellationToken);

            var account = _mapper.Map<Account>(accountDto);

            _dbContext.Account.Add(account);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return _mapper.Map<AccountDTO>(account);
        }

        public async Task<AccountDTO> EditAccount(AccountDTO accountDto, CancellationToken cancellationToken = default)
        {
            if (!await _dbContext.Account.AnyAsync(a => a.Id == accountDto.Id, cancellationToken))
            {
                throw new NotFoundException($"Account with id = {accountDto.Id} does not exist");
            }

            await AssertBudgetOwnerIsEligible(accountDto.BudgetOwnerId, cancellationToken);

            var account = _mapper.Map<Account>(accountDto);

            _dbContext.Account.Update(account);
            await _dbContext.SaveChangesAsync(cancellationToken);

            return _mapper.Map<AccountDTO>(account);
        }

        public Task<List<AccountShortDTO>> GetAccountShorts()
        {
            return _dbContext.Account
                .ProjectTo<AccountShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<AccountEligibleBudgetOwnerDTO> GetEligibleBudgetOwners(CancellationToken cancellationToken)
        {
            return _memoryCache.GetOrCreateAsync(EligibleBudgetOwnersCacheKey, async entry =>
            {
                entry.AbsoluteExpirationRelativeToNow = TimeSpan.FromMinutes(15);
                
                var activeEmployeeQueryable = _dbContext.Employee.Where(e => e.Active && e.Level >= (int)EmployeeLevel.Manager);

                var eligibleUsers = await _dbContext.User
                    .Join(activeEmployeeQueryable, u => u.Id, e => e.Id, (u, _) => u)
                    .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
                    .ToArrayAsync(cancellationToken);

                return new AccountEligibleBudgetOwnerDTO
                {
                    DefaultBudgetOwnerId = DefaultBudgetOwnerId,
                    EligibleBudgetOwners = eligibleUsers
                };
            });
        }

        private async Task AssertBudgetOwnerIsEligible(string budgetOwnerId, CancellationToken cancellationToken)
        {
            var eligibleBudgetOwners = await GetEligibleBudgetOwners(cancellationToken);
            var eligibleBudgetOwnersIds = eligibleBudgetOwners.EligibleBudgetOwners.Select(bo => bo.Id); 

            if (!eligibleBudgetOwnersIds.Contains(budgetOwnerId))
            {
                throw new ConflictException($"User with id = {budgetOwnerId} is not eligible budget owner");
            }
        }
    }
}
