﻿using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class ReserveService
    {
        private readonly LeobitContext _dbContext;
        private readonly IVacationService _vacationService;

        public ReserveService(LeobitContext dbContext, IVacationService vacationService)
        {
            _dbContext = dbContext;
            _vacationService = vacationService;
        }

        public async Task<List<ReserveEmployeeInformationDTO>> GetReserveEmployeesInformation(int year)
        {
            var employeeTimeReportsInReserveDetails = await _dbContext.TimeReport
                .AsNoTracking()
                .Include(tr => tr.Project)
                .Include(tr => tr.User)
                .Where(tr => tr.ReportingDate.Year == year
                    && (tr.Project.ProjectPriority == (short)ProjectPriority.VeryLow
                        || tr.Project.ProjectPriority == (short)ProjectPriority.Low))
                .GroupBy(tr => new
                {
                    Id = tr.User.Id,
                    FirstName = tr.User.FirstName,
                    LastName = tr.User.LastName
                })
                .Where(g => g.Sum(tr => tr.InternalHours) > 8)
                .OrderBy(g => g.Key.FirstName)
                    .ThenBy(g => g.Key.LastName)
                .Select(g => new
                {
                    EmployeeId = g.Key.Id,
                    EmployeeFullName = $"{g.Key.FirstName} {g.Key.LastName}",
                    LastReportTimestamp = g.Max(x => x.Timestamp)
                })
                .ToListAsync();

            var employeeIdsInReserve = employeeTimeReportsInReserveDetails.Select(e => e.EmployeeId).ToList();
            var activeEmployeeIdsInReserve = await _dbContext.Employee
                .Where(e => !e.TerminationDate.HasValue
                    && employeeIdsInReserve.Contains(e.Id))
                .Select(e => e.Id)
                .ToListAsync();

            var employeeVacationInformationDictionary = (await _vacationService
                .GetVacationsInformation(activeEmployeeIdsInReserve, year))
                .Where(evi => evi.CurrentVacationBalance > 0)
                .ToDictionary(evi => evi.EmployeeId, evi => evi);

            var reserveEmployeeInformations = employeeTimeReportsInReserveDetails
                .Where(tr => employeeVacationInformationDictionary.Keys.Contains(tr.EmployeeId))
                .Select(etr =>
                {
                    return new ReserveEmployeeInformationDTO
                    {
                        EmployeeId = etr.EmployeeId,
                        EmployeeFullName = etr.EmployeeFullName,
                        LastReportTimestamp = etr.LastReportTimestamp,
                        ReportedDaysForCurrentYear = employeeVacationInformationDictionary[etr.EmployeeId].ReportedReserveDays,
                        CurrentVacationBalance = employeeVacationInformationDictionary[etr.EmployeeId].CurrentVacationBalance
                    };
                })
                .OrderBy(etr => etr.EmployeeFullName)
                .ToList();

            var previousMonth = DateTime.Now.Month - 1;

            var customDayOffs = (await _vacationService.GetCustomDayOffs(activeEmployeeIdsInReserve, year)).ToList();
            var customDayOffsForCurrentYear = customDayOffs
                .GroupBy(cdo => new
                {
                    UserId = cdo.User.Id
                })
                .Select(g => new
                {
                    UserId = g.Key.UserId,
                    PenaltyAmountForCurrentYear = g.Sum(x => x.ExtraDayOff),
                    VacationPenaltyForPreviouMonth = g.FirstOrDefault(x => x.Month == previousMonth)
                })
                .ToList();

            foreach (var customDayOffForCurrentYear in customDayOffsForCurrentYear)
            {
                var reserveEmployeeInformation = reserveEmployeeInformations.FirstOrDefault(e => e.EmployeeId == customDayOffForCurrentYear.UserId);
                if (reserveEmployeeInformation is not null)
                {
                    reserveEmployeeInformation.PenaltyAmountForCurrentYear = (double)customDayOffForCurrentYear.PenaltyAmountForCurrentYear;
                    reserveEmployeeInformation.VacationPenalty = customDayOffForCurrentYear.VacationPenaltyForPreviouMonth;
                }
            }

            var vacationInformation = await _vacationService.GetVacationsInformation(activeEmployeeIdsInReserve, year);

            foreach (var reserveEmployeeInformation in reserveEmployeeInformations)
            {
                var vacationInformationForEmployee = vacationInformation.FirstOrDefault(vi => vi.EmployeeId == reserveEmployeeInformation.EmployeeId);
                if (vacationInformationForEmployee is not null)
                {
                    var totalVacationDaysTakenForCurrentYear = vacationInformationForEmployee.YearData.Sum(yd => yd.Used.GetValueOrDefault());
                    reserveEmployeeInformation.AfterVacationTaken = reserveEmployeeInformation.ReportedDaysForCurrentYear - (totalVacationDaysTakenForCurrentYear * 3);
                }
            }

            return reserveEmployeeInformations;
        }
    }
}
