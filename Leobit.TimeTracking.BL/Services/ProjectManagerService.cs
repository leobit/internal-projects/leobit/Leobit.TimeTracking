﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class ProjectManagerService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;
        
        public ProjectManagerService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public async Task<UserShortDTO> GetEmployeePrimaryManager(string employeeId, CancellationToken cancellationToken = default)
        {
            var employee = await _dbContext.Employee.AsNoTracking().FirstOrDefaultAsync(e => e.Id == employeeId, cancellationToken)
                ?? throw new ConflictException("Employee not found");

            var primaryManager = await _dbContext.Employee
                .ProjectTo<UserSlackInfoDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(e => e.FullName == employee.Manager, cancellationToken);

            if (primaryManager is null)
            {
                var settings = await _dbContext.Settings.AsNoTracking().SingleAsync();
                if (settings.DefaultBo is null)
                {
                    throw new ConflictException("Primary manager not found");
                }
                return await _dbContext.User
                    .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
                    .FirstOrDefaultAsync(e => e.Id == settings.DefaultBo, cancellationToken);
                
            }

            return await _dbContext.User
                .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync(e => e.Id == primaryManager.UserId, cancellationToken);
        }

        public async Task<UserManagerDTO> GetEmployeeManagers(string userId)
        {
            var lastBusinessMonthStart = DateTime.UtcNow.AddMonths(-1).FirstDateTimeOfMonth();

            var userRecentlyReportedProjectsIds = await _dbContext.TimeReport
                .ByUserIds(new[] { userId })
                .Where(tr => tr.Project.ProjectType == (int)ProjectType.External)
                .Where(tr => tr.ReportingDate >= lastBusinessMonthStart)
                .GroupBy(tr => tr.ProjectId)
                .Where(g => g.Sum(tr => tr.InternalHours) > 20)
                .Select(g => g.Key)
                .Distinct()
                .ToListAsync();

            var employee = await _dbContext.Employee.AsNoTracking().FirstOrDefaultAsync(e => e.Id == userId)
                ?? throw new ConflictException("Employee not found");

            var primaryManager = await _dbContext.Employee.AsNoTracking().FirstOrDefaultAsync(e => e.FullName == employee.Manager);

            var primaryManagerId = primaryManager?.Id ?? "";

            var managersEntities = await _dbContext.ProjectManager
                .AsNoTracking()
                .Include(pm => pm.User)
                .Include(pm => pm.Project)
                .Where(pm => userRecentlyReportedProjectsIds.Contains(pm.ProjectId))
                .ToListAsync();

            var managers = managersEntities
                .GroupBy(g => g.ProjectId)
                .Where(g => g.All(pm => pm.UserId != primaryManagerId))
                .SelectMany(g => g.Select(pm => new ManagerDTO
                {
                    ProjectName = pm.Project.Name,
                    UserId = pm.User.Id
                }))
                .GroupBy(g => g.UserId)
                .Where(g => g.Key != userId)
                .Select(g => new ManagerDTO
                {
                    UserId = g.Key,
                    ProjectName = string.Join(", ", g.Select(m => m.ProjectName))
                })
                .ToList();

            var managersIds = managers.Select(m => m.UserId).ToList();

            var employeesData = _dbContext.Employee
                .Where(e => managersIds.Contains(e.Id) || e.Id == userId)
                .ToDictionary(e => e.Id);

            managers.ForEach(manager =>
            {
                manager.FullName = employeesData[manager.UserId].FullName;
                manager.SlackId = employeesData[manager.UserId].SlackUserId;
            });

            if (primaryManager != null)
            {
                managers.Add(new ManagerDTO
                {
                    IsPrimary = true,
                    UserId = primaryManager.Id,
                    FullName = primaryManager.FullName,
                    SlackId = primaryManager.SlackUserId
                });
            }

            return new UserManagerDTO
            {
                UserId = userId,
                FullName = employeesData[userId].FullName,
                SlackId = employeesData[userId].SlackUserId,
                Managers = managers
            };
        }

        public async Task<string> GetManagerId(string userId)
        {
            var employee = await _dbContext.Employee.AsNoTracking().FirstOrDefaultAsync(e => e.Id == userId)
                           ?? throw new ConflictException("Employee not found");

            var primaryManager = await _dbContext.Employee.AsNoTracking().FirstOrDefaultAsync(e => e.FullName == employee.Manager);

            if (primaryManager is null)
            {
                var settings = await _dbContext.Settings.AsNoTracking().SingleAsync();
                if (settings.DefaultBo is null)
                {
                    throw new Exception("Default BO not found, contact administrator");
                }
                return settings.DefaultBo;
            }

            return primaryManager.Id;
        }

        public async Task<ProjectManagerDTO> CreateProjectManager(ProjectManagerDTO projectManagerDTO)
        {
            var user = await _dbContext.User
                .ById(projectManagerDTO.User.Id)
                .FirstOrDefaultAsync();

            var project = await _dbContext.Project
                .ById(projectManagerDTO.Project.Id)
                .Include(p => p.Account)
                .FirstOrDefaultAsync();

            if (user != null && project != null)
            {
                var existingProjectManager = await _dbContext.ProjectManager
                    .ByUserIds(new[] { projectManagerDTO.User.Id })
                    .ByProjectIds(new[] { projectManagerDTO.Project.Id })
                    .FirstOrDefaultAsync();

                if (existingProjectManager == null)
                {
                    var projectManager = _mapper.Map<ProjectManager>(projectManagerDTO);

                    _dbContext.ProjectManager.Add(projectManager);
                    await _dbContext.SaveChangesAsync();

                    return _mapper.Map<ProjectManagerDTO>(projectManager);
                }
                else
                {
                    throw new ConflictException("Manager already assigned to project");
                }
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task DeleteProjectManager(ProjectManagerDTO projectManagerDTO)
        {
            var projectManager = await _dbContext.ProjectManager
                .ByUserIds(new[] { projectManagerDTO.User.Id })
                .ByProjectIds(new[] { projectManagerDTO.Project.Id })
                .FirstOrDefaultAsync();

            if (projectManager != null)
            {
                _dbContext.ProjectManager.Remove(projectManager);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }
    }
}
