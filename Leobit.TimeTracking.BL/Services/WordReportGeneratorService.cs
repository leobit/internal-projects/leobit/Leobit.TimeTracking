﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DocumentFormat.OpenXml;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Wordprocessing;
using DW = DocumentFormat.OpenXml.Drawing.Wordprocessing;
using A = DocumentFormat.OpenXml.Drawing;
using PIC = DocumentFormat.OpenXml.Drawing.Pictures;
using System.IO;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.BL.Services
{
    public class WordReportGeneratorService
    {

        private const string _logoPath = "Resources/leobitLogo.jpeg";
        private const string _defaultFont = "Calibri";
        private const string _headerFont = "Arial";
        private const string _headerFontSize = "32";
        private const string _defaultFontSize = "22";
        private const string _headerBackgroundColor = "70AD47";
        private const string _headerFontColor = "FFFFFF";
        private const string _dataBackgroundColor = "E2EFD9";

        public async Task<MemoryStream> CreateTimeReportsWordDocumentAsync(List<TimeReportDTO> timeReports, DateTime dateFrom, DateTime dateTo)
        {
            var projectName = GetProjectName(timeReports);
            var dateRange = $"{dateFrom:M/d/yyyy} - {dateTo:M/d/yyyy}";

            var timeReportsExportItems = timeReports
                .Select(tr => new TimeReportExportItem(tr))
                .ToList();

            var stream = new MemoryStream();

            using (var wordDocument = WordprocessingDocument.Create(stream, WordprocessingDocumentType.Document))
            {
                var mainPart = wordDocument.AddMainDocumentPart();
                mainPart.Document = new Document();
                var body = mainPart.Document.AppendChild(new Body());

                AddHeader(mainPart, projectName);
                AddEmptyLines(body, 2);
                AddSummaryTable(body, timeReportsExportItems, dateRange);
                AddEmptyLines(body, 2);
                AddMainTable(body, timeReportsExportItems);
                AddEmptyLines(body, 1);

                mainPart.Document.Save();
            }

            stream.Position = 0;
            return stream;
        }

        private string GetProjectName(List<TimeReportDTO> timeReports)
        {
            var timeReportsWithoutOperational = timeReports.Where(tr => !ProjectService.IsOperationalProjectType(tr.Project.ProjectType));
            var accounts = timeReportsWithoutOperational.Select(tr => tr.Account.Id).Distinct().ToArray();
            if (accounts.Length > 1)
            {
                throw new InvalidOperationException("You can create a report only for one account. You still can do Excel export");
            }
            var projects = timeReportsWithoutOperational.Select(tr => tr.Project).Distinct().ToArray();
            if (projects.Length == 1)
            {
                var project = projects[0];
                return project.Account.Name == project.Name ? project.Name : $"{project.Account.Name} - {project.Name}";
            }
            if (projects.Length == 0)
            {
                throw new InvalidOperationException("Select not only Operational projects. You still can do Excel export");
            }
            return projects[0].Account.Name;
        }

        private void AddHeader(MainDocumentPart mainPart, string projectName)
        {
            var headerPart = mainPart.AddNewPart<HeaderPart>();
            var header = new Header();
            var headerTable = CreateHeaderTable(projectName, headerPart);
            header.Append(headerTable);
            headerPart.Header = header;

            var headerReference = new HeaderReference() { Type = HeaderFooterValues.Default, Id = mainPart.GetIdOfPart(headerPart) };
            var sectionProperties = new SectionProperties();
            sectionProperties.Append(headerReference);
            mainPart.Document.Body.Append(sectionProperties);
        }

        private Table CreateHeaderTable(string projectName, HeaderPart headerPart)
        {
            var headerTable = new Table(
                new TableProperties(
                    new TableWidth() { Width = "100%", Type = TableWidthUnitValues.Pct },
                    new TableBorders(
                        new TopBorder { Val = BorderValues.None },
                        new BottomBorder { Val = BorderValues.None },
                        new LeftBorder { Val = BorderValues.None },
                        new RightBorder { Val = BorderValues.None },
                        new InsideHorizontalBorder { Val = BorderValues.None },
                        new InsideVerticalBorder { Val = BorderValues.None }
                    )
                ),
                new TableGrid(
                    new GridColumn() { Width = "50%" },
                    new GridColumn() { Width = "50%" }
                )
            );

            var logoCell = CreateLogoCell(headerPart);
            var textCell = CreateTextCell($"Time Report for {projectName}");

            headerTable.Append(new TableRow(logoCell, textCell));
            return headerTable;
        }

        private TableCell CreateLogoCell(HeaderPart headerPart)
        {
            var imagePart = headerPart.AddImagePart(ImagePartType.Jpeg);
            using (var imageStream = new FileStream(_logoPath, FileMode.Open))
            {
                imagePart.FeedData(imageStream);
            }

            long widthEmus = (long)(1.89 * 914400);
            long heightEmus = (long)(0.55 * 914400);

            return new TableCell(
                new Paragraph(
                    new Run(
                        new Drawing(
                            new DW.Inline(
                                new DW.Extent() { Cx = widthEmus, Cy = heightEmus },
                                new DW.EffectExtent() { LeftEdge = 0L, TopEdge = 0L, RightEdge = 0L, BottomEdge = 0L },
                                new DW.DocProperties() { Id = (UInt32Value)1U, Name = "Picture 1" },
                                new DW.NonVisualGraphicFrameDrawingProperties(new A.GraphicFrameLocks() { NoChangeAspect = true }),
                                new A.Graphic(
                                    new A.GraphicData(
                                        new PIC.Picture(
                                            new PIC.NonVisualPictureProperties(
                                                new PIC.NonVisualDrawingProperties() { Id = (UInt32Value)0U, Name = "New Bitmap Image.jpg" },
                                                new PIC.NonVisualPictureDrawingProperties()
                                            ),
                                            new PIC.BlipFill(
                                                new A.Blip(
                                                    new A.BlipExtensionList(
                                                        new A.BlipExtension() { Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" }
                                                    )
                                                )
                                                { Embed = headerPart.GetIdOfPart(imagePart), CompressionState = A.BlipCompressionValues.Print },
                                                new A.Stretch(new A.FillRectangle())
                                            ),
                                            new PIC.ShapeProperties(
                                                new A.Transform2D(
                                                    new A.Offset() { X = 0L, Y = 0L },
                                                    new A.Extents() { Cx = widthEmus, Cy = heightEmus }
                                                ),
                                                new A.PresetGeometry(new A.AdjustValueList()) { Preset = A.ShapeTypeValues.Rectangle }
                                            )
                                        )
                                    )
                                    { Uri = "http://schemas.openxmlformats.org/drawingml/2006/picture" }
                                )
                            )
                            {
                                DistanceFromTop = (UInt32Value)0U,
                                DistanceFromBottom = (UInt32Value)0U,
                                DistanceFromLeft = (UInt32Value)0U,
                                DistanceFromRight = (UInt32Value)0U,
                                EditId = "50D07946"
                            }
                        )
                    )
                )
            );
        }

        private TableCell CreateTextCell(string text)
        {
            return new TableCell(
                new Paragraph(
                    new Run(
                        new RunProperties(
                            new Bold(),
                            new RunFonts { Ascii = _headerFont, HighAnsi = _headerFont },
                            new FontSize { Val = _headerFontSize }
                        ),
                        new Text(text) { Space = SpaceProcessingModeValues.Preserve }
                    )
                )
                {
                    ParagraphProperties = new ParagraphProperties(
                        new Justification() { Val = JustificationValues.Right }
                    )
                }
            )
            {
                TableCellProperties = new TableCellProperties(
                    new TableCellVerticalAlignment { Val = TableVerticalAlignmentValues.Center }
                )
            };
        }

        private void AddEmptyLines(Body body, int count)
        {
            for (int i = 0; i < count; i++)
            {
                body.Append(new Paragraph(new Run(new Text(""))));
            }
        }

        private void AddSummaryTable(Body body, List<TimeReportExportItem> timeReportsExportItems, string dateRange)
        {
            var summaryTable = CreateSummaryTable(timeReportsExportItems, dateRange);
            body.Append(summaryTable);
        }

        private void AddMainTable(Body body, List<TimeReportExportItem> timeReportsExportItems)
        {
            var table = CreateTable(timeReportsExportItems);
            body.Append(table);
        }

        private Table CreateSummaryTable(List<TimeReportExportItem> timeReportsExportItems, string dateRange)
        {
            var table = new Table();

            var tableProperties = new TableProperties(
                new TableBorders(
                    new TopBorder { Val = BorderValues.Single, Size = 6 },
                    new BottomBorder { Val = BorderValues.Single, Size = 6 },
                    new LeftBorder { Val = BorderValues.Single, Size = 6 },
                    new RightBorder { Val = BorderValues.Single, Size = 6 },
                    new InsideHorizontalBorder { Val = BorderValues.Single, Size = 6 },
                    new InsideVerticalBorder { Val = BorderValues.Single, Size = 6 }
                ),
                new TableWidth { Width = "5000", Type = TableWidthUnitValues.Pct }
            );
            table.AppendChild(tableProperties);

            var headerRow = new TableRow();
            var headers = new List<string> { "Date Range" };
            headers.AddRange(timeReportsExportItems.Select(item => item.User).Distinct());
            headers.Add("Total");

            foreach (var header in headers)
            {
                var tableCell = new TableCell(new Paragraph(new Run(new Text(header))
                {
                    RunProperties = new RunProperties(
                        new Bold(),
                        new Color { Val = _headerFontColor },
                        new RunFonts { Ascii = _defaultFont, HighAnsi = _defaultFont },
                        new FontSize { Val = _defaultFontSize }
                    )
                })
                {
                    ParagraphProperties = new ParagraphProperties(
                        new Justification { Val = JustificationValues.Center }
                    )
                })
                {
                    TableCellProperties = new TableCellProperties(
                        new TableCellWidth { Type = TableWidthUnitValues.Auto },
                        new Shading { Val = ShadingPatternValues.Clear, Color = "auto", Fill = _headerBackgroundColor },
                        new TableCellVerticalAlignment { Val = TableVerticalAlignmentValues.Center }
                    )
                };
                headerRow.Append(tableCell);
            }
            table.Append(headerRow);

            var dataRow = new TableRow();
            var dateRangeCell = new TableCell(new Paragraph(new Run(new Text(dateRange))
            {
                RunProperties = new RunProperties(
                    new RunFonts { Ascii = _defaultFont, HighAnsi = _defaultFont },
                    new FontSize { Val = _defaultFontSize }
                )
            }));
            dataRow.Append(dateRangeCell);

            var totalHours = 0.0;
            var userHours = timeReportsExportItems.GroupBy(item => item.User)
                .Select(group => new { User = group.Key, Hours = group.Sum(item => item.ExternalHours) });

            foreach (var userHour in userHours)
            {
                var hoursText = new Text(userHour.Hours.ToString());
                var hoursRun = new Run(hoursText)
                {
                    RunProperties = new RunProperties(
                        new RunFonts { Ascii = _defaultFont, HighAnsi = _defaultFont },
                        new FontSize { Val = _defaultFontSize }
                    )
                };
                var hoursParagraph = new Paragraph(hoursRun)
                {
                    ParagraphProperties = new ParagraphProperties(
                        new Justification { Val = JustificationValues.Center }
                    )
                };
                var hoursCell = new TableCell(hoursParagraph)
                {
                    TableCellProperties = new TableCellProperties(
                        new Shading { Val = ShadingPatternValues.Clear, Color = "auto", Fill = _dataBackgroundColor },
                        new TableCellVerticalAlignment { Val = TableVerticalAlignmentValues.Center }
                    )
                };
                dataRow.Append(hoursCell);
                totalHours += userHour.Hours;
            }

            var totalCell = new TableCell(new Paragraph(new Run(new Text(totalHours.ToString()))
            {
                RunProperties = new RunProperties(
                    new RunFonts { Ascii = _defaultFont, HighAnsi = _defaultFont },
                    new FontSize { Val = _defaultFontSize }
                )
            })
            {
                ParagraphProperties = new ParagraphProperties(
                    new Justification { Val = JustificationValues.Center }
                )
            })
            {
                TableCellProperties = new TableCellProperties(
                    new Shading { Val = ShadingPatternValues.Clear, Color = "auto", Fill = _dataBackgroundColor },
                    new TableCellVerticalAlignment { Val = TableVerticalAlignmentValues.Center }
                )
            };
            dataRow.Append(totalCell);

            table.Append(dataRow);

            return table;
        }

        private Table CreateTable(List<TimeReportExportItem> timeReportsExportItems)
        {
            var table = new Table();

            var tableProperties = new TableProperties(
                new TableBorders(
                    new TopBorder { Val = BorderValues.Single, Size = 6 },
                    new BottomBorder { Val = BorderValues.Single, Size = 6 },
                    new LeftBorder { Val = BorderValues.Single, Size = 6 },
                    new RightBorder { Val = BorderValues.Single, Size = 6 },
                    new InsideHorizontalBorder { Val = BorderValues.Single, Size = 6 },
                    new InsideVerticalBorder { Val = BorderValues.Single, Size = 6 }
                ),
                new TableWidth { Width = "5000", Type = TableWidthUnitValues.Pct }
            );
            table.AppendChild(tableProperties);

            var headerRow = new TableRow();
            var headers = new[] { "Date", "Position", "Work Description", "Reported Efforts, Hours" };
            var columnWidths = new[] { "1428", "1428", "5716", "1428" };

            for (int i = 0; i < headers.Length; i++)
            {
                var tableCell = new TableCell(new Paragraph(new Run(new Text(headers[i]))
                {
                    RunProperties = new RunProperties(
                        new Bold(),
                        new Color { Val = _headerFontColor },
                        new RunFonts { Ascii = _defaultFont, HighAnsi = _defaultFont },
                        new FontSize { Val = _defaultFontSize }
                    )
                })
                {
                    ParagraphProperties = new ParagraphProperties(
                        new Justification { Val = JustificationValues.Center }
                    )
                })
                {
                    TableCellProperties = new TableCellProperties(
                        new Shading
                        {
                            Val = ShadingPatternValues.Clear,
                            Color = "auto",
                            Fill = _headerBackgroundColor
                        },
                        new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = columnWidths[i] },
                        new TableCellVerticalAlignment { Val = TableVerticalAlignmentValues.Center }
                    )
                };
                headerRow.Append(tableCell);
            }
            table.Append(headerRow);

            foreach (var report in timeReportsExportItems)
            {
                var row = new TableRow();
                var cells = new[]
                {
            report.ReportingDate,
            report.User,
            report.WorkDescription,
            report.ExternalHours.ToString()
        };

                for (int i = 0; i < cells.Length; i++)
                {
                    var tableCell = new TableCell(new Paragraph(new Run(new Text(cells[i]))
                    {
                        RunProperties = new RunProperties(
                            new RunFonts { Ascii = _defaultFont, HighAnsi = _defaultFont },
                            new FontSize { Val = _defaultFontSize }
                        )
                    })
                    {
                        ParagraphProperties = new ParagraphProperties(
                            new Justification
                            {
                                Val = i == 3 ? JustificationValues.Right : JustificationValues.Left
                            }
                        )
                    })
                    {
                        TableCellProperties = new TableCellProperties(
                            new Shading
                            {
                                Val = ShadingPatternValues.Clear,
                                Color = "auto",
                                Fill = _dataBackgroundColor
                            },
                            new TableCellWidth { Type = TableWidthUnitValues.Dxa, Width = columnWidths[i] },
                            new TableCellVerticalAlignment { Val = TableVerticalAlignmentValues.Bottom }
                        )
                    };
                    row.Append(tableCell);
                }
                table.Append(row);
            }

            return table;
        }
    }
}
