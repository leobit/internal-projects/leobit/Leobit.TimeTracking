﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class OperationalService : IOperationalService
    {
        private readonly LeobitContext _dbContext;
        private readonly IMapper _mapper;

        public OperationalService(LeobitContext context, IMapper mapper)
        {
            _dbContext = context ?? throw new ArgumentNullException(nameof(context));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public async Task<List<HolidayDTO>> UpdateHolidays(IEnumerable<HolidayDTO> holidays)
        {
            var ssDefaultDate = new DateTime(1970, 1, 1);
            var filterHolidays = holidays.Where(h => h.Date != ssDefaultDate);
            var newHolidays = _mapper
                .Map<IEnumerable<Holiday>>(filterHolidays)
                .Distinct(new HolidayEqualityComparer());

            var allHolidays = await _dbContext.Holiday.ToListAsync();

            _dbContext.Holiday.RemoveRange(allHolidays);
            _dbContext.Holiday.AddRange(newHolidays);

            await _dbContext.SaveChangesAsync();

            return _mapper.Map<List<HolidayDTO>>(newHolidays);
        }

        public Task<List<HolidayDTO>> GetHolidays(int? year = null, int? month = null)
        {
            var query = _dbContext.Holiday.AsQueryable();

            if (year.HasValue)
            {
                query = query.Where(h => h.Year == year.Value);

                if (month.HasValue)
                {
                    query = query.Where(h => h.Month == month.Value);
                }
            }

            return query.ProjectTo<HolidayDTO>(_mapper.ConfigurationProvider)
                        .ToListAsync();
        }

        public async Task<List<HolidayDTO>> GetHolidaysInRange(DateTime from, DateTime to)
        {
            var (fromYear, toYear) = (from.Year, to.Year);

            var inMemoryHolidays = await _dbContext.Holiday
                .AsNoTracking()
                .Where(h => h.Year >= fromYear && h.Year <= toYear)
                .ToListAsync();

            var holidaysInRange = inMemoryHolidays
                .Where(h => new DateTime(h.Year, h.Month, h.Day) >= from && new DateTime(h.Year, h.Month, h.Day) <= to)
                .ToList();

            return _mapper.Map<List<HolidayDTO>>(holidaysInRange);
        }

        public async Task<List<MonthHoursDTO>> GetBaseLine(int? year = null, int? month = null)
        {
            if (month.HasValue && (month.Value < 1 || month.Value > 12))
            {
                throw new ArgumentException("Value is not valid", nameof(month));
            }

            var holidays = await GetHolidays(year, month);

            var res = new List<MonthHoursDTO>();
            var today = DateTime.UtcNow;

            var iterationYear = year.HasValue
                ? year.Value
                : holidays.Any()
                    ? holidays.Min(h => h.Date).Year
                    : today.Year;

            var endYear = year.HasValue
                ? year.Value
                : holidays.Any()
                    ? holidays.Max(h => h.Date).Year
                    : today.Year;

            do
            {
                var startMonth = month.HasValue ? month.Value : 1;
                var rangeLength = month.HasValue ? 1 : 12;
                foreach (var m in Enumerable.Range(startMonth, rangeLength))
                {
                    var bd = BisunessDaysInMonth(iterationYear, m);
                    var hc = holidays.Count(h => h.Date.Year == iterationYear && h.Date.Month == m);
                    res.Add(new MonthHoursDTO
                    {
                        Year = iterationYear,
                        Month = m,
                        Hours = (bd - hc) * 8
                    });
                }
                iterationYear++;
            }
            while (iterationYear <= endYear);

            return res;
        }

        private static int BisunessDaysInMonth(int year, int month)
        {
            // Does not handle if holiday is moved to next month
            // In case 2 holidays are at same day threat them as 2 dayoffs
            var daysInMonth = DateTime.DaysInMonth(year, month);
            var firstDayOfMonth = (int)new DateTime(year, month, 1).DayOfWeek;
            var weekEndDays = 8;
            var daysLeft = daysInMonth - 7 * 4;
            switch (daysLeft)
            {
                case 1:
                    if (firstDayOfMonth == 6) weekEndDays += 1;
                    if (firstDayOfMonth == 0) weekEndDays += 1;
                    break;
                case 2:
                    if (firstDayOfMonth == 5) weekEndDays += 1;
                    if (firstDayOfMonth == 6) weekEndDays += 2;
                    if (firstDayOfMonth == 0) weekEndDays += 1;
                    break;
                case 3:
                    if (firstDayOfMonth == 4) weekEndDays += 1;
                    if (firstDayOfMonth == 5) weekEndDays += 2;
                    if (firstDayOfMonth == 6) weekEndDays += 2;
                    if (firstDayOfMonth == 0) weekEndDays += 1;
                    break;
                default:
                    break;
            }
            return daysInMonth - weekEndDays;
        }

        private class HolidayEqualityComparer : IEqualityComparer<Holiday>
        {
            public bool Equals(Holiday x, Holiday y)
            {
                return GetHashCode(x) == GetHashCode(y);
            }

            public int GetHashCode(Holiday obj)
            {
                return HashCode.Combine(obj.Year, obj.Month, obj.Day, obj.Name);
            }
        }
    }
}
