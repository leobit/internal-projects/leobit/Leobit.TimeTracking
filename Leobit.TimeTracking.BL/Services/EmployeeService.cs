﻿using System;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;

namespace Leobit.TimeTracking.BL.Services
{
    public class EmployeeService : IEmployeeService
    {
        private readonly LeobitContext _dbContext;
        private readonly IMapper _mapper;

        public EmployeeService(LeobitContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext;
            _mapper = mapper;
        }

        public Task<EmployeeBaseDTO> GetEmployeeById(string employeeId)
        {
            return _dbContext.Employee
                .AsNoTracking()
                .Where(x => x.Id == employeeId)
                .ProjectTo<EmployeeBaseDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<(DateTime StartDate, DateTime? EndDate)[]> GetTmPeriods(string employeeId)
        {
            var employeeHistory = await _dbContext.EmployeeHistory
                .AsNoTracking()
                .Where(x => x.UserId == employeeId)
                .OrderBy(x => x.Timestamp)
                .ToListAsync();

            var filteredHistory = employeeHistory
                .Where((record, index) => index == 0 || record.Employment != employeeHistory[index - 1].Employment)
                .ToList();

            var tmPeriods = filteredHistory
                .Select((record, index) => new
                {
                    record.Employment,
                    PeriodStart = record.Timestamp,
                    PeriodEnd = index + 1 < filteredHistory.Count
                        ? filteredHistory[index + 1].Timestamp
                        : (DateTime?)null
                })
                .Where(period => period.Employment == Constants.EmploymentTypeTM)
                .Select(period => (period.PeriodStart, period.PeriodEnd))
                .ToArray();

            return tmPeriods;
        }
    }
}
