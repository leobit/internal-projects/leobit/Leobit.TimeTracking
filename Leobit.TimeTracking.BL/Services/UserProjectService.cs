using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class UserProjectService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;
        private readonly IHttpContextAccessor _httpContextAccessor;


        public UserProjectService(IMapper mapper, LeobitContext dbContext, IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _httpContextAccessor = httpContextAccessor;
        }

        public Task<List<UserProjectDTO>> GetTeamUserProjects(string userId, ICollection<string> userIds, ICollection<int> projectIds)
        {
            return _dbContext.UserProject
                .ByTeamUserId(userId)
                .ByUserIds(userIds)
                .ByProjectIds(projectIds)
                .ProjectTo<UserProjectDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<UserProjectDTO>> GetUserProjects(ICollection<string> userIds, ICollection<int> projectIds)
        {
            var userProjects = await _dbContext.UserProject
                .AsNoTracking()
                .ByUserIds(userIds)
                .ByProjectIds(projectIds)
                .ProjectTo<UserProjectDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();

            var totalInternalHours = await _dbContext.TimeReport
               .AsNoTracking()
               .ByUserIds(userIds)
               .ByProjectIds(projectIds)
               .GroupBy(tr => new { tr.UserId, tr.ProjectId })
               .Select(g => new
               {
                   g.Key.UserId,
                   g.Key.ProjectId,
                   TotalHours = g.Sum(tr => tr.InternalHours)
               })
               .ToDictionaryAsync(x => (x.UserId, x.ProjectId), x => x.TotalHours);

            foreach (var userProject in userProjects)
            {
                var key = (userProject.User.Id, userProject.Project.Id);
                userProject.TotalInternalHours = totalInternalHours.GetValueOrDefault(key, 0);
            }

            return userProjects;
        }

        public async Task<UserProjectDTO> GetUserProject(string userId, int projectId)
        {
            return await _dbContext.UserProject
                .ByUserId(userId)
                .ByProjectIds(new[] { projectId })
                .ProjectTo<UserProjectDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<UserProjectDTO> CreateTeamUserProject(string userId, UserProjectDTO userProjectDTO)
        {
            var user = await _dbContext.User
                .ById(userProjectDTO.User.Id)
                .FirstOrDefaultAsync();

            var project = await _dbContext.Project
                .ByUserId(userId)
                .ById(userProjectDTO.Project.Id)
                .Include(p => p.Account)
                .FirstOrDefaultAsync();

            if (user != null && project != null)
            {
                var existingUserProject = await _dbContext.UserProject
                    .ByUserIds(new[] { userProjectDTO.User.Id })
                    .ByProjectIds(new[] { userProjectDTO.Project.Id })
                    .FirstOrDefaultAsync();

                if (existingUserProject == null)
                {
                    userProjectDTO.AssignmentDate = DateTime.UtcNow;
                    var userProject = _mapper.Map<UserProject>(userProjectDTO);

                    _dbContext.UserProject.Add(userProject);
                    await _dbContext.SaveChangesAsync();

                    return _mapper.Map<UserProjectDTO>(userProject);
                }
                else
                {
                    throw new ConflictException("User already assigned to project");
                }
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task<UserProjectDTO> CreateUserProject(UserProjectDTO userProjectDTO)
        {
            var user = await _dbContext.User
                .ById(userProjectDTO.User.Id)
                .FirstOrDefaultAsync();

            var project = await _dbContext.Project
                .ById(userProjectDTO.Project.Id)
                .Include(p => p.Account)
                .FirstOrDefaultAsync();

            if (user != null && project != null)
            {
                var existingUserProject = await _dbContext.UserProject
                    .ByUserIds(new[] { userProjectDTO.User.Id })
                    .ByProjectIds(new[] { userProjectDTO.Project.Id })
                    .FirstOrDefaultAsync();

                if (existingUserProject == null)
                {
                    userProjectDTO.AssignmentDate = DateTime.UtcNow;
                    var userProject = _mapper.Map<UserProject>(userProjectDTO);

                    _dbContext.UserProject.Add(userProject);
                    await _dbContext.SaveChangesAsync();

                    return _mapper.Map<UserProjectDTO>(userProject);
                }
                else
                {
                    throw new ConflictException("User already assigned to project");
                }
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task CreateUserProjects(string userId, List<int> projectIds)
        {
            var user = await _dbContext.User.Include(p => p.UserProject).ThenInclude(p => p.Project).FirstOrDefaultAsync(u => u.Id == userId);
            var projectsToAssign = await _dbContext.Project
                    .Where(p => projectIds.Contains(p.Id))
                    .ToListAsync();

            if (user != null && projectsToAssign.Any())
            {
                projectsToAssign = projectsToAssign
                    .Where(p => user.UserProject != null && !p.AutoAssignNewUsers && !user.UserProject.Any(up => up.ProjectId == p.Id))
                    .ToList();

                var projectsToRemove = user.UserProject
                    .Where(up => up.Project != null && !up.Project.AutoAssignNewUsers && !projectIds.Contains(up.ProjectId))
                    .ToList();

                var newAssignments = projectsToAssign.Select(p => new UserProject
                {
                    UserId = userId,
                    ProjectId = p.Id,
                    AssignmentDate = DateTime.UtcNow
                });
                _dbContext.UserProject.RemoveRange(projectsToRemove);
                _dbContext.UserProject.AddRange(newAssignments);

                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task<UserProjectDTO> UpdateUserProject(UserProjectDTO userProjectDTO)
        {
            var userProject = await _dbContext.UserProject
                .ByUserIds(new[] { userProjectDTO.User.Id })
                .ByProjectIds(new[] { userProjectDTO.Project.Id })
                .FirstOrDefaultAsync();

            if (userProject is null) throw new NotFoundException();

            userProject.StartDate = userProjectDTO.StartDate;
            userProject.EndDate = userProjectDTO.EndDate;
            userProject.Hours = userProjectDTO.Hours;

            await _dbContext.SaveChangesAsync();

            return userProjectDTO;
        }

        public async Task DeleteTeamUserProject(string userId, UserProjectDTO userProjectDTO)
        {
            if (await _dbContext.Project
                .ById(userProjectDTO.Project.Id)
                .ByUserId(userId)
                .AnyAsync())
            {
                var userProject = await _dbContext.UserProject
                    .ByUserIds(new[] { userProjectDTO.User.Id })
                    .ByProjectIds(new[] { userProjectDTO.Project.Id })
                    .FirstOrDefaultAsync();

                if (userProject != null)
                {
                    _dbContext.UserProject.Remove(userProject);
                    await _dbContext.SaveChangesAsync();
                }
                else
                {
                    throw new NotFoundException();
                }
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task DeleteUserProject(UserProjectDTO userProjectDTO)
        {
            var userProject = await _dbContext.UserProject
                .ByUserIds(new[] { userProjectDTO.User.Id })
                .ByProjectIds(new[] { userProjectDTO.Project.Id })
                .FirstOrDefaultAsync();

            if (userProject != null)
            {
                _dbContext.UserProject.Remove(userProject);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task DeleteUserNonDefaultProjects(string userId)
        {
            var userNonDefaultProjects = await _dbContext.UserProject
                .ByUserId(userId)
                .Where(x => !x.Project.AutoAssignNewUsers)
                .ToListAsync();

            if (userNonDefaultProjects.Any())
            {
                _dbContext.UserProject.RemoveRange(userNonDefaultProjects);
            }
        }

        public async Task CleanupProjectAssigments()
        {
            var terminatedEmployeeId = await _dbContext.Employee
                .Where(e => !e.Active)
                .Select(e => e.Id)
                .ToListAsync();

            // reserve unassign
            {
                var lastBusinessMonthStartDate = DateTime.UtcNow.FirstDateTimeOfMonth().AddMonths(-1);
                var newReservistsAssignmentBufferDate = DateTime.UtcNow.AddWeeks(-2);

                var longTimeReservists = await _dbContext.UserProject
                    .Where(x => x.AssignmentDate.HasValue && x.Project.ProjectType == (int)ProjectType.Reserve)
                    .Where(x => x.AssignmentDate < newReservistsAssignmentBufferDate)
                    .Select(x => x.UserId)
                    .Distinct()
                    .ToListAsync();

                var reservistsWithFreshReports = await _dbContext.TimeReport
                    .Where(tr => tr.Project.ProjectType == (int)ProjectType.Reserve)
                    .Where(tr => tr.ReportingDate >= lastBusinessMonthStartDate)
                    .Select(tr => tr.UserId)
                    .Distinct()
                    .ToListAsync();

                var staleReservists = longTimeReservists.Except(reservistsWithFreshReports).ToList();

                var toRemove = await _dbContext.UserProject
                    .Where(up =>
                        up.Project.ProjectType == (int)ProjectType.Reserve
                        && (terminatedEmployeeId.Contains(up.UserId)
                        || staleReservists.Contains(up.UserId)))
                    .ToListAsync();

                if (toRemove.Count > 0)
                {
                    _dbContext.RemoveRange(toRemove);
                }
            }

            // clean up T&M vacation & sick leaves
            {
                var tnmEmployeeIds = await _dbContext.Employee
                    .Where(e => e.Active && e.Employment == "T&M")
                    .Select(e => e.Id)
                    .ToListAsync();

                var toRemove = await _dbContext.UserProject
                    .Where(up =>
                        tnmEmployeeIds.Contains(up.UserId)
                        && (up.Project.ProjectType == (int)ProjectType.Vacation
                        || up.Project.ProjectType == (int)ProjectType.SickLeaveWithCertificate
                        || up.Project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate))
                    .ToListAsync();

                _dbContext.RemoveRange(toRemove);
            }

            // assign default projects to FT
            {
                var ftEmployeeIds = await _dbContext.Employee
                    .Join(_dbContext.User, e => e.Id, u => u.Id, (e, u) => new { Employee = e, User = u })
                    .Where(eu => eu.Employee.Active && eu.Employee.Employment == "FT")
                    .Select(eu => eu.Employee.Id)
                    .ToListAsync();

                var projectIds = await _dbContext.Project
                    .Where(p => p.AutoAssignNewUsers)
                    .Select(p => p.Id)
                    .ToListAsync();

                var expectedProjectAssigments = ftEmployeeIds
                    .Join(projectIds, l => true, r => true, (u, p) => new { ProjectId = p, UserId = u })
                    .ToList();

                var existing = await _dbContext.UserProject
                    .Where(up => ftEmployeeIds.Contains(up.UserId) && projectIds.Contains(up.ProjectId))
                    .Select(up => new { ProjectId = up.ProjectId, UserId = up.UserId })
                    .ToListAsync();

                var toAssign = expectedProjectAssigments
                    .Except(existing)
                    .Select(up => new UserProject { UserId = up.UserId, ProjectId = up.ProjectId });

                _dbContext.UserProject.AddRange(toAssign);
            }

            // Clean onboarding
            {
                var trialPeriodStart = DateTime.UtcNow.AddDays(-90);
                var employeesOnTrial = _dbContext.Employee
                    .Where(e =>
                        e.StartDate.HasValue
                        && e.StartDate >= trialPeriodStart);
                var employeesWithUndefinedStartDate = _dbContext.Employee
                    .Where(e => !e.StartDate.HasValue);

                var employeeIds = await employeesOnTrial
                    .Union(employeesWithUndefinedStartDate)
                    .Select(e => e.Id)
                    .ToListAsync();

                var toRemove = await _dbContext.UserProject
                    .Where(up =>
                        up.Project.ProjectType == (int)ProjectType.Onboarding
                        && (!employeeIds.Contains(up.UserId)
                        || terminatedEmployeeId.Contains(up.UserId)))
                    .ToListAsync();

                _dbContext.RemoveRange(toRemove);
            }

            // unassign inactive employees
            {
                var defaultProjectIds = await _dbContext.Project
                    .Where(p => p.AutoAssignNewUsers)
                    .Select(p => p.Id)
                    .ToListAsync();

                var toUnassign = await _dbContext.UserProject
                    .Where(up => terminatedEmployeeId.Contains(up.UserId) && !defaultProjectIds.Contains(up.ProjectId))
                    .ToListAsync();

                _dbContext.UserProject.RemoveRange(toUnassign);
            }

            // clean terminated project managers
            {
                var toRemove = await _dbContext.ProjectManager
                    .Where(pm => terminatedEmployeeId.Contains(pm.UserId))
                    .ToListAsync();

                _dbContext.ProjectManager.RemoveRange(toRemove);
            }

            // reset role for terminated employees
            {
                var defaultRole = _dbContext.Role.FirstOrDefault();
                if (defaultRole != null)
                {
                    var employeesToChangeRole = await _dbContext.User
                        .Where(u => u.RoleId != defaultRole.Id && terminatedEmployeeId.Contains(u.Id))
                        .ToListAsync();

                    employeesToChangeRole.ForEach(e => e.Role = defaultRole);
                    _dbContext.User.UpdateRange(employeesToChangeRole);
                }
            }

            await _dbContext.SaveChangesAsync();
        }
    }
}