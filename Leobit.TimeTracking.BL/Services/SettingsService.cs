﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class SettingsService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        public SettingsService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<SettingsDTO> GetSettings()
        {
            return _dbContext.Settings
                .ProjectTo<SettingsDTO>(_mapper.ConfigurationProvider)
                .SingleAsync();
        }

        public async Task<SettingsDTO> EditSettings(SettingsDTO settingsDTO)
        {
            var settings = _mapper.Map<Settings>(settingsDTO);

            _dbContext.Update(settings);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<SettingsDTO>(settings);
        }
    }
}
