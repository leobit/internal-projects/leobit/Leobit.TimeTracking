﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class PermissionService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        public PermissionService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<List<PermissionDTO>> GetPermissions()
        {
            return _dbContext.Permission
                .ProjectTo<PermissionDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
