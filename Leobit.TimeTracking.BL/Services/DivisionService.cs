﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;

namespace Leobit.TimeTracking.BL.Services
{
    public class DivisionService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        public DivisionService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<List<DivisionDTO>> GetDivisions(int? id = null)
        {
            return _dbContext.Division
                .ById(id)
                .ProjectTo<DivisionDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<DivisionDTO> CreateDivision(DivisionDTO divisionDTO)
        {
            var division = _mapper.Map<Division>(divisionDTO);

            _dbContext.Division.Add(division);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<DivisionDTO>(division);
        }

        public async Task<DivisionDTO> EditDivision(DivisionDTO divisionDTO)
        {
            if (_dbContext.Division.Any(d => d.Id == divisionDTO.Id))
            {
                var division = _mapper.Map<Division>(divisionDTO);

                _dbContext.Division.Update(division);
                await _dbContext.SaveChangesAsync();

                return _mapper.Map<DivisionDTO>(division);
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task DeleteDivision(int id)
        {
            var division = await _dbContext.Division.ById(id).FirstOrDefaultAsync();
            if (division != null)
            {
                _dbContext.Division.Remove(division);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }
    }
}
