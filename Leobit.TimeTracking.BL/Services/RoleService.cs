﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class RoleService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        public RoleService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<List<RoleShortDTO>> GetRoles()
        {
            return _dbContext.Role
                .ProjectTo<RoleShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<RoleShortDTO> GetUserRole(string userId)
        {
            return _dbContext.Role
                .ByUserId(userId)
                .ProjectTo<RoleShortDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public async Task<RoleDTO> GetFullRoleById(int roleId)
        {
            var role = await _dbContext.Role
                .ById(roleId)
                .Include(r => r.RolePermission)
                .ThenInclude(rp => rp.Permission)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            var allPermissions = await _dbContext.Permission.AsNoTracking().ToListAsync();

            var permissionIsNotExist = allPermissions
                .Where(p => !role.RolePermission.Any(rp => rp.PermissionId == p.Id))
                .Select(p => new RolePermission { PermissionId = p.Id, RoleId = roleId, Value = 0, Permission = p })
                .ToList();

            role.RolePermission = role.RolePermission.Concat(permissionIsNotExist).ToList();

            return _mapper.Map<RoleDTO>(role);
        }

        public async Task<RoleShortDTO> CreateRole(RoleDTO roleDTO)
        {
            var role = _mapper.Map<Role>(roleDTO);

            var protectedPermissions = await _dbContext.Permission
                .Where(p => p.IsProtected)
                .Select(p => p.Id)
                .ToListAsync();

            foreach (var rp in role.RolePermission)
            {
                if (protectedPermissions.Contains(rp.PermissionId) && rp.Value != 0)
                {
                    throw new ForbidException();
                }
            }

            _dbContext.Role.Add(role);
            await _dbContext.SaveChangesAsync();

            return _mapper.Map<RoleShortDTO>(role);
        }

        public async Task<RoleShortDTO> EditRole(RoleDTO roleDTO)
        {
            var oldRole = await _dbContext.Role
                .AsNoTracking()
                .ById(roleDTO.Id)
                .Include(r => r.RolePermission)
                .ThenInclude(rp => rp.Permission)
                .FirstOrDefaultAsync();

            if (oldRole == null)
            {
                throw new NotFoundException();
            }

            var role = _mapper.Map<Role>(roleDTO);

            // validate if protected permissions was not modified
            var protectedPermissions = await _dbContext.Permission
                .Where(p => p.IsProtected)
                .Select(p => p.Id)
                .ToListAsync();

            foreach (var rp in role.RolePermission)
            {
                var oldPermissionValue = oldRole.RolePermission
                    .FirstOrDefault(orp => orp.PermissionId == rp.PermissionId)?.Value ?? 0;

                if (protectedPermissions.Contains(rp.PermissionId) && rp.Value != oldPermissionValue)
                {
                    throw new ForbidException();
                }
            }

            // saving modified nested collection

            _dbContext.ChangeTracker.TrackGraph(role, e =>
            {
                if (e.Entry.Entity is Role)
                {
                    e.Entry.State = EntityState.Modified;
                }
                else if (e.Entry.Entity is RolePermission rolePermission)
                {
                    if (oldRole.RolePermission.Any(rp => rp.PermissionId == rolePermission.PermissionId))
                    {
                        e.Entry.State = EntityState.Modified;
                    }
                    else
                    {
                        e.Entry.State = EntityState.Added;
                    }
                }
            });

            await _dbContext.SaveChangesAsync();

            return _mapper.Map<RoleShortDTO>(role);
        }

        public async Task DeleteRole(int roleId, int? fallbackRoleId = null)
        {
            var role = await _dbContext.Role
                .ById(roleId)
                .Include(r => r.RolePermission)
                .Include(r => r.User)
                .AsSplitQuery()
                .FirstOrDefaultAsync();

            if (role is null)
            {
                throw new NotFoundException($"Role with Id = {roleId} does not exist");
            }

            if (role.IsSystemRole)
            {
                throw new ConflictException($"Role with Id = {roleId} is system role and can not be deleted");
            }

            if (role.User.Any() && !await IsRoleExist(fallbackRoleId))
            {
                throw new ConflictException($"Fallback role with Id = {fallbackRoleId} does not exist");
            }

            var strategy = _dbContext.Database.CreateExecutionStrategy();

            await strategy.ExecuteAsync(async () =>
            {
                await using var transaction = await _dbContext.Database.BeginTransactionAsync();

                try
                {
                    if (fallbackRoleId.HasValue)
                    {
                        foreach (var user in role.User)
                        {
                            user.RoleId = fallbackRoleId.Value;
                        }
                    }

                    role.RolePermission.Clear();

                    _dbContext.Remove(role);
                    await _dbContext.SaveChangesAsync();

                    await transaction.CommitAsync();
                }
                catch (Exception)
                {
                    await transaction.RollbackAsync();
                }
            });
        }

        private Task<bool> IsRoleExist(int? roleId)
        {
            if (!roleId.HasValue) return Task.FromResult(false);

            return _dbContext.Role.Where(r => r.Id == roleId).AnyAsync();
        }
    }
}
