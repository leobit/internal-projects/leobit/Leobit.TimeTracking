﻿using AutoMapper;
using Leobit.TimeTracking.BL.Extensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public enum TimeReportIssueType
    {
        None = 0,
        DuplicateContent = 1,
        ShortContent = 2,
        SickleaveAbuse = 4,
        SelfEducation = 8,
        KE = 16,
        Holidays = 32,
        Inactive = 64
    }

    public class AnalyticService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbDontext;

        public AnalyticService(LeobitContext dbContext, IMapper mapper)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _dbDontext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
        }

        public async Task<List<TimeReportIssueDTO>> GetTimeReportsIssues(int year, int month, TimeReportIssueType type, CancellationToken cancellationToken)
        {
            var result = new List<TimeReportIssueDTO>();

            if ((type & TimeReportIssueType.ShortContent) == TimeReportIssueType.ShortContent && !cancellationToken.IsCancellationRequested)
            {
                var start = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);
                var end = start.AddMonths(1).AddDays(-1);

                var operationalProjects = new List<int>
                {
                    (int)ProjectType.Vacation,
                    (int)ProjectType.VacationUnpaid,
                    (int)ProjectType.SickLeaveWithoutCertificate,
                    (int)ProjectType.SickLeaveWithCertificate,
                };

                var shortContentReportsEntities = await _dbDontext.TimeReport
                    .AsNoTracking()
                    .Include(tr => tr.User)
                    .Include(tr => tr.Project.Account)
                    .Where(tr => tr.ReportingDate >= start && tr.ReportingDate <= end)
                    .Where(tr => !operationalProjects.Contains(tr.Project.ProjectType))
                    .Where(tr => tr.WorkDescription.Length < 5)
                    .ToListAsync(cancellationToken);

                var shortContentReports = shortContentReportsEntities
                    .GroupBy(tr => new
                    {
                        FirstName = tr.User.FirstName,
                        LastName = tr.User.LastName,
                        Id = tr.User.Id
                    })
                    .Select(gr => new TimeReportIssueDTO
                    {
                        IssueType = TimeReportIssueType.ShortContent,
                        Reports = _mapper.Map<List<TimeReportDTO>>(gr.ToList()),
                        User = new UserShortDTO
                        {
                            Id = gr.Key.Id,
                            FirstName = gr.Key.FirstName,
                            LastName = gr.Key.LastName,
                        }
                    })
                    .ToList();

                result.AddRange(shortContentReports);
            }

            if ((type & TimeReportIssueType.SelfEducation) == TimeReportIssueType.SelfEducation && !cancellationToken.IsCancellationRequested)
            {
                var start = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);
                var end = start.AddMonths(1).AddDays(-1);

                var selfEducationReportsEntities = await _dbDontext.TimeReport
                    .AsNoTracking()
                    .Include(tr => tr.User)
                    .Include(tr => tr.Project.Account)
                    .Where(tr => tr.ReportingDate >= start && tr.ReportingDate <= end)
                    .Where(tr => tr.Project.ProjectType == (int)ProjectType.Internal
                                 || tr.Project.ProjectType == (int)ProjectType.Reserve)
                    .Where(tr => tr.WorkDescription.StartsWith("self")
                                 || tr.WorkDescription.StartsWith("-self")
                                 || tr.WorkDescription.Contains(" self"))
                    .ToListAsync(cancellationToken);

                var selfEducation = selfEducationReportsEntities
                    .GroupBy(tr => new
                    {
                        Id = tr.User.Id,
                        FirstName = tr.User.FirstName,
                        LastName = tr.User.LastName
                    })
                    .Select(gr => new TimeReportIssueDTO
                    {
                        Reports = _mapper.Map<List<TimeReportDTO>>(gr.ToList()),
                        IssueType = TimeReportIssueType.SelfEducation,
                        User = new UserShortDTO
                        {
                            Id = gr.Key.Id,
                            FirstName = gr.Key.FirstName,
                            LastName = gr.Key.LastName
                        }
                    });

                result.AddRange(selfEducation);
            }

            if ((type & TimeReportIssueType.KE) == TimeReportIssueType.KE && !cancellationToken.IsCancellationRequested)
            {
                var monthStart = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);
                var start = monthStart.AddDays(-7);
                var end = start.AddMonths(1).AddDays(6);

                var kePreparationReportsEntities = await _dbDontext.TimeReport
                    .AsNoTracking()
                    .Include(tr => tr.User)
                    .Include(tr => tr.Project.Account)
                    .Where(tr => tr.ReportingDate >= start && tr.ReportingDate <= end)
                    .Where(tr => tr.Project.ProjectType == (int)ProjectType.Reserve)
                    .Where(tr => tr.WorkDescription.StartsWith("KE")
                                 || tr.WorkDescription.StartsWith(" KE")
                                 || tr.WorkDescription.StartsWith("-KE")
                                 || tr.WorkDescription.Contains("Knowledge Evaluation"))
                    .ToListAsync(cancellationToken);

                var kePreparation = kePreparationReportsEntities
                    .GroupBy(tr => new
                    {
                        Id = tr.User.Id,
                        FirstName = tr.User.FirstName,
                        LastName = tr.User.LastName
                    })
                    .Where(gr => gr.Sum(tr => tr.InternalHours) > 24)
                    .Select(gr => new TimeReportIssueDTO
                    {
                        Reports = _mapper.Map<List<TimeReportDTO>>(gr),
                        IssueType = TimeReportIssueType.KE,
                        User = new UserShortDTO
                        {
                            Id = gr.Key.Id,
                            FirstName = gr.Key.FirstName,
                            LastName = gr.Key.LastName
                        }
                    });

                result.AddRange(kePreparation);
            }

            if ((type & TimeReportIssueType.Holidays) == TimeReportIssueType.Holidays && !cancellationToken.IsCancellationRequested)
            {
                var start = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc).AddMonths(-1);
                var end = start.AddMonths(1).AddDays(-1);
                
                var holidays = await _dbDontext.Holiday
                    .AsNoTracking()
                    .Where(h => h.Year == year && h.Month == month)
                    .Select(h => new DateTime(h.Year, h.Month, h.Day, 0, 0, 0, DateTimeKind.Utc))
                    .ToListAsync(cancellationToken);

                var weekends = new List<DateTime>();
                var day = new DateTime(year, month, 1);
                for (int i = 0; i < 30; i++)
                {
                    if (day.Month != month) break;
                    if (day.IsWeekend()) weekends.Add(day);
                    day = day.AddDays(1);
                }

                var officialDayoffs = weekends.Union(holidays);

                var holidayReports = await _dbDontext.TimeReport
                    .AsNoTracking()
                    .Include(tr => tr.Project.Account)
                    .Where(tr => officialDayoffs.Contains(tr.ReportingDate))
                    .Select(tr => new TimeReportIssueDTO
                    {
                        Reports = new List<TimeReportDTO> { _mapper.Map<TimeReportDTO>(tr) },
                        IssueType = TimeReportIssueType.Holidays,
                        User = _mapper.Map<UserShortDTO>(tr.User)
                    })
                    .ToListAsync(cancellationToken);

                result.AddRange(holidayReports);
            }

            if ((type & TimeReportIssueType.SickleaveAbuse) == TimeReportIssueType.SickleaveAbuse && !cancellationToken.IsCancellationRequested)
            {
                var monthStart = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc);
                var start = monthStart.AddMonths(-1);
                var end = monthStart.AddMonths(2);

                var operationalProjects = new List<int>
                {
                    (int)ProjectType.Vacation,
                    (int)ProjectType.VacationUnpaid,
                    (int)ProjectType.SickLeaveWithoutCertificate,
                };

                var holidays = await _dbDontext.Holiday
                    .AsNoTracking()
                    .Where(h =>
                        (h.Year == year && (h.Month == month - 1 || h.Month == month || h.Month == month + 1))
                        || (month == 1 && h.Year == year - 1 && h.Month == 12)
                        || (month == 12 && h.Year == year + 1 && h.Month == 1))
                    .ToListAsync(cancellationToken);

                var sickLeaveAbuseCandidatesEntities = await _dbDontext.TimeReport
                    .AsNoTracking()
                    .Include(tr => tr.User)
                    .Include(tr => tr.Project.Account)
                    .Where(tr => tr.ReportingDate >= start && tr.ReportingDate <= end)
                    .Where(tr => operationalProjects.Contains(tr.Project.ProjectType))
                    .OrderBy(tr => tr.ReportingDate)
                    .ToListAsync(cancellationToken);

                var sickLeaveAbuseCandidates = sickLeaveAbuseCandidatesEntities
                    .GroupBy(gr => gr.UserId)
                    .ToList();

                var detectedAbuse = new List<(string userId, int groupId, TimeReport timeReport)>();
                var abuseReportsGroup = new List<TimeReport>();
                foreach (var gr in sickLeaveAbuseCandidates)
                {
                    abuseReportsGroup.Clear();
                    var groupKey = 0;
                    var typeSwitchDetected = false;
                    var reports = gr.ToList();
                    for (int i = 0; i < reports.Count - 1; i++)
                    {
                        var curr = reports[i];
                        var next = reports[i + 1];
                        abuseReportsGroup.Add(curr);
                        if (curr.ReportingDate.AddBusinessDays(1, holidays) == next.ReportingDate)
                        {
                            if (!typeSwitchDetected
                                && IsDifferentOperationType(curr.Project.ProjectType, next.Project.ProjectType))
                            {
                                typeSwitchDetected = true;
                            }
                        }
                        else
                        {
                            if (typeSwitchDetected && abuseReportsGroup.Any(tr => tr.ReportingDate.Month == month))
                            {
                                detectedAbuse.AddRange(abuseReportsGroup.Select(mr => (gr.Key, groupKey, mr)));
                                groupKey++;
                            }
                            typeSwitchDetected = false;
                            abuseReportsGroup.Clear();
                        }
                    }

                    if (typeSwitchDetected && abuseReportsGroup.Any())
                    {
                        abuseReportsGroup.Add(reports.Last());
                        if (abuseReportsGroup.Any(tr => tr.ReportingDate.Month == month))
                        {
                            detectedAbuse.AddRange(abuseReportsGroup.Select(mr => (gr.Key, groupKey, mr)));
                        }
                    }
                }

                var abuseResult = detectedAbuse
                    .GroupBy(x => new { UserId = x.userId, GroupId = x.groupId })
                    .Select(gr => new TimeReportIssueDTO
                    {
                        IssueType = TimeReportIssueType.SickleaveAbuse,
                        User = _mapper.Map<UserShortDTO>(gr.First().timeReport.User),
                        Reports = _mapper.Map<List<TimeReportDTO>>(gr.Select(x => x.timeReport)),
                    });

                var upfrontSickLeaveIssues = GetUpfrontSickLeaveIssues(sickLeaveAbuseCandidates);

                result.AddRange(upfrontSickLeaveIssues);
                result.AddRange(abuseResult);
            }

            if ((type & TimeReportIssueType.DuplicateContent) == TimeReportIssueType.DuplicateContent && !cancellationToken.IsCancellationRequested)
            {
                var start = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc).AddDays(-7);
                var end = start.AddMonths(1).AddDays(-1);

                var operationalProjects = new List<int>
                {
                    (int)ProjectType.Vacation,
                    (int)ProjectType.VacationUnpaid,
                    (int)ProjectType.SickLeaveWithoutCertificate,
                    (int)ProjectType.SickLeaveWithCertificate,
                };

                var toAnalyzeReportsEntities = await _dbDontext.TimeReport
                    .AsNoTracking()
                    .Include(tr => tr.User)
                    .Include(tr => tr.Project)
                        .ThenInclude(p => p.Account)
                    .Where(tr => tr.ReportingDate >= start && tr.ReportingDate <= end)
                    .Where(tr => !operationalProjects.Contains(tr.Project.ProjectType))
                    .OrderBy(tr => tr.UserId)
                        .ThenBy(tr => tr.ProjectId)
                        .ThenByDescending(tr => tr.ReportingDate)
                    .ToListAsync(cancellationToken);

                var toAnalyze = toAnalyzeReportsEntities
                    .GroupBy(gr => gr.UserId)
                    .ToList();

                var duplicatedContentGroups = new List<(string userId, int groupId, TimeReport timeReport)>();
                var addedAsDuplicates = new HashSet<(int, int)>();
                foreach (var userGroup in toAnalyze)
                {
                    var group = 0;
                    var userReports = userGroup.ToList();
                    for (int i = 0; i < userReports.Count - 1; i++)
                    {
                        var current = userReports[i];
                        var previous = i + 1 < userReports.Count ? userReports[i + 1] : null;
                        //var prePrevious = i + 2 < userReports.Count ? userReports[i + 2] : null;

                        if (previous != null && current.ProjectId == previous.ProjectId)
                        {
                            var descriptionDistance = CalcTextSimilarity(current.WorkDescription, previous.WorkDescription);
                            if (descriptionDistance >= 0.99)
                            {
                                // add to duplicate group
                                if (!addedAsDuplicates.Contains((current.Id, group)))
                                {
                                    addedAsDuplicates.Add((current.Id, group));
                                    duplicatedContentGroups.Add((userGroup.Key, group, current));
                                }

                                if (!addedAsDuplicates.Contains((previous.Id, group)))
                                {
                                    addedAsDuplicates.Add((previous.Id, group));
                                    duplicatedContentGroups.Add((userGroup.Key, group, previous));
                                }
                                continue;
                            }
                        }

                        group++;
                        //continue;

                        //if (prePrevious != null && current.ProjectId == prePrevious.ProjectId)
                        //{
                        //    var descriptionDistance = CalcTextSimilarity(current.WorkDescription, prePrevious.WorkDescription);
                        //    if (descriptionDistance > 0.85)
                        //    {
                        //        // add to duplicate group
                        //        if (!addedAsDuplicates.Contains((current.Id, group)))
                        //        {
                        //            addedAsDuplicates.Add((current.Id, group));
                        //            duplicatedContentGroups.Add((userGroup.Key, group, current.Id, current.WorkDescription));
                        //        }
                        //        if (!addedAsDuplicates.Contains((previous.Id, group)))
                        //        {
                        //            addedAsDuplicates.Add((previous.Id, group));
                        //            duplicatedContentGroups.Add((userGroup.Key, group, prePrevious.Id, prePrevious.WorkDescription));
                        //        }
                        //    }
                        //}
                    }
                }

                var resultDuplicates = duplicatedContentGroups
                .GroupBy(x => new { User = x.userId, GroupId = x.groupId })
                .Select(gr => new TimeReportIssueDTO
                {
                    IssueType = TimeReportIssueType.DuplicateContent,
                    User = _mapper.Map<UserShortDTO>(gr.First().timeReport.User),
                    Reports = _mapper.Map<List<TimeReportDTO>>(gr.Select(x => x.timeReport))
                });

                result.AddRange(resultDuplicates);
            }

            if ((type & TimeReportIssueType.Inactive) == TimeReportIssueType.Inactive && !cancellationToken.IsCancellationRequested)
            {
                var end = new DateTime(year, month, 1, 0, 0, 0, DateTimeKind.Utc).AddMonths(1).AddDays(-1);
                var start = end.AddMonths(-2).AddDays(1);
                var activeEmployees = await _dbDontext.Employee.Where(e => e.Active).ToListAsync();
                var activeEmployeesIds = activeEmployees.Select(e => e.Id);
                
                var withReportsEmployeeIds = await _dbDontext.TimeReport
                    .Where(tr => activeEmployeesIds.Contains(tr.UserId))
                    .Where(tr => tr.ReportingDate >= start && tr.ReportingDate <= end)
                    .Select(tr => tr.UserId)
                    .Distinct()
                    .ToListAsync(cancellationToken);

                var withoutReportsEmployeeIds = activeEmployeesIds.Except(withReportsEmployeeIds.Union(new[] { "os" }));
                if (withReportsEmployeeIds.Any())
                {
                    var latestReportsEntities = await _dbDontext.TimeReport
                        .Where(tr => withoutReportsEmployeeIds.Contains(tr.UserId))
                        .Where(tr => tr.ReportingDate > DateTime.UtcNow.AddMonths(-6))
                        .Include(tr => tr.User)
                        .Include(tr => tr.Project.Account)
                        .ToListAsync(cancellationToken);

                    var latestReports = latestReportsEntities
                        .GroupBy(tr => tr.UserId)
                        .Select(g => g.OrderByDescending(tr => tr.ReportingDate).FirstOrDefault())
                        .ToList();

                    var resultInactiveEmployees = latestReports.Select(lr => new TimeReportIssueDTO
                    {
                        IssueType = TimeReportIssueType.Inactive,
                        Description = $"Inactive for {Math.Round((DateTime.UtcNow - lr.ReportingDate).TotalDays)} days",
                        User = _mapper.Map<UserShortDTO>(lr.User),
                        Reports = _mapper.Map<List<TimeReportDTO>>(new[] { lr })
                    });

                    result.AddRange(resultInactiveEmployees);
                }
            }

            return result;
        }

        // Based on Levenshtein distance
        private static double CalcTextSimilarity(string a, string b)
        {
            return a == b ? 1 : -1;

            if (string.IsNullOrEmpty(a) || string.IsNullOrEmpty(b))
            {
                return 0;
            }

            if (a.Equals(b))
            {
                return 1;
            }

            int lengthA = a.Length;
            int lengthB = b.Length;
            var distances = new int[lengthA + 1, lengthB + 1];
            for (int i = 0; i <= lengthA; distances[i, 0] = i++) ;
            for (int j = 0; j <= lengthB; distances[0, j] = j++) ;

            for (int i = 1; i <= lengthA; i++)
                for (int j = 1; j <= lengthB; j++)
                {
                    int cost = b[j - 1] == a[i - 1] ? 0 : 1;
                    distances[i, j] = Math.Min
                        (
                        Math.Min(distances[i - 1, j] + 1, distances[i, j - 1] + 1),
                        distances[i - 1, j - 1] + cost
                        );
                }

            var editCost = (double)distances[lengthA, lengthB];
            return 1d - editCost / (lengthA + lengthB);
        }

        private static bool IsDifferentOperationType(int p1, int p2)
        {
            if (p1 == p2) return false;
            var pt1 = (ProjectType)p1;
            var pt2 = (ProjectType)p2;
            return ((pt1 == ProjectType.Vacation || pt1 == ProjectType.VacationUnpaid)
                    && (pt2 == ProjectType.SickLeaveWithoutCertificate || pt2 == ProjectType.SickLeaveWithCertificate))
                    || ((pt1 == ProjectType.SickLeaveWithoutCertificate || pt1 == ProjectType.SickLeaveWithCertificate)
                    && (pt2 == ProjectType.Vacation || pt2 == ProjectType.VacationUnpaid));
        }

        private List<TimeReportIssueDTO> GetUpfrontSickLeaveIssues(List<IGrouping<string, TimeReport>> sickLeaveAbuse)
        {
            var upfrontSickLeaveIssues = new List<TimeReportIssueDTO>();

            foreach (var userGroup in sickLeaveAbuse)
            {
                var userId = userGroup.Key;
                var timeReports = userGroup
                    .Where(report =>
                        report.Project != null &&
                        report.Project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate &&
                        (report.ReportingDate - report.Timestamp).TotalDays > 2)
                    .OrderBy(report => report.ReportingDate)
                    .Select((report, index) => new { Report = report, Index = index })
                    .GroupBy(pair => (pair.Report.ReportingDate - DateTime.UnixEpoch).Days - pair.Index)
                    .Select(g => new TimeReportIssueDTO
                    {
                        IssueType = TimeReportIssueType.SickleaveAbuse,
                        Description = $"{(g.First().Report.ReportingDate - g.First().Report.Timestamp).Days} days upfront",
                        User = _mapper.Map<UserShortDTO>(g.First().Report.User),
                        Reports = _mapper.Map<List<TimeReportDTO>>(g.Select(pair => pair.Report)),
                    })
                    .ToList();

                upfrontSickLeaveIssues.AddRange(timeReports);
            }

            return upfrontSickLeaveIssues;
        }
    }
}
