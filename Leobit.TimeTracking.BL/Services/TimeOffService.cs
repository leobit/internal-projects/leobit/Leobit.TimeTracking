﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Extensions;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using static Leobit.TimeTracking.BL.Constants;

namespace Leobit.TimeTracking.BL.Services
{
    public class TimeOffService : ITimeOffService
    {
        private readonly LeobitContext _dbContext;

        private readonly IEmployeeService _employeeService;
        private readonly IOperationalService _operationalService;
        private readonly IMapper _mapper;

        public TimeOffService(
            LeobitContext dbContext,
            IEmployeeService employeeService,
            IOperationalService operationalService,
            IMapper mapper)
        {
            _dbContext = dbContext;
            _employeeService = employeeService;
            _operationalService = operationalService;
            _mapper = mapper;
        }

        public Task<List<EmployeeMonthlyActivityView>> GetTimeOffsViewByEmployeeId(string employeeId, int? year, int? month = null)
        {
            return _dbContext.EmployeeMonthlyActivityView
                .ByEmployeeId(employeeId)
                .ByYearAndMonth(year, month)
                .ToListAsync();
        }

        public Task<List<TransferredVacationBalanceDTO>> GetTransferredVacationBalance(string employeeId, int? year = null)
        {
            return _dbContext.TransferredVacationBalance
                .AsNoTracking()
                .ByEmployeeId(employeeId)
                .ByYear(year)
                .ProjectTo<TransferredVacationBalanceDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<CustomDayOffDTO[]> GetCustomDaysOff(string userId, int? year = null, int? month = null)
        {
            return _dbContext.CustomDayOff
                .AsNoTracking()
                .ByUserId(userId)
                .ByYearAndMonth(year, month)
                .ProjectTo<CustomDayOffDTO>(_mapper.ConfigurationProvider)
                .ToArrayAsync();
        }

        public async Task<VacationInformationDTO> GetTimeOffInformation(string userId, int year)
        {
            var now = DateTime.UtcNow;
            EmployeeBaseDTO employee = await _employeeService.GetEmployeeById(userId);
            if (employee == null || year > now.Year || (employee.StartDate.HasValue && employee.StartDate.Value.Year > year))
            {
                return new VacationInformationDTO(userId);
            }

            var timeoffs = await GetTimeOffsViewByEmployeeId(userId, year);

            var tmPeriods = await _employeeService.GetTmPeriods(employee.Id);
            var standingYearsExpected = GetStandingYearsExpected(employee, year, tmPeriods);
            var transferredBalance = (await GetTransferredVacationBalance(employee.Id, year)).FirstOrDefault();
            var customDaysoff = await GetCustomDaysOff(employee.Id, year);
            var holidays = await _operationalService.GetHolidays(year);
            var firstReportedDate = await _dbContext.TimeReport
                 .AsNoTracking()
                 .Where(tr => tr.UserId == userId)
                 .OrderBy(tr => tr.ReportingDate)
                 .Select(x => (DateTime?)x.ReportingDate)
                 .FirstOrDefaultAsync();

            var totalSickLeavesUsed = 0m;

            List<VacationMonthDataDTO> yearData = new(MonthsInYear);
            List<SickLeaveMonthDataDTO> sickLeaveYearData = new(MonthsInYear);

            for (int monthNumber = 1; monthNumber <= MonthsInYear; monthNumber++)
            {
                var timeOffMonth = timeoffs.FirstOrDefault(x => x.Month == monthNumber);
                var accrualRate = standingYearsExpected.TryGetValue(monthNumber, out double expectedStandingYears)
                            ? (double)AccrualRates
                                .First(x => expectedStandingYears >= x.MinYears && expectedStandingYears < x.MaxYears)
                                .Rate / MonthsInYear
                            : 0d;

                if (timeOffMonth == null)
                {
                    sickLeaveYearData.Add(new SickLeaveMonthDataDTO());
                    yearData.Add(new VacationMonthDataDTO(employee.Employment, year, monthNumber, accrualRate));

                    continue;
                }

                var holidaysInMonth = holidays.Count(x => x.Month == timeOffMonth.Month);
                var calculatedAccrual = await CalculateMonthlyAccrualDays((decimal)accrualRate, timeOffMonth, holidaysInMonth, tmPeriods);
                yearData.Add(new VacationMonthDataDTO(timeOffMonth, employee.Employment, accrualRate, calculatedAccrual));

                var (fullyPaid, partiallyPaid) = CalculateSickLeavesByCompensationType(year, timeOffMonth, ref totalSickLeavesUsed);

                sickLeaveYearData.Add(new SickLeaveMonthDataDTO
                {
                    FullyPaid = fullyPaid,
                    PartiallyPaid = partiallyPaid,
                    WithoutCertificate = (double)timeOffMonth.SickLeaveWithoutCertUsed,
                    WithCertificate = (double)timeOffMonth.SickLeaveWithCertUsed,
                });
            }

            var targetDate = DateTime.UtcNow.Year <= year ? DateTime.UtcNow : year.LastDateOnlyOfYear();
            var currentStandingYear = CalculateStandingYears(employee, targetDate);
            var daysInReserve = (double)timeoffs.Sum(x => x.DaysInReserve);
            var daysInReserveLastSixMonth = (double)timeoffs.Where(x => x.Month <= targetDate.Month && x.Month > Math.Max(0, targetDate.Month - 6)).Sum(x => x.DaysInReserve);

            var vacationBalance = year == DateTime.UtcNow.Year
                ? (double)(transferredBalance?.Transferred ?? 0) + (double)customDaysoff.Sum(x => x.ExtraDayOff) + yearData.Where((x, index) => index < targetDate.Month - 1).Sum(x => x.Accrual)
                : (double)(transferredBalance?.Transferred ?? 0) + (double)customDaysoff.Sum(x => x.ExtraDayOff) + yearData.Sum(x => x.Accrual);

            // TODO: start storing vacation leftovers in separate db table
            var vacationLeftovers = 0d;
            var monthesBeforeTm = GetLastMonthInAnyTMPeriod(year, yearData, tmPeriods);
            if (monthesBeforeTm != null && monthesBeforeTm.Any())
            {
                var accrualBeforeTm = monthesBeforeTm.Sum(x => x.Accrual);
                var usedVacationsBeforeTm = monthesBeforeTm.Sum(x => x.Used.Value);
                var vacactionBalanceBeforeTm = (double)(transferredBalance?.Transferred ?? 0) + 
                                (double)customDaysoff.Where(x => x.Month <= monthesBeforeTm.Length).Sum(x => x.ExtraDayOff) +
                                monthesBeforeTm.Sum(x => x.Accrual.Value);

                vacationLeftovers = vacactionBalanceBeforeTm - usedVacationsBeforeTm;
            }

            return new VacationInformationDTO(
                employee,
                firstReportedDate,
                year,
                currentStandingYear,
                transferredBalance,
                vacationLeftovers,
                yearData,
                daysInReserve,
                daysInReserveLastSixMonth,
                sickLeaveYearData,
                customDaysoff);
        }

        private async Task<decimal> CalculateMonthlyAccrualDays(
        decimal accrualRateMonthly,
        EmployeeMonthlyActivityView timeOff,
        int holidaysInMonth,
        (DateTime StartDate, DateTime? EndDate)[] tmPeriods)
        {
            var monthStart = new DateTime(timeOff.Year, timeOff.Month, 1);
            var monthEnd = monthStart.LastDateOfMonth();
            var reportedHours = timeOff.TotalReportedDays * DailyWorkingHours;

            var overlappingPeriods = tmPeriods
                .Where(p => p.StartDate <= monthEnd && (p.EndDate ?? DateTime.MaxValue) >= monthStart)
                .ToList();

            if (overlappingPeriods.Any())
            {
                if (overlappingPeriods.Any(p => p.StartDate <= monthStart && (p.EndDate ?? DateTime.MaxValue) >= monthEnd))
                {
                    reportedHours = 0;
                }
                else
                {
                    var monthlyReports = await _dbContext.TimeReport
                       .AsNoTracking()
                       .Include(x => x.Project)
                       .ByDateRange(monthStart, monthEnd)
                       .ByUserId(timeOff.EmployeeId)
                       .ToListAsync();

                    var reportedHoursOnFullTime = monthlyReports
                        .Where(x => x.Project.ProjectType != (int)ProjectType.VacationUnpaid &&
                                    !overlappingPeriods.Any(tm => tm.StartDate.Date <= x.ReportingDate.Date &&
                                                                  (tm.EndDate.HasValue ? tm.EndDate.Value.Date : DateTime.MaxValue.Date) > x.ReportingDate.Date))
                        .Sum(x => x.InternalHours);

                    reportedHours = reportedHoursOnFullTime;
                }
            }

            var businessDays = monthStart.GetBusinessDays(holidaysInMonth);
            var totalWorkHoursInMonth = businessDays * DailyWorkingHours;

            return Math.Min(
                (reportedHours / totalWorkHoursInMonth) * accrualRateMonthly,
                accrualRateMonthly
            );
        }

        private static Dictionary<int, double> GetStandingYearsExpected(
            EmployeeBaseDTO employee,
            int year,
            (DateTime StartDate, DateTime? EndDate)[] tmPeriods)
        {
            Dictionary<int, double> monthStandingYear = new(MonthsInYear);

            if (!employee.StartDate.HasValue)
            {
                return monthStandingYear;
            }

            DateTime baseDate = employee.StartDate.Value;

            if (tmPeriods != null && tmPeriods.Length > 0)
            {
                var validPeriods = tmPeriods.Where(period => 
                    period.EndDate.HasValue &&
                    (period.EndDate.Value - period.StartDate).TotalDays >= 180);

                if (validPeriods.Any())
                {
                    var selectedPeriod = validPeriods.OrderByDescending(p => p.EndDate.Value).First();
                    baseDate = selectedPeriod.EndDate.Value;
                }
            }

            for (int monthNumber = 1; monthNumber <= MonthsInYear; monthNumber++)
            {
                var dateMonth = new DateTime(year, monthNumber, 1);
                double standingYears = dateMonth < baseDate ? 0 : (dateMonth - baseDate).TotalDays / 365.0;
                monthStandingYear.Add(monthNumber, standingYears);
            }

            return monthStandingYear;
        }

        private static double CalculateStandingYears(EmployeeBaseDTO employee, DateTime dateMonth)
        {
            var totalDaysWorked = (employee.StartDate < dateMonth)
                ? (dateMonth - employee.StartDate.Value).TotalDays
                : 0;

            return totalDaysWorked / 365;
        }

        private static VacationMonthDataDTO[] GetLastMonthInAnyTMPeriod(int year, List<VacationMonthDataDTO> data, (DateTime StartDate, DateTime? EndDate)[] tmPeriods)
        {
            for (int monthIndex = 11; monthIndex >= 0; monthIndex--)
            {
                var lastDayOfMonth = new DateTime(year, monthIndex + 1, DateTime.DaysInMonth(year, monthIndex + 1));

                if (tmPeriods.Any(period => lastDayOfMonth >= period.StartDate && (lastDayOfMonth <= period.EndDate || period.EndDate is null)))
                {
                    return data.Where((x, index) => index <= monthIndex).ToArray();
                }
            }

            return null;
        }

        private static (double FullyPaid, double PartiallyPaid) CalculateSickLeavesByCompensationType(
            int year,
            EmployeeMonthlyActivityView timeOffMonth,
            ref decimal totalSickLeavesUsed)
        {
            if (year < 2025)
            {
                var fullyPaid = Math.Clamp(SickLeavesFullPay - totalSickLeavesUsed, 
                                            0, 
                                            timeOffMonth.SickLeaveWithoutCertUsed + timeOffMonth.SickLeaveWithCertUsed);
                totalSickLeavesUsed += timeOffMonth.SickLeaveWithCertUsed + timeOffMonth.SickLeaveWithoutCertUsed;
                var partiallyPaid = Math.Max(totalSickLeavesUsed - SickLeavesFullPay, 0);

                return ((double)fullyPaid, (double)partiallyPaid);
            }
            else
            {
                var fullyPaid = Math.Clamp(SickLeavesFullPay - totalSickLeavesUsed, 0, timeOffMonth.SickLeaveWithCertUsed);
                totalSickLeavesUsed += timeOffMonth.SickLeaveWithCertUsed;
                var partiallyPaid = Math.Max(totalSickLeavesUsed - SickLeavesFullPay, 0) + timeOffMonth.SickLeaveWithoutCertUsed;

                return ((double)fullyPaid, (double)partiallyPaid);
            }
        }
    }
}