﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;

namespace Leobit.TimeTracking.BL.Services
{
    public class OrganizationUnitService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        public OrganizationUnitService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<List<OrganizationUnitDTO>> GetOrganizationUnits(int? id)
        {
            return _dbContext.OrganizationUnit
                .ById(id)
                .ProjectTo<OrganizationUnitDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<OrganizationUnitShortDTO>> GetOrganizationUnitsShort(int? id = null, int? divisionId = null)
        {
            return _dbContext.OrganizationUnit
                .ById(id)
                .ByDivisionId(divisionId)
                .ProjectTo<OrganizationUnitShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<OrganizationUnitDTO> CreateOrganizationUnit(OrganizationUnitDTO organizationUnitDTO)
        {
            var division = await _dbContext.Division.ById(organizationUnitDTO.Division.Id).FirstOrDefaultAsync();

            if (division != null)
            {
                var organizationUnit = _mapper.Map<OrganizationUnit>(organizationUnitDTO);

                _dbContext.OrganizationUnit.Add(organizationUnit);
                await _dbContext.SaveChangesAsync();

                return _mapper.Map<OrganizationUnitDTO>(organizationUnit);
            }
            else
            {
                throw new NotFoundException($"Division with id = '{organizationUnitDTO.Division.Id}' does not exist");
            }
        }

        public async Task<OrganizationUnitDTO> EditOrganizationUnit(OrganizationUnitDTO organizationUnitDTO)
        {
            if (await _dbContext.OrganizationUnit.AnyAsync(o => o.Id == organizationUnitDTO.Id))
            {
                var division = await _dbContext.Division.ById(organizationUnitDTO.Division.Id).FirstOrDefaultAsync();

                if (division != null)
                {
                    var organizationUnit = _mapper.Map<OrganizationUnit>(organizationUnitDTO);

                    _dbContext.OrganizationUnit.Update(organizationUnit);
                    await _dbContext.SaveChangesAsync();

                    return _mapper.Map<OrganizationUnitDTO>(organizationUnit);
                }
            }
            throw new NotFoundException();
        }

        public async Task DeleteOrganizationUnit(int id)
        {
            var organizationUnit = await _dbContext.OrganizationUnit.ById(id).FirstOrDefaultAsync();

            if (organizationUnit != null)
            {
                _dbContext.OrganizationUnit.Remove(organizationUnit);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException($"OrganizationUnit with id = '{id}' does not exist");
            }
        }
    }
}
