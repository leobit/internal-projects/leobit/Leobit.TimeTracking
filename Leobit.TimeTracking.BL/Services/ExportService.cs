﻿using AutoMapper;
using Leobit.TimeTracking.BL.Extensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;
using TimeZoneConverter;

namespace Leobit.TimeTracking.BL.Services
{
    public class ExportService
    {
        private const int ExportQueryTimeoutSeconds = 120;

        private readonly LeobitContext _dbContext;
        private readonly IMapper _mapper;

        private static string SpreadsheetTimeZoneId
        {
            get
            {
                var spreadsheetTimeZoneId = "FLE Standard Time";
                if (!RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    return TZConvert.WindowsToIana(spreadsheetTimeZoneId);
                }
                return spreadsheetTimeZoneId; ;
            }
        }
        private static TimeZoneInfo SpreadsheetTimeZoneInfo = TimeZoneInfo.FindSystemTimeZoneById(SpreadsheetTimeZoneId);
        private static NumberFormatInfo DotNumberFormat = new NumberFormatInfo() { NumberDecimalSeparator = "." };
        private static string TimestampFormat = "M/d/yyyy H:mm:ss";
        private static string ReportingDateFormat = "M/d/yyyy";
        private static string OldReportingDateFormat = "M/1/yyyy";
        private static string MonthFormat = "yyyy-MM";
        private static GregorianCalendar calendar = new GregorianCalendar(GregorianCalendarTypes.USEnglish);

        public ExportService(LeobitContext dbContext, IMapper mapper)
        {
            _dbContext = dbContext ?? throw new ArgumentNullException(nameof(dbContext));
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
        }

        public class ExportConfig
        {
            public DateTime? FromDate { get; set; }
            public DateTime? HardMergeDeadLine { get; set; }
            public DateTime? SoftMergeDeadLine { get; set; }

            public static ExportConfig DefaultConfig(DateTime? dateFrom = null)
            {
                return new ExportConfig
                {
                    FromDate = dateFrom,
                    HardMergeDeadLine = new DateTime(DateTime.Today.AddMonths(-13).Year, DateTime.Today.AddMonths(-13).Month, 1),
                    SoftMergeDeadLine = DateTime.Today.AddDays(-DateTime.Today.Day + 1).AddMonths(-2)
                };
            }

            public static ExportConfig MonthlyReport(DateTime? dateFrom = null)
            {
                return new ExportConfig
                {
                    FromDate = dateFrom,
                    SoftMergeDeadLine = DateTime.Today.AddDays(1)
                };
            }
        }

        public async Task<string[][]> GetTimeReportsToExport(ExportConfig config, CancellationToken cancellationToken = default)
        {
            var merged = await GetTimeReportsDataSet(config, cancellationToken);
            return MapData(merged).ToArray();
        }

        public async Task<SpreadSheetTimeReportDTO[]> GetTimeReportsToExportResource(ExportConfig config, CancellationToken cancellationToken)
        {
            var merged = await GetTimeReportsDataSet(config, cancellationToken);
            return merged.ToArray();
        }

        public async Task<BigQueryTimeReportDTO[]> GetTimeReportsToExportBigQuery(ExportConfig config, CancellationToken cancellationToken)
        {
            var reports = await GetTimeReportsDataSet(config, cancellationToken);

            return MapData(reports).Select(row => new BigQueryTimeReportDTO
            {
                Timestamp = row[0],
                EmailAddress = row[1],
                TimeStart = row[2],
                TimeEnd = row[3],
                InternalHours = row[4],
                ExternalHours = row[5],
                IsOverhead = row[6],
                WorkDescription = row[7],
                Account = row[8],
                Project = row[9],
                ReportingDate = row[10],
                Month = row[11],
                Week = row[12],
                NonBillableHours = row[13],
                CheckDifference = row[14],
                Name = row[15],
                Duplicate = row[16],
                Division = row[17],
                OrganizationUnit = row[18],
                Category = row[19],
                ProjectPriority = row[20],
                ProjectType = row[21],
                BudgetOwnerId = row[22]
            }).ToArray();
        }

        private async Task<IEnumerable<SpreadSheetTimeReportDTO>> GetTimeReportsDataSet(ExportConfig config, CancellationToken cancellationToken)
        {
            var reports = await LoadSpreadsheetTimeReports(config.FromDate, cancellationToken);

            var usersWithLastReport = new HashSet<string>();
            var squashedReports = new Dictionary<GroupKey, SpreadSheetTimeReportDTO>();

            foreach (var item in reports)
            {
                if (!usersWithLastReport.Contains(item.UserId))
                {
                    squashedReports.Add(item.GetGroupKey(GroupType.Raw), item);
                    usersWithLastReport.Add(item.UserId);
                }
                else
                {
                    // raw report
                    if ((!config.SoftMergeDeadLine.HasValue || item.ReportingDate >= config.SoftMergeDeadLine)
                        && (!config.HardMergeDeadLine.HasValue || item.ReportingDate >= config.HardMergeDeadLine))
                    {
                        squashedReports.Add(item.GetGroupKey(GroupType.Raw), item);
                    }
                    // group report
                    else
                    {
                        var groupingType = (config.HardMergeDeadLine.HasValue, config.SoftMergeDeadLine.HasValue) switch
                        {
                            (true, false) => item.ReportingDate >= config.HardMergeDeadLine ? GroupType.Raw : GroupType.Year,
                            (false, true) => item.ReportingDate >= config.SoftMergeDeadLine ? GroupType.Raw : GroupType.Month,
                            (true, true) => item.ReportingDate >= config.HardMergeDeadLine ? (item.ReportingDate >= config.SoftMergeDeadLine ? GroupType.Raw : GroupType.Month) : GroupType.Year,
                            _ => GroupType.Raw
                        };

                        var key = item.GetGroupKey(groupingType);
                        // squash logic
                        if (squashedReports.ContainsKey(key))
                        {
                            squashedReports[key].Timestamp = item.Timestamp;
                            squashedReports[key].ReportingDate = item.ReportingDate;
                            squashedReports[key].InternalHours += item.InternalHours;
                            squashedReports[key].ExternalHours += item.ExternalHours;
                        }
                        else
                        {
                            item.WorkDescription = string.Empty;
                            squashedReports.Add(key, item);
                        }
                    }
                }
            }

            return squashedReports.Values.Reverse();
        }

        private async Task<List<SpreadSheetTimeReportDTO>> LoadSpreadsheetTimeReports(DateTime? fromDate = null, CancellationToken cancellationToken = default)
        {
            var currentTimeout = _dbContext.Database.GetCommandTimeout();

            _dbContext.Database.SetCommandTimeout(ExportQueryTimeoutSeconds);

            var query = _dbContext.TimeReport.AsNoTracking();

            if (fromDate.HasValue)
            {
                query = query.Where(tr => tr.ReportingDate >= fromDate.Value);
            }

            query = query.OrderByDescending(tr => tr.ReportingDate);

            try
            {
                return await _mapper.ProjectTo<SpreadSheetTimeReportDTO>(query).ToListAsync(cancellationToken);
            }
            finally
            {
                _dbContext.Database.SetCommandTimeout(currentTimeout);
            }
        }

        private static IEnumerable<string[]> MapData(IEnumerable<SpreadSheetTimeReportDTO> data)
        {
            return data.Select(tr => new string[]
            {
                TimeZoneInfo.ConvertTimeFromUtc(tr.Timestamp, SpreadsheetTimeZoneInfo).ToString(TimestampFormat),         // Timestamp
                tr.UserId + "@leobit.com",                                                                                 // EmailAddress
                "",                                                                                                       // TimeStart
                "",                                                                                                       // TimeEnd
                tr.InternalHours.ToString(DotNumberFormat),                                                               // InternalHours
                tr.ExternalHours.ToString(DotNumberFormat),                                                               // ExternalHours
                tr.IsOverhead ? "yes" : string.Empty,                                                                     // IsOverhead
                tr.WorkDescription,                                                                                       // WorkDescription
                tr.Account,                                                                                               // Account
                tr.Project,                                                                                               // Project
                tr.ReportingDate.ToString(ReportingDateFormat),                                                           // ReportingDate
                tr.ReportingDate.ToString(MonthFormat),                                                                   // Month
                calendar.GetWeekOfYear(tr.ReportingDate, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Sunday).ToString(), // Week
                (tr.InternalHours - tr.ExternalHours).ToString(DotNumberFormat),                                          // Non-billable hours
                "",                                                                                                       // Check Difference
                tr.Username,                                                                                              // Name
                "",                                                                                                        // Duplicate
                tr.Division,                                                                                              // Division
                tr.OrganizationUnit,                                                                                      // OrganizationUnit
                tr.Category,                                                                                              // Category,
                Enum.GetName(tr.ProjectPriority),                                                                         // Project priority
                Enum.GetName(tr.ProjectType),                                                                             // Project type 
                tr.BudgetOwnerId
            });
        }
    }
}