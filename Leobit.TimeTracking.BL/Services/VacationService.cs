using AutoMapper;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.BL.Services
{
    public class VacationService : IVacationService
    {
        private readonly LeobitContext _dbContext;
        private readonly IMapper _mapper;
        private readonly IVacationDataProvider _dataProvider;

        public VacationService(LeobitContext dbContext, IMapper mapper, IVacationDataProvider dataProvider,
            IMemoryCache memoryCache)
        {
            _dbContext = dbContext;
            _mapper = mapper;
            _dataProvider = dataProvider;
        }

        public async Task<VacationInformationDTO> GetVacationInformation(string userId, int year)
        {
            var employee = await _dataProvider.GetEmployee(userId);

            var transferredVacationBalance = await _dataProvider.GetTransferredVacationBalance(userId, year);

            var (daysReportedInReserve, daysReportedInReserveForLastSixMonths) =
                await _dataProvider.GetReportedReserveDaysCombined(userId, year, employee.TerminationDate);

            var yearData = await _dataProvider.GetVacationYearData(employee, year);

            var currentMonth = DateTime.UtcNow.Month;
            var transferredBalance = !yearData.HasBeenOnTm ? transferredVacationBalance.Transferred : 0;
            var accrualToDate = yearData.MonthData.Where(data => !data.ExcludeFromYearlyTotal).Take(year == DateTime.UtcNow.Year ? currentMonth - 1 : 12).Sum(x => x?.Accrual) ?? 0;
            var accrualToEoy = yearData.MonthData.Where(data => !data.ExcludeFromYearlyTotal).Sum(x => x?.Accrual) ?? 0;
            var usedDays = yearData.MonthData.Where(data => !data.ExcludeFromYearlyTotal).Sum(x => x?.Used) ?? 0;

            return new VacationInformationDTO
            {
                EmployeeId = userId,
                EmploymentDate = employee.StartDate,
                TerminationDate = employee.TerminationDate,
                FirstReportedDate = await _dataProvider.GetFirstReportingDate(userId),
                StandingYears = VacationDataProvider.TotalYears(employee.StartDate, DateTime.UtcNow.Date, employee.TerminationDate),
                TransferredBalance = (double)transferredBalance,
                MinimumAllowedBalance = transferredVacationBalance.CustomCredit ?? (employee.StartDate.HasValue ? -5 : 0),
                CurrentVacationBalance = (double)transferredBalance + accrualToDate - usedDays,
                ExpectedEoyVacationBalance = (double)transferredBalance + accrualToEoy - usedDays,
                ReportedReserveDays = (double)daysReportedInReserve,
                ReportedReserveDaysForLastSixMonths = (double)daysReportedInReserveForLastSixMonths,
                YearData = yearData.MonthData
            };
        }

        public async Task<List<VacationInformationDTO>> GetVacationsInformation(ICollection<string> userIds, int year)
        {
            if (userIds == null || !userIds.Any())
            {
                return new List<VacationInformationDTO>();
            }

            var firstReportingMap = _dbContext.TimeReport
                .AsNoTracking()
                .ByUserIds(userIds)
                .AsEnumerable()
                .GroupBy(tr => tr.UserId)
                .ToDictionary(g => g.Key,
                    g => g.OrderBy(tr => tr.ReportingDate)
                        .FirstOrDefault()
                        ?.ReportingDate);

            var employees = _dbContext.Employee
                .AsNoTracking()
                .ByIds(userIds)
                .AsEnumerable()
                .Select(e =>
                 {
                     var firstReportingDate = firstReportingMap.GetValueOrDefault(e.Id);
                     return new Employee
                     {
                         Id = e.Id,
                         StartDate = VacationDataProvider.GetEffectiveFullTimeStartDate(e.StartDate, firstReportingDate),
                         TerminationDate = e?.TerminationDate,
                         Employment = e?.Employment
                     };
                 })
                .ToList();

            if (!employees.Any())
            {
                return new List<VacationInformationDTO>();
            }

            var existingUserIds = employees.Select(e => e.Id).ToArray();

            var transferredBalances = await _dbContext.TransferredVacationBalance
                .AsNoTracking()
                .Where(tvb => existingUserIds.Contains(tvb.UserId) && tvb.Year == year)
                .ToListAsync();

            foreach (var userId in existingUserIds.Except(transferredBalances.Select(tb => tb.UserId)))
            {
                transferredBalances.Add(new TransferredVacationBalance
                {
                    CustomCredit = null,
                    Transferred = 0,
                    UserId = userId,
                    Year = DateTime.UtcNow.Year
                });
            }

            var transferredBalancesMap = transferredBalances
                .OrderBy(tb => tb.UserId)
                .ToDictionary(tvb => tvb.UserId, tvb => new
                {
                    tvb.CustomCredit,
                    tvb.Transferred
                });

            Task<List<TimeReport>> GetVacationReports(string[] userIds, DateTime firstDayOfYear, DateTime lastDayOfYear, ProjectType type)
            {
                return _dbContext.TimeReport
                    .AsNoTracking()
                    .ByUserIds(userIds)
                    .ByProjectTypes(new[] { (int)type })
                    .ByDateRange(firstDayOfYear, lastDayOfYear)
                    .ToListAsync();
            }

            var employeeDayCountToReserveMap = employees
                .ToDictionary(
                    e => e.Id,
                    e => e.TerminationDate != null && e.TerminationDate < year.LastDateOnlyOfYear()
                        ? (DateTime)e.TerminationDate : year.LastDateOnlyOfYear());

            var reserveTimeReports = _dbContext.TimeReport
                .AsNoTracking()
                .ByUserIds(existingUserIds)
                .Where(tr =>
                    tr.Project.ProjectPriority == (short)ProjectPriority.VeryLow
                        || tr.Project.ProjectPriority == (short)ProjectPriority.Low)
                .AsEnumerable()
                .GroupBy(tr => tr.UserId)
                .Select(g => new
                {
                    UserId = g.Key,
                    DaysReportedInReserve = g
                        .Where(tr => tr.ReportingDate.Year == year)
                        .Sum(tr => tr.InternalHours) / 8,
                    DaysReportedInReserveForLastSixMonths = g
                        .Where(tr =>
                            tr.ReportingDate >= employeeDayCountToReserveMap[g.Key].AddMonths(-6)
                                && tr.ReportingDate <= employeeDayCountToReserveMap[g.Key])
                        .Sum(tr => tr.InternalHours) / 8
                })
                .ToList();

            var userYearDataMap = await _dataProvider.GetVacationMonthDatasMap(employees, year, GetVacationReports, DateTime.UtcNow.FirstDateTimeOfMonth());

            var result = new List<VacationInformationDTO>();
            var currentMonth = DateTime.UtcNow.Month;

            foreach (var employee in employees)
            {
                var transferredBalance = !userYearDataMap[employee.Id].HasBeenOnTm ? transferredBalancesMap[employee.Id].Transferred : 0;
                var accrualToDate = userYearDataMap[employee.Id].MonthData.Where(data => !data.ExcludeFromYearlyTotal).Take(year == DateTime.UtcNow.Year ? currentMonth - 1 : 12).Sum(x => x?.Accrual) ?? 0;
                var accrualToEoy = userYearDataMap[employee.Id].MonthData.Where(data => !data.ExcludeFromYearlyTotal).Sum(x => x?.Accrual) ?? 0;
                var usedDays = userYearDataMap[employee.Id].MonthData.Where(data => !data.ExcludeFromYearlyTotal).Sum(x => x?.Used) ?? 0;

                var reserveTimeReport = reserveTimeReports.FirstOrDefault(tr => tr.UserId == employee.Id);
                var reportedReserveDays = reserveTimeReport?.DaysReportedInReserve ?? 0;
                var reportedReserveDaysForLastSixMonths = reserveTimeReport?.DaysReportedInReserveForLastSixMonths ?? 0;

                result.Add(new VacationInformationDTO()
                {
                    EmployeeId = employee.Id,
                    EmploymentDate = employee.StartDate,
                    TerminationDate = employee.TerminationDate,
                    FirstReportedDate = firstReportingMap.GetValueOrDefault(employee.Id),
                    StandingYears = VacationDataProvider.TotalYears(employee.StartDate, DateTime.UtcNow.Date, employee.TerminationDate),
                    TransferredBalance = (double)transferredBalance,
                    MinimumAllowedBalance = transferredBalancesMap[employee.Id].CustomCredit ?? (employee.StartDate.HasValue ? -5 : 0),
                    CurrentVacationBalance = (double)transferredBalance + accrualToDate - usedDays,
                    ExpectedEoyVacationBalance = (double)transferredBalance + accrualToEoy - usedDays,
                    ReportedReserveDays = (double)reportedReserveDays,
                    ReportedReserveDaysForLastSixMonths = (double)reportedReserveDaysForLastSixMonths,
                    YearData = userYearDataMap[employee.Id].MonthData
                });
            }

            return result;
        }

        public async Task TransferVacationBalance(int year)
        {
            var activeEmployees = await _dbContext.Employee
                .AsNoTracking()
                .Where(e => !e.TerminationDate.HasValue)
                .Where(e => e.FullTimeStartDate.HasValue)
                .Select(e => e.Id)
                .ToListAsync();

            var existingBalanceList = await _dbContext.TransferredVacationBalance
                .Where(tvb => tvb.Year == year)
                .ToListAsync();

            var existingUsers = existingBalanceList.Select(b => b.UserId).ToArray();

            var vacationInfoList = await GetVacationsInformation(activeEmployees, year - 1);

            foreach (var vacationInfo in vacationInfoList)
            {
                var daysToBeTransferred = CalculateTransferredDays(vacationInfo);

                if (existingUsers.Contains(vacationInfo.EmployeeId))
                {
                    var userBalance = existingBalanceList.First(b => b.UserId == vacationInfo.EmployeeId);
                    userBalance.Transferred = daysToBeTransferred;

                    _dbContext.TransferredVacationBalance.Update(userBalance);
                }
                else
                {
                    _dbContext.TransferredVacationBalance.Add(new TransferredVacationBalance
                    {
                        Year = year,
                        UserId = vacationInfo.EmployeeId,
                        Transferred = daysToBeTransferred
                    });
                }
            }

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<EmployeeManagerShortDTO>> GetManagersList()
        {
            var managerNames = await _dbContext.Employee
                .Where(e => !e.TerminationDate.HasValue && e.FullTimeStartDate.HasValue)
                .Where(e => e.Active)
                .Select(e => e.Manager)
                .Distinct()
                .Where(n => !string.IsNullOrEmpty(n))
                .ToListAsync();

            var managers = await _dbContext.Employee
                .Where(e => managerNames.Contains(e.FullName))
                .Select(e => new EmployeeManagerShortDTO(e.Id, e.FullName))
                .ToListAsync();

            return managers;
        }

        public async Task<List<VacationStatisticModel>> GetAllEmployeesBalance(int year, string managerId, int? projectId)
        {
            var transferredMap = await _dbContext.TransferredVacationBalance
                .AsNoTracking()
                .Where(x => x.Year == year)
                .ToDictionaryAsync(x => x.UserId, x => x.Transferred);

            var activeEmployeesQuery = _dbContext.Employee
                .Where(e => !e.TerminationDate.HasValue)
                .Where(e => e.StartDate.HasValue)
                .Where(e => e.FullTimeStartDate.HasValue)
                .Where(e => e.Active);

            if (!string.IsNullOrEmpty(managerId))
            {
                var manager = await _dbContext.Employee.AsNoTracking().FirstOrDefaultAsync(e => e.Id == managerId);

                if (manager != null)
                {
                    activeEmployeesQuery = activeEmployeesQuery.Where(e => e.Manager == manager.FullName);
                }
            }

            if (projectId.HasValue)
            {
                var employeeIds = await _dbContext.UserProject
                    .Where(userProject => userProject.ProjectId == projectId)
                    .Select(userProject => userProject.UserId)
                    .ToListAsync();

                activeEmployeesQuery = activeEmployeesQuery.Where(employee => employeeIds.Contains(employee.Id));
            }

            var employees = await activeEmployeesQuery
                .ToListAsync();

            var activeEmployeeIds = employees.Select(employee => employee.Id);

            var employeeReportingDates = await _dbContext
                .TimeReport
                .AsNoTracking()
                .Where(tReport => activeEmployeeIds.Contains(tReport.UserId))
                .GroupBy(tReport => tReport.UserId)
                .Select(grp => new
                {
                    UserId = grp.Key,
                    FirstReportingDate = grp.Min(tr => tr.ReportingDate)
                })
                .ToDictionaryAsync(x => x.UserId, x => x.FirstReportingDate);

            var activeEmployees = employees.Select(e => new Employee
            {
                Id = e.Id,
                FullName = e.FullName,
                StartDate = VacationDataProvider.GetEffectiveFullTimeStartDate(e.StartDate, employeeReportingDates.TryGetValue(e.Id, out var reportingDate) ? reportingDate : null),
                TerminationDate = e.TerminationDate
            })
            .ToList();

            Task<List<TimeReport>> GetVacationReports(string[] userIds, DateTime firstDayOfYear, DateTime lastDayOfYear, ProjectType type)
            {
                var activeEmployeeVacationReports = _dbContext.TimeReport
                    .AsNoTracking()
                    .ByUserIds(activeEmployees.Select(e => e.Id).ToArray())
                    .ByProjectTypes(new[] { (int)ProjectType.Vacation })
                    .ByDateRange(year.FirstDateOnlyOfYear(), year.LastDateOnlyOfYear())
                    .ToList();

                var reports = type == ProjectType.Vacation
                    ? activeEmployeeVacationReports.Where(x => userIds.Contains(x.UserId)).ToList()
                    : new List<TimeReport>();

                return Task.FromResult(reports);
            }

            var yearDataMap = await _dataProvider.GetVacationMonthDatasMap(activeEmployees, year, GetVacationReports, DateTime.UtcNow.FirstDateTimeOfMonth());
            var currentMonth = DateTime.UtcNow.Month;
            var vacationStat = new List<VacationStatisticModel>();

            foreach (var employee in activeEmployees)
            {
                var userYearDataMap = yearDataMap[employee.Id];

                var accrualToEoy = userYearDataMap.MonthData.Where(e => !e.ExcludeFromYearlyTotal).Sum(x => x?.Accrual) ?? 0;
                var accrualToDate = userYearDataMap.MonthData.Where(e => !e.ExcludeFromYearlyTotal).Take(currentMonth - 1).Sum(x => x?.Accrual) ?? 0;
                var usedDays = userYearDataMap.MonthData.Where(e => !e.ExcludeFromYearlyTotal).Sum(x => x?.Used) ?? 0;

                var totalUsed = userYearDataMap.MonthData.Where(x => !x.ExcludeFromYearlyTotal).Sum(x => x?.Used ?? 0);

                var transferred = !userYearDataMap.HasBeenOnTm ? (double)transferredMap.GetValueOrDefault(employee.Id) : 0;
                var annualBalance = transferred + accrualToEoy;
                var availableDaysForUse = annualBalance - Constants.DefaultMaxTransferredDays;

                var totalUsedPercent = Math.Round((totalUsed / availableDaysForUse), 2);
                var totalUsedPercentPure = Math.Round((totalUsed / annualBalance), 2);

                var expectedEoyVacationBalance = annualBalance - usedDays;
                var currentVacationBalance = transferred + accrualToDate - usedDays;
                var totalAtThisYear = Math.Round(annualBalance, 1);
                var employeeTotalUsed = Math.Round(totalUsed, 1);
                var employeeTotalUsedPercent = double.IsNaN(totalUsedPercent) ? 0 : totalUsedPercent;
                var employeeTotalUsedPercentPure = double.IsNaN(totalUsedPercentPure) ? 0 : totalUsedPercentPure;

                vacationStat.Add(new VacationStatisticModel(
                    employee.Id,
                    employee.FullName,
                    transferred,
                    expectedEoyVacationBalance,
                    currentVacationBalance,
                    totalAtThisYear,
                    employeeTotalUsed,
                    employeeTotalUsedPercent,
                    employeeTotalUsedPercentPure,
                    userYearDataMap.MonthData));
            }

            return vacationStat;
        }

        public async Task<List<CustomDayOffDTO>> GetCustomDayOffs(string userId, int? year = null, int? month = null)
        {
            var query = _dbContext.CustomDayOff.AsNoTracking().ByUserId(userId);

            if (year.HasValue)
            {
                query = query.ByYear(year.Value);

                if (month.HasValue)
                {
                    query = query.ByMonth(month.Value);
                }
            }

            var customDayOffs = await query
                .Include(cdo => cdo.User)
                .Include(cdo => cdo.LastEditor)
                .ToListAsync();

            return _mapper.Map<List<CustomDayOffDTO>>(customDayOffs);
        }

        public async Task<List<CustomDayOffDTO>> GetCustomDayOffs(ICollection<string> userIds, int? year = null, int? month = null)
        {
            var query = _dbContext.CustomDayOff
                .AsNoTracking()
                .ByUserIds(userIds);

            if (year.HasValue)
            {
                query = query.ByYear(year.Value);

                if (month.HasValue)
                {
                    query = query.ByMonth(month.Value);
                }
            }

            var customDayOffs = await query
                .Include(cdo => cdo.User)
                .Include(cdo => cdo.LastEditor)
                .ToListAsync();

            return _mapper.Map<List<CustomDayOffDTO>>(customDayOffs);
        }

        public async Task<List<CustomDayOffDTO>> GetAllCustomDayOffsByYear(int year)
        {
            var customDayOffs = await _dbContext.CustomDayOff
                .AsNoTracking()
                .ByYear(year)
                .Include(cdo => cdo.User)
                .Include(cdo => cdo.LastEditor)
                .ToListAsync();

            return _mapper.Map<List<CustomDayOffDTO>>(customDayOffs)
                .OrderByDescending(cdo => cdo.LastEditTimestamp)
                .ToList();
        }

        public async Task<CustomDayOffDTO> CreateCustomDayOff(string editorId, CustomDayOffDTO customDayOffDTO)
        {
            if (customDayOffDTO.Month <= 0)
            {
                throw new ConflictException("Custom day off Month value must be positive");
            }

            var employee = await _dbContext.Employee.ById(customDayOffDTO.User.Id).FirstOrDefaultAsync();

            // For further EF context navigation mapping 
            var interactors = await _dbContext.User.Where(u => u.Id == customDayOffDTO.User.Id || u.Id == editorId).ToListAsync();
            var editor = interactors.FirstOrDefault(u => u.Id == editorId);

            if (employee == null || editor == null)
            {
                throw new NotFoundException();
            }

            var firstDayOfMonth = new DateTime(customDayOffDTO.Year, customDayOffDTO.Month, 1);
            var lastDayOfMonth = new DateTime(customDayOffDTO.Year, customDayOffDTO.Month, 1).AddMonths(1).AddDays(-1);

            if (lastDayOfMonth >= employee.StartDate
                && (employee.TerminationDate == null || firstDayOfMonth <= employee.TerminationDate))
            {
                var customDayOff = _mapper.Map<CustomDayOff>(customDayOffDTO);
                customDayOff.LastEditorId = editorId;
                customDayOff.LastEditTimestamp = DateTime.UtcNow;

                _dbContext.CustomDayOff.Add(customDayOff);
                await _dbContext.SaveChangesAsync();

                return _mapper.Map<CustomDayOffDTO>(customDayOff);
            }

            throw new InvalidOperationException();

        }

        public async Task<CustomDayOffDTO> EditCustomDayOff(string editorId, CustomDayOffDTO customDayOffDTO)
        {
            if (customDayOffDTO.Month <= 0)
            {
                throw new ConflictException("Custom day off Month value must be positive");
            }

            if (await _dbContext.CustomDayOff.ById(customDayOffDTO.Id).AnyAsync())
            {
                var user = await _dbContext.User.ById(customDayOffDTO.User.Id).FirstOrDefaultAsync();
                var editor = await _dbContext.User.ById(editorId).FirstOrDefaultAsync();

                if (user != null && editor != null)
                {
                    var customDayOff = _mapper.Map<CustomDayOff>(customDayOffDTO);
                    customDayOff.LastEditorId = editorId;
                    customDayOff.LastEditTimestamp = DateTime.UtcNow;

                    _dbContext.CustomDayOff.Update(customDayOff);
                    await _dbContext.SaveChangesAsync();

                    return _mapper.Map<CustomDayOffDTO>(customDayOff);
                }
            }
            throw new NotFoundException();
        }

        public async Task DeleteCustomDayOff(int id)
        {
            var customDayOff = await _dbContext.CustomDayOff.ById(id).FirstOrDefaultAsync();
            if (customDayOff != null)
            {
                _dbContext.CustomDayOff.Remove(customDayOff);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task EditEmploymentDate(string id, string employmentDate)
        {
            var user = await _dbContext.Employee.FirstOrDefaultAsync(x => x.Id == id);
            if (user == null) throw new NotFoundException();

            if (employmentDate != null)
            {
                var date = DateTime.Parse(employmentDate);
                user.FullTimeStartDate = date;
            }
            else user.FullTimeStartDate = null;

            _dbContext.Employee.Update(user);
            await _dbContext.SaveChangesAsync();
        }

        public async Task<bool> IsEmployeeOnVacation(string userId, DateTime date)
        {
            var vacation = await _dbContext.TimeReport
                .AsNoTracking()
                .ByUserIds(new[] { userId })
                .ByProjectTypes(new[] { (int)ProjectType.Vacation, (int)ProjectType.VacationUnpaid })
                .ByDate(date)
                .ToListAsync();

            return vacation.Any();
        }

        public async Task<Dictionary<string, bool>> AreEmployeesOnVacation(ICollection<string> userIds, DateTime date)
        {
            var vacations = await _dbContext.TimeReport
                .AsNoTracking()
                .ByUserIds(userIds)
                .ByProjectTypes(new[] { (int)ProjectType.Vacation, (int)ProjectType.VacationUnpaid })
                .ByDate(date)
                .ToListAsync();

            var userVacationStatus = userIds.ToDictionary(
                userId => userId,
                userId => vacations.Any(v => v.UserId == userId)
            );

            return userVacationStatus;
        }

        private static decimal CalculateTransferredDays(VacationInformationDTO vacationInformation)
        {
            return vacationInformation.ReportedReserveDays < Constants.MaxReserveDaysForVacationTransfer
                ? (decimal)Math.Min(vacationInformation.ExpectedEoyVacationBalance, Constants.DefaultMaxTransferredDays)
                : 0;
        }
    }
}