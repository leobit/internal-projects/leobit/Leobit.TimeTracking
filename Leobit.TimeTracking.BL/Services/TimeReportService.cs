﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Extensions;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Common;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.AmazonSQS;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;

namespace Leobit.TimeTracking.BL.Services
{
    public class TimeReportService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        private readonly IVacationService _vacationService;
        private readonly SickLeaveService _sickLeaveService;
        private readonly IOperationalService _operationalService;
        private readonly AmazonSqsService _amazonSqsService;
        private readonly UserProjectService _userProjectService;


        public TimeReportService(
            IMapper mapper,
            LeobitContext dbContext,
            IVacationService vacationService,
            SickLeaveService sickLeaveService,
            IOperationalService operationalService,
            AmazonSqsService amazonSqsService,
            UserProjectService userProjectService)
        {
            _mapper = mapper;
            _dbContext = dbContext;

            _vacationService = vacationService;
            _sickLeaveService = sickLeaveService;
            _operationalService = operationalService;

            _amazonSqsService = amazonSqsService;
            _userProjectService = userProjectService;
        }

        public Task<TimeReportDTO> GetTimeReport(int id)
        {
            return _dbContext.TimeReport
                .ById(id)
                .ProjectTo<TimeReportDTO>(_mapper.ConfigurationProvider)
                .FirstOrDefaultAsync();
        }

        public Task<List<TimeReportDTO>> GetTimeReports(int[] ids, ICollection<string> userIds)
        {
            return _dbContext.TimeReport
                .ByIds(ids)
                .ByUserIds(userIds)
                .OrderByDateDescending()
                .ProjectTo<TimeReportDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<List<TimeReportDTO>> GetTimeReports(int? id, DateTime dateFrom, DateTime dateTo, ICollection<int> projectIds, ICollection<string> userIds, bool? isOverhead)
        {
            var projectReportingReopenRequests = await _dbContext.ProjectReportingReopenRequest
                .Where(r => !r.IsDeleted &&
                            r.ApprovalTimestamp != null &&
                            (projectIds.Count == 0 || projectIds.Contains(r.ProjectId)) &&
                            (userIds.Count == 0 || userIds.Contains(r.SenderId)))
                .ToListAsync();

            var timeReportsQuery = _dbContext.TimeReport
                .Include(tr => tr.Project)
                .ThenInclude(p => p.Account)
                .ById(id)
                .ByDateRange(dateFrom, dateTo)
                .ByProjectIds(projectIds)
                .ByUserIds(userIds)
                .ByOverhead(isOverhead);

            var timeReportDtos = await timeReportsQuery
                .OrderByDateDescending()
                .ProjectTo<TimeReportDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();

            var globalSettings = await _dbContext.Settings.AsNoTracking().SingleAsync();

            var projectDateRange = (await timeReportsQuery
                .Select(tr => tr.Project)
                .Distinct()
                .ToListAsync())
                .ToDictionary(
                    project => project.Id,
                    project =>
                    {
                        var closingType = project.OverrideClosingSettings ? project.ClosingType : (project.Account.OverrideClosingSettings ? project.Account.ClosingType : globalSettings.ClosingType);
                        var beforeClosing = project.OverrideClosingSettings ? project.BeforeClosing : (project.Account.OverrideClosingSettings ? project.Account.BeforeClosing : globalSettings.BeforeClosing);
                        var afterClosing = project.OverrideClosingSettings ? project.AfterClosing : (project.Account.OverrideClosingSettings ? project.Account.AfterClosing : globalSettings.AfterClosing);

                        return TimeReportRangeCalculationHelper.CalculateAllowedDateRange(closingType, beforeClosing, afterClosing, DateTime.UtcNow);
                    }
                );

            foreach (var dto in timeReportDtos)
            {
                dto.IsEditable = projectReportingReopenRequests.Any(request =>
                    request.ProjectId == dto.Project.Id &&
                    request.SenderId == dto.User.Id &&
                    request.StartDate <= dto.ReportingDate &&
                    request.EndDate >= dto.ReportingDate &&
                    !request.IsDeleted &&
                    request.ApprovalTimestamp is not null) ||
                                 (!dto.Project.IsClosed &&
                                 dto.ReportingDate >= projectDateRange[dto.Project.Id].StartDate &&
                                 dto.ReportingDate <= projectDateRange[dto.Project.Id].EndDate);
            }

            return timeReportDtos;
        }

        public Task<List<TimeReportDTO>> GetTeamTimeReports(string userId, int? id, DateTime dateFrom, DateTime dateTo, ICollection<int> projectIds, ICollection<string> userIds, bool? isOverhead)
        {
            throw new NotImplementedException();
        }

        public async Task<TimeReportDTO> CreateTimeReport(TimeReportDTO timeReportDto, bool validate = true, CancellationToken cancellationToken = default)
        {
            if (validate)
            {
                await ValidateTimeReportDto(
                    timeReportDto: timeReportDto,
                    validateSameUserProjectDate: true,
                    validateDateRanges: true,
                    validateHours: true,
                    validateVacation: true,
                    validateSickLeave: true,
                    validateSickLeaveWithoutCert: true,
                    validateExternalHours: true,
                    validateHoliday: true,
                    validateUserAssignmentExpiration: true,
                    cancellationToken: cancellationToken);
            }

            var timeReport = _mapper.Map<TimeReport>(timeReportDto);
            timeReport.Timestamp = DateTime.UtcNow;

            _dbContext.TimeReport.Add(timeReport);
            await _dbContext.SaveChangesAsync(cancellationToken);

            if (HasToBeSyncedWithGoogleCalendar(timeReportDto))
            {
                await _amazonSqsService.SendTimeReportMessage(timeReportDto, TimeReportActionType.Create);
            }

            return _mapper.Map<TimeReportDTO>(timeReport);
        }

        public async Task<List<TimeReportDTO>> CreateTimeReports(List<TimeReportDTO> timeReportDtos, bool validate = true, CancellationToken cancellationToken = default)
        {
            var reportsWithoutHolidays = await ExcludeOfficialHolidaysReports(timeReportDtos);
            if (!reportsWithoutHolidays.Any())
            {
                throw new ConflictException("You have chosen only the holidays.");
            }

            if (validate)
            {
                foreach (var report in reportsWithoutHolidays)
                {
                    await ValidateTimeReportDto(
                        timeReportDto: report,
                        validateSameUserProjectDate: true,
                        validateDateRanges: true,
                        validateHours: true,
                        validateVacation: true,
                        validateSickLeave: true,
                        validateSickLeaveWithoutCert: true,
                        validateHoliday: false,
                        validateExternalHours: true,
                        validateBatchReportAndTotalHours: reportsWithoutHolidays.Sum(t => t.InternalHours),
                        validateUserAssignmentExpiration: true,
                        cancellationToken: cancellationToken);
                }
            }

            var timeReports = _mapper.Map<List<TimeReport>>(reportsWithoutHolidays);
            var timestamp = DateTime.UtcNow;
            timeReports.ForEach(tr => tr.Timestamp = timestamp);

            _dbContext.TimeReport.AddRange(timeReports);
            await _dbContext.SaveChangesAsync(cancellationToken);

            var toBeSynced = reportsWithoutHolidays.Where(HasToBeSyncedWithGoogleCalendar).ToList();
            if (toBeSynced.Any())
            {
                await _amazonSqsService.SendTimeReportsMessages(toBeSynced, TimeReportActionType.Create);
            }

            return _mapper.Map<List<TimeReportDTO>>(timeReports.OrderBy(x => x.ReportingDate));
        }

        public TimeReportDTO CreateTeamTimeReport(string userId, TimeReportDTO timeReportDTO)
        {
            throw new NotImplementedException();
        }

        public async Task<TimeReportDTO> EditTimeReport(TimeReportDTO timeReportDto)
        {
            var originalTimeReport = await _dbContext.TimeReport
                .ById(timeReportDto.Id)
                .Include(tr => tr.Project)
                .Include(tr => tr.User)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (originalTimeReport is null)
            {
                throw new NotFoundException();
            }

            var originalTimeReportDto = _mapper.Map<TimeReportDTO>(originalTimeReport);

            await ValidateTimeReportDto(
                timeReportDto: originalTimeReportDto,
                validateSameUserProjectDate: false,
                validateDateRanges: true,
                validateHours: false,
                validateVacation: false,
                validateSickLeave: false,
                validateSickLeaveWithoutCert: false,
                validateExternalHours: false,
                validateUserAssignmentExpiration: true,
                validateHoliday: false);

            await ValidateTimeReportDto(
                timeReportDto: timeReportDto,
                validateSameUserProjectDate: true,
                validateDateRanges: true,
                validateHours: true,
                validateVacation: true,
                validateSickLeave: true,
                validateSickLeaveWithoutCert: true,
                validateHoliday: true,
                validateExternalHours: true,
                validateUserAssignmentExpiration: true,
                originalInternalHours: originalTimeReportDto.InternalHours);

            var toBackup = _mapper.Map<TimeReport>(originalTimeReportDto).ToHistory(new BackupOptions
            {
                EditAction = EditAction.Edit,
                EditorId = originalTimeReportDto.User.Id,
                EditGroup = null,
                EditTimestamp = DateTime.UtcNow,
            });
            _dbContext.TimeReportHistory.Add(toBackup);

            var timeReport = _mapper.Map<TimeReport>(timeReportDto);

            timeReport.ProjectPrioritySnapshot = timeReport.ProjectId == originalTimeReport.ProjectId
                ? originalTimeReport.ProjectPrioritySnapshot
                : timeReportDto.Project.ProjectPriority;

            _dbContext.TimeReport.Update(timeReport);
            _dbContext.Entry(timeReport).Property(tr => tr.Timestamp).IsModified = false;
            await _dbContext.SaveChangesAsync();

            if (HasToBeSyncedWithGoogleCalendar(originalTimeReportDto))
            {
                await _amazonSqsService.SendTimeReportMessage(originalTimeReportDto, TimeReportActionType.Delete);
            }
            if (HasToBeSyncedWithGoogleCalendar(timeReportDto))
            {
                await _amazonSqsService.SendTimeReportMessage(timeReportDto, TimeReportActionType.Create);
            }

            return _mapper.Map<TimeReportDTO>(timeReport);
        }

        // no validation needed
        public async Task<List<TimeReportDTO>> EditTimeReports(string userId, List<TimeReportDTO> timeReportDtos)
        {
            var timeReports = _mapper.Map<List<TimeReport>>(timeReportDtos);

            var ids = timeReports.Select(x => x.Id).ToArray();
            var originalTimeReports = _dbContext.TimeReport
                .Where(tr => ids.Contains(tr.Id))
                .Include(tr => tr.Project)
                .Include(tr => tr.User)
                .AsNoTracking()
                .ToList();

            foreach (var timeReport in timeReports)
            {
                var originalTimeReport = originalTimeReports.FirstOrDefault(otr => otr.Id == timeReport.Id);
                if (originalTimeReport == null)
                {
                    throw new NotFoundException();
                }

                if (timeReport.ProjectId == originalTimeReport.ProjectId)
                {
                    timeReport.ProjectPrioritySnapshot = originalTimeReport.ProjectPrioritySnapshot;
                }
                else
                {
                    var projectPriority = timeReportDtos.FirstOrDefault(tr => tr.Id == timeReport.Id)
                            ?.Project
                            ?.ProjectPriority
                        ?? originalTimeReport.ProjectPrioritySnapshot;
                    timeReport.ProjectPrioritySnapshot = projectPriority;
                }
            }

            var editGroupId = Guid.NewGuid().ToString("N");
            var originalTimeReportHistory = originalTimeReports.Select(trh => trh.ToHistory(new BackupOptions
            {
                EditAction = EditAction.Edit,
                EditGroup = editGroupId,
                EditorId = userId,
                EditTimestamp = DateTime.UtcNow
            })).ToList();

            _dbContext.TimeReport.UpdateRange(timeReports);

            // otherwise Change detector mark all nested navigation properties as added
            originalTimeReportHistory.ForEach(x =>
            {
                _dbContext.Entry(x).State = EntityState.Added;
            });
            timeReports.ForEach(tr =>
            {
                _dbContext.Entry(tr).Property(t => t.Timestamp).IsModified = false;
            });
            await _dbContext.SaveChangesAsync();

            var toBeSyncedOriginalDto = originalTimeReports
                .Where(HasToBeSyncedWithGoogleCalendar)
                .ToList();

            if (toBeSyncedOriginalDto.Any())
            {
                await _amazonSqsService.SendTimeReportsMessages(toBeSyncedOriginalDto, TimeReportActionType.Delete);
            }

            var toBeSyncedDto = timeReportDtos.Where(HasToBeSyncedWithGoogleCalendar).ToList();
            if (toBeSyncedDto.Any())
            {
                await _amazonSqsService.SendTimeReportsMessages(toBeSyncedDto, TimeReportActionType.Create);
            }

            return _mapper.Map<List<TimeReportDTO>>(timeReports);
        }

        public Task<TimeReportDTO> EditTeamTimeReport(string userId, TimeReportDTO timeReportDto)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteTimeReport(string userId, int id)
        {
            var originalTimeReport = await _dbContext.TimeReport
                .ById(id)
                .FirstOrDefaultAsync();

            if (originalTimeReport is null)
            {
                throw new NotFoundException();
            }

            var originalTimeReportDto = _mapper.Map<TimeReportDTO>(originalTimeReport);

            await ValidateTimeReportDto(
                timeReportDto: originalTimeReportDto,
                validateSameUserProjectDate: false,
                validateDateRanges: true,
                validateHours: false,
                validateVacation: false,
                validateSickLeave: false,
                validateSickLeaveWithoutCert: false,
                validateExternalHours: false,
                validateHoliday: false,
                validateUserAssignmentExpiration: true);

            var toBackUp = originalTimeReport.ToHistory(new BackupOptions
            {
                EditAction = EditAction.Delete,
                EditGroup = null,
                EditorId = userId,
                EditTimestamp = DateTime.UtcNow,
            });
            _dbContext.TimeReportHistory.Add(toBackUp);

            _dbContext.TimeReport.Remove(originalTimeReport);
            await _dbContext.SaveChangesAsync();

            if (HasToBeSyncedWithGoogleCalendar(originalTimeReportDto))
            {
                await _amazonSqsService.SendTimeReportMessage(originalTimeReportDto, TimeReportActionType.Delete);
            }
        }

        // no validation needed
        public async Task DeleteTimeReports(string userId, int[] ids)
        {
            var timeReportsToDelete = await _dbContext.TimeReport
                .Where(tr => ids.Contains(tr.Id))
                .Include(tr => tr.User)
                .Include(tr => tr.Project)
                .ToListAsync();

            var editGroupId = Guid.NewGuid().ToString("N");
            var toBackup = timeReportsToDelete.Select(tr => tr.ToHistory(new BackupOptions
            {
                EditAction = EditAction.Delete,
                EditGroup = editGroupId,
                EditorId = userId,
                EditTimestamp = DateTime.UtcNow
            }));

            if (toBackup.Any())
            {
                _dbContext.TimeReportHistory.AddRange(toBackup);
                _dbContext.TimeReport.RemoveRange(timeReportsToDelete);
                await _dbContext.SaveChangesAsync();

                var timeReportsToDeleteDto = timeReportsToDelete
                    .Where(HasToBeSyncedWithGoogleCalendar)
                    .ToList();

                if (timeReportsToDeleteDto.Any())
                {
                    await _amazonSqsService.SendTimeReportsMessages(timeReportsToDeleteDto, TimeReportActionType.Delete);
                }
            }
        }

        public Task DeleteTeamTimeReport(string userId, int id)
        {
            throw new NotImplementedException();
        }

        public async Task DeleteOwnTimeReport(string userId, int id, CancellationToken cancellationToken = default)
        {
            var originalTimeReport = _dbContext.TimeReport
                .ById(id)
                .ByUserIds(new[] { userId })
                .FirstOrDefault();

            if (originalTimeReport is null)
            {
                throw new NotFoundException();
            }

            // nice job
            var timeReportDto = _mapper.Map<TimeReportDTO>(originalTimeReport);
            timeReportDto.User = new UserShortDTO { Id = originalTimeReport.UserId };
            timeReportDto.Project = new ProjectShortDTO { Id = originalTimeReport.ProjectId };

            await ValidateTimeReportDto(
                timeReportDto: timeReportDto,
                validateSameUserProjectDate: false,
                validateDateRanges: true,
                validateHours: false,
                validateVacation: false,
                validateSickLeave: false,
                validateSickLeaveWithoutCert: false,
                validateExternalHours: false,
                validateHoliday: false,
                validateUserAssignmentExpiration: true,
                cancellationToken: cancellationToken);

            var toBackup = originalTimeReport.ToHistory(new BackupOptions
            {
                EditAction = EditAction.Delete,
                EditGroup = null,
                EditorId = userId,
                EditTimestamp = DateTime.UtcNow,
            });

            _dbContext.TimeReportHistory.Add(toBackup);
            _dbContext.TimeReport.Remove(originalTimeReport);
            await _dbContext.SaveChangesAsync(cancellationToken);

            if (HasToBeSyncedWithGoogleCalendar(timeReportDto))
            {
                await _amazonSqsService.SendTimeReportMessage(timeReportDto, TimeReportActionType.Delete);
            }
        }

        public Task<List<TimeReportBatchCommitDTO>> GetTimeReportCommits()
        {
            return _dbContext.TimeReportHistory
                .Where(trc => trc.EditGroup != null)
                .GroupBy(trc => new
                {
                    trc.EditGroup,
                    trc.EditTimestamp,
                    trc.EditorId,
                    trc.Editor.FirstName,
                    trc.Editor.LastName,
                    trc.EditAction
                })
                .OrderByDescending(trc => trc.Key.EditTimestamp)
                .Take(15)
                .Select(g => new TimeReportBatchCommitDTO
                {
                    Id = g.Key.EditGroup,
                    DateTime = g.Key.EditTimestamp,
                    EditorFullName = g.Key.FirstName + " " + g.Key.LastName,
                    EditAction = g.Key.EditAction == (short)EditAction.Edit ? "Edit" : "Delete"
                })
                .ToListAsync();
        }

        public class TimeReportCommitDetails
        {
            public DateTime ReportingDate { get; set; }
            public string UserFullname { get; set; }
            public string ProjectFrom { get; set; }
            public string ProjectTo { get; set; }
            public decimal IntFrom { get; set; }
            public decimal? IntTo { get; set; }
            public decimal ExtFrom { get; set; }
            public decimal? ExtTo { get; set; }
            public string WorkDescFrom { get; set; }
        }

        public Task<List<TimeReportCommitDetails>> GetTimeReportCommitDetails(string id)
        {
            return _dbContext.TimeReportHistory
                .Where(trc => trc.EditGroup == id)
                .GroupJoin(_dbContext.TimeReport,
                    trh => trh.TimeReportId,
                    tr => tr.Id,
                    (trh, tr) => new { trh, tr }
                )
                .SelectMany(x => x.tr.DefaultIfEmpty(),
                    (jr, tr) => new TimeReportCommitDetails
                    {
                        ReportingDate = jr.trh.ReportingDate,
                        UserFullname = $"{jr.trh.User.FirstName} {jr.trh.User.LastName}",
                        ProjectFrom = jr.trh.Project.Name,
                        ProjectTo = tr.Project.Name ?? null,
                        IntFrom = jr.trh.InternalHours,
                        IntTo = tr != null ? tr.InternalHours : null,
                        ExtFrom = jr.trh.ExternalHours,
                        ExtTo = tr != null ? tr.ExternalHours : null,
                        WorkDescFrom = jr.trh.WorkDescription
                    }
                )
                .ToListAsync();
        }

        public async Task RevertTimeReportCommit(string commitId)
        {
            var commitRecord = _dbContext.TimeReportHistory.AsNoTrackingWithIdentityResolution().FirstOrDefault(trh => trh.EditGroup == commitId);

            if (commitRecord == null)
            {
                throw new NotFoundException();
            }

            var backupUngrouped = await _dbContext.TimeReportHistory
                .AsNoTrackingWithIdentityResolution()
                .Include(trh => trh.Project)
                .Include(trh => trh.User)
                .Where(trh => trh.EditTimestamp >= commitRecord.EditTimestamp && trh.EditGroup != null)
                .OrderBy(trh => trh.Timestamp)
                .ToListAsync();

            var backup = backupUngrouped
                .GroupBy(trh => trh.TimeReportId);

            var toUpdate = _mapper.Map<IEnumerable<TimeReport>>(backup
                .Where(g => !g.Any(trh => trh.EditAction == (short)EditAction.Delete))
                .Select(g => g.First()));

            var toUpdateIds = toUpdate.Select(tr => tr.Id).ToList();
            var toUpdateOriginalToBeSynced = await _dbContext.TimeReport
                .AsNoTrackingWithIdentityResolution()
                .Where(tr => toUpdateIds.Contains(tr.Id))
                .Include(tr => tr.Project)
                .Include(tr => tr.User)
                .ToListAsync();

            toUpdateOriginalToBeSynced = toUpdateOriginalToBeSynced
                .Where(HasToBeSyncedWithGoogleCalendar)
                .ToList();

            var toUpdateNewTobeSynced = toUpdate.Where(HasToBeSyncedWithGoogleCalendar).ToList();

            _dbContext.TimeReport.UpdateRange(toUpdate);
            await _dbContext.SaveChangesAsync();

            if (toUpdateOriginalToBeSynced.Any())
            {
                await _amazonSqsService.SendTimeReportsMessages(toUpdateOriginalToBeSynced, TimeReportActionType.Delete);
            }

            if (toUpdateNewTobeSynced.Any())
            {
                await _amazonSqsService.SendTimeReportsMessages(toUpdateNewTobeSynced, TimeReportActionType.Create);
            }

            var toInsert = _mapper.Map<IEnumerable<TimeReport>>(backup
                .Where(g => g.Any(trh => trh.EditAction == (short)EditAction.Delete))
                .Select(g => g.First()))
                .ToList();

            _dbContext.Database.OpenConnection();
            try
            {
                _dbContext.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.TimeReport ON");
                // otherwise changedetector add all nested navigation properties as added
                // and query fails
                toInsert.ForEach(x =>
                {
                    _dbContext.Entry(x).State = EntityState.Added;
                });
                _dbContext.SaveChanges();
                _dbContext.Database.ExecuteSqlRaw("SET IDENTITY_INSERT dbo.TimeReport ON");
            }
            finally
            {
                _dbContext.Database.CloseConnection();
            }

            var toInsertToBeSynched = toInsert.Where(tr => HasToBeSyncedWithGoogleCalendar(tr)).ToList();
            if (toInsertToBeSynched.Any())
            {
                await _amazonSqsService.SendTimeReportsMessages(toInsertToBeSynched, TimeReportActionType.Create);
            }

            _dbContext.TimeReportHistory.RemoveRange(backupUngrouped);

            await _dbContext.SaveChangesAsync();
        }

        public async Task<List<TimeReportReminderDTO>> GetTimeReportReminders(string type)
        {
            var whiteListEmployees = new List<string> { "os" };

            var now = DateTime.UtcNow;
            var activeFtEmployees = await _dbContext.Employee.Where(e => e.Active
                    && (!e.TerminationDate.HasValue || e.TerminationDate > now)
                    && e.AccountType == "Employee"
                    && e.FullTimeStartDate.HasValue
                    && !whiteListEmployees.Contains(e.Id))
                .AsNoTracking()
                .ToListAsync();

            var activeEmployeesIds = activeFtEmployees.Select(e => e.Id).ToList();

            var currMonth = now.FirstDateTimeOfMonth();
            var prevMonth = currMonth.AddMonths(-1);
            var nextMonth = currMonth.AddMonths(1);

            var holidays = await _dbContext.Holiday.Where(h =>
                    (h.Year == prevMonth.Year && h.Month == prevMonth.Month)
                    || (h.Year == currMonth.Year && h.Month == currMonth.Month)
                    || (h.Year == nextMonth.Year && h.Month == nextMonth.Year))
                .ToListAsync();

            var periodStart = type switch
            {
                "week" => now.FirstDateOfWeek().Date,
                "month" => now.FirstDateTimeOfMonth().Date,
                _ => now.FirstDateOfWeek().Date
            };

            var periodEnd = type switch
            {
                "week" => periodStart.AddDays(6).Date,
                "month" => periodStart.AddMonths(1).AddDays(-1).Date,
                _ => periodStart.AddDays(6).Date,
            };

            var companyBusinessDays = Enumerable.Range(0, (periodEnd - periodStart).Days + 1)
                .Select(x => periodStart.AddDays(x))
                .Where(x => x.DayOfWeek != DayOfWeek.Saturday && x.DayOfWeek != DayOfWeek.Sunday)
                .Where(d => !holidays.Any(h => h.Year == d.Year && h.Month == d.Month && h.Day == d.Day))
                .ToList();

            (DateTime? last, double expected, double reported) calcEmployeeStat(DAL.Entities.Employee emp, IGrouping<string, TimeReport> timeReports)
            {
                var empNonWorkingDays = timeReports.Where(tr =>
                    tr.Project.ProjectType == (int)ProjectType.Vacation
                    || tr.Project.ProjectType == (int)ProjectType.VacationUnpaid
                    || tr.Project.ProjectType == (int)ProjectType.SickLeaveWithCertificate
                    || tr.Project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate
                ).Select(tr => tr.ReportingDate);

                var daysExpectedToBeReported = companyBusinessDays
                    .Where(d => d >= emp.FullTimeStartDate)
                    .Where(d => !emp.TerminationDate.HasValue || d <= emp.TerminationDate.Value);

                var lastBusinessDay = daysExpectedToBeReported
                    .Except(empNonWorkingDays)
                    .LastOrDefault();

                return (lastBusinessDay, daysExpectedToBeReported.Count() * 8, (double)timeReports.Sum(tr => tr.InternalHours));
            }

            var employeesTimeReports = await _dbContext.TimeReport
                .Where(tr => activeEmployeesIds.Contains(tr.UserId))
                .Where(tr => tr.ReportingDate >= periodStart)
                .Where(tr => tr.ReportingDate <= periodEnd)
                .Include(tr => tr.Project)
                .AsNoTracking()
                .ToListAsync();

            var reminders = employeesTimeReports
                .GroupBy(tr => tr.UserId)
                .Select(g =>
                {
                    var employee = activeFtEmployees.First(e => e.Id == g.Key);
                    var (lastBusinessDay, expectedHours, reportedHours) = calcEmployeeStat(employee, g);
                    return new TimeReportReminderDTO
                    {
                        Id = employee.Id,
                        FullName = employee.FullName,
                        SlackUserId = employee.SlackUserId,
                        ReminderDay = lastBusinessDay,
                        ExpectedHours = expectedHours,
                        ReportedHours = reportedHours
                    };
                });

            var employeesIdWithReports = employeesTimeReports.Select(tr => tr.UserId).Distinct().ToArray();
            var employeeWithNoReportsReminders = activeFtEmployees
                .Where(emp => !employeesIdWithReports.Contains(emp.Id))
                .Select(emp => new TimeReportReminderDTO
                {
                    Id = emp.Id,
                    FullName = emp.FullName,
                    SlackUserId = emp.SlackUserId,
                    ExpectedHours = companyBusinessDays.Count * 8,
                    ReportedHours = 0,
                    ReminderDay = companyBusinessDays.Last()
                });

            return reminders.Union(employeeWithNoReportsReminders).ToList();
        }

        public async Task<decimal> GetTotalInternalTimeReported(string userId, int projectId)
        {
            var totalHours = await _dbContext.TimeReport
                .ByUserIds(new[] { userId })
                .ByProjectIds(new[] { projectId })
                .SumAsync(tr => tr.InternalHours);

            return totalHours;
        }

        public async Task<TimeReportDTO> UpdateTimeReport(TimeReportDTO timeReportDto, decimal newInternalHours)
        {
            var totalHours = await GetTotalInternalTimeReported(timeReportDto.User.Id, timeReportDto.Project.Id);
            var userProject = await _userProjectService.GetUserProject(timeReportDto.User.Id, timeReportDto.Project.Id);
            var editedReport = await GetTimeReport(timeReportDto.Id);

            if (userProject.Hours is not null && totalHours - (decimal)editedReport.InternalHours + newInternalHours >= userProject.Hours)
            {
                throw new InvalidOperationException($"You can't report more hours, you already reported {totalHours}, your limit is {userProject.Hours}");
            }

            return await EditTimeReport(timeReportDto);
        }

        private async Task ValidateTimeReportDto(
            TimeReportDTO timeReportDto,
            bool validateSameUserProjectDate,
            bool validateDateRanges,
            bool validateHours,
            bool validateVacation,
            bool validateSickLeave,
            bool validateSickLeaveWithoutCert,
            bool validateHoliday,
            bool validateExternalHours,
            bool validateUserAssignmentExpiration,
            double? validateBatchReportAndTotalHours = null,
            double? originalInternalHours = null,
            CancellationToken cancellationToken = default)
        {
            if (timeReportDto.WorkDescription.Trim().Length == 0)
            {
                throw new ConflictException("Work description should not be empty");
            }

            var userProject = await _dbContext.UserProject
                .ByUserIds(new[] { timeReportDto.User.Id })
                .ByProjectIds(new[] { timeReportDto.Project.Id })
                .Include(up => up.User)
                .Include(up => up.Project)
                    .ThenInclude(p => p.Account)
                .FirstOrDefaultAsync(cancellationToken);

            var globalSettings = await _dbContext.Settings.AsNoTracking().SingleAsync(cancellationToken);

            // validate project assignment
            if (userProject == null)
            {
                throw new ConflictException("User not assigned to this project");
            }

            var project = userProject.Project;

            if (validateExternalHours)
            {
                if (project.ProjectType != (int)ProjectType.External && timeReportDto.ExternalHours != 0)
                {
                    throw new ConflictException("External hours cannot be reported on non-external project");
                }
            }

            if (validateUserAssignmentExpiration)
            {
                if (userProject.EndDate.HasValue && userProject.EndDate < timeReportDto.ReportingDate)
                {
                    throw new ConflictException($"Action not allowed: Your assignment for this project ended on {userProject.EndDate.Value.ToShortDateString()}");
                }
            }

            if (validateBatchReportAndTotalHours.HasValue)
            {
                if (!IsAllowedForBatchReporting(project))
                {
                    throw new ConflictException("ProjectType is not allowed for batch reporting");
                }
            }

            timeReportDto.User.FirstName = userProject.User.FirstName;
            timeReportDto.User.LastName = userProject.User.LastName;
            timeReportDto.Project.ProjectType = project.ProjectType;
            timeReportDto.Project.ProjectPriority = project.ProjectPriority;

            if (project.IsClosed)
            {
                throw new ConflictException("Project is closed");
            }

            if (validateSameUserProjectDate)
            {
                if (await _dbContext.TimeReport
                    .ByUserIds(new[] { timeReportDto.User.Id })
                    .ByProjectIds(new[] { timeReportDto.Project.Id })
                    .ByDate(timeReportDto.ReportingDate)
                    .AnyAsync(tr => tr.Id != timeReportDto.Id, cancellationToken: cancellationToken))
                {
                    throw new ConflictException("The same combination of project and date has already been reported. Update your existing report instead, if you need to add more hours to it");
                }
            }

            if (validateDateRanges)
            {
                var allowedDateRange = project.GetProjectReportingDates(globalSettings, DateTime.UtcNow);

                if (allowedDateRange != null)
                {
                    var (minDate, maxDate) = allowedDateRange.Value;

                    if (!(timeReportDto.ReportingDate >= minDate && timeReportDto.ReportingDate <= maxDate))
                    {
                        var reportingRequests = await _dbContext.ProjectReportingReopenRequest.Where(r =>
                            r.ProjectId == timeReportDto.Project.Id
                            && r.SenderId == timeReportDto.User.Id
                            && !r.IsDeleted
                            && r.ProjectManagerApprovedId != null)
                            .ToListAsync(cancellationToken);

                        var isWithinAllowedRange = reportingRequests.Any(r =>
                            timeReportDto.ReportingDate >= r.StartDate &&
                            timeReportDto.ReportingDate <= r.EndDate);

                        if (!isWithinAllowedRange)
                        {
                            throw new ConflictException("Reporting for this date is closed");
                        }

                    }
                }
            }

            if (validateHours)
            {
                var hoursSettings = new
                {
                    MinInternal = project.OverrideHoursSettings ? project.MinInternal :
                                  project.Account.OverrideHoursSettings ? project.Account.MinInternal :
                                  globalSettings.MinInternal,

                    MaxInternal = project.OverrideHoursSettings ? project.MaxInternal :
                                  project.Account.OverrideHoursSettings ? project.Account.MaxInternal :
                                  globalSettings.MaxInternal,

                    MinExternal = project.OverrideHoursSettings ? project.MinExternal :
                                  project.Account.OverrideHoursSettings ? project.Account.MinExternal :
                                  globalSettings.MinExternal,

                    MaxExternal = project.OverrideHoursSettings ? project.MaxExternal :
                                  project.Account.OverrideHoursSettings ? project.Account.MaxExternal :
                                  globalSettings.MaxExternal
                };

                if (!(timeReportDto.InternalHours >= (double)hoursSettings.MinInternal &&
                      timeReportDto.InternalHours <= (double)hoursSettings.MaxInternal &&
                      timeReportDto.ExternalHours >= (double)hoursSettings.MinExternal &&
                      timeReportDto.ExternalHours <= (double)hoursSettings.MaxExternal))
                {
                    throw new ConflictException("Hours not allowed");
                }

                if (project.MaxTotal.HasValue)
                {
                    var reported = await _dbContext.TimeReport
                        .ByUserIds(new[] { timeReportDto.User.Id })
                        .ByProjectIds(new[] { timeReportDto.Project.Id })
                        .Where(tr => tr.Id != timeReportDto.Id)
                        .GroupBy(tr => true)
                        .Select(g => new
                        {
                            InternalSum = g.Sum(tr => tr.InternalHours),
                            ExternalSum = g.Sum(tr => tr.ExternalHours)
                        })
                        .FirstOrDefaultAsync(cancellationToken);

                    if (reported != null
                        && (reported.InternalSum + (decimal)timeReportDto.InternalHours > project.MaxTotal.Value
                        || reported.ExternalSum + (decimal)timeReportDto.ExternalHours > project.MaxTotal.Value))
                    {
                        throw new ConflictException($"This project is not allowed to report more then {project.MaxTotal} hours in total");
                    }
                }

                var hoursOnReportingDay = await _dbContext
                    .TimeReport
                    .Include(x => x.Project)
                    .ByUserIds(new[] { timeReportDto.User.Id })
                    .ByDate(timeReportDto.ReportingDate)
                    .Where(tr => tr.Id != timeReportDto.Id)
                    .AsNoTracking()
                    .ToArrayAsync(cancellationToken);

                var allExistingOperationalHours = hoursOnReportingDay
                    .Where(x => x.Project.ProjectType is (int)ProjectType.Vacation or
                        (int)ProjectType.SickLeaveWithoutCertificate or
                        (int)ProjectType.SickLeaveWithCertificate or
                        (int)ProjectType.VacationUnpaid)
                    .Sum(tr => tr.InternalHours);

                var allExistingHours = hoursOnReportingDay.Sum(tr => tr.InternalHours);

                var allHoursOnThisDay = allExistingHours + (decimal)timeReportDto.InternalHours;
                var allOperationalHoursOnThisDay = allExistingOperationalHours +
                                                   (decimal)(timeReportDto.IsOperational(ProjectType.Vacation,
                                                                                         ProjectType.SickLeaveWithoutCertificate,
                                                                                         ProjectType.SickLeaveWithCertificate,
                                                                                         ProjectType.VacationUnpaid) ?
                                                             timeReportDto.InternalHours : 0);
                if (allOperationalHoursOnThisDay > 0 && allHoursOnThisDay > 8)
                {
                    throw new ConflictException("You can not report more than 8 hours daily while on a day off or sick leave. Adjust reporting hours for this or existing reports.");
                }
            }

            if (validateVacation)
            {
                if (project.ProjectType == (int)ProjectType.Vacation)
                {
                    var employee = await _dbContext.Employee.ById(timeReportDto.User.Id)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(cancellationToken);

                    var trialPeriodEndDate = employee.GetTrialEndDate();
                    if (trialPeriodEndDate > DateTime.Now.Date)
                    {
                        throw new ConflictException($"You are not able to report day offs until your trial period ends at {trialPeriodEndDate.Value.ToShortDateString()}. If you have any questions please contact your HR.");
                    }

                    if (employee.TerminationDate.HasValue)
                    {
                        throw new ConflictException("You cannot report day offs because you're in the process of leaving the company. If you are confident this day off should be reported, please get in touch with your HR.");
                    }

                    var vacationInformation = await _vacationService.GetVacationInformation(timeReportDto.User.Id, timeReportDto.ReportingDate.Year);

                    // accrued until previous month
                    var toTake = timeReportDto.ReportingDate.Year < DateTime.UtcNow.Year ? 12 : DateTime.UtcNow.Month - 1;
                    var totalAccrued = vacationInformation.YearData.Take(toTake).Sum(m => m?.Accrual ?? 0);
                    var totalUsed = vacationInformation.YearData.Sum(m => m?.Used ?? 0);

                    var allowed = vacationInformation.TransferredBalance + totalAccrued - totalUsed - vacationInformation.MinimumAllowedBalance;

                    var internalHours = validateBatchReportAndTotalHours ?? timeReportDto.InternalHours;
                    ValidateHours(internalHours, allowed, originalInternalHours, "You are not allowed to report day off. Check your vacation balance.");
                }
            }

            if (validateSickLeave)
            {
                if (project.ProjectType is (int)ProjectType.SickLeaveWithoutCertificate or (int)ProjectType.SickLeaveWithCertificate)
                {
                    var sickLeaveInformation = await _sickLeaveService.GetSickLeaveInformation(timeReportDto.User.Id, timeReportDto.ReportingDate.Year);

                    var allowed = project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate ?
                        sickLeaveInformation.ProvidedSickLeaveDaysWithoutCertificate - sickLeaveInformation.YearData.Sum(m => m?.WithoutCertificate ?? 0) :
                        sickLeaveInformation.ProvidedSickLeaveDaysWithCertificate - sickLeaveInformation.YearData.Sum(m => m?.WithCertificate ?? 0);

                    var internalHours = validateBatchReportAndTotalHours ?? timeReportDto.InternalHours;
                    ValidateHours(internalHours, allowed, originalInternalHours, "You are not allowed to report sick leave. Check your balance.");

                    // Validate 3 sick w/o cert in a row 
                    if (project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate)
                    {
                        var maxAllowedSequence = 2;
                        var windowStart = timeReportDto.ReportingDate.Date.AddMonths(-1);
                        var windowEnd = timeReportDto.ReportingDate.Date.AddMonths(1);

                        var holidays = await _dbContext.Holiday.Where(h =>
                                h.Year >= windowStart.Year && h.Year <= windowEnd.Year
                                && h.Month >= windowStart.Month && h.Month <= windowEnd.Month)
                            .AsNoTracking()
                            .ToListAsync(cancellationToken);

                        var daysToCheck = new List<DateTime>();
                        for (int i = -maxAllowedSequence; i <= maxAllowedSequence; i++)
                        {
                            if (i != 0) daysToCheck.Add(timeReportDto.ReportingDate.AddBusinessDays(i, holidays));
                        }
                        var (min, max) = (daysToCheck.Min(), daysToCheck.Max());

                        var sicknessDates = await _dbContext.TimeReport.Where(tr =>
                                tr.Id != timeReportDto.Id
                                && tr.ReportingDate >= min
                                && tr.ReportingDate <= max
                                && tr.UserId == timeReportDto.User.Id
                                && tr.Project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate)
                            .Select(tr => tr.ReportingDate)
                            .ToArrayAsync(cancellationToken);

                        for (int i = 0; i < daysToCheck.Count - maxAllowedSequence + 1; i++)
                        {
                            var moreThanAllowed = daysToCheck.GetRange(i, maxAllowedSequence).All(d => sicknessDates.Contains(d));
                            if (moreThanAllowed)
                            {
                                throw new ConflictException($"You cannot report {maxAllowedSequence + 1} consequtive reports of that type. If you have any questions please contact your HR.");
                            }
                        }
                    }
                }
            }

            if (validateSickLeaveWithoutCert)
            {
                if (project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate)
                {
                    var employee = await _dbContext.Employee.ById(timeReportDto.User.Id)
                        .AsNoTracking()
                        .FirstOrDefaultAsync(cancellationToken);

                    var trialPeriodEndDate = employee.GetTrialEndDate();

                    if (trialPeriodEndDate > DateTime.Now.Date)
                    {
                        throw new ConflictException($"You are not able to report sick-leave without certificate until your trial period ends at {trialPeriodEndDate.Value.ToShortDateString()}. If you have any questions please contact your HR.");
                    }

                    if (employee.TerminationDate.HasValue && employee.TerminationDate.Value > DateTime.UtcNow)
                    {
                        throw new ConflictException($"You cannot report report sick leaves without certificates because you're in the process of leaving the company.");
                    }
                }
            }

            if (validateHoliday)
            {
                if (project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate
                   || project.ProjectType == (int)ProjectType.SickLeaveWithCertificate
                   || project.ProjectType == (int)ProjectType.Vacation
                   || project.ProjectType == (int)ProjectType.VacationUnpaid
                   || project.ProjectType == (int)ProjectType.Reserve
                   || project.ProjectType == (int)ProjectType.Onboarding)
                {
                    // TODO Fix timezones issue
                    if (timeReportDto.ReportingDate.DayOfWeek == DayOfWeek.Sunday
                       || timeReportDto.ReportingDate.DayOfWeek == DayOfWeek.Saturday
                       || _dbContext.Holiday.Any(h => h.Year == timeReportDto.ReportingDate.Year
                                                    && h.Month == timeReportDto.ReportingDate.Month
                                                    && h.Day == timeReportDto.ReportingDate.Day))
                    {
                        throw new ConflictException("Vacation, sickness or reserve can not be reported on official holiday");
                    }
                }
            }
        }

        private static void ValidateHours(double internalHours, double allowed, double? originalInternalHours, string message)
        {
            if (internalHours > allowed * 8 + (originalInternalHours ?? 0))
            {
                throw new ConflictException(message);
            }
        }

        private static bool HasToBeSyncedWithGoogleCalendar(TimeReportDTO timeReportDto)
        {
            return ProjectService.IsOperationalProjectType(timeReportDto.Project.ProjectType);
        }

        private static bool HasToBeSyncedWithGoogleCalendar(TimeReport timeReport)
        {
            return ProjectService.IsOperationalProjectType(timeReport.Project.ProjectType);
        }

        private static bool HasToBeSyncedWithGoogleCalendar(TimeReportHistory timeReportHistory)
        {
            return ProjectService.IsOperationalProjectType(timeReportHistory.Project.ProjectType);
        }

        private async Task<List<TimeReportDTO>> ExcludeOfficialHolidaysReports(List<TimeReportDTO> timeReportDTOs)
        {
            var minReportingDate = timeReportDTOs.Min(x => x.ReportingDate);
            var maxReportingDate = timeReportDTOs.Max(x => x.ReportingDate);

            var holidays = await _operationalService.GetHolidaysInRange(minReportingDate, maxReportingDate);

            var filteredReports = timeReportDTOs
                .Where(report => !holidays.Any(holiday => holiday.Date == report.ReportingDate)
                              && !report.ReportingDate.IsWeekend())
                .ToList();

            return filteredReports;
        }

        private static bool IsAllowedForBatchReporting(Project project)
        {
            return ProjectService.IsOperationalProjectType(project.ProjectType)
                && project.ProjectType != (int)ProjectType.SickLeaveWithoutCertificate;
        }
    }
}
