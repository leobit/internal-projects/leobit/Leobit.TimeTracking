﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class CategoryService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;

        public CategoryService(IMapper mapper, LeobitContext dbContext)
        {
            _mapper = mapper;
            _dbContext = dbContext;
        }

        public Task<List<CategoryDTO>> GetCategories(int? id = null)
        {
            return _dbContext.Category
                .ById(id)
                .Include(c => c.OrganizationUnit)
                    .ThenInclude(ou => ou.Division)
                .ProjectTo<CategoryDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<CategoryShortDTO>> GetCategoriesShort(int? id, int? organizationUnitId)
        {
            return _dbContext.Category
                .ById(id)
                .ByOrganizationUnitId(organizationUnitId)
                .ProjectTo<CategoryShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<CategoryDTO> CreateCategory(CategoryInputDTO categoryDTO)
        {
            var organizationUnit = await _dbContext.OrganizationUnit
                .ById(categoryDTO.OrganizationUnit.Id)
                .Include(ou => ou.Division)
                .AsNoTracking()
                .FirstOrDefaultAsync();

            if (organizationUnit != null)
            {
                var category = _mapper.Map<Category>(categoryDTO);
                category.OrganizationUnit = null;

                _dbContext.Category.Add(category);
                await _dbContext.SaveChangesAsync();

                category.OrganizationUnit = organizationUnit;
                return _mapper.Map<CategoryDTO>(category);
            }
            else
            {
                throw new NotFoundException($"OrganizationUnit with id = '{categoryDTO.OrganizationUnit.Id}' does not exist");
            }
        }

        public async Task<CategoryDTO> EditCategory(CategoryInputDTO categoryDTO)
        {
            if (await _dbContext.Category.AnyAsync(c => c.Id == categoryDTO.Id))
            {
                var organizationUnit = await _dbContext.OrganizationUnit
                    .ById(categoryDTO.OrganizationUnit.Id)
                    .Include(ou => ou.Division)
                    .AsNoTracking()
                    .FirstOrDefaultAsync();

                if (organizationUnit != null)
                {
                    var category = _mapper.Map<Category>(categoryDTO);

                    _dbContext.Category.Update(category);
                    await _dbContext.SaveChangesAsync();
                    category.OrganizationUnit = organizationUnit;
                    return _mapper.Map<CategoryDTO>(category);
                }
            }
            throw new NotFoundException();
        }

        public async Task DeleteCategory(int id)
        {
            var category = await _dbContext.Category.ById(id).FirstOrDefaultAsync();
            if (category != null)
            {
                _dbContext.Category.Remove(category);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public Task<List<ProjectShortDTO>> GetProjectsByCategoryId(int categoryId)
        {
            return _dbContext.Project
                .Where(p => p.CategoryId == categoryId)
                .ProjectTo<ProjectShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<ProjectShortDTO> EditProjectCategory(int projectId, int categoryId)
        {
            var project = await _dbContext.Project.ById(projectId).FirstOrDefaultAsync();
            var category = await _dbContext.Category.ById(categoryId).FirstOrDefaultAsync();
            if(project != null && category != null)
            {
                project.CategoryId = categoryId;
                project.Category = category;
                await _dbContext.SaveChangesAsync();

                return _mapper.Map<ProjectShortDTO>(project);
            }
            else
            {
                throw new NotFoundException();
            }
        }
    }
}
