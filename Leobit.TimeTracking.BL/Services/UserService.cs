﻿using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    // not sure about this service
    // maybe all methods should be in TimeReportService
    public class UserService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;
        private readonly UserProjectService _userProjectService;
        private readonly IHttpContextAccessor _httpContextAccessor;

        public UserService(IMapper mapper, LeobitContext dbContext, UserProjectService userProjectService, IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _userProjectService = userProjectService;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<UserDefaultsDTO> GetUserDefaults(string userId)
        {
            var lastNonVacationReport = await _dbContext.TimeReport
                .AsNoTracking()
                .ByUserIds(new[] { userId })
                .OrderByDateDescending()
                .Include(tr => tr.Project)
                .Where(tr => tr.Project.ProjectType != (int)ProjectType.Vacation
                             && tr.Project.ProjectType != (int)ProjectType.VacationUnpaid)
                .FirstOrDefaultAsync();

            if (lastNonVacationReport != null)
            {
                var reportingDate = lastNonVacationReport.ReportingDate.AddDays(1);
                while (reportingDate.DayOfWeek == DayOfWeek.Saturday ||
                       reportingDate.DayOfWeek == DayOfWeek.Sunday)
                {
                    reportingDate = reportingDate.AddDays(1);
                }

                return new UserDefaultsDTO
                {
                    AccountId = lastNonVacationReport.Project.AccountId,
                    ProjectId = lastNonVacationReport.ProjectId,
                    ReportingDate = reportingDate,
                    InternalHours = (double)lastNonVacationReport.InternalHours,
                    ExternalHours = GetExternalHours(lastNonVacationReport),
                    Defaults = await GetDefaultsForAllProjects(userId),
                };
            }

            return new UserDefaultsDTO
            {
                ReportingDate = DateTime.Now.Date,
                InternalHours = 0,
                ExternalHours = 0
            };
        }

        public Task<List<UserShortDTO>> GetActiveUsers(int? roleId = null)
        {
            return _dbContext.User
                .Join(_dbContext.Employee,
                    user => user.Id,
                    emp => emp.Id,
                    (user, emp) => new { User = user, Employee = emp })
                .Where(ue => ue.Employee.Active)
                .Select(ue => ue.User)
                .ByRoleId(roleId)
                .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<UserShortDTO>> GetInactiveUsers(int? roleId, string userId)
        {
            return _dbContext.User
            .Join(_dbContext.Employee,
                user => user.Id,
                emp => emp.Id,
                (user, emp) => new { User = user, Employee = emp })
            .Where(ue => !ue.Employee.Active)
            .Select(ue => ue.User)
            .ByRoleId(roleId)
            .ProjectTo<UserShortDTO>(_mapper.ConfigurationProvider)
            .ToListAsync();
        }

        public Task<bool> HasRoleAssigned(int roleId)
        {
            return _dbContext.User.ByRoleId(roleId).AnyAsync();
        }

        public async Task<UserShortDTO> EditUser(UserShortDTO userDTO)
        {
            if (await _dbContext.User.ById(userDTO.Id).AnyAsync() && await _dbContext.Role.ById(userDTO.RoleId).AnyAsync())
            {
                var user = _mapper.Map<User>(userDTO);

                _dbContext.User.Update(user);
                await _dbContext.SaveChangesAsync();

                return _mapper.Map<UserShortDTO>(user);
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task UpdateTerminatedUser(string userId)
        {
            var user = await _dbContext.User.ById(userId).FirstOrDefaultAsync();
            if (user != null)
            {
                user.RoleId = GetDefaultRoleId();
                _dbContext.User.Update(user);
                await _userProjectService.DeleteUserNonDefaultProjects(userId);

                await _dbContext.SaveChangesAsync();
            }
        }

        private async Task<List<UserDefaultDTO>> GetDefaultsForAllProjects(string userId)
        {
            var recentTimeReportsStartDate = DateTime.UtcNow.AddYears(-1);

            var userDefaults = await _dbContext.TimeReport
                .Where(r => r.ReportingDate >= recentTimeReportsStartDate)
                .Where(r => r.UserId == userId)
                .OrderByDescending(r => r.ReportingDate)
                .Select(report => new UserDefaultDTO
                {
                    AccountId = report.Project.AccountId,
                    ProjectId = report.ProjectId,
                    ReportingDate = report.ReportingDate,
                    InternalHours = (double)report.InternalHours,
                    ExternalHours = GetExternalHours(report)
                })
                .ToListAsync();

            return userDefaults
                .GroupBy(report => report.ProjectId)
                .Select(g => g.FirstOrDefault())
                .ToList();
        }

        public Task<List<ProjectManagerDTO>> GetProjectManagers(ICollection<string> userIds, ICollection<int> projectIds)
        {
            return _dbContext.ProjectManager
                .ByUserIds(userIds)
                .ByProjectIds(projectIds)
                .ProjectTo<ProjectManagerDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        // By default every user has "User" role with Id = 1
        private static int GetDefaultRoleId() => 1;

        private static double GetExternalHours(TimeReport report) => report.Project?.ProjectType == (int)ProjectType.External ? (double)report.ExternalHours : 0;
    }
}
