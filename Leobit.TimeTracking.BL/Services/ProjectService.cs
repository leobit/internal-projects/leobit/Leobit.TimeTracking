using AutoMapper;
using AutoMapper.QueryableExtensions;
using Leobit.TimeTracking.BL.Extensions;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Background;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.AspNetCore.Http;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class ProjectService
    {
        private readonly IMapper _mapper;
        private readonly LeobitContext _dbContext;
        private readonly ProjectReportingReopenRequestService _projectReportingReopenRequestService;
        private readonly IHttpContextAccessor _httpContextAccessor;
        private readonly TaskQueue _taskQueue;

        public ProjectService(
            IMapper mapper,
            LeobitContext dbContext,
            ProjectReportingReopenRequestService projectReportingReopenRequestService,
            TaskQueue taskQueue,
            IHttpContextAccessor httpContextAccessor)
        {
            _mapper = mapper;
            _dbContext = dbContext;
            _projectReportingReopenRequestService = projectReportingReopenRequestService;
            _taskQueue = taskQueue;
            _httpContextAccessor = httpContextAccessor;
        }

        public async Task<List<ProjectDTO>> GetProjects(int? id, ICollection<int> accountIds)
        {
            var userId = _httpContextAccessor.GetUserId();
            var isAdmin = _httpContextAccessor.IsAdmin();

            if (userId is null)
            {
                throw new ArgumentException("User ID cannot be null.");
            }

            var user = await _dbContext.Employee.ById(userId).FirstOrDefaultAsync();
            if (user is null)
            {
                throw new InvalidOperationException("User not found.");
            }

            var isBudgetOwner = await _dbContext.Employee
                .Where(e => e.Active && e.Level >= (int)EmployeeLevel.Director)
                .AnyAsync(e => e.Id == userId);

            IQueryable<Project> query;
            if (user.Level >= (int)EmployeeLevel.VP || isAdmin)
            {
                query = _dbContext.Project
                    .ById(id)
                    .ByAccountIds(accountIds);
            }
            else
            {
                var managedProjectIds = await _dbContext.ProjectManager
                    .Where(pm => pm.UserId == userId)
                    .Select(pm => pm.ProjectId)
                    .ToListAsync();

                var budgetOwnerAccountIds = await _dbContext.Account
                    .Where(a => a.BudgetOwnerId == userId)
                    .Select(a => a.Id)
                    .ToListAsync();

                query = _dbContext.Project
                    .Where(p => (managedProjectIds.Contains(p.Id) || budgetOwnerAccountIds.Contains(p.AccountId)))
                    .ByAccountIds(accountIds);
            }

            var projects = await query
                .Include(p => p.ProjectSettingsBackup)
                .Include(p => p.ProjectReportingReopenRequests)
                .ThenInclude(r => r.Sender)
                .OrderByDescending(p => p.Id)
                .ProjectTo<ProjectDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();

            projects.ForEach(project =>
            {
                project.ProjectReopenRequests = project.ProjectReopenRequests
                    .Where(r => !r.IsDeleted && r.ExpirationTime > DateTime.UtcNow)
                    .ToList();

                foreach (var request in project.ProjectReopenRequests)
                {
                    request.CanApprove = !(request.SenderId == userId || (request.StartDate <= DateTime.UtcNow.AddDays(-1 * (int)DateTime.UtcNow.DayOfWeek).Date.AddDays(-7) && !isBudgetOwner));
                }
            });

            return projects;
        }

        public async Task<ProjectDTO> CreateProject(ProjectDTO projectDTO, string userId)
        {
            var account = await _dbContext.Account.ById(projectDTO.Account.Id).FirstOrDefaultAsync();
            var category = await _dbContext.Category.ById(projectDTO.Category.Id).FirstOrDefaultAsync();
            var user = await _dbContext.User.ById(userId).FirstOrDefaultAsync();

            if (account != null && category != null && user != null)
            {
                var project = _mapper.Map<Project>(projectDTO);

                _dbContext.Project.Add(project);
                _dbContext.ProjectManager.Add(new ProjectManager
                {
                    User = user,
                    Project = project
                });

                await _dbContext.SaveChangesAsync();

                return _mapper.Map<ProjectDTO>(project);
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public async Task<ProjectDTO> EditProject(ProjectDTO projectDTO)
        {
            var existingProject = await _dbContext.Project
                .Include(p => p.ProjectSettingsBackup)
                .Include(p => p.ProjectReportingReopenRequests)
                .ThenInclude(r => r.Sender)
                .Include(p => p.ProjectReportingReopenRequests)
                .ThenInclude(r => r.ProjectManagerApproved)
                .FirstOrDefaultAsync(p => p.Id == projectDTO.Id);

            if (existingProject == null)
            {
                throw new NotFoundException("Project not found.");
            }

            _mapper.Map(projectDTO, existingProject);

            await _projectReportingReopenRequestService.OverrideProjectSettingsBackup(projectDTO, existingProject);

            var account = await _dbContext.Account.ById(projectDTO.Account.Id).FirstOrDefaultAsync();
            var category = await _dbContext.Category.ById(projectDTO.Category.Id).FirstOrDefaultAsync();

            if (account != null && category != null)
            {
                if (projectDTO.OverrideTimeReportPriorities)
                {
                    _taskQueue.Enqueue(async (serviceProvider, cancellationToken) =>
                    {
                        var dbContext = serviceProvider.GetRequiredService<LeobitContext>();

                        var timeReports = await dbContext.TimeReport.Where(tr => tr.ProjectId == projectDTO.Id).ToListAsync(cancellationToken);
                        timeReports.ForEach(tr => tr.ProjectPrioritySnapshot = projectDTO.ProjectPriority);

                        dbContext.TimeReport.UpdateRange(timeReports);
                        await dbContext.SaveChangesAsync(cancellationToken);
                    });
                }

                _dbContext.Project.Update(existingProject);
                await _dbContext.SaveChangesAsync();

                return _mapper.Map<ProjectDTO>(existingProject);
            }
            throw new NotFoundException();
        }

        public async Task DeleteProject(int id)
        {
            var project = await _dbContext.Project.ById(id).FirstOrDefaultAsync();
            if (project != null)
            {
                _dbContext.Project.Remove(project);
                await _dbContext.SaveChangesAsync();
            }
            else
            {
                throw new NotFoundException();
            }
        }

        public Task<List<ProjectShortDTO>> GetProjectShorts()
        {
            return _dbContext.Project
                .NotClosed()
                .ProjectTo<ProjectShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public Task<List<ProjectShortDTO>> GetAssignedProjectShorts(string userId)
        {
            return _dbContext.Project
                .ByUserId(userId)
                .NotClosed()
                .ProjectTo<ProjectShortDTO>(_mapper.ConfigurationProvider)
                .ToListAsync();
        }

        public async Task<IEnumerable<ProjectShortDTO>> GetFilterProjectShortsByPeriod(string userId, DateTime? dateFrom, DateTime? dateTo)
        {
            var reportedProjectIds = await _dbContext.TimeReport
                .Where(t => t.UserId == userId)
                .Where(x => x.ReportingDate >= dateFrom && x.ReportingDate <= dateTo)
                .Select(x => x.ProjectId)
                .Distinct()
                .ToListAsync();

            var assignedProjectIds = await GetAssignedUserProjectIds(userId);

            var projectIds = reportedProjectIds.Concat(assignedProjectIds).Distinct().ToArray();

            var projects = await _dbContext.Project
                .AsNoTracking()
                .ByIds(projectIds)
                .Include(p => p.Account)
                .Include(p => p.UserProject)
                .ToListAsync();

            var reportingRequests = await _dbContext.ProjectReportingReopenRequest.Where(r =>
                r.SenderId == userId
                && !r.IsDeleted
                && r.ProjectManagerApprovedId != null)
                .ToListAsync();

            return await ProjectShortsWithRestriction(projects, reportingRequests, userId);
        }

        public async Task<IEnumerable<int>> GetAssignedUserProjectIds(string userId)
        {
            return await _dbContext.Project.ByUserId(userId)
                .NotClosed()
                .Select(x => x.Id)
                .Distinct()
                .ToListAsync();
        }

        private async Task<List<ProjectShortDTO>> ProjectShortsWithRestriction(List<Project> projects,
            List<ProjectReportingReopenRequest> reportingRequests, string userId, bool includeAssignmentRestrictions = true)
        {
            var globalSettings = await _dbContext.Settings.AsNoTracking().SingleAsync();

            var res = projects.Select(project =>
            {
                var result = new ProjectShortDTO();
                var allowedDateRangeResults = project.GetProjectReportingDates(globalSettings, DateTime.UtcNow, userId);

                result.Id = project.Id;
                result.Name = project.Name;
                result.Account = _mapper.Map<AccountShortDTO>(project.Account);
                result.IsClosed = project.IsClosed;
                result.Restriction = new RestrictionDTO(project, globalSettings, null);
                result.ProjectType = project.ProjectType;

                if (allowedDateRangeResults is not null)
                {
                    var (firstAllowedDate, lastAllowedDate) = allowedDateRangeResults.Value;
                    var allowedDateRanges = new List<AllowedDateRangeDTO>
                    {
                        new(firstAllowedDate, lastAllowedDate)
                    };

                    var reportingRequestsForProject = reportingRequests
                    .Where(request => request is not null && request.ProjectId == project.Id && request.ExpirationTime > DateTime.UtcNow)
                    .Select(reportingRequest => new AllowedDateRangeDTO(reportingRequest.StartDate, reportingRequest.EndDate));

                    allowedDateRanges.AddRange(reportingRequestsForProject);

                    result.Restriction.SetAllowedDateRanges(allowedDateRanges);

                    return result;
                }

                if (!includeAssignmentRestrictions)
                    return null;

                var userProject = project.UserProject.FirstOrDefault(up => up.UserId == userId);

                if (userProject is not { StartDate: not null, EndDate: not null })
                    return null;

                result.Restriction.SetAllowedDateRanges(userProject.StartDate.Value, userProject.EndDate.Value);

                return result;
            }).Where(p => p != null).ToList();

            return res;
        }

        public async Task<List<ProjectShortDTO>> GetAssignedProjectShortsWithRestriction(string userId)
        {
            var projects = await _dbContext.Project
                .AsNoTracking()
                .ByUserId(userId)
                .NotClosed()
                .Include(p => p.Account)
                .Include(p => p.UserProject)
                .ToListAsync();

            var reportingRequests = await _dbContext.ProjectReportingReopenRequest.Where(r =>
                r.SenderId == userId
                && !r.IsDeleted
                && r.ProjectManagerApprovedId != null)
                .ToListAsync();

            return await ProjectShortsWithRestriction(projects, reportingRequests, userId, includeAssignmentRestrictions: false);
        }

        public async Task CleanUpProjects(DateTime expirationTime)
        {
            var twoMonthsAgo = expirationTime.AddMonths(-2);

            var projectsToClose = await _dbContext.Project.Include(p => p.TimeReport)
                .Where(p => !p.IsClosed && p.TimeReport.Any() && p.TimeReport.All(tr => tr.ReportingDate <= twoMonthsAgo))
                .ToListAsync();

            if (projectsToClose.Any())
            {
                foreach (var project in projectsToClose)
                {
                    project.IsClosed = true;
                }

                await _dbContext.SaveChangesAsync();
            }
        }

        public static bool IsOperationalProjectType(int projectType)
        {
            if (projectType == (int)ProjectType.SickLeaveWithoutCertificate
                    || projectType == (int)ProjectType.SickLeaveWithCertificate
                    || projectType == (int)ProjectType.Vacation
                    || projectType == (int)ProjectType.VacationUnpaid)
            {
                return true;
            }

            return false;
        }

        public static (int beforeClosing, int afterClosing) CalculateReopenDateRange(Project currentProject,
            Settings globalSettings, DateTime startDate, DateTime endDate, DateTime currentDate)
        {
            if (currentProject.Account is null)
            {
                throw new ArgumentException($"{nameof(Project.Account)} is null");
            }

            var currentBeforeClosing = currentProject.GetBeforeClosingSetting(globalSettings);
            var currentAfterClosing = currentProject.GetAfterClosingSetting(globalSettings);
            var currentClosingType = currentProject.GetClosingTypeSetting(globalSettings);

            var (firstAllowedDate, lastAllowedDate) = TimeReportRangeCalculationHelper.CalculateAllowedDateRange(
                currentClosingType, currentBeforeClosing, currentAfterClosing, currentDate);

            var diffBeforClosing = (int)(firstAllowedDate - startDate).TotalDays;
            var diffAfterClosing = (int)(endDate - lastAllowedDate).TotalDays;
            var beforeClosing = diffBeforClosing > 0 ? diffBeforClosing + currentBeforeClosing : currentBeforeClosing;
            var afterClosing = diffAfterClosing > 0 ? diffAfterClosing + currentAfterClosing : currentAfterClosing;
            return (beforeClosing, afterClosing);
        }
    }
}
