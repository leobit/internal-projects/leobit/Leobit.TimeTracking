﻿using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class FiltersPresetService
    {
        private const int UserFiltersPresetMaxCount = 20;

        private readonly LeobitContext _dbContext;

        public FiltersPresetService(LeobitContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<FiltersPresetDTO> GetFiltersPresetAsync(string userId, int filtersPresetId)
        {
            var filtersPreset = await GetFiltersPresetAsync(filtersPresetId);
            if (filtersPreset == null)
            {
                throw new NotFoundException();
            }

            return new FiltersPresetDTO
            {
                Id = filtersPreset.Id,
                UserId = userId,
                Name = filtersPreset.Name,
                EmployeeReportingFilters = JsonConvert.DeserializeObject<EmployeeReportingFiltersDTO>(filtersPreset.EmployeeReportingFilters)
            };
        }

        public async Task<List<FiltersPresetDTO>> GetFiltersPresetsAsync(string userId)
        {
            return await _dbContext.FiltersPreset
                .Where(fp => fp.UserId == userId)
                .Select(fp => new FiltersPresetDTO
                {
                    Id = fp.Id,
                    Name = fp.Name,
                    UserId = fp.UserId,
                    EmployeeReportingFilters = JsonConvert.DeserializeObject<EmployeeReportingFiltersDTO>(fp.EmployeeReportingFilters)
                })
                .ToListAsync();
        }

        public async Task<FiltersPresetDTO> CreateFiltersPresetAsync(FiltersPresetDTO filtersPresetDTO)
        {
            if (await CheckUserFiltersPresetsLimitAsync(filtersPresetDTO.UserId))
            {
                throw new ConflictException("Maximum filter presets limit reached");
            }

            var filtersPreset = new FiltersPreset()
            {
                UserId = filtersPresetDTO.UserId,
                Name = filtersPresetDTO.Name,
                EmployeeReportingFilters = JsonConvert.SerializeObject(filtersPresetDTO.EmployeeReportingFilters)
            };
            _dbContext.FiltersPreset.Add(filtersPreset);
            await _dbContext.SaveChangesAsync();

            return new FiltersPresetDTO
            {
                Id = filtersPreset.Id,
                Name = filtersPreset.Name,
                UserId = filtersPreset.UserId,
                EmployeeReportingFilters = filtersPresetDTO.EmployeeReportingFilters
            };
        }

        public async Task EditEmployeeReportingFiltersAsync(int filtersPresetId, EmployeeReportingFiltersDTO employeeReportingFiltersDTO)
        {
            var filtersPreset = await GetFiltersPresetAsync(filtersPresetId);
            if (filtersPreset == null)
            {
                throw new NotFoundException();
            }

            filtersPreset.EmployeeReportingFilters = JsonConvert.SerializeObject(employeeReportingFiltersDTO);
            _dbContext.FiltersPreset.Update(filtersPreset);
            await _dbContext.SaveChangesAsync();
        }

        public async Task EditFiltersPresetNameAsync(int filtersPresetId, string userId, string filtersPresetName)
        {
            if (string.IsNullOrWhiteSpace(filtersPresetName))
            {
                throw new ConflictException("Filter preset name cannot be null or empty");
            }

            var filtersPreset = await GetFiltersPresetAsync(filtersPresetId);
            if (filtersPreset == null)
            {
                throw new NotFoundException();
            }

            var userFiltersPresetNames = await _dbContext.FiltersPreset
                .Where(fp => fp.UserId == userId)
                .Select(fp => fp.Name)
                .ToListAsync();

            if (userFiltersPresetNames.Contains(filtersPresetName))
            {
                throw new ConflictException($"Filter preset with name {filtersPresetName} already exists");
            }

            filtersPreset.Name = filtersPresetName;

            _dbContext.FiltersPreset.Update(filtersPreset);
            await _dbContext.SaveChangesAsync();
        }

        public async Task DeleteFiltersPresetAsync(int filtersPresetId)
        {
            var filtersPreset = await GetFiltersPresetAsync(filtersPresetId);
            if (filtersPreset == null)
            {
                throw new NotFoundException();
            }

            _dbContext.FiltersPreset.Remove(filtersPreset);
            await _dbContext.SaveChangesAsync();
        }

        private async Task<bool> CheckUserFiltersPresetsLimitAsync(string userId)
        {
            return await _dbContext.FiltersPreset
                .Where(fp => fp.UserId == userId)
                .CountAsync() >= UserFiltersPresetMaxCount;
        }

        private Task<FiltersPreset> GetFiltersPresetAsync(int filtersPresetId)
        {
            return _dbContext.FiltersPreset.FirstOrDefaultAsync(fp => fp.Id == filtersPresetId);
        }
    }
}
