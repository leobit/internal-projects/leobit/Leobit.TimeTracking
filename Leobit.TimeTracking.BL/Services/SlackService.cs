﻿using Leobit.TimeTracking.BL.Infrastructure.Slack;
using Leobit.TimeTracking.BL.Models;
using Microsoft.Extensions.Options;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text.Json;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Services
{
    public class SlackService
    {
        private readonly SlackSettings _settings;
        private readonly IHttpClientFactory _httpClientFactory;

        public SlackService(
            IOptions<SlackSettings> settings,
            IHttpClientFactory httpClientFactory)
        {
            _settings = settings.Value;
            _httpClientFactory = httpClientFactory;
        }

        public Task<SlackSendResponse> SendReopenProjectRequestMessageForApprovalAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateRequestForApprovalMessageBody(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SenderFullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate,
                CreateApproveReopenProjectReportingRequestUrl(slackMessageReopenProjectDTO));

            var messagePreview = SlackMessageReopenProjectBuilder.CreateRequestMessagePreview(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SenderFullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName);

            return SendMessageAsync(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.SlackUserId,
                messageBodyJson,
                messagePreview);
        }

        public Task<SlackSendResponse> SendReopenProjectRequestMessageForNotificationAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateRequestForNotificationMessageBody(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SenderFullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            var messagePreview = SlackMessageReopenProjectBuilder.CreateRequestMessagePreview(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SenderFullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName);

            return SendMessageAsync(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.SlackUserId,
                messageBodyJson,
                messagePreview);
        }

        public Task<SlackSendResponse> SendReopenProjectRequestMessageForUserAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateRequestMessageBodyForSender(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SenderFullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            var messagePreview = SlackMessageReopenProjectBuilder.CreateRequestMessagePreviewForSender(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName);

            return SendMessageAsync(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.SlackUserId,
                messageBodyJson,
                messagePreview);
        }

        public Task<SlackSendResponse> SendDeniedRequestAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateResponseMessageOnDeniedRequest(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            var messagePreview = SlackMessageReopenProjectBuilder.CreateDeniedResponseMessagePreview(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName
                );

            return SendAndDeletePreviousMessageAsync(
                slackMessageReopenProjectDTO,
                messageBodyJson,
                messagePreview);
        }

        public Task<SlackSendResponse> SendApprovedRequestAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO,
            UserSlackInfoDTO approverSlackInfoDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateResponseMessageOnApprovedRequest(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                approverSlackInfoDTO.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            var messagePreview = SlackMessageReopenProjectBuilder.CreateApprovedResponseMessagePreview(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName
                );

            return SendAndDeletePreviousMessageAsync(
                slackMessageReopenProjectDTO,
                messageBodyJson,
                messagePreview);
        }

        public Task<SlackSendResponse> SendApprovedRequestForApproverAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateResponseMessageOnApprovedRequestForApprover(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            var messagePreview = SlackMessageReopenProjectBuilder.CreateApprovedResponseMessagePreview(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName);

            return SendAndDeletePreviousMessageAsync(
                slackMessageReopenProjectDTO,
                messageBodyJson,
                messagePreview);
        }

        public Task<SlackSendResponse> SendReopenProjectResponseMessageOnApprovedRequestForSenderAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO,
            UserSlackInfoDTO approverSlackInfoDTO)
        {
            var messageBodyJson = SlackMessageReopenProjectBuilder.CreateResponseMessageOnApprovedRequestForSender(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                approverSlackInfoDTO.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            var messagePreview = SlackMessageReopenProjectBuilder.CreateApprovedResponseMessagePreview(
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName);

            return SendAndDeletePreviousMessageAsync(
                slackMessageReopenProjectDTO,
                messageBodyJson,
                messagePreview);
        }

        public async Task<SlackUpdateResponse> UpdateReopenProjectMessageOnExpiration(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO, string timeStamp, string channelId)
        {
            var message = SlackMessageReopenProjectBuilder.CreateExpiredMessage(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.FullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SenderFullName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.ProjectName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.AccountName,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.StartDate,
                slackMessageReopenProjectDTO.ProjectReopenRequestInfo.EndDate);

            return await UpdateMessageAsync(channelId, timeStamp, message);
        }

        private async Task<SlackUpdateResponse> UpdateMessageAsync(string channelId, string timeStamp,
            string messageBodyJson)
        {
            var httpClient = _httpClientFactory.CreateClient("Slack");

            var response = await httpClient.PostAsync("/api/chat.update", JsonContent.Create(new
            {
                channel = channelId,
                ts = timeStamp,
                blocks = JsonSerializer.Deserialize<object>(messageBodyJson),
                text = "Reopen request expired"
            }));

            var text = response.Content.ReadAsStringAsync().Result;

            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<SlackUpdateResponse>()
                : new SlackUpdateResponse() { IsSuccess = false };
        }

        private async Task<SlackSendResponse> SendAndDeletePreviousMessageAsync(
            ProjectReopenRequestSlackMessageDTO slackMessageReopenProjectDTO,
            string messageBodyJson,
            string messagePreview)
        {
            var result = await SendMessageAsync(
                slackMessageReopenProjectDTO.ReceiverSlackInfo.SlackUserId,
                messageBodyJson,
                messagePreview);

            if (result.IsSuccess)
            {
                var timeStamp = slackMessageReopenProjectDTO.ProjectReopenRequestInfo.SentMessages
                    .FirstOrDefault(m => m.UserId == slackMessageReopenProjectDTO.ReceiverSlackInfo.UserId)?.TimeStamp;

                if (timeStamp != null)
                {
                    var deleteResult = await DeleteMessageAsync(
                        slackMessageReopenProjectDTO.ReceiverSlackInfo.SlackUserId,
                        timeStamp);
                    result.IsSuccess = deleteResult.IsSuccess;
                }
                else
                {
                    result.IsSuccess = false;
                }
            }

            return result;
        }

        private async Task<SlackSendResponse> SendMessageAsync(
            string slackUserId,
            string messageBodyJson,
            string messagePreview)
        {
            var httpClient = _httpClientFactory.CreateClient("Slack");

            var response = await httpClient.PostAsync("/api/chat.postMessage", JsonContent.Create(new
            {
                channel = slackUserId,
                blocks = JsonSerializer.Deserialize<object>(messageBodyJson),
                text = messagePreview,
            }));

            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<SlackSendResponse>()
                : new SlackSendResponse() { IsSuccess = false };
        }

        private async Task<SlackDeleteResponse> DeleteMessageAsync(string slackUserId, string timeStamp)
        {
            var httpClient = _httpClientFactory.CreateClient("Slack");

            var response = await httpClient.PostAsync("/api/chat.delete", JsonContent.Create(new
            {
                channel = slackUserId,
                ts = timeStamp
            }));

            return response.IsSuccessStatusCode
                ? await response.Content.ReadFromJsonAsync<SlackDeleteResponse>()
                : new SlackDeleteResponse() { IsSuccess = false };
        }

        private string CreateApproveReopenProjectReportingRequestUrl(ProjectReopenRequestSlackMessageDTO slackMessageDTO)
        {
            return
                $"{_settings.TimeTrackingHostUrl}/projects/approve-reopen-reporting" +
                $"/{slackMessageDTO.ProjectReopenRequestInfo.ProjectReportingReopenRequestId}";
        }
    }
}