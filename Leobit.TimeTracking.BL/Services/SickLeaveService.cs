﻿using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Interfaces;
using Microsoft.Extensions.Caching.Memory;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.BL.Services
{
    public class SickLeaveService
    {
        private readonly LeobitContext _dbContext;
        private readonly IVacationDataProvider _vacationDataProvider;
        private readonly IMemoryCache _memoryCache;

        public SickLeaveService(LeobitContext dbContext, IVacationDataProvider vacationDataProvider, IMemoryCache memoryCache)
        {
            _dbContext = dbContext;
            _vacationDataProvider = vacationDataProvider;
            _memoryCache = memoryCache;
        }

        public async Task<SickLeaveInformationDTO> GetSickLeaveInformation(string userId, int year)
        {
            var providedSickLeaveDaysWithoutCertificate = (int)Constants.SickLeavesWithoutCert;
            var providedSickLeaveDaysWithCertificate = (int)Constants.SickLeaveDaysWithCertificate;
            var providedSickLeaveDaysToBeCompensated100 = (int)Constants.SickLeavesFullPayWithCert;
            var providedSickLeaveDaysToBeCompensated75 = (int)Constants.SickLeavesPartialPayWithCert;
            var overuserdSickLeaveFine = 0d;

            var yearData = await GetSickLeaveMonthDatas(userId, year, providedSickLeaveDaysToBeCompensated100);
            var usedSickLeaves = yearData.Sum(x => x?.WithCertificate + x?.WithoutCertificate) ?? 0;

            var employee = await _vacationDataProvider.GetEmployee(userId);

            var employmentDate = employee?.FullTimeStartDate;
            var terminationDate = employee?.TerminationDate;

            if (!employmentDate.HasValue || employmentDate.Value.Year > year
                || employee.Employment == Constants.EmploymentTypeTM)
            {
                providedSickLeaveDaysWithoutCertificate = 0;
                providedSickLeaveDaysWithCertificate = 0;
                providedSickLeaveDaysToBeCompensated100 = 0;
                providedSickLeaveDaysToBeCompensated75 = 0;
            }

            if (employmentDate.HasValue && terminationDate.HasValue && terminationDate.Value.Year == year)
            {
                var yearStart = year.FirstDateOnlyOfYear();
                var countFrom = yearStart > employmentDate.Value ? yearStart : employmentDate.Value;
                var employmentDuration = (terminationDate.Value - countFrom).TotalDays;
                var yearDuration = (year.LastDateOnlyOfYear() - yearStart).TotalDays + 1;
                var providedDays = providedSickLeaveDaysToBeCompensated100 + 0.75 * providedSickLeaveDaysToBeCompensated75;
                var dayAccrual = (double)providedDays / yearDuration;
                overuserdSickLeaveFine = Math.Max(0, usedSickLeaves - (employmentDuration * dayAccrual));
            }

            return new SickLeaveInformationDTO
            {
                ProvidedSickLeaveDaysWithoutCertificate = providedSickLeaveDaysWithoutCertificate,
                ProvidedSickLeaveDaysWithCertificate = providedSickLeaveDaysWithCertificate,
                ProvidedSickLeaveDaysToBeCompensated100 = providedSickLeaveDaysToBeCompensated100,
                ProvidedSickLeaveDaysToBeCompensated75 = providedSickLeaveDaysToBeCompensated75,
                CurrentSickLeaveBalance = providedSickLeaveDaysWithoutCertificate + providedSickLeaveDaysWithCertificate - usedSickLeaves,
                OverusedSickLeaveFine = overuserdSickLeaveFine,
                YearData = yearData
            };
        }

        private async Task<SickLeaveMonthDataDTO[]> GetSickLeaveMonthDatas(string userId, int year, int providedSickLeaveDaysToBeCompensated100)
        {
            var firstReporting = await _vacationDataProvider.GetFirstReportingDate(userId);

            var employmentDate = firstReporting ?? DateTime.UtcNow.Date;

            var firstDayOfYear = year.FirstDateOnlyOfYear();
            var lastDayOfYear = year.LastDateOnlyOfYear();

            var sickDaysMonthMap = _dbContext.TimeReport
                .AsNoTracking()
                .ByUserIds(new[] { userId })
                .ByProjectTypes(new[] { (int)ProjectType.SickLeaveWithoutCertificate, (int)ProjectType.SickLeaveWithCertificate })
                .ByDateRange(firstDayOfYear, lastDayOfYear)
                .Include(tr => tr.Project)
                .AsEnumerable()
                .GroupBy(tr => tr.ReportingDate.Month)
                .ToDictionary(g => g.Key, g => new SickLeaveMonthDataDTO
                {
                    WithoutCertificate = (double)g.Where(tr => tr.Project.ProjectType == (int)ProjectType.SickLeaveWithoutCertificate).Sum(tr => tr.InternalHours) / 8,
                    WithCertificate = (double)g.Where(tr => tr.Project.ProjectType == (int)ProjectType.SickLeaveWithCertificate).Sum(tr => tr.InternalHours) / 8
                });

            var missedMonths = Enumerable.Range(1, 12)
                .Except(sickDaysMonthMap.Keys)
                .ToDictionary(x => x, x => new SickLeaveMonthDataDTO());

            foreach (var month in missedMonths)
            {
                sickDaysMonthMap.Add(month.Key, month.Value);
            }

            var totalUsed = 0d;
            foreach (var month in Enumerable.Range(1, year < DateTime.UtcNow.Year ? 12 : DateTime.UtcNow.Month - 1))
            {
                var monthData = sickDaysMonthMap[month];
                monthData.FullyPaid = Math.Max(Math.Min(providedSickLeaveDaysToBeCompensated100 - totalUsed, monthData.WithoutCertificate + monthData.WithCertificate), 0);
                totalUsed += monthData.WithoutCertificate + monthData.WithCertificate;
                monthData.PartiallyPaid = Math.Max(totalUsed - providedSickLeaveDaysToBeCompensated100, 0);
            }

            var result = Enumerable.Range(1, 12)
                .Select(x => new DateTime(year, x, 1))
                .Select(x =>
                    employmentDate.FirstDateTimeOfMonth() <= x ?
                    sickDaysMonthMap.GetValueOrDefault(x.Month) :
                    new SickLeaveMonthDataDTO
                    {
                        FullyPaid = 0,
                        PartiallyPaid = 0,
                        WithCertificate = 0,
                        WithoutCertificate = 0
                    }
                 ).ToArray();


            return result;
        }
    }
}
