﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.BL.Interfaces
{
    public interface IVacationDataProvider
    {
        Task<Employee> GetEmployee(string userId);

        Task<DateTime?> GetFirstReportingDate(string userId);

        Task<TransferredVacationBalance> GetTransferredVacationBalance(string userId, int year);

        Task<(decimal totalReserveDays, decimal reserveDaysLastSixMonths)> GetReportedReserveDaysCombined(
            string userId, int year, DateTime? terminationDate);

        Task<(VacationMonthDataDTO[] MonthData, bool HasBeenOnTm)> GetVacationYearData(Employee employee, int year);
        
        Task<IDictionary<string, (VacationMonthDataDTO[] MonthData, bool HasBeenOnTm)>> GetVacationMonthDatasMap(
            List<Employee> employees,
            int year,
            Func<string[], DateTime, DateTime, ProjectType, Task<List<TimeReport>>> reportsProvider,
            DateTime? reportingTimeLimit = null);
    }
}
