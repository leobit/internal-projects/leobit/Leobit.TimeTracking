﻿using Leobit.TimeTracking.BL.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Interfaces
{
    public interface ITimeOffService
    {
        Task<List<TransferredVacationBalanceDTO>> GetTransferredVacationBalance(string employeeId, int? year = null);
        Task<CustomDayOffDTO[]> GetCustomDaysOff(string userId, int? year = null, int? month = null);
        Task<VacationInformationDTO> GetTimeOffInformation(string userId, int year);
    }
}
