﻿using Leobit.TimeTracking.BL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Interfaces
{
    public interface IVacationService
    {
        Task<VacationInformationDTO> GetVacationInformation(string userId, int year);

        Task<List<VacationInformationDTO>> GetVacationsInformation(ICollection<string> userIds, int year);

        Task TransferVacationBalance(int year);

        Task<List<EmployeeManagerShortDTO>> GetManagersList();

        Task<List<VacationStatisticModel>> GetAllEmployeesBalance(int year, string managerId, int? projectId);

        Task<List<CustomDayOffDTO>> GetCustomDayOffs(string userId, int? year = null, int? month = null);

        Task<List<CustomDayOffDTO>> GetCustomDayOffs(ICollection<string> userIds, int? year = null, int? month = null);

        Task<List<CustomDayOffDTO>> GetAllCustomDayOffsByYear(int year);

        Task<CustomDayOffDTO> CreateCustomDayOff(string editorId, CustomDayOffDTO customDayOffDTO);

        Task<CustomDayOffDTO> EditCustomDayOff(string editorId, CustomDayOffDTO customDayOffDTO);

        Task DeleteCustomDayOff(int id);

        Task EditEmploymentDate(string id, string employmentDate);

        Task<bool> IsEmployeeOnVacation(string userId, DateTime date);

        Task<Dictionary<string, bool>> AreEmployeesOnVacation(ICollection<string> userIds, DateTime date);
    }
}
