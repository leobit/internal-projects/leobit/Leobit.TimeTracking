﻿using Leobit.TimeTracking.BL.Models;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.BL.Interfaces
{
    public interface IOperationalService
    {
        Task<List<HolidayDTO>> UpdateHolidays(IEnumerable<HolidayDTO> holidays);
        Task<List<HolidayDTO>> GetHolidays(int? year = null, int? month = null);
        Task<List<HolidayDTO>> GetHolidaysInRange(DateTime from, DateTime to);
        Task<List<MonthHoursDTO>> GetBaseLine(int? year = null, int? month = null);
    }
}
