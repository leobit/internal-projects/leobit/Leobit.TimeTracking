﻿using System;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.BL.Interfaces
{
    public interface IEmployeeService
    {
        Task<EmployeeBaseDTO> GetEmployeeById(string employeeId);
        Task<(DateTime StartDate, DateTime? EndDate)[]> GetTmPeriods(string employeeId);
    }
}
