﻿using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Leobit.TimeTracking.BL.Extensions
{
    public static class DateTimeExtensions
    {
        public static bool IsWeekend(this DateTime date)
        {
            return date.DayOfWeek == DayOfWeek.Sunday || date.DayOfWeek == DayOfWeek.Saturday;
        }

        public static DateTime AddBusinessDays(this DateTime date, int value, List<Holiday> holidays)
        {
            int count = 0;
            while (count != Math.Abs(value))
            {
                date = (value < 0) ? date.AddDays(-1) : date.AddDays(1);
                if (!(date.IsWeekend() || holidays.Any(h => h.Year == date.Year
                    && h.Month == date.Month
                    && h.Day == date.Day)))
                {
                    count++;
                }
            }

            return date;
        }

        public static int GetBusinessDays(this DateTime date, int holidays = 0)
        {
            int daysInMonth = DateTime.DaysInMonth(date.Year, date.Month);
            int businessDays = 0;
            DateTime firstDayOfMonth = new DateTime(date.Year, date.Month, 1);

            for (int i = 0; i < daysInMonth; i++)
            {
                DayOfWeek dayOfWeek = firstDayOfMonth.AddDays(i).DayOfWeek;

                if (dayOfWeek != DayOfWeek.Saturday && dayOfWeek != DayOfWeek.Sunday)
                {
                    businessDays++;
                }
            }

            return businessDays - holidays;
        }
    }
}
