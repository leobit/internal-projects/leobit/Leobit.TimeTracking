﻿using Leobit.TimeTracking.BL.Models;

namespace Leobit.TimeTracking.BL.Extensions
{
    public struct GroupKey
    {
        public string UserId { get; set; }
        public string Username { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public bool IsOverhead { get; set; }
        public string ReportingMonth { get; set; }
        public int Id { get; set; }
    }

    public enum GroupType
    {
        Raw,
        Month,
        Year
    }

    public static class SpreadSheetExtensions
    {
        public static GroupKey GetGroupKey(this SpreadSheetTimeReportDTO sstr, GroupType type = GroupType.Month)
        {
            return new GroupKey
            {
                Account = sstr.Account,
                Project = sstr.Project,
                IsOverhead = sstr.IsOverhead,
                ReportingMonth = type == GroupType.Year ? sstr.ReportingYear.ToString() : sstr.ReportingMonth,
                UserId = sstr.UserId,
                Username = sstr.Username,
                Id = type == GroupType.Raw ? sstr.Id : 0
            };
        }
    }
}
