﻿using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.General.AmazonSQS;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.Json;
using System.Text.Json.Serialization;
using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.BL.Extensions
{
    public static class AmazonSqsExtensions
    {
        private static readonly JsonSerializerOptions _jsonSerializerOptions;

        static AmazonSqsExtensions()
        {
            _jsonSerializerOptions = new JsonSerializerOptions();
            _jsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
        }

        public static Task<bool> SendTimeReportMessage(this AmazonSqsService service, TimeReportDTO timeReport, TimeReportActionType actionType)
        {
            TimeReportSqsMessage messageDTO = MapToSqsMessage(timeReport, actionType);
            var message = JsonSerializer.Serialize(messageDTO, _jsonSerializerOptions);
            return service.SendSqsMessage(message, timeReport.User.Id);
        }

        public static Task<bool> SendTimeReportsMessages(this AmazonSqsService service, List<TimeReportDTO> timeReports, TimeReportActionType actionType)
        {
            var messages = timeReports.Select(tr => (JsonSerializer.Serialize(MapToSqsMessage(tr, actionType), _jsonSerializerOptions), tr.User.Id)).ToList();
            return service.SendSqsMessages(messages);
        }

        public static Task<bool> SendTimeReportsMessages(this AmazonSqsService service, List<TimeReport> timeReports, TimeReportActionType actionType)
        {
            var messages = timeReports.Select(tr => (JsonSerializer.Serialize(MapToSqsMessage(tr, actionType), _jsonSerializerOptions), tr.User.Id)).ToList();
            return service.SendSqsMessages(messages);
        }

        public static Task<bool> SendTimeReportsMessages(this AmazonSqsService service, List<TimeReportHistory> timeReports, TimeReportActionType actionType)
        {
            var messages = timeReports.Select(tr => (JsonSerializer.Serialize(MapToSqsMessage(tr, actionType), _jsonSerializerOptions), tr.User.Id)).ToList();
            return service.SendSqsMessages(messages);
        }

        private static TimeReportSqsMessage MapToSqsMessage(TimeReportDTO timeReport, TimeReportActionType actionType)
        {
            return MapToSqsMessage(timeReport.User.FirstName,
                                   timeReport.User.LastName,
                                   timeReport.User.Id,
                                   timeReport.Project.ProjectType,
                                   timeReport.ReportingDate,
                                   timeReport.StartTime,
                                   timeReport.InternalHours,
                                   actionType);
        }

        private static TimeReportSqsMessage MapToSqsMessage(TimeReport timeReport, TimeReportActionType actionType)
        {
            return MapToSqsMessage(timeReport.User.FirstName,
                                   timeReport.User.LastName,
                                   timeReport.User.Id,
                                   timeReport.Project.ProjectType,
                                   timeReport.ReportingDate,
                                   timeReport.StartTime,
                                   (double)timeReport.InternalHours,
                                   actionType);
        }

        private static TimeReportSqsMessage MapToSqsMessage(TimeReportHistory timeReport, TimeReportActionType actionType)
        {
            return MapToSqsMessage(timeReport.User.FirstName,
                                   timeReport.User.LastName,
                                   timeReport.User.Id,
                                   timeReport.Project.ProjectType,
                                   timeReport.ReportingDate,
                                   timeReport.StartTime,
                                   (double)timeReport.InternalHours,
                                   actionType);
        }

        private static TimeReportSqsMessage MapToSqsMessage(
            string firstName, 
            string lastName, 
            string userId, 
            int projectType, 
            DateTime date, 
            DateTime? startTime, 
            double duration, 
            TimeReportActionType actionType)
        {
            return new TimeReportSqsMessage
            {
                TimeReport = new SqsTimeReport
                {
                    Project = projectType switch
                    {
                        (int)ProjectType.SickLeaveWithCertificate => new DuCase<OperationalTimeReportProjectType>(OperationalTimeReportProjectType.SicknessProject),
                        (int)ProjectType.SickLeaveWithoutCertificate => new DuCase<OperationalTimeReportProjectType>(OperationalTimeReportProjectType.SicknessProject),
                        (int)ProjectType.Vacation => new DuCase<OperationalTimeReportProjectType>(OperationalTimeReportProjectType.DayoffProject),
                        (int)ProjectType.VacationUnpaid => new DuCase<OperationalTimeReportProjectType>(OperationalTimeReportProjectType.DayoffProject),
                        _ => new DuCase<OperationalTimeReportProjectType>(OperationalTimeReportProjectType.OtherProject)
                    },
                    Date = date,
                    Employee = new Models.Employee
                    {
                        FullName = $"{firstName} {lastName}",
                        UserName = userId
                    },
                    StartTime = startTime,
                    Duration = duration == Constants.FullDayHours 
                                ? null 
                                : duration,
                },
                Action = new DuCase<TimeReportActionType>(actionType)
            };
        }
    }
}
