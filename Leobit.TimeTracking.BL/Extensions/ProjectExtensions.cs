﻿using System;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.BL.Types;

using static Leobit.TimeTracking.BL.Helpers.TimeReportRangeCalculationHelper;
using System.Linq;

namespace Leobit.TimeTracking.BL.Extensions
{
    public static class ProjectExtensions
    { 
        public static DateRange? GetProjectReportingDates(this Project project, Settings globalSettings, DateTime currentDate, string userId = null)
        {
            if (project is null)
            {
                throw new ArgumentNullException(nameof(project));
            }

            if (project.Account is null)
            {
                throw new ArgumentException($"{nameof(Project.Account)} is null", nameof(project));
            }

            if (globalSettings is null)
            {
                throw new ArgumentNullException(nameof(globalSettings));
            }

            var userProject = project.UserProject.FirstOrDefault(up => up.UserId == userId);

            var allowedDateRange = project switch
            {
                { OverrideClosingSettings: true } => AdjustDateRange(
                    CalculateAllowedDateRange(project.ClosingType, project.BeforeClosing, project.AfterClosing, currentDate),
                    userProject),

                { Account: { OverrideClosingSettings: true } account } => AdjustDateRange(
                    CalculateAllowedDateRange(account.ClosingType, account.BeforeClosing, account.AfterClosing, currentDate),
                    userProject),

                _ => AdjustDateRange(
                    CalculateAllowedDateRange(globalSettings.ClosingType, globalSettings.BeforeClosing, globalSettings.AfterClosing, currentDate),
                    userProject)
            };

            return allowedDateRange;
        }

        public static int GetBeforeClosingSetting(this Project project, Settings globalSettings)
        {
            if (project.Account is null)
            {
                throw new ArgumentException($"{nameof(Project.Account)} is null", nameof(project));
            }

            return project switch
            {
                { OverrideClosingSettings: true } => project.BeforeClosing,

                { Account: { OverrideClosingSettings: true } account } => account.BeforeClosing,

                _ => globalSettings.BeforeClosing
            };
        }

        public static int GetAfterClosingSetting(this Project project, Settings globalSettings)
        {
            if (project.Account is null)
            {
                throw new ArgumentException($"{nameof(Project.Account)} is null", nameof(project));
            }

            return project switch
            {
                { OverrideClosingSettings: true } => project.AfterClosing,

                { Account: { OverrideClosingSettings: true } account } => account.AfterClosing,

                _ => globalSettings.AfterClosing
            };
        }

        public static short GetClosingTypeSetting(this Project project, Settings globalSettings)
        {
            if (project.Account is null)
            {
                throw new ArgumentException($"{nameof(Project.Account)} is null", nameof(project));
            }

            return project switch
            {
                { OverrideClosingSettings: true } => project.ClosingType,

                { Account: { OverrideClosingSettings: true } account } => account.ClosingType,

                _ => globalSettings.ClosingType
            };
        }

        private static DateRange? AdjustDateRange(DateRange allowedDateRange, UserProject userProject)
        {
            if (userProject != null)
            {
                var tempStartDate = allowedDateRange.StartDate;
                var tempEndDate = allowedDateRange.EndDate;

                var startDate = userProject.StartDate;
                var endDate = userProject.EndDate;

                if (startDate.HasValue && tempStartDate < startDate.Value)
                {
                    tempStartDate = startDate.Value;
                }

                if (endDate.HasValue && tempEndDate > endDate.Value)
                {
                    tempEndDate = endDate.Value;
                }

                if (allowedDateRange.EndDate < tempStartDate || allowedDateRange.StartDate > tempEndDate)
                {
                    return null;
                }
                else
                {
                    allowedDateRange = new DateRange(tempStartDate, tempEndDate);
                }
            }

            return allowedDateRange;
        }
    }
}
