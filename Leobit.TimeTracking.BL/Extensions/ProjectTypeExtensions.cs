﻿using Leobit.TimeTracking.BL.Models;
using System.Linq;

namespace Leobit.TimeTracking.BL.Extensions
{
    public static class TimeReportDTOExtensions
    {
        public static bool IsOperational(this TimeReportDTO timeReportDTO, params ProjectType[] type)
        {
            return type.Any(x => timeReportDTO.Project.ProjectType == (int)x);
        }
    }
}