﻿using Leobit.TimeTracking.General.Infratstructure;
using Leobit.TimeTracking.General;
using System;
using Leobit.TimeTracking.BL.Types;
using System.Runtime.InteropServices;

namespace Leobit.TimeTracking.BL.Helpers
{
    public static class TimeReportRangeCalculationHelper
    {
        public static DateRange CalculateAllowedDateRange(int closingType, int beforeClosing, int afterClosing, DateTime currentDate)
        {
            var timeZoneId = RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? "FLE Standard Time" : "Europe/Kyiv";

            var uaTimeZone = TimeZoneInfo.FindSystemTimeZoneById(timeZoneId);
            var currentDateInUA = TimeZoneInfo.ConvertTime(currentDate, uaTimeZone);

            var (startUnitDate, endUnitDate) = closingType switch
            {
                (int)ClosingType.Monthly => CalculateMonthlyClosingDateRange(currentDateInUA, afterClosing),

                (int)ClosingType.Weekly => (currentDateInUA.FirstDateOfWeek().AddBusinessDays(-1 * beforeClosing), currentDateInUA.LastDateOfWeek().AddBusinessDays(afterClosing)),

                (int)ClosingType.Daily => (currentDateInUA.Date.AddBusinessDays(-1 * beforeClosing), currentDateInUA.Date.AddBusinessDays(afterClosing)),

                (int)ClosingType.Monday => CalculateMondayClosingDateRange(currentDateInUA),

                (int)ClosingType.MondayThursday => CalculateMondayThursdayClosingDateRange(currentDateInUA),

                _ => throw new ArgumentOutOfRangeException(nameof(closingType))
            };


            return new DateRange(startUnitDate, endUnitDate);
        }

        private static (DateTime StartUnitDate, DateTime EndUnitDate) CalculateMonthlyClosingDateRange(
            DateTime currentDate,
            int afterClosing)
        {
            DateTime startUnitDate = currentDate.FirstDateOfWeek();

            if (currentDate.DayOfWeek == DayOfWeek.Monday && currentDate.TimeOfDay < TimeSpan.FromHours(12))
            {
                startUnitDate = startUnitDate.AddDays(-7);
            }

            DateTime endUnitDate = currentDate.LastDateOfMonth().AddBusinessDays(afterClosing);

            return (startUnitDate, endUnitDate);
        }


        private static (DateTime, DateTime) CalculateMondayClosingDateRange(DateTime currentDate)
        {
            var startUnitDate = currentDate.FirstDateOfWeek();
            var endUnitDate = currentDate.LastDateOfWeek();

            if (currentDate.DayOfWeek == DayOfWeek.Monday && currentDate.TimeOfDay < TimeSpan.FromHours(12))
            {
                startUnitDate = startUnitDate.AddDays(-7);
            }

            return (startUnitDate, endUnitDate);
        }

        private static (DateTime, DateTime) CalculateMondayThursdayClosingDateRange(DateTime currentDate)
        {
            var startUnitDate = currentDate.FirstDateOfWeek();
            var endUnitDate = currentDate.LastDateOfWeek();

            if (currentDate.DayOfWeek == DayOfWeek.Monday && currentDate.TimeOfDay < TimeSpan.FromHours(12))
            {
                startUnitDate = startUnitDate.AddDays(-4);
            }
            else if (currentDate.DayOfWeek >= DayOfWeek.Thursday && currentDate.TimeOfDay > TimeSpan.FromHours(12))
            {
                startUnitDate = startUnitDate.AddDays(3);
            }
            return (startUnitDate, endUnitDate);

        }
    }
}
