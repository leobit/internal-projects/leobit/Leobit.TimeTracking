﻿using System;
using System.Runtime.InteropServices;

namespace Leobit.TimeTracking.BL.Helpers
{
    public class UkrainianDateTimeProvider
    {
        private readonly TimeZoneInfo _timeZone;

        private const string FleStandardTime = "FLE Standard Time";
        private const string IanaKyivTimezone = "Europe/Kyiv";

        public virtual DateTime Now => TimeZoneInfo.ConvertTime(UtcNow, _timeZone);
        public virtual DateTime UtcNow => DateTime.UtcNow;

        public UkrainianDateTimeProvider()
        {
            _timeZone = TimeZoneInfo.FindSystemTimeZoneById(
                RuntimeInformation.IsOSPlatform(OSPlatform.Windows) ? FleStandardTime : IanaKyivTimezone);
        }

        public DateTime ConvertUtcToLocalTime(DateTime dateTime)
        {
            return TimeZoneInfo.ConvertTimeFromUtc(
                DateTime.SpecifyKind(dateTime, DateTimeKind.Utc), _timeZone);
        }
    }
}
