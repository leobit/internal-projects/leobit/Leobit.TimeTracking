﻿using Leobit.TimeTracking.BL.Models;
using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Helpers
{
    public static class TmPeriodProcessor
    {
        public static void ProcessTmPeriods(
        List<(DateTime Start, DateTime? End)> tmPeriods,
        VacationMonthDataDTO[] vacationMonthData,
        int year)
        {
            foreach (var tmPeriod in tmPeriods)
            {
                if (tmPeriod.Start.Year == year && tmPeriod.End?.Year == year)
                {
                    ProcessPeriodWithinYear(tmPeriod, vacationMonthData, year);
                }
                else if (tmPeriod.Start.Year == year && tmPeriod.End?.Year > year)
                {
                    ProcessPeriodSpanningToNextYear(tmPeriod, vacationMonthData, year);
                }
                else if (tmPeriod.Start.Year == year && tmPeriod.End == null)
                {
                    ProcessOngoingPeriodFromStartYear(tmPeriod, vacationMonthData, year);
                }
                else if (tmPeriod.Start.Year < year && tmPeriod.End?.Year == year)
                {
                    ProcessPeriodFromPreviousYear(tmPeriod, vacationMonthData, year);
                }
                else if (tmPeriod.Start.Year < year && tmPeriod.End == null)
                {
                    ProcessOngoingPeriodAcrossYears(vacationMonthData);
                }
            }
        }

        private static void ProcessPeriodWithinYear(
            (DateTime Start, DateTime? End) tmPeriod,
            VacationMonthDataDTO[] vacationMonthData,
            int year)
        {
            if (tmPeriod.End == null)
                return;

            for (int month = 1; month <= tmPeriod.End.Value.Month; month++)
            {
                if (month == tmPeriod.Start.Month)
                {
                    AdjustForStartOfMonth(tmPeriod.Start, vacationMonthData[month - 1], year);
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
                else if (month == tmPeriod.End.Value.Month)
                {
                    AdjustForEndOfMonth(tmPeriod.End.Value, vacationMonthData[month - 1], year);
                }
                else if (month > tmPeriod.Start.Month && month < tmPeriod.End.Value.Month)
                {
                    ResetMonth(vacationMonthData[month - 1]);
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
                else if (month < tmPeriod.End.Value.Month)
                {
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
            }
        }

        private static void ProcessPeriodSpanningToNextYear(
            (DateTime Start, DateTime? End) tmPeriod,
            VacationMonthDataDTO[] vacationMonthData,
            int year)
        {
            for (int month = 1; month <= 12; month++)
            {
                if (month == tmPeriod.Start.Month)
                {
                    AdjustForStartOfMonth(tmPeriod.Start, vacationMonthData[month - 1], year);
                }
                else
                {
                    ResetMonth(vacationMonthData[month - 1]);
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
            }
        }

        private static void ProcessPeriodFromPreviousYear(
            (DateTime Start, DateTime? End) tmPeriod,
            VacationMonthDataDTO[] vacationMonthData,
            int year)
        {
            if (tmPeriod.End == null)
                return;

            for (int month = 1; month <= tmPeriod.End.Value.Month; month++)
            {
                if (month == tmPeriod.End?.Month)
                {
                    AdjustForEndOfMonth(tmPeriod.End.Value, vacationMonthData[month - 1], year);
                }
                else
                {
                    ResetMonth(vacationMonthData[month - 1]);
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
            }
        }

        private static void ProcessOngoingPeriodAcrossYears(
            VacationMonthDataDTO[] vacationMonthData)
        {
            for (int month = 1; month <= 12; month++)
            {
                ResetMonth(vacationMonthData[month - 1]);
                ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
            }
        }

        private static void ProcessOngoingPeriodFromStartYear(
            (DateTime Start, DateTime? End) tmPeriod,
            VacationMonthDataDTO[] vacationMonthData,
            int year)
        {
            for (int month = 1; month <= 12; month++)
            {
                if (month == tmPeriod.Start.Month)
                {
                    AdjustForStartOfMonth(tmPeriod.Start, vacationMonthData[month - 1], year);
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
                else if (month > tmPeriod.Start.Month)
                {
                    ResetMonth(vacationMonthData[month - 1]);
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
                else
                {
                    ExcludeFromYearlyTotal(vacationMonthData[month - 1]);
                }
            }
        }

        private static void AdjustForStartOfMonth(DateTime start, VacationMonthDataDTO monthData, int year)
        {
            var daysInMonth = DateTime.DaysInMonth(year, start.Month);
            var daysOnTM = daysInMonth - (start.Day - 1);

            AdjustVacationData(monthData, daysOnTM, daysInMonth);
        }

        private static void AdjustForEndOfMonth(DateTime end, VacationMonthDataDTO monthData, int year)
        {
            var daysInMonth = DateTime.DaysInMonth(year, end.Month);
            var daysOnTM = end.Day - 1;

            AdjustVacationData(monthData, daysOnTM, daysInMonth);
        }

        private static void AdjustVacationData(VacationMonthDataDTO monthData, int daysOnTM, int daysInMonth)
        {
            var dailyAccrualRate = monthData.Accrual / daysInMonth;
            monthData.Accrual -= dailyAccrualRate * daysOnTM;
            monthData.Used = 0;
            monthData.UsedUnpaid = 0;
        }

        private static void ResetMonth(VacationMonthDataDTO monthData)
        {
            monthData.Accrual = 0;
            monthData.Used = 0;
            monthData.UsedUnpaid = 0;
        }

        private static void ExcludeFromYearlyTotal(VacationMonthDataDTO monthData)
        {
            monthData.ExcludeFromYearlyTotal = true;
        }
    }
}
