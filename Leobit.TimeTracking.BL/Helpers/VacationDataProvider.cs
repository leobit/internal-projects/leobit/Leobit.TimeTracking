﻿using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.DAL.Extensions;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.Extensions.Caching.Memory;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.BL.Helpers
{
    public class VacationDataProvider : IVacationDataProvider
    {
        private readonly LeobitContext _dbContext;
        private readonly IOperationalService _operationalService;

        public VacationDataProvider(LeobitContext dbContext, IOperationalService operationalService,
            IMemoryCache memoryCache)
        {
            _dbContext = dbContext;
            _operationalService = operationalService;
        }

        public async Task<Employee> GetEmployee(string userId)
        {
            var firstReporting = await _dbContext.TimeReport
                .AsNoTracking()
                .OrderBy(tr => tr.ReportingDate)
                .FirstOrDefaultAsync(tr => tr.UserId == userId);

            var firstReportingDate = firstReporting?.ReportingDate;
            var employee = await _dbContext.Employee
                .AsNoTracking()
                .Where(e => e.Id == userId)
                .Select(e => new Employee
                {
                    Id = e.Id,
                    StartDate = GetEffectiveFullTimeStartDate(e.StartDate, firstReportingDate),
                    FullTimeStartDate = GetEffectiveFullTimeStartDate(e.FullTimeStartDate, firstReportingDate),
                    TerminationDate = e != null ? e.TerminationDate : null,
                    Employment = e.Employment
                })
                .FirstOrDefaultAsync();

            if (employee == null)
            {
                throw new NotFoundException($"Could not find employee {userId}");
            }

            return employee;
        }

        public async Task<DateTime?> GetFirstReportingDate(string userId)
        {
            var firstReport = await _dbContext.TimeReport
                .AsNoTracking()
                .OrderBy(tr => tr.ReportingDate)
                .FirstOrDefaultAsync(tr => tr.UserId == userId);

            return firstReport?.ReportingDate;
        }

        public async Task<TransferredVacationBalance> GetTransferredVacationBalance(string userId, int year)
        {
            return await _dbContext.TransferredVacationBalance
                       .FirstOrDefaultAsync(x => x.UserId == userId && x.Year == year)
                   ?? new TransferredVacationBalance
                   {
                       CustomCredit = null,
                       Transferred = 0,
                       UserId = userId,
                       Year = DateTime.UtcNow.Year
                   };
        }

        //Get total reserve days reported for a given year
        public async Task<(decimal totalReserveDays, decimal reserveDaysLastSixMonths)> GetReportedReserveDaysCombined(
            string userId, int year, DateTime? terminationDate)
        {
            var reserveTimeReports = _dbContext.TimeReport
                .AsNoTracking()
                .Where(tr => tr.UserId == userId
                             && (tr.Project.ProjectPriority == (short)ProjectPriority.VeryLow
                                 || tr.Project.ProjectPriority == (short)ProjectPriority.Low));

            // Calculate date range for the whole year
            var startOfYear = year.FirstDateOnlyOfYear();
            var endOfYear = year.LastDateTimeOfYear();

            // Calculate date range for last six months
            var lastDayInYear = year.LastDateOnlyOfYear();
            var dayToCountReserve = terminationDate != null && terminationDate < lastDayInYear
                ? (DateTime)terminationDate
                : lastDayInYear;

            // Fetch the reports for the whole year and for the last six months at once
            var allReports = await reserveTimeReports
                .Where(tr => tr.ReportingDate >= startOfYear && tr.ReportingDate <= endOfYear)
                .ToListAsync();

            var totalReserveDays = allReports.Sum(tr => tr.InternalHours) / 8;

            var lastSixMonthsReserveDays = allReports
                .Where(tr => tr.ReportingDate >= dayToCountReserve.AddMonths(-6) && tr.ReportingDate <= dayToCountReserve)
                .Sum(tr => tr.InternalHours) / 8;

            return (totalReserveDays, lastSixMonthsReserveDays);
        }

        public async Task<(VacationMonthDataDTO[] MonthData, bool HasBeenOnTm)> GetVacationYearData(Employee employee, int year)
        {
            Task<List<TimeReport>> GetVacationReports(string[] userIds, DateTime firstDayOfYear, DateTime lastDayOfYear, ProjectType type)
            {
                return _dbContext.TimeReport
                    .AsNoTracking()
                    .ByUserIds(userIds)
                    .ByProjectTypes(new[] { (int)type })
                    .ByDateRange(firstDayOfYear, lastDayOfYear)
                .ToListAsync();
            }

            var yearDataMap = await GetVacationMonthDatasMap(new List<Employee> { employee }, year, GetVacationReports, DateTime.UtcNow.FirstDateTimeOfMonth());

            if (yearDataMap.TryGetValue(employee.Id, out var yearData))
            {
                return yearData;
            }

            throw new NotFoundException($"Year data for employee {employee.Id} not found");
        }

        public async Task<IDictionary<string, (VacationMonthDataDTO[] MonthData, bool HasBeenOnTm)>> GetVacationMonthDatasMap(
            List<Employee> employees,
            int year,
            Func<string[], DateTime, DateTime, ProjectType, Task<List<TimeReport>>> reportsProvider,
            DateTime? reportingTimeLimit = null)
        {
            var firstDayOfYear = year.FirstDateOnlyOfYear();
            var lastDayOfYear = year.LastDateOnlyOfYear();
            var daysInYear = (lastDayOfYear - firstDayOfYear).TotalDays + 1;

            var userIds = employees.Select(e => e.Id).ToArray();
            var usersUsedDays = await reportsProvider(userIds, firstDayOfYear, lastDayOfYear, ProjectType.Vacation);
            var usersUsedUnpaindDays = await reportsProvider(userIds, firstDayOfYear, lastDayOfYear, ProjectType.VacationUnpaid);

            var accrualKoefs = await GetAccrualKoefs(userIds, year, reportingTimeLimit);

            var usersCustomDayOffs = await _dbContext.CustomDayOff
                .AsNoTracking()
                .ByUserIds(userIds)
                .ByYear(year)
                .ByMonthRange(firstDayOfYear.Month, lastDayOfYear.Month)
                .ToListAsync();

            var result = new Dictionary<string, (VacationMonthDataDTO[] MonthData, bool HasBeenOnTm)>();

            var tmEmployeeIds = await _dbContext.EmployeeHistory
                .AsNoTracking()
                .Where(e => e.Employment == Constants.EmploymentTypeTM)
                .Select(e => e.UserId)
                .Distinct()
                .ToListAsync();

            var employeeHistory = await _dbContext.EmployeeHistory
                .AsNoTracking()
                .Where(eh => userIds.Contains(eh.UserId) &&
                             eh.StartDate.HasValue &&
                             tmEmployeeIds.Contains(eh.UserId))
                .OrderBy(eh => eh.Timestamp)
                .ToListAsync();

            var employeesHistoryDictionary = employeeHistory
                .GroupBy(history => history.UserId)
                .ToDictionary(e => e.Key, value => value.ToList());


            foreach (var employee in employees)
            {
                var hasEmployeeHistoryData = employeesHistoryDictionary.TryGetValue(employee.Id, out var employeeHistoryList);
                var hasBeenOnTm = false;

                var employmentDate = employee.StartDate;
                (DateTime? FullTimeStartDate, bool HasBeenMoreThanSixMonth) effectiveFullTimeStartDate = (null, false);

                if (hasEmployeeHistoryData)
                {
                    effectiveFullTimeStartDate = GetEffectiveFullTimeStartDate(employeeHistoryList);

                    if (effectiveFullTimeStartDate.FullTimeStartDate is null
                        && employee.Employment == Constants.EmploymentTypeTM)
                    {
                        employmentDate = null;
                        employee.StartDate = null;
                    }
                    else if (effectiveFullTimeStartDate.HasBeenMoreThanSixMonth)
                    {
                        employmentDate = effectiveFullTimeStartDate.FullTimeStartDate;
                        employee.StartDate = effectiveFullTimeStartDate.FullTimeStartDate;
                    }
                }

                var terminationDate = employee.TerminationDate;

                var usedDaysMonthMap = usersUsedDays
                    .Where(d => d.UserId == employee.Id)
                    .GroupBy(tr => tr.ReportingDate.Month)
                    .ToDictionary(g => g.Key, g => g.Sum(tr => (double?)tr.InternalHours / 8));

                var usedUnpaidDaysMonthMap = usersUsedUnpaindDays
                    .Where(d => d.UserId == employee.Id)
                    .GroupBy(tr => tr.ReportingDate.Month)
                    .ToDictionary(g => g.Key, g => g.Sum(tr => (double?)tr.InternalHours / 8));

                var accrualDaysMonthMap = Enumerable.Range(1, 12)
                    .Select(x => new DateTime(year, x, 1))
                    .ToDictionary(x => x.Month, x =>
                    {
                        var workingYears = TotalYears(employmentDate, x, terminationDate);
                        var vacationDays = workingYears < 2 ? 18d
                            : workingYears < 3 ? 20d
                            : workingYears < 4 ? 21d
                            : 22d;
                        return terminationDate.HasValue && terminationDate < x
                                ? 0
                                : vacationDays / 12.0 * (year < Constants.NewAccrualSystemYear ? 1 : accrualKoefs[(employee.Id, x.Month)]);
                    });

                var missedMonths = Enumerable.Range(1, 12)
                    .ToDictionary(x => x, x => 0d);

                foreach (var month in missedMonths)
                {
                    if (!usedDaysMonthMap.ContainsKey(month.Key)) usedDaysMonthMap.Add(month.Key, month.Value);
                    if (!usedUnpaidDaysMonthMap.ContainsKey(month.Key)) usedUnpaidDaysMonthMap.Add(month.Key, month.Value);
                }

                var vacationMonthData = Enumerable.Range(1, 12)
                    .Select(x => new DateTime(year, x, 1))
                    .Select(x =>
                        employmentDate.HasValue && employmentDate.Value.FirstDateTimeOfMonth() <= x
                        ? new VacationMonthDataDTO
                        {
                            Accrual = accrualDaysMonthMap[x.Month],
                            Used = usedDaysMonthMap[x.Month],
                            UsedUnpaid = usedUnpaidDaysMonthMap[x.Month]
                        }
                        : new VacationMonthDataDTO
                        {
                            Accrual = 0,
                            Used = 0,
                            UsedUnpaid = 0
                        }
                    ).ToArray();

                var tmPeriods = FindAllTmPeriods(employeeHistoryList);

                if (tmPeriods is not null && tmPeriods.Any())
                {
                    hasBeenOnTm = true;
                    TmPeriodProcessor.ProcessTmPeriods(tmPeriods, vacationMonthData, year);
                }

                if (year < Constants.NewAccrualSystemYear)
                {
                    // first working month correction
                    if (employmentDate.HasValue && lastDayOfYear >= employmentDate)
                    {
                        var koef = Math.Min(((lastDayOfYear - employmentDate.Value).TotalDays + 1) / daysInYear, 1);
                        var maxAccrualDays = accrualDaysMonthMap.Values.Sum() * koef;

                        var totalAccrualDays = vacationMonthData.Sum(x => x?.Accrual ?? 0);

                        if (totalAccrualDays > maxAccrualDays)
                        {
                            vacationMonthData[employmentDate.Value.Month - 1].Accrual -= (totalAccrualDays - maxAccrualDays);
                        }
                    }

                    // last working month correction
                    if (terminationDate.HasValue && terminationDate.Value.Year == firstDayOfYear.Year)
                    {
                        var lastDayOfMonth = terminationDate.Value.AddMonths(1).FirstDateTimeOfMonth().AddDays(-1);
                        var daysInFullMonthes = (lastDayOfMonth - firstDayOfYear).TotalDays + 1;
                        var koef = Math.Min(((terminationDate.Value - firstDayOfYear).TotalDays + 1) / daysInFullMonthes, 1);
                        var maxAccrualDays = accrualDaysMonthMap.Values.Sum() * koef;
                        var totalAccrualDays = vacationMonthData.Sum(x => x?.Accrual ?? 0);

                        if (totalAccrualDays > maxAccrualDays)
                        {
                            vacationMonthData[terminationDate.Value.Month - 1].Accrual -= (totalAccrualDays - maxAccrualDays);
                        }
                    }
                }

                // bonus days accrual
                var companyDay2020 = new DateTime(2020, 7, 31);
                if (employmentDate.HasValue && employmentDate < companyDay2020
                    && (!terminationDate.HasValue || terminationDate > companyDay2020)
                    && year == companyDay2020.Year)
                {
                    vacationMonthData[companyDay2020.Month - 1].Accrual += 1;
                }

                result.Add(employee.Id, (vacationMonthData, hasBeenOnTm));
            }


            return result;
        }

        public static DateTime? GetEffectiveFullTimeStartDate(DateTime? fullTimeStartDate, DateTime? firstReportingDate)
        {
            if (!fullTimeStartDate.HasValue)
                return null;

            if (!firstReportingDate.HasValue || fullTimeStartDate > firstReportingDate)
                return fullTimeStartDate;

            return firstReportingDate;
        }

        public static double TotalYears(DateTime? employmentDate, DateTime currentDate, DateTime? terminationDate)
        {
            if (!employmentDate.HasValue || employmentDate.Value > currentDate)
            {
                return 0;
            }

            var effectiveEndDate = terminationDate.HasValue && terminationDate.Value < currentDate
                ? terminationDate.Value
                : currentDate;

            var fullYears = effectiveEndDate.Month >= employmentDate.Value.Month
                            && effectiveEndDate.Day >= employmentDate.Value.Day
                ? effectiveEndDate.Year - employmentDate.Value.Year
                : effectiveEndDate.Year - employmentDate.Value.Year - 1;

            var baselineDays = (effectiveEndDate - effectiveEndDate.AddYears(-1)).TotalDays;
            var fractionOfYears = (effectiveEndDate - employmentDate.Value.AddYears(fullYears)).TotalDays / baselineDays;

            return Math.Round(fullYears + fractionOfYears, 2);
        }

        private (DateTime? FullTimeStartDate, bool HasBeenMoreThanSixMonth) GetEffectiveFullTimeStartDate(List<EmployeeHistory> employeeHistory)
        {
            DateTime? currentFullTimeStartDate = null;
            DateTime? currentTmStartDate = null;
            bool hasBeenMoreThanSixMonth = false;

            foreach (var history in employeeHistory)
            {
                switch (history.Employment)
                {
                    case Constants.EmploymentTypeFT:
                        {
                            if (currentTmStartDate.HasValue &&
                                IsMoreThanNMonths(currentTmStartDate.Value, history.Timestamp, Constants.SixMonthTMEmployeesValidation))
                            {
                                hasBeenMoreThanSixMonth = true;
                                currentFullTimeStartDate = history.Timestamp;
                            }

                            currentTmStartDate = null;
                            currentFullTimeStartDate ??= history.StartDate;
                            break;
                        }
                    case Constants.EmploymentTypeTM:
                        {
                            currentTmStartDate ??= history.Timestamp;
                            break;
                        }
                    default:
                        break;
                }
            }

            return (currentFullTimeStartDate, hasBeenMoreThanSixMonth);
        }

        private bool IsMoreThanNMonths(DateTime start, DateTime end, ushort months)
        {
            return (end.Year - start.Year) * 12 + end.Month - start.Month > months;
        }

        private async Task<IDictionary<(string, int), double>> GetAccrualKoefs(string[] userIds, int year, DateTime? reportingTimeLimit = null)
        {
            if (year < Constants.NewAccrualSystemYear)
            {
                return new Dictionary<(string, int), double>();
            }

            var query = _dbContext.TimeReport
                .Where(tr => tr.Project.ProjectType != (int)ProjectType.VacationUnpaid
                             && tr.ReportingDate.Year == year);

            if (reportingTimeLimit.HasValue)
            {
                query = query.Where(tr => tr.ReportingDate < reportingTimeLimit);
            }

            var hoursReported = await query
                .GroupBy(tr => new
                {
                    tr.UserId,
                    tr.ReportingDate.Month
                })
                .Select(g => new
                {
                    UserId = g.Key.UserId,
                    Month = g.Key.Month,
                    Hours = g.Sum(tr => tr.InternalHours)
                })
                .ToDictionaryAsync(x => (x.UserId, x.Month), x => x.Hours);

            var usersIds = hoursReported.Keys.Select(k => k.UserId)
                .Union(userIds)
                .Distinct()
                .ToArray();

            var baseline = (await _operationalService.GetBaseLine(year))
                .ToDictionary(x => x.Month, x => x.Hours);

            return baseline.Keys
                .SelectMany(monthKey => usersIds, (monthKey, user) => new { monthKey, user })
                .ToDictionary(
                    keySelector: pair => (pair.user, pair.monthKey),
                    elementSelector: pair =>
                        hoursReported.TryGetValue((pair.user, pair.monthKey), out var reportedInternal)
                            ? (double)Math.Min(reportedInternal / baseline[pair.monthKey], 1)
                            : 1
                );
        }

        private List<(DateTime Start, DateTime? End)> FindAllTmPeriods(List<EmployeeHistory> employeeHistoryList)
        {
            if (employeeHistoryList is null || !employeeHistoryList.Any())
                return new List<(DateTime Start, DateTime? End)>();

            var tmPeriods = new List<(DateTime Start, DateTime? End)>();
            DateTime? currentTmStart = null;

            foreach (var record in employeeHistoryList.OrderBy(e => e.Timestamp))
            {
                if (record.Employment == Constants.EmploymentTypeTM)
                {
                    // Start of a new TM period
                    if (currentTmStart == null)
                    {
                        currentTmStart = record.Timestamp;
                    }
                }
                else if (record.Employment != Constants.EmploymentTypeTM && currentTmStart != null)
                {
                    // End of a TM period
                    tmPeriods.Add((currentTmStart.Value, record.Timestamp));
                    currentTmStart = null;
                }
            }

            // Handle case where TM period ends at the last record
            if (currentTmStart != null)
            {
                tmPeriods.Add((currentTmStart.Value, null));
            }

            return tmPeriods;
        }
    }
}
