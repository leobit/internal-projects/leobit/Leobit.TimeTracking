﻿namespace Leobit.TimeTracking.BL
{
    public enum EmployeeLevel: int
    {
        Trainee = 0,
        Junior = 1,
        Middle = 2,
        Senior = 3,
        Lead = 4,
        Architect = 5,
        Manager = 6,
        Director= 7,
        VP = 8,
        EVP = 9,
        CEO = 10
    }
}
