﻿namespace Leobit.TimeTracking.BL
{
    public enum SlackReopenRequestMessageType
    {
        ForApproval = 0,
        ForNotification = 1,
    }
}
