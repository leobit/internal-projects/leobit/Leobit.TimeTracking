﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Leobit.TimeTracking.BL.Models
{
    public class UserDefaultsDTO
    {
        public int? AccountId { get; set; }
        public int? ProjectId { get; set; }
        public DateTime ReportingDate { get; set; }
        public double InternalHours { get; set; }
        public double ExternalHours { get; set; }

        public IEnumerable<UserDefaultDTO> Defaults { get; set; } = Enumerable.Empty<UserDefaultDTO>();
    }

    public class UserDefaultDTO
    {
        public int? AccountId { get; set; }
        public int? ProjectId { get; set; }
        public DateTime ReportingDate { get; set; }
        public double InternalHours { get; set; }
        public double ExternalHours { get; set; }
    }
}
