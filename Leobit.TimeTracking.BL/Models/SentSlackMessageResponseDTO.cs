﻿using Leobit.TimeTracking.BL.Infrastructure.Slack;

namespace Leobit.TimeTracking.BL.Models
{
    public class SentSlackMessageResponseDTO
    {
        public string UserId { get; set; }

        public SlackSendResponse SlackSendResponse {get; set;}
    }
}
