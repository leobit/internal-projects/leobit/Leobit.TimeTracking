﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class SlackReopenRequestMessageDto
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public SlackReopenRequestMessageType Type { get; set; }
        public UserSlackInfoDTO UserSlackInfo { get; set; }
    }
}

