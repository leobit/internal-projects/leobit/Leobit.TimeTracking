﻿using Leobit.TimeTracking.BL.Services;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class TimeReportIssueDTO
    {
        public List<TimeReportDTO> Reports { get; set; }
        public UserShortDTO User { get; set; }
        public TimeReportIssueType IssueType { get; set; }
        public string Description { get; set; }
    }
}
