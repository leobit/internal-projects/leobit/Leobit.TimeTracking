﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class SpreadSheetTimeReportDTO
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string UserId { get; set; }
        public string Username { get; set; }
        public decimal InternalHours { get; set; }
        public decimal ExternalHours { get; set; }
        public string WorkDescription { get; set; }
        public bool IsOverhead { get; set; }
        public string Division { get; set; }
        public string OrganizationUnit { get; set; }
        public string Category { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public DateTime ReportingDate { get; set; }
        public string ReportingMonth { get; set; }
        public int ReportingYear { get; set; }
        public int? DivisionId { get; set; }
        public int? OrganizationUnitId { get; set; }
        public int? CategoryId { get; set; }
        public int? AccountId { get; set; }
        public int? ProjectId { get; set; }
        public ProjectPriority ProjectPriority { get; set; }
        public ProjectType ProjectType { get; set; }
        public string BudgetOwnerId { get; set; }
    }
}
