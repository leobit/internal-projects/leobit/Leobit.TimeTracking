using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class CustomDayOffDTO
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal ExtraDayOff { get; set; }
        public string Description { get; set; }
        public DateTime LastEditTimestamp { get; set; }
        public UserShortDTO User { get; set; }
        public UserShortDTO LastEditor { get; set; }
    }
}
