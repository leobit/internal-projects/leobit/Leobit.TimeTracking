﻿namespace Leobit.TimeTracking.BL.Models
{
    public class CategoryDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public OrganizationUnitDTO OrganizationUnit { get; set; }
    }

    public class CategoryInputDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public OrganizationUnitShortDTO OrganizationUnit { get; set; }
    }

    public class CategoryShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
