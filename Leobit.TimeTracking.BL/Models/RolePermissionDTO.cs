﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Models
{
    public class RolePermissionDTO
    {
        public byte Value { get; set; }
        public PermissionDTO Permission { get; set; }
    }
}
