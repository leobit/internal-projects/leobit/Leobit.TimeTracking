﻿namespace Leobit.TimeTracking.BL.Models
{
    public class DivisionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
