﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class TimeReportReminderDTO
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string SlackUserId { get; set; }
        public double ExpectedHours { get; set; }
        public double ReportedHours { get; set; }
        public DateTime? ReminderDay { get; set; }
    }
}
