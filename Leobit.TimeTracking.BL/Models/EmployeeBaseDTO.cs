﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class EmployeeBaseDTO
    {
        public string Id { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string Employment { get; set; }
    }
}
