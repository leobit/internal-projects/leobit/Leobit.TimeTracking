﻿namespace Leobit.TimeTracking.BL.Models
{
    public class UserSlackInfoDTO
    {
        public string UserId { get; set; }
        public string SlackUserId { get; set; }
        public string FullName { get; set; }
    }
}
