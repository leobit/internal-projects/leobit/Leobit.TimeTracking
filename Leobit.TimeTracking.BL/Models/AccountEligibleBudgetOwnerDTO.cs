﻿namespace Leobit.TimeTracking.BL.Models
{
    public class AccountEligibleBudgetOwnerDTO
    {
        public string DefaultBudgetOwnerId { get; set; }
        public UserShortDTO[] EligibleBudgetOwners { get; set; }
    }
}
