using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class ProjectSettingsBackupDTO
    {
        public int Id { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string ConfigurationSnapshot { get; set; }
    }
}
