﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class TimeReportBatchCommitDTO
    {
        public string Id { get; set; }
        public DateTime DateTime { get; set; }
        public string EditorFullName { get; set; }
        public string EditAction { get; set; }
    }
}
