﻿namespace Leobit.TimeTracking.BL.Models
{
    public class OrganizationUnitDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DivisionDTO Division { get; set; }
    }

    public class OrganizationUnitShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
