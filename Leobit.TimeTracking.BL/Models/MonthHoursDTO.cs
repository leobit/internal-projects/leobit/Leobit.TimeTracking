﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Models
{
    public class MonthHoursDTO
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Hours { get; set; }
    }
}
