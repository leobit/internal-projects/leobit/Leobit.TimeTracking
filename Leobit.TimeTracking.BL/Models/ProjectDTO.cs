using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class ProjectDTO
    {
        public int Id { get; set; }
        public AccountShortDTO Account { get; set; }
        public CategoryShortDTO Category { get; set; }
        public string Name { get; set; }
        public bool OverrideHoursSettings { get; set; }
        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public bool OverrideClosingSettings { get; set; }
        public short ClosingType { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
        public bool IsClosed { get; set; }
        public bool AutoAssignNewUsers { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool OverrideTimeReportPriorities { get; set; }
        public int ProjectType { get; set; }
        public int ProjectPriority { get; set; }
        public bool RestoreClosingSettingsLater { get; set; }
        public ProjectSettingsBackupDTO ProjectSettingsBackup { get; set; }
        public ICollection<ProjectReopenRequestDetailsDTO> ProjectReopenRequests { get; set; }
    }

    public class ProjectShortDTO
    {
        public int Id { get; set; }
        public AccountShortDTO Account { get; set; }
        public RestrictionDTO Restriction { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public string Name { get; set; }
        public bool IsClosed { get; set; }
        public int ProjectType { get; set; }
        public int ProjectPriority { get; set; }
        public bool IsInternal => MinExternal == 0 && MaxExternal == 0;
    }

    public class ProjectClosingSettingsDTO
    {
        public bool OverrideClosingSettings { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
    }
}
