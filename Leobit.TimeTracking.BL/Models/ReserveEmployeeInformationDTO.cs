﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class ReserveEmployeeInformationDTO
    {
        public string EmployeeId { get; set; }
        public string EmployeeFullName { get; set; }
        public DateTime LastReportTimestamp { get; set; }
        public double ReportedDaysForCurrentYear { get; set; }
        public double CurrentVacationBalance { get; set; }
        public double PenaltyAmountForCurrentYear { get; set; }
        public CustomDayOffDTO VacationPenalty { get; set; }
        public double AfterVacationTaken { get; set; }
    }
}
