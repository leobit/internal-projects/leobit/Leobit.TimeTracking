﻿namespace Leobit.TimeTracking.BL.Models
{
    public class SettingsDTO
    {
        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public short ClosingType { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
        public string DefaultBo { get; set; }
    }
}
