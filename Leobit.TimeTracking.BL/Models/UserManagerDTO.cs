﻿using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class UserManagerDTO
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string SlackId { get; set; }
        public List<ManagerDTO> Managers { get; set; } = new List<ManagerDTO>();
    }

    public class ManagerDTO
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public bool IsPrimary { get; set; }
        public string ProjectName { get; set; }
        public string SlackId { get; set; }
    }
}
