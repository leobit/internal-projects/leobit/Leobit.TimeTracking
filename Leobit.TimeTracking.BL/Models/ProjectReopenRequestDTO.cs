﻿using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class ProjectReopenRequestDTO
    {
        public string SenderId { get; set; }
        public string SenderFullName { get; set; }
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public string AccountName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ProjectManagerApprovedId { get; set; }
        public int ProjectReportingReopenRequestId { get; set; }
        public DateTime CreationTimestamp { get; set; }
        public ICollection<SentSlackMessageDTO> SentMessages { get; set; }
    }

    public class ProjectReopenRequestDetailsDTO
    {
        public int Id { get; set; }
        public string SenderFullName { get; set; }
        public string ProjectManagerApprovedFullName { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string SenderId { get; set; }
        public bool CanApprove { get; set; }
    }
}
