﻿namespace Leobit.TimeTracking.BL.Models
{
    public class SickLeaveMonthDataDTO
    {
        public double WithoutCertificate { get; set; }
        public double WithCertificate { get; set; }
        public double FullyPaid { get; set; }
        public double PartiallyPaid { get; set; }
    }

    public class SickLeaveInformationDTO
    {
        public double ProvidedSickLeaveDaysWithoutCertificate { get; set; }
        public double ProvidedSickLeaveDaysWithCertificate { get; set; }
        public double ProvidedSickLeaveDaysToBeCompensated100 { get; set; }
        public double ProvidedSickLeaveDaysToBeCompensated75 { get; set; }
        public double CurrentSickLeaveBalance { get; set; }
        public double OverusedSickLeaveFine { get; set; }
        public SickLeaveMonthDataDTO[] YearData { get; set; }
    }
}
