﻿using System;
using System.Collections.Generic;
using System.Linq;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General.Infratstructure;

namespace Leobit.TimeTracking.BL.Models
{
    public class VacationMonthDataDTO
    {
        public VacationMonthDataDTO() // remove after proper testing Timeoffs
        {            
        }

        public VacationMonthDataDTO(string employment, int year, int monthNumber, double accrualRate)
        {
            var now = DateTime.UtcNow;
            Used = 0;
            UsedUnpaid = 0;
            Accrual = employment != Constants.EmploymentTypeTM && now.Year == year && now.Month <= monthNumber
                                ? accrualRate
                                : 0;
        }

        public VacationMonthDataDTO(EmployeeMonthlyActivityView timeOffMonth, string employment, double accrualRate, decimal accrual)
        {
            var now = DateTime.UtcNow;
            Used = timeOffMonth?.DaysOffUsed != null ? (double)timeOffMonth.DaysOffUsed : 0;
            UsedUnpaid = (double)timeOffMonth.UnpaidDaysOffUsed;
            Accrual = timeOffMonth.Year == now.Year && now.Month == timeOffMonth.Month && employment != Constants.EmploymentTypeTM
                ? accrualRate
                : Math.Min((double)accrual, accrualRate);

        }

        public double? Accrual { get; set; }
        public double? Used { get; set; }
        public double? UsedUnpaid { get; set; }
        public bool ExcludeFromYearlyTotal { get; set; }
    }

    public class VacationInformationDTO
    {
        public string EmployeeId { get; set; }
        public DateTime? EmploymentDate { get; set; }
        public bool IsTMEmployee { get; set; }
        public DateTime? TerminationDate { get; set; }
        public DateTime? FirstReportedDate { get; set; }
        public double StandingYears { get; set; }
        public double TransferredBalance { get; set; }
        public int MinimumAllowedBalance { get; set; }
        public VacationMonthDataDTO[] YearData { get; set; }
        public double CurrentVacationBalance { get; set; }
        public double ExpectedEoyVacationBalance { get; set; }
        public double CompensatedVacationLeftovers { get; set; }
        public double ExtraDaysOff { get; set; }
        public double ReportedReserveDays { get; set; }
        public double ReportedReserveDaysForLastSixMonths { get; set; }

        public double ProvidedSickLeaveDaysWithoutCertificate { get; set; }
        public double ProvidedSickLeaveDaysWithCertificate { get; set; }
        public double ProvidedSickLeaveDaysToBeCompensated100 { get; set; }
        public double ProvidedSickLeaveDaysToBeCompensated75 { get; set; }
        public double CurrentSickLeaveBalance { get; set; }
        public double OverusedSickLeaveFine { get; set; }
        public SickLeaveMonthDataDTO[] SickLeaveYearData { get; set; }

        public VacationInformationDTO() // remove after proper testing Timeoffs
        {            
        }

        public VacationInformationDTO(string userId)
        {
            EmployeeId = userId;
            YearData = Array.Empty<VacationMonthDataDTO>();
            SickLeaveYearData = Array.Empty<SickLeaveMonthDataDTO>();
        }

        public VacationInformationDTO(EmployeeBaseDTO employee, DateTime? firstReportedDate, int year, double standingYears,
            TransferredVacationBalanceDTO transferredBalance,
            double vacationLeftovers,
            List<VacationMonthDataDTO> yearData, double daysInReserve, double daysInReserveLastSixMonth,
            List<SickLeaveMonthDataDTO> sickLeaveYearData,
            CustomDayOffDTO[] customDaysoff)
        {
            var overuserdSickLeaveFine = 0d;
            var usedVacations = yearData.Where(x => x.Used.HasValue).Sum(x => x.Used.Value);
            var usedSickLeaves = sickLeaveYearData.Sum(x => x?.WithCertificate + x?.WithoutCertificate) ?? 0;
            var targetDate = DateTime.UtcNow.Year <= year ? DateTime.UtcNow : year.LastDateOnlyOfYear();
            var customDaysOff = (double)customDaysoff.Sum(x => x.ExtraDayOff);

            var vacationBalance = year == DateTime.UtcNow.Year
                ? (double)(transferredBalance?.Transferred ?? 0) + customDaysOff + yearData.Where((x, index) => index < targetDate.Month - 1).Sum(x => x.Accrual)
                : (double)(transferredBalance?.Transferred ?? 0) + customDaysOff + yearData.Sum(x => x.Accrual);

            if (employee.StartDate.HasValue && employee.TerminationDate.HasValue && employee.TerminationDate.Value.Year == year)
            {
                var yearStart = year.FirstDateOnlyOfYear();
                var countFrom = yearStart > employee.StartDate.Value ? yearStart : employee.StartDate.Value;
                var employmentDuration = (employee.TerminationDate.Value - countFrom).TotalDays;
                var yearDuration = (year.LastDateOnlyOfYear() - yearStart).TotalDays + 1;
                const decimal providedDays = Constants.SickLeavesFullPay + 0.75m * Constants.SickLeavesPartialPayWithCert;
                var dayAccrual = (double)providedDays / yearDuration;
                overuserdSickLeaveFine = Math.Max(0, usedSickLeaves - (employmentDuration * dayAccrual));
            }

            EmployeeId = employee.Id;
            EmploymentDate = employee.StartDate;
            IsTMEmployee = employee.Employment == Constants.EmploymentTypeTM;
            TerminationDate = employee.TerminationDate;
            FirstReportedDate = firstReportedDate;
            StandingYears = standingYears;
            TransferredBalance = (double)(transferredBalance?.Transferred ?? 0);
            MinimumAllowedBalance = transferredBalance?.CustomCredit ?? (employee.StartDate.HasValue ? 5 : 0);
            CurrentVacationBalance = Math.Round((double)vacationBalance - usedVacations - vacationLeftovers, 2);
            ExpectedEoyVacationBalance = Math.Round((double)yearData.Sum(x => x.Accrual), 2) + customDaysOff + TransferredBalance - (usedVacations + vacationLeftovers);
            CompensatedVacationLeftovers = vacationLeftovers;
            ExtraDaysOff = customDaysOff;
            ReportedReserveDays = daysInReserve;
            ReportedReserveDaysForLastSixMonths = daysInReserveLastSixMonth;
            YearData = yearData.ToArray();
            
            OverusedSickLeaveFine = overuserdSickLeaveFine;
            SickLeaveYearData = sickLeaveYearData.ToArray();
            if (employee.Employment != Constants.EmploymentTypeTM)
            {
                ProvidedSickLeaveDaysWithoutCertificate = (double)Constants.SickLeavesWithoutCert;
                ProvidedSickLeaveDaysWithCertificate = (double)Constants.SickLeaveDaysWithCertificate;
                ProvidedSickLeaveDaysToBeCompensated100 = (double)Constants.SickLeavesFullPay - (double)Constants.SickLeavesWithoutCert;
                ProvidedSickLeaveDaysToBeCompensated75 = (double)Constants.SickLeavesPartialPayWithCert;
                CurrentSickLeaveBalance = (double)Constants.SickLeavesTotal - usedSickLeaves;
            }
        }


    }
}
