﻿using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class RoleShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsCustom { get; set; }
        public bool IsSystemRole { get; set; }
        public ICollection<RolePermissionDTO> RolePermission { get; set; }
    }
}
