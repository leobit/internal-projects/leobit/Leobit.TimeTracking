using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class UserProjectDTO
    {
        public UserShortDTO User { get; set; }
        public ProjectShortDTO Project { get; set; }
        public DateTime? AssignmentDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Hours { get; set; }
        public decimal TotalInternalHours { get; set; }
    }
}