namespace Leobit.TimeTracking.BL.Models
{
    public class UserShortDTO
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int RoleId { get; set; }
    }
}
