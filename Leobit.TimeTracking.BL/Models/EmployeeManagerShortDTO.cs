﻿namespace Leobit.TimeTracking.BL.Models
{
    public class EmployeeManagerShortDTO
    {
        public EmployeeManagerShortDTO(string id, string fullName)
        {
            Id = id;
            FullName = fullName;
        }

        public string Id { get; set; }
        public string FullName { get; set; }
    }
}
