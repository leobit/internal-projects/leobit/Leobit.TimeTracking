namespace Leobit.TimeTracking.BL.Models
{
    public class ProjectManagerDTO
    {
        public UserShortDTO User { get; set; }
        public ProjectShortDTO Project { get; set; }
    }
}