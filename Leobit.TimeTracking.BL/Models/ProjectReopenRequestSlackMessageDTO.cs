﻿namespace Leobit.TimeTracking.BL.Models
{
    public class ProjectReopenRequestSlackMessageDTO
    {
        public UserSlackInfoDTO ReceiverSlackInfo { get; set; }
        public ProjectReopenRequestDTO ProjectReopenRequestInfo { get; set; }
    }
}
