﻿using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.DAL.Interfaces;
using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class RestrictionDTO //it's not DTO - it's should become a Model
    {
        public RestrictionDTO(Project project, Settings globalSettings, List<AllowedDateRangeDTO> allowedDateRange)
        {
            if (project.OverrideHoursSettings)
            {
                SetHoursRestrictionBasedOn(project);
            }
            else if (project.Account.OverrideHoursSettings)
            {
                SetHoursRestrictionBasedOn(project.Account);
            }
            else
            {
                SetHoursRestrictionBasedOn(globalSettings);
            }

            AllowedDateRanges = allowedDateRange;
        }

        internal void SetAllowedDateRanges(DateTime startDate, DateTime endDate)
        {
            AllowedDateRanges = new List<AllowedDateRangeDTO>
            {
                new(startDate, endDate)
            };
        }

        internal void SetAllowedDateRanges(List<AllowedDateRangeDTO> dateRanges)
        {
            AllowedDateRanges = dateRanges;
        }

        private void SetHoursRestrictionBasedOn<T>(T entity) where T : IHoursRestrictionRange
        {
            MinInternal = entity.MinInternal;
            MaxInternal = entity.MaxInternal;
            MinExternal = entity.MinExternal;
            MaxExternal = entity.MaxExternal;
        }

        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public ICollection<AllowedDateRangeDTO> AllowedDateRanges { get; set; }
    }

    public class AllowedDateRangeDTO
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }

        public AllowedDateRangeDTO(DateTime startDate, DateTime endDate)
        {
            StartDate = startDate;
            EndDate = endDate;
        }
    }
}
