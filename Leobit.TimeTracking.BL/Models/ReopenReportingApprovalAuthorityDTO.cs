﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class ReopenReportingApprovalAuthorityDTO
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime ReopenAllowedFromDate { get; set; }
        public DateTime? ReopenAllowedEndDate { get; set; }
    }
}
