﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Models
{
    public class UserSpreadsheetDTO
    {
        public string Username { get; set; }

        public string Employment { get; set; }

        public string Active { get; set; }

        public string Timestamp { get; set; }
    }
}
