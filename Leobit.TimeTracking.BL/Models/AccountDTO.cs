namespace Leobit.TimeTracking.BL.Models
{
    public class AccountDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string BudgetOwnerId { get; set; }
        public bool OverrideHoursSettings { get; set; }
        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public bool OverrideClosingSettings { get; set; }
        public short ClosingType { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
        public int AccountType { get; set; }
        public bool IsActive { get; set; }
    }

    public class AccountShortDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
