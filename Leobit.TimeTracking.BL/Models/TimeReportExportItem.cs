﻿using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.BL.Models
{
    public class TimeReportExportItem
    {
        public int Id { get; set; }
        public string Timestamp { get; set; }
        public string User { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public string WorkDescription { get; set; }
        public double InternalHours { get; set; }
        public double ExternalHours { get; set; }
        public bool IsOverhead { get; set; }
        public string ReportingDate { get; set; }

        public TimeReportExportItem(TimeReportDTO tr)
        {
            Id = tr.Id;
            Timestamp = tr.Timestamp.ToString("MM/dd/yyyy hh\\:mm\\:ss");
            User = $"{tr.User.FirstName} {tr.User.LastName}";
            Account = tr.Account.Name;
            Project = tr.Project.Name;
            WorkDescription = tr.WorkDescription;
            InternalHours = tr.InternalHours;
            ExternalHours = tr.ExternalHours;
            IsOverhead = tr.IsOverhead;
            ReportingDate = tr.ReportingDate.ToString("MM/dd/yyyy");
        }
    }

}
