﻿using System;
using System.Linq;

namespace Leobit.TimeTracking.BL.Models
{
    public class VacationStatisticModel
    {
        public VacationStatisticModel(string employeeId, string employeeFullName, double transferredBalance, double expectedEoyVacationBalance,
            double currentVacationBalance, double totalAtThisYear, double totalUsed, double totalUsedPercent, double totalUsedPercentPure, VacationMonthDataDTO[] monthData)
        {
            Id = employeeId;
            FullName = employeeFullName;
            TransferredBalance = transferredBalance;
            ExpectedEoyVacationBalance = expectedEoyVacationBalance;
            CurrentVacationBalance = currentVacationBalance;
            TotalAtThisYear = totalAtThisYear;
            TotalUsed = totalUsed;
            TotalUsedPercent = totalUsedPercent;
            TotalUsedPercentPure = totalUsedPercentPure;
            SetEmployeeUsedDays(monthData);
        }

        public VacationStatisticModel(string employeeId, string employeeFullName, double transferredBalance, double expectedEoyVacationBalance,
            double currentVacationBalance, double totalAtThisYear, double totalUsed, double totalUsedPercent, double totalUsedPercentPure)
        {
            Id = employeeId;
            FullName = employeeFullName;
            TransferredBalance = transferredBalance;
            ExpectedEoyVacationBalance = expectedEoyVacationBalance;
            CurrentVacationBalance = currentVacationBalance;
            TotalAtThisYear = totalAtThisYear;
            TotalUsed = totalUsed;
            TotalUsedPercent = totalUsedPercent;
            TotalUsedPercentPure = totalUsedPercentPure;
        }


        public string Id { get; set; }
        public string FullName { get; set; }
        public double TransferredBalance { get; set; }
        public double ExpectedEoyVacationBalance { get; set; }
        public double CurrentVacationBalance { get; set; }
        public double TotalAtThisYear { get; set; }
        public double TotalUsed { get; set; }
        public double TotalUsedPercent { get; set; }
        public double TotalUsedPercentPure { get; set; }
        public string[] UsedDaysMonthData { get; set; }

        internal void SetEmployeeUsedDays(VacationMonthDataDTO[] monthData)
        {
            UsedDaysMonthData ??= new string[12];

            for (int i = 0; i < UsedDaysMonthData.Length; i++)
            {
                UsedDaysMonthData[i] = GetUsedDays(monthData[i]);
            }
        }

        private string GetUsedDays(VacationMonthDataDTO monthData)
        {
            if (monthData == null || monthData.ExcludeFromYearlyTotal) return "";
            return monthData.Used.HasValue ? Math.Round(monthData.Used.Value, 1).ToString() : "0";
        }
    }
}