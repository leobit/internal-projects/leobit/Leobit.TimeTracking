﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Leobit.TimeTracking.BL.Models
{
    public class PermissionDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public bool IsProtected { get; set; }
    }
}
