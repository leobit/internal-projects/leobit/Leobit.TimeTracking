﻿namespace Leobit.TimeTracking.BL.Models
{
    public class FiltersPresetDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string UserId { get; set; }
        public EmployeeReportingFiltersDTO EmployeeReportingFilters { get; set; }
    }

    public class EmployeeReportingFiltersDTO
    {
        public DateRangeDTO DateRange { get; set; }
        public SelectedItemListDTO<int> ProjectIds { get; set; }
        public SelectedItemListDTO<string> UserIds { get; set; }
    }
}
