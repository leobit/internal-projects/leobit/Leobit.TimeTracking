﻿namespace Leobit.TimeTracking.BL.Models
{
    public class SentSlackMessageDTO
    {
        public string UserId { get; set; }
        public string TimeStamp { get; set; }
        public string ChannelId { get; set; }
    }
}
