﻿namespace Leobit.TimeTracking.BL.Models
{
    public class TransferredVacationBalanceDTO
    {
        public string UserId { get; set; }
        public int Year { get; set; }
        public decimal Transferred { get; set; }
        public int? CustomCredit { get; set; }
    }
}
