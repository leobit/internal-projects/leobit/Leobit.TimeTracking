﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public enum TimeReportActionType
    {
        Create,
        Read,
        Update,
        Delete
    }

    public class DuCase<T>
    {
        public T Case { get; set; }

        public DuCase(T caseOption)
        {
            Case = caseOption;
        }
    }

    public enum OperationalTimeReportProjectType
    {
        DayoffProject,
        SicknessProject,
        OtherProject
    }

    public class TimeReportSqsMessage
    {
        public SqsTimeReport TimeReport { get; set; }
        public DuCase<TimeReportActionType> Action { get; set; }
    }

    public class SqsTimeReport
    {
        public Employee Employee { get; set; }
        public DateTime Date { get; set; }
        public DuCase<OperationalTimeReportProjectType> Project { get; set; }
        public DateTime? StartTime { get; set; }
        public double? Duration { get; set; }
    }

    public class Employee
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
    }

}
