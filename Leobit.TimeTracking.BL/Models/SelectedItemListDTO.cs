﻿using System.Collections.Generic;

namespace Leobit.TimeTracking.BL.Models
{
    public class SelectedItemListDTO<T>
    {
        public ICollection<T> SelectedItems { get; set; }
        public bool? AllSelected { get; set; }
    }
}
