﻿namespace Leobit.TimeTracking.BL.Models
{
    public class BigQueryTimeReportDTO
    {
        public string Timestamp { get; set; }
        public string EmailAddress { get; set; }
        public string TimeStart { get; set; }
        public string TimeEnd { get; set; }
        public string InternalHours { get; set; }
        public string ExternalHours { get; set; }
        public string IsOverhead { get; set; }
        public string WorkDescription { get; set; }
        public string Account { get; set; }
        public string Project { get; set; }
        public string ReportingDate { get; set; }
        public string Month { get; set; }
        public string Week { get; set; }
        public string NonBillableHours { get; set; }
        public string CheckDifference { get; set; }
        public string Name { get; set; }
        public string Duplicate { get; set; }
        public string Division { get; set; }
        public string OrganizationUnit { get; set; }
        public string Category {  get; set; }
        public string ProjectPriority { get; set; }
        public string ProjectType { get; set; }
        public string BudgetOwnerId { get; set; }
    }
}
