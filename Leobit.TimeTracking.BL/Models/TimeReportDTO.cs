using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class TimeReportDTO
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public UserShortDTO User { get; set; }
        public AccountShortDTO Account { get; set; }
        public ProjectShortDTO Project { get; set; }
        public string WorkDescription { get; set; }
        public DateTime ReportingDate { get; set; }
        public double InternalHours { get; set; }
        public double ExternalHours { get; set; }
        public bool IsOverhead { get; set; }
        public DateTime? StartTime { get; set; }
        public bool IsEditable { get; set; }
    }
}
