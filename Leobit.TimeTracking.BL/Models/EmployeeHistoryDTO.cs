﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class EmployeeHistoryDTO
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Employment { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? Timestamp { get; set; }
    }
}
