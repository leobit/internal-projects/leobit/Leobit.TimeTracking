﻿using System;

namespace Leobit.TimeTracking.BL.Models
{
    public class HolidayDTO
    {
        public DateTime Date { get; set; }

        public string Name { get; set; }

        public int Year { get; set; }

        public int Month { get; set; }
    }
}
