﻿namespace Leobit.TimeTracking.BL
{
    public enum ProjectPriority : int
    {
        VeryLow = 0,
        Low = 1,
        Medium = 2,
        High = 3
    }
}
