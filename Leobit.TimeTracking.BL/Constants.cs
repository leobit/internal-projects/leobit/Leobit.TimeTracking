﻿using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.BL
{
    public static class Constants
    {
        public const double FullDayHours = 8.0;

        public const string EmploymentTypeTM = "T&M";
        public const string EmploymentTypeFT = "FT";

        public const ushort SixMonthTMEmployeesValidation = 6;

        public const int MonthsInYear = 12;

        //Vacation constants
        public const int NewAccrualSystemYear = 2024;
        public const double DefaultMaxTransferredDays = 5d;
        public const double MaxReserveDaysForVacationTransfer = 20d;
        public const decimal DailyWorkingHours = 8m;

        public static readonly DateTime CompanyDay2020 = new(2020, 7, 31);

        public const decimal UnpaidDayOffs = 10m;
        public const decimal SickLeavesPartialPayWithCert = 10m;
        public const decimal SickLeavesFullPayWithCert = 10m;
        public const decimal SickLeavesWithoutCert = 5m;
        public const decimal SickLeaveDaysWithCertificate = SickLeavesPartialPayWithCert + SickLeavesFullPayWithCert;
        public const decimal SickLeavesFullPay = SickLeavesFullPayWithCert + SickLeavesWithoutCert;
        public const decimal SickLeavesTotal = SickLeavesFullPay + SickLeavesPartialPayWithCert - SickLeavesWithoutCert;

        public static readonly List<AccrualRateRange> AccrualRates = new()
        {
            new AccrualRateRange { MinYears = 0, MaxYears = 2, Rate = 18m },
            new AccrualRateRange { MinYears = 2, MaxYears = 3, Rate = 20m },
            new AccrualRateRange { MinYears = 3, MaxYears = 4, Rate = 21m },
            new AccrualRateRange { MinYears = 4, MaxYears = 100, Rate = 22m }
        };
    }

    public class AccrualRateRange
    {
        public double MinYears { get; set; }
        public double MaxYears { get; set; }
        public decimal Rate { get; set; }
    }
}