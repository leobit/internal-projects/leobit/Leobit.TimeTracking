﻿using FluentAssertions;
using Leobit.TimeTracking.BL;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General.Infratstructure;
using Leobit.TimeTracking.Tests.VacationTests.Lib;
using Microsoft.EntityFrameworkCore;
using Moq;
using Moq.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.Extensions.Caching.Memory;
using Xunit;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.Tests.VacationTests
{
    public class InMemoryVacationServiceTests : VacationServiceTestsBase
    {
        private readonly Mock<IMemoryCache> _memoryCacheMock;

        public InMemoryVacationServiceTests() : base(useInMemoryDb: true)
        {
            _memoryCacheMock = new Mock<IMemoryCache>();
        }

        [Fact]
        public async Task TransferVacationBalance_ShouldAddNewBalanceForEmployeesWithoutExistingBalance()
        {
            // Arrange
            int year = 2023;

            var expectedTransferredBalance = 5;

            var testEmployeeData = new Employee
            {
                Id = "1",
                FullName = "Test Employee",
                FullTimeStartDate = new DateTime(2020, 1, 1),
                TerminationDate = null
            };

            await Context.Employee.AddAsync(testEmployeeData);

            await Context.SaveChangesAsync();

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(new List<MonthHoursDTO>());
            var vacationDataProvider = new VacationDataProvider(Context, operationalServiceMock.Object, _memoryCacheMock.Object);

            // Act
            var vacationService = new VacationService(Context, null, vacationDataProvider, _memoryCacheMock.Object);
            await vacationService.TransferVacationBalance(year);

            // Assert
            var transferredBalance = await Context.TransferredVacationBalance
                .SingleOrDefaultAsync(balance => balance.UserId == testEmployeeData.Id && balance.Year == year);

            transferredBalance.Should().NotBeNull();
            Assert.Equal(expectedTransferredBalance, transferredBalance.Transferred);
        }

        [Fact]
        public async Task TransferVacationBalance_ShouldUpdateExistingBalanceForEmployee()
        {
            // Arrange
            int year = 2023;

            var existingTransferredBalance = 3;
            var updatedTransferredBalance = 5;

            var testEmployeeData = new Employee
            {
                Id = "1",
                FullName = "Existing Employee",
                FullTimeStartDate = new DateTime(2020, 1, 1),
                TerminationDate = null
            };

            await Context.Employee.AddAsync(testEmployeeData);

            await Context.TransferredVacationBalance.AddAsync(new TransferredVacationBalance
            {
                UserId = "1",
                Year = year,
                Transferred = existingTransferredBalance
            });

            await Context.SaveChangesAsync();

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(new List<MonthHoursDTO>());
            var vacationDataProvider = new VacationDataProvider(Context, operationalServiceMock.Object, _memoryCacheMock.Object);

            // Act
            var vacationService = new VacationService(Context, null, vacationDataProvider, _memoryCacheMock.Object);
            await vacationService.TransferVacationBalance(year);

            // Assert
            var transferredBalance = await Context.TransferredVacationBalance
                .SingleOrDefaultAsync(balance => balance.UserId == "1" && balance.Year == year);

            transferredBalance.Should().NotBeNull();
            Assert.Equal(updatedTransferredBalance, transferredBalance.Transferred);
        }

        [Fact]
        public async Task TransferVacationBalance_ShouldIgnoreInactiveEmployees()
        {
            // Arrange
            int year = 2023;

            var activeEmployee = new Employee
            {
                Id = "1",
                FullName = "Active Employee",
                FullTimeStartDate = new DateTime(2020, 1, 1),
                TerminationDate = null
            };

            var inactiveEmployee = new Employee
            {
                Id = "2",
                FullName = "Inactive Employee",
                FullTimeStartDate = new DateTime(2019, 1, 1),
                TerminationDate = new DateTime(2022, 12, 31)
            };

            await Context.Employee.AddRangeAsync(activeEmployee, inactiveEmployee);
            await Context.SaveChangesAsync();

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(new List<MonthHoursDTO>());
            var vacationDataProvider = new VacationDataProvider(Context, operationalServiceMock.Object, _memoryCacheMock.Object);

            // Act
            var vacationService = new VacationService(Context, null, vacationDataProvider, _memoryCacheMock.Object);
            await vacationService.TransferVacationBalance(year);

            // Assert
            var activeBalance = await Context.TransferredVacationBalance
                .SingleOrDefaultAsync(balance => balance.UserId == activeEmployee.Id && balance.Year == year);

            var inactiveBalance = await Context.TransferredVacationBalance
                .SingleOrDefaultAsync(balance => balance.UserId == inactiveEmployee.Id && balance.Year == year);

            activeBalance.Should().NotBeNull();
            inactiveBalance.Should().BeNull();
        }

        [Fact]
        public async Task TransferVacationBalance_ShouldLimitTransferredDaysBasedOnReserveDaysAndDefaultMax_WithYearMinusOne()
        {
            // Arrange
            int year = 2023;
            const int DEFAULT_MAX_TRANSFERRED_DAYS = 5;
            const int MAX_RESERVE_DAYS_FOR_VACATION_TRANSFER = 20;

            var employees = new List<Employee>
            {
                new Employee
                {
                    Id = "1",
                    FullName = "Employee Within Limit",
                    FullTimeStartDate = new DateTime(2020, 1, 1),
                    TerminationDate = null
                },
                new Employee
                {
                    Id = "2",
                    FullName = "Employee Exceeding Limit",
                    FullTimeStartDate = new DateTime(2020, 1, 1),
                    TerminationDate = null
                }
            };
            await Context.Employee.AddRangeAsync(employees);

            var timeReports = new List<TimeReport>
            {
                new TimeReport { UserId = "1", ProjectId = 1, ReportingDate = new DateTime(year - 1, 1, 1), InternalHours = 120 }, // 15 days
                new TimeReport { UserId = "2", ProjectId = 2, ReportingDate = new DateTime(year - 1, 1, 1), InternalHours = 320 }  // 40 days
            };
            await Context.TimeReport.AddRangeAsync(timeReports);

            var projects = new List<Project>
            {
                new Project { Id = 1, ProjectPriority = (short)ProjectPriority.VeryLow },
                new Project { Id = 2, ProjectPriority = (short)ProjectPriority.Low }
            };
            await Context.Project.AddRangeAsync(projects);

            var balances = new List<TransferredVacationBalance>
            {
                new TransferredVacationBalance { UserId = "1", Year = year - 1, Transferred = 5 },
                new TransferredVacationBalance { UserId = "2", Year = year - 1, Transferred = 5 }
            };
            await Context.TransferredVacationBalance.AddRangeAsync(balances);
            await Context.SaveChangesAsync();

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(new List<MonthHoursDTO>());
            var vacationDataProvider = new VacationDataProvider(Context, operationalServiceMock.Object, _memoryCacheMock.Object);

            // Act
            var vacationService = new VacationService(Context, null, vacationDataProvider, _memoryCacheMock.Object);
            await vacationService.TransferVacationBalance(year);

            // Assert
            var balanceWithinLimit = await Context.TransferredVacationBalance
                .SingleOrDefaultAsync(balance => balance.UserId == "1" && balance.Year == year);

            var balanceExceedingLimit = await Context.TransferredVacationBalance
                .SingleOrDefaultAsync(balance => balance.UserId == "2" && balance.Year == year);

            balanceWithinLimit.Should().NotBeNull();
            Assert.Equal(Math.Min(5, DEFAULT_MAX_TRANSFERRED_DAYS), balanceWithinLimit.Transferred);

            balanceExceedingLimit.Should().NotBeNull();
            Assert.Equal(0, balanceExceedingLimit.Transferred);
        }

        [Fact]
        public async Task AreEmployeesOnVacation_ShouldReturnTrueIfUserIsOnUnpaidVacation()
        {
            // Arrange
            var date = new DateTime(2023, 7, 1);

            var userId = "1";

            var timeReport = new TimeReport
            {
                UserId = userId,
                ReportingDate = date,
                Project = new Project { ProjectType = (int)ProjectType.VacationUnpaid },
                InternalHours = 8
            };

            await Context.TimeReport.AddAsync(timeReport);
            await Context.SaveChangesAsync();

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            var result = await service.AreEmployeesOnVacation(new List<string> { userId }, date);

            // Assert
            Assert.NotNull(result);
            Assert.True(result[userId]);
        }

        [Fact]
        public async Task AreEmployeesOnVacation_ShouldReturnFalseIfUserIsNotOnVacation()
        {
            // Arrange
            var date = new DateTime(2023, 7, 1);

            var userId = "1";

            var timeReport = new TimeReport
            {
                UserId = userId,
                ReportingDate = date,
                Project = new Project { ProjectType = (int)ProjectType.Internal },
                InternalHours = 8
            };

            await Context.TimeReport.AddAsync(timeReport);
            await Context.SaveChangesAsync();

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            var result = await service.AreEmployeesOnVacation(new List<string> { userId }, date);

            // Assert
            Assert.NotNull(result);
            Assert.False(result[userId]);
        }

        [Fact]
        public async Task IsEmployeeOnVacation_ShouldReturnTrueIfUserIsOnVacation()
        {
            // Arrange
            var date = new DateTime(2023, 7, 1);

            var timeReport = new TimeReport
            {
                UserId = "1",
                ReportingDate = date,
                Project = new Project { ProjectType = (int)ProjectType.Vacation },
                InternalHours = 8
            };

            await Context.TimeReport.AddAsync(timeReport);
            await Context.SaveChangesAsync();

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            var result = await service.IsEmployeeOnVacation("1", date);

            // Assert
            Assert.True(result);
        }

        [Fact]
        public async Task IsEmployeeOnVacation_ShouldReturnFalseIfUserHasNoRecords()
        {
            // Arrange
            var date = new DateTime(2023, 7, 1);

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            var result = await service.IsEmployeeOnVacation("1", date);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task IsEmployeeOnVacation_ShouldReturnFalseIfUserIsNotOnVacation()
        {
            // Arrange
            var date = new DateTime(2023, 7, 1);

            var timeReport = new TimeReport
            {
                UserId = "1",
                ReportingDate = date,
                Project = new Project { ProjectType = (int)ProjectType.Internal },
                InternalHours = 8
            };

            await Context.TimeReport.AddAsync(timeReport);
            await Context.SaveChangesAsync();

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            var result = await service.IsEmployeeOnVacation("1", date);

            // Assert
            Assert.False(result);
        }

        [Fact]
        public async Task EditEmploymentDate_ShouldUpdateEmploymentDate_WhenDateIsProvided()
        {
            var user = new Employee
            {
                Id = "1",
                FullName = "Test User",
                FullTimeStartDate = new DateTime(2020, 1, 1)
            };
            await Context.Employee.AddAsync(user);
            await Context.SaveChangesAsync();

            var newEmploymentDate = "2023-07-01";

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            await service.EditEmploymentDate("1", newEmploymentDate);

            // Assert
            var updatedUser = await Context.Employee.FirstOrDefaultAsync(x => x.Id == "1");
            Assert.NotNull(updatedUser);
            Assert.Equal(DateTime.Parse(newEmploymentDate), updatedUser.FullTimeStartDate);
        }

        [Fact]
        public async Task EditEmploymentDate_ShouldSetEmploymentDateToNull_WhenDateIsNotProvided()
        {
            // Arrange
            var user = new Employee
            {
                Id = "1",
                FullName = "Test User",
                FullTimeStartDate = new DateTime(2020, 1, 1)
            };
            await Context.Employee.AddAsync(user);
            await Context.SaveChangesAsync();

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            await service.EditEmploymentDate("1", null);

            // Assert
            var updatedUser = await Context.Employee.FirstOrDefaultAsync(x => x.Id == "1");
            Assert.NotNull(updatedUser);
            Assert.Null(updatedUser.FullTimeStartDate);
        }

        [Fact]
        public async Task EditEmploymentDate_ShouldThrowNotFoundException_WhenUserDoesNotExist()
        {
            // Arrange
            var testEmployeeId = "1";
            var testEmployeeDate = "2023-07-01";

            // Act & Assert
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            await Assert.ThrowsAsync<NotFoundException>(() => service.EditEmploymentDate(testEmployeeId, testEmployeeDate));
        }

        [Fact]
        public async Task DeleteCustomDayOff_ShouldRemoveCustomDayOff_WhenRecordExists()
        {
            // Arrange
            var customDayOff = new CustomDayOff
            {
                Id = 1,
                UserId = "1",
                Year = 2023,
                Month = 7,
                ExtraDayOff = 1.5m
            };

            await Context.CustomDayOff.AddAsync(customDayOff);
            await Context.SaveChangesAsync();

            // Act
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            await service.DeleteCustomDayOff(1);

            // Assert
            var deletedDayOff = await Context.CustomDayOff.FirstOrDefaultAsync(x => x.Id == 1);
            Assert.Null(deletedDayOff);
        }

        [Fact]
        public async Task DeleteCustomDayOff_ShouldThrowNotFoundException_WhenRecordDoesNotExist()
        {
            // Arrange & Act & Assert
            var service = new VacationService(Context, null, null, _memoryCacheMock.Object);
            await Assert.ThrowsAsync<NotFoundException>(() => service.DeleteCustomDayOff(99));
        }

        [Fact]
        public async Task EditCustomDayOff_ShouldUpdateCustomDayOff_WhenAllDataIsValid()
        {
            // Arrange
            var customDayOff = new CustomDayOff
            {
                Id = 23,
                UserId = "1",
                Year = 2023,
                Month = 7,
                ExtraDayOff = 1.5m
            };
            var user = new User { Id = "1", FirstName = "Test User" };
            var editor = new User { Id = "2", FirstName = "Editor User" };

            await Context.CustomDayOff.AddAsync(customDayOff);
            await Context.User.AddRangeAsync(user, editor);
            await Context.SaveChangesAsync();
            Context.ChangeTracker.Clear();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            var updatedDTO = new CustomDayOffDTO
            {
                Id = 23,
                User = new UserShortDTO { Id = "1", FirstName = "Test User" },
                Year = 2023,
                Month = 8,
                ExtraDayOff = 2.0m
            };

            // Act
            var result = await service.EditCustomDayOff("2", updatedDTO);

            // Assert
            var updatedEntity = await Context.CustomDayOff.FirstOrDefaultAsync(x => x.Id == 23);
            Assert.NotNull(updatedEntity);
            Assert.Equal(8, updatedEntity.Month);
            Assert.Equal(2.0m, updatedEntity.ExtraDayOff);
            Assert.Equal("2", updatedEntity.LastEditorId);
            Assert.NotNull(result);
            Assert.Equal(updatedDTO.Month, result.Month);
        }

        [Fact]
        public async Task EditCustomDayOff_ShouldThrowConflictException_WhenMonthIsNegative()
        {
            // Arrange
            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);
            var invalidDTO = new CustomDayOffDTO
            {
                Id = 1,
                User = new UserShortDTO { Id = "1" },
                Year = 2023,
                Month = -1,
                ExtraDayOff = 2.0m
            };

            // Act & Assert
            await Assert.ThrowsAsync<ConflictException>(() => service.EditCustomDayOff("2", invalidDTO));
        }

        [Fact]
        public async Task CreateCustomDayOff_ShouldCreateCustomDayOff_WhenAllDataIsValid()
        {
            // Arrange
            var employee = new Employee
            {
                Id = "1",
                StartDate = new DateTime(2023, 1, 1),
                TerminationDate = null
            };
            var editor = new User { Id = "2", FirstName = "Editor User" };

            await Context.Employee.AddAsync(employee);
            await Context.User.AddAsync(editor);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            var newDTO = new CustomDayOffDTO
            {
                User = new UserShortDTO { Id = "1" },
                Year = 2023,
                Month = 7,
                ExtraDayOff = 1.5m
            };

            // Act
            var result = await service.CreateCustomDayOff("2", newDTO);

            // Assert
            var createdEntity = await Context.CustomDayOff.FirstOrDefaultAsync(x => x.UserId == "1");
            Assert.NotNull(createdEntity);
            Assert.Equal(7, createdEntity.Month);
            Assert.Equal(1.5m, createdEntity.ExtraDayOff);
            Assert.Equal("2", createdEntity.LastEditorId);
            Assert.NotNull(result);
            Assert.Equal(newDTO.Month, result.Month);
        }

        [Fact]
        public async Task CreateCustomDayOff_ShouldThrowConflictException_WhenMonthIsNegative()
        {
            // Arrange
            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            var invalidDTO = new CustomDayOffDTO
            {
                User = new UserShortDTO { Id = "1" },
                Year = 2023,
                Month = -1,
                ExtraDayOff = 1.5m
            };

            // Act & Assert
            await Assert.ThrowsAsync<ConflictException>(() => service.CreateCustomDayOff("2", invalidDTO));
        }

        [Fact]
        public async Task CreateCustomDayOff_ShouldThrowNotFoundException_WhenUserOrEditorDoesNotExist()
        {
            // Arrange
            var editor = new User { Id = "2", FirstName = "Editor User" };
            await Context.User.AddAsync(editor);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            var newDTO = new CustomDayOffDTO
            {
                User = new UserShortDTO { Id = "99" }, // Неіснуючий користувач
                Year = 2023,
                Month = 7,
                ExtraDayOff = 1.5m
            };

            // Act & Assert
            await Assert.ThrowsAsync<NotFoundException>(() => service.CreateCustomDayOff("2", newDTO));
        }

        [Fact]
        public async Task CreateCustomDayOff_ShouldThrowInvalidOperationException_WhenDatesAreOutOfRange()
        {
            // Arrange
            var employee = new Employee
            {
                Id = "1",
                StartDate = new DateTime(2023, 1, 1),
                TerminationDate = new DateTime(2023, 6, 30)
            };
            var editor = new User { Id = "2", FirstName = "Editor User" };

            await Context.Employee.AddAsync(employee);
            await Context.User.AddAsync(editor);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            var invalidDTO = new CustomDayOffDTO
            {
                User = new UserShortDTO { Id = "1" },
                Year = 2023,
                Month = 7,
                ExtraDayOff = 1.5m
            };

            // Act & Assert
            await Assert.ThrowsAsync<InvalidOperationException>(() => service.CreateCustomDayOff("2", invalidDTO));
        }

        [Fact]
        public async Task CreateCustomDayOff_ShouldCreateCustomDayOff_WhenDatesAreWithinRangeForTerminatedEmployee()
        {
            // Arrange
            var employee = new Employee
            {
                Id = "1",
                StartDate = new DateTime(2023, 1, 1),
                TerminationDate = new DateTime(2023, 7, 31)
            };
            var editor = new User { Id = "2", FirstName = "Editor User" };

            await Context.Employee.AddAsync(employee);
            await Context.User.AddAsync(editor);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            var validDTO = new CustomDayOffDTO
            {
                User = new UserShortDTO { Id = "1" },
                Year = 2023,
                Month = 7, // У межах діапазону
                ExtraDayOff = 1.5m
            };

            // Act
            var result = await service.CreateCustomDayOff("2", validDTO);

            // Assert
            var createdEntity = await Context.CustomDayOff.FirstOrDefaultAsync(x => x.UserId == "1");
            Assert.NotNull(createdEntity);
            Assert.Equal(7, createdEntity.Month);
            Assert.Equal(1.5m, createdEntity.ExtraDayOff);
            Assert.Equal("2", createdEntity.LastEditorId);
            Assert.NotNull(result);
            Assert.Equal(validDTO.Month, result.Month);
        }


        [Fact]
        public async Task GetAllCustomDayOffsByYear_ShouldReturnCustomDayOffsForSpecifiedYear()
        {
            // Arrange
            var employee = new User { Id = "1", FirstName = "Test User" };
            var editor = new User { Id = "2", FirstName = "Editor User" };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff
                {
                    Id = 1,
                    UserId = "1",
                    LastEditorId = "2",
                    Year = 2023,
                    Month = 7,
                    ExtraDayOff = 1.5m,
                    LastEditTimestamp = new DateTime(2023, 7, 15)
                },
                new CustomDayOff
                {
                    Id = 2,
                    UserId = "1",
                    LastEditorId = "2",
                    Year = 2023,
                    Month = 8,
                    ExtraDayOff = 2.0m,
                    LastEditTimestamp = new DateTime(2023, 8, 10)
                },
                new CustomDayOff
                {
                    Id = 3,
                    UserId = "1",
                    LastEditorId = "2",
                    Year = 2022,
                    Month = 12,
                    ExtraDayOff = 1.0m,
                    LastEditTimestamp = new DateTime(2022, 12, 31)
                }
            };

            await Context.User.AddRangeAsync(employee, editor);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetAllCustomDayOffsByYear(2023);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count);
            Assert.Equal(8, result.First().Month);
            Assert.Equal(7, result.Last().Month);
        }

        [Fact]
        public async Task GetAllCustomDayOffsByYear_ShouldReturnEmptyList_WhenNoRecordsForYear()
        {
            // Arrange
            var customDayOff = new CustomDayOff
            {
                Id = 1,
                UserId = "1",
                LastEditorId = "2",
                Year = 2022,
                Month = 12,
                ExtraDayOff = 1.0m,
                LastEditTimestamp = new DateTime(2022, 12, 31)
            };

            await Context.CustomDayOff.AddAsync(customDayOff);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetAllCustomDayOffsByYear(2023);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldReturnAllRecordsForSpecifiedUsers()
        {
            // Arrange
            var users = new List<User>
            {
                new User { Id = "1", FirstName = "User 1" },
                new User { Id = "2", FirstName = "User 2" }
            };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff
                {
                    Id = 1,
                    UserId = "1",
                    LastEditorId = "2",
                    Year = 2023,
                    Month = 7,
                    ExtraDayOff = 1.5m
                },
                new CustomDayOff
                {
                    Id = 2,
                    UserId = "2",
                    LastEditorId = "1",
                    Year = 2023,
                    Month = 8,
                    ExtraDayOff = 2.0m
                },
                new CustomDayOff
                {
                    Id = 3,
                    UserId = "2",
                    LastEditorId = "1",
                    Year = 2023,
                    Month = 9,
                    ExtraDayOff = 3.0m
                }
            };

            await Context.User.AddRangeAsync(users);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(new List<string> { "1", "2" });

            // Assert
            Assert.NotNull(result);
            Assert.Equal(3, result.Count);
            Assert.Contains(result, c => c.User.Id == "1");
            Assert.Contains(result, c => c.User.Id == "2");
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldReturnRecordsForSpecifiedYear()
        {
            // Arrange
            var users = new List<User>
            {
                new User { Id = "1", FirstName = "User 1" },
                new User { Id = "2", FirstName = "User 2" }
            };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff { Id = 1, UserId = "1", LastEditorId = "2", Year = 2023, Month = 7, ExtraDayOff = 1.5m },
                new CustomDayOff { Id = 2, UserId = "1", LastEditorId = "2", Year = 2022, Month = 8, ExtraDayOff = 2.0m }
            };

            await Context.User.AddRangeAsync(users);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(new List<string> { "1" }, year: 2023);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result);
            Assert.Equal(2023, result.First().Year);
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldReturnRecordsForSpecifiedYearAndMonth()
        {
            // Arrange
            var users = new List<User>
            {
                new User { Id = "1", FirstName = "User 1" },
                new User { Id = "2", FirstName = "User 2" }
            };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff { Id = 1, UserId = "1", LastEditorId = "2", Year = 2023, Month = 7, ExtraDayOff = 1.5m },
                new CustomDayOff { Id = 2, UserId = "1", LastEditorId = "2", Year = 2023, Month = 8, ExtraDayOff = 2.0m }
            };

            await Context.User.AddRangeAsync(users);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(new List<string> { "1" }, year: 2023, month: 7);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result);
            Assert.Equal(7, result.First().Month);
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldReturnEmptyList_WhenNoRecordsExist()
        {
            // Arrange
            var customDayOff = new CustomDayOff
            {
                Id = 1,
                UserId = "1",
                LastEditorId = "2",
                Year = 2022,
                Month = 7,
                ExtraDayOff = 1.5m
            };

            await Context.CustomDayOff.AddAsync(customDayOff);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(new List<string> { "1" }, year: 2023);

            // Assert
            Assert.NotNull(result);
            Assert.Empty(result);
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldReturnAllRecordsForUser()
        {
            // Arrange
            var userId = "1";
            var lastEditorUserId = "2";
            
            var users = new List<User>
            {
                new User { Id = userId, FirstName = "User 1" },
                new User { Id = lastEditorUserId, FirstName = "User 2" }
            };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff { Id = 1, UserId = userId, LastEditorId = lastEditorUserId, Year = 2023, Month = 1, ExtraDayOff = 1.5m },
                new CustomDayOff { Id = 2, UserId = userId, LastEditorId = lastEditorUserId, Year = 2023, Month = 2, ExtraDayOff = 2.0m }
            };

            await Context.User.AddRangeAsync(users);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(userId);

            // Assert
            Assert.NotNull(result);
            Assert.Equal(2, result.Count); 
            Assert.All(result, r => Assert.Equal(userId, r.User.Id));
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldFilterByYear()
        {
            // Arrange
            var userId = "1";
            var lastEditorUserId = "2";

            var users = new List<User>
            {
                new User { Id = userId, FirstName = "User 1" },
                new User { Id = lastEditorUserId, FirstName = "User 2" }
            };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff { Id = 1, UserId = userId, LastEditorId = lastEditorUserId, Year = 2023, Month = 1, ExtraDayOff = 1.5m },
                new CustomDayOff { Id = 2, UserId = userId, LastEditorId = lastEditorUserId, Year = 2022,  Month = 2, ExtraDayOff = 2.0m }
            };

            await Context.User.AddRangeAsync(users);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(userId, year: 2023);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result); // Має повернути один запис за 2023 рік
            Assert.All(result, r => Assert.Equal(2023, r.Year));
        }

        [Fact]
        public async Task GetCustomDayOffs_ShouldFilterByYearAndMonth()
        {
            // Arrange
            var userId = "1";
            var lastEditorUserId = "2";

            var users = new List<User>
            {
                new User { Id = userId, FirstName = "User 1" },
                new User { Id = lastEditorUserId, FirstName = "User 2" }
            };

            var customDayOffs = new List<CustomDayOff>
            {
                new CustomDayOff { Id = 1, UserId = userId, LastEditorId = lastEditorUserId, Year = 2023, Month = 1, ExtraDayOff = 1.5m },
                new CustomDayOff { Id = 2, UserId = userId, LastEditorId = lastEditorUserId, Year = 2023, Month = 2, ExtraDayOff = 2.0m }
            };

            await Context.User.AddRangeAsync(users);
            await Context.CustomDayOff.AddRangeAsync(customDayOffs);
            await Context.SaveChangesAsync();

            var service = new VacationService(Context, Mapper, null, _memoryCacheMock.Object);

            // Act
            var result = await service.GetCustomDayOffs(userId, year: 2023, month: 1);

            // Assert
            Assert.NotNull(result);
            Assert.Single(result); 
            Assert.Equal(1, result.First().Month);
        }

        //[Fact]
        //public async Task GetAllEmployeesBallance_ShouldReturnAllActiveEmployees()
        //{
        //    // Arrange
        //    var employees = new List<Employee>
        //    {
        //        new Employee { Id = "1", FullName = "Employee 1", FullTimeStartDate = new DateTime(2020, 1, 1), Active = true },
        //        new Employee { Id = "2", FullName = "Employee 2", FullTimeStartDate = new DateTime(2021, 1, 1), Active = true },
        //        new Employee { Id = "3", FullName = "Employee 3", FullTimeStartDate = new DateTime(2022, 1, 1), Active = false } 
        //    };

        //    var balances = new List<TransferredVacationBalance>
        //    {
        //        new TransferredVacationBalance { Year = 2023, UserId = "1", Transferred = 5 },
        //        new TransferredVacationBalance { Year = 2023, UserId = "2", Transferred = 10 }
        //    };

        //    await Context.Employee.AddRangeAsync(employees);
        //    await Context.TransferredVacationBalance.AddRangeAsync(balances);
        //    await Context.SaveChangesAsync();

        //    var service = new VacationService(Context, null, Mapper);

        //    // Act
        //    var result = await service.GetAllEmployeesBallance(2023, null, null);

        //    // Assert
        //    Assert.NotNull(result);
        //    Assert.Equal(2, result.Count); 
        //    Assert.Contains(result, x => x.Id == "1");
        //    Assert.Contains(result, x => x.Id == "2");
        //}

        //[Fact]
        //public async Task GetAllEmployeesBallance_ShouldFilterByManager()
        //{
        //    // Arrange
        //    var testEmployeeId = "1";

        //    var employees = new List<Employee>
        //    {
        //        new Employee { Id = testEmployeeId, FullName = "Employee 1", FullTimeStartDate = new DateTime(2020, 1, 1), Active = true, Manager = "Manager A" },
        //        new Employee { Id = "2", FullName = "Employee 2", FullTimeStartDate = new DateTime(2021, 1, 1), Active = true, Manager = "Manager B" }
        //    };

        //    var manager = new Employee { Id = "100", FullName = "Manager A" };

        //    await Context.Employee.AddRangeAsync(employees);
        //    await Context.Employee.AddAsync(manager);
        //    await Context.SaveChangesAsync();

        //    var service = new VacationService(Context, null, Mapper);

        //    // Act
        //    var result = await service.GetAllEmployeesBallance(2023, manager.Id, null);
        //    var testEmployeeBalanceInfo = result.FirstOrDefault(balance => balance.Id == testEmployeeId);

        //    // Assert
        //    result.Should().NotBeEmpty();
        //    Assert.NotNull(testEmployeeBalanceInfo);
        //    result.Should().HaveCount(1);
        //    Assert.Equal(testEmployeeId, testEmployeeBalanceInfo.Id);
        //}

    }
}
