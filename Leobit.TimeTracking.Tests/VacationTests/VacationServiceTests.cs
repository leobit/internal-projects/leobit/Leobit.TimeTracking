﻿using FluentAssertions;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General.Infratstructure;
using Leobit.TimeTracking.Tests.VacationTests.Lib;
using Moq;
using Moq.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Helpers;
using Microsoft.Extensions.Caching.Memory;
using Xunit;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.Tests.VacationTests
{
    public class VacationServiceTests : VacationServiceTestsBase
    {
        private readonly IMemoryCache _memoryCache = new MemoryCache(new MemoryCacheOptions());

        [Fact]
        public async Task GetVacationsInformation_ShouldReturnEmptyList_WhenNoUserIdsProvided()
        {
            // Arrange
            var userIds = new List<string>();
            var year = 2024;

            var mockDbContext = new Mock<LeobitContext>();

            var vacationService = new VacationService(mockDbContext.Object, null, null, _memoryCache);

            // Act
            var result = await vacationService.GetVacationsInformation(userIds, year);

            // Assert
            result.Should().BeNullOrEmpty();
        }

        [Fact]
        public async Task GetVacationsInformation_WhenEmployeeFTShouldReturnValidVacationInfo()
        {
            // Arrange
            var userIds = new List<string> { VacationMoqDataHelper.EmployeeFTEmployment.Id };
            var year = 2024;

            var expectedStandingYears = 4.1d;
            var expectedEoyVacationBalance = 21.1;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = new List<Employee> { VacationMoqDataHelper.EmployeeFTEmployment };
            var testTimeReportData = new List<TimeReport>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryOnlyFT;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(testEmployeeData);
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(testTimeReportData);
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseline => baseline.Year == year).ToList());
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);

            // Act
            var result = await vacationService.GetVacationsInformation(userIds, year);
            var vacationResult = result.FirstOrDefault();

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().BeApproximately(expectedStandingYears, 0.1);
            vacationResult.ExpectedEoyVacationBalance.Should().BeApproximately(expectedEoyVacationBalance, 0.1);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().BeApproximately(expectedYearData[i].Accrual, 0.1);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }

        [Fact]
        public async Task GetVacationsInformation_WhenEmployeeAlwaysOnTM_ShouldNotAccumulateSeniority()
        {
            // Arrange
            var userIds = new List<string> { VacationMoqDataHelper.EmployeeFTEmployment.Id };
            int? year = 2024;

            var expectedStandingYears = 0;
            var expectedEoyVacationBalance = 0;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = VacationMoqDataHelper.EmployeeTMEmployment;
            var testTimeReportData = Enumerable.Empty<TimeReport>();
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryOnlyTM;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(testTimeReportData);
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            // Act
            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);
            var result = await vacationService.GetVacationsInformation(userIds, year.Value);
            var vacationResult = result.FirstOrDefault();

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().Be(expectedStandingYears);
            vacationResult.ExpectedEoyVacationBalance.Should().Be(expectedEoyVacationBalance);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().Be(expectedYearData[i].Accrual);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }

        [Fact]
        public async Task GetVacationsInformation_ShouldAccumulateSeniority_WhenEmployeeWasLessThanSixMonthOnTM()
        {
            // Arrange
            var userIds = new List<string> { VacationMoqDataHelper.EmployeeFTEmployment.Id };
            int? year = 2024;

            var expectedStandingYears = 4.1d;
            var expectedEoyVacationBalance = 21.1;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = VacationMoqDataHelper.EmployeeFTEmployment;
            var testTimeReportData = Enumerable.Empty<TimeReport>();
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryWithTMLessThanSixMonth;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(testTimeReportData);
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            // Act
            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);
            var result = await vacationService.GetVacationsInformation(userIds, year.Value);
            var vacationResult = result.FirstOrDefault();

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().BeApproximately(expectedStandingYears, 0.1);
            vacationResult.ExpectedEoyVacationBalance.Should().BeApproximately(expectedEoyVacationBalance, 0.1);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().BeApproximately(expectedYearData[i].Accrual, 0.1);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }


        [Fact]
        public async Task GetVacationInformation_ShouldThrowNotFoundException_WhenEmployeeDoesNotExist()
        {
            // Arrange
            var userId = "invalid-id";
            var year = 2024;

            var testEmployeeData = new List<Employee> { VacationMoqDataHelper.EmployeeFTEmployment };

            var testTimereportData = new List<TimeReport>();

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(testEmployeeData);
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(testTimereportData);
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, null, _memoryCache);

            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);

            // Act & Assert
            await Assert.ThrowsAsync<NotFoundException>(() => vacationService.GetVacationInformation(userId, year));
        }

        [Fact]
        public async Task GetVacationInformation_WhenEmployeeFTShouldReturnValidVacationInfo()
        {
            // Arrange
            var userId = "bv";
            int? year = 2024;

            var expectedStandingYears = 4.1d;
            var expectedEoyVacationBalance = 21.1;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = VacationMoqDataHelper.EmployeeFTEmployment;
            var testTimeReportData = VacationMoqDataHelper.EmployeeBillableTimeReport;
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryOnlyFT;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(new List<TimeReport> { testTimeReportData });
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            // Act
            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);
            var vacationResult = await vacationService.GetVacationInformation(userId, year.Value);

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().BeApproximately(expectedStandingYears, 0.1);
            vacationResult.ExpectedEoyVacationBalance.Should().BeApproximately(expectedEoyVacationBalance, 0.1);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().BeApproximately(expectedYearData[i].Accrual, 0.1);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }

        [Fact]
        public async Task GetVacationInformation_WhenEmployeeAlwaysOnTM_ShouldNotAccumulateSeniority()
        {
            // Arrange
            var userId = "bv";
            int? year = 2024;

            var expectedStandingYears = 0;
            var expectedEoyVacationBalance = 0;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 0, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = VacationMoqDataHelper.EmployeeTMEmployment;
            var testTimeReportData = VacationMoqDataHelper.EmployeeBillableTimeReport;
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryOnlyTM;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(new List<TimeReport> { testTimeReportData });
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            // Act
            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);
            var vacationResult = await vacationService.GetVacationInformation(userId, year.Value);

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().Be(expectedStandingYears);
            vacationResult.ExpectedEoyVacationBalance.Should().Be(expectedEoyVacationBalance);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().Be(expectedYearData[i].Accrual);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }

        [Fact]
        public async Task GetVacationInformation_ShouldAccumulateSeniority_WhenEmployeeWasLessThanSixMonthOnTM()
        {
            // Arrange
            var userId = "bv";
            int? year = 2024;

            var expectedStandingYears = 4.1d;
            var expectedEoyVacationBalance = 21.1;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.83, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = VacationMoqDataHelper.EmployeeFTEmployment;
            var testTimeReportData = VacationMoqDataHelper.EmployeeReserveTimeReport;
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryWithTMLessThanSixMonth;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(new List<TimeReport>{ testTimeReportData });
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());

            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            // Act
            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);
            var vacationResult = await vacationService.GetVacationInformation(userId, year.Value);

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().BeApproximately(expectedStandingYears, 0.1);
            vacationResult.ExpectedEoyVacationBalance.Should().BeApproximately(expectedEoyVacationBalance, 0.1);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().BeApproximately(expectedYearData[i].Accrual, 0.1);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }

        [Fact]
        public async Task GetVacationInformation_ShouldResetVacationCoef_WhenEmployeeWasMoreThanSixMonthOnTM()
        {
            // Arrange
            var userId = "bv";
            int? year = 2024;

            var expectedStandingYears = 4.1;
            var expectedEoyVacationBalance = 20.16;
            var expectedYearData = new List<VacationMonthDataDTO>
            {
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.66, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
                new(){ Accrual = 1.75, Used = 0, UsedUnpaid = 0 },
            };

            var testEmployeeData = VacationMoqDataHelper.EmployeeFTEmployment;
            var testTimeReportData = VacationMoqDataHelper.EmployeeReserveTimeReport;
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryWithTMMoreThanSixMonth;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(new List<TimeReport> { testTimeReportData });
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            // Act
            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);
            var vacationResult = await vacationService.GetVacationInformation(userId, year.Value);

            // Assert
            vacationResult.Should().NotBeNull();
            vacationResult.StandingYears.Should().BeApproximately(expectedStandingYears, 0.1);
            vacationResult.ExpectedEoyVacationBalance.Should().BeApproximately(expectedEoyVacationBalance, 0.1);

            vacationResult.YearData.Should().HaveCount(expectedYearData.Count);
            for (int i = 0; i < expectedYearData.Count; i++)
            {
                vacationResult.YearData[i].Accrual.Should().BeApproximately(expectedYearData[i].Accrual, 0.1);
                vacationResult.YearData[i].Used.Should().Be(expectedYearData[i].Used);
                vacationResult.YearData[i].UsedUnpaid.Should().Be(expectedYearData[i].UsedUnpaid);
            }
        }

        [Fact]
        public async Task GetManagersList_ShouldNotBeEmptyWhenManagersExist()
        {
            // Arrange
            var testEmployeeData = new List<Employee>
            {
                new Employee
                {
                    Id = "1",
                    FullName = "Manager One",
                    Active = true,
                    FullTimeStartDate = new DateTime(2020, 1, 1),
                    Manager = "Executive Manager",
                    TerminationDate = null
                },
                new Employee
                {
                    Id = "2",
                    FullName = "Employee One",
                    Active = true,
                    FullTimeStartDate = new DateTime(2020, 1, 1),
                    Manager = "Manager One",
                    TerminationDate = null
                },
                new Employee
                {
                    Id = "3",
                    FullName = "Manager Two",
                    Active = true,
                    FullTimeStartDate = new DateTime(2020, 1, 1),
                    Manager = "Executive Manager",
                    TerminationDate = null
                },
                new Employee
                {
                    Id = "4",
                    FullName = "Employee Two",
                    Active = false, // Not active, should be ignored
                    FullTimeStartDate = new DateTime(2020, 1, 1),
                    Manager = "Manager Two",
                    TerminationDate = null
                }
            };

            var expectedManagers = new List<EmployeeManagerShortDTO>
            {
                new ("1", "Manager One"),
            };

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(testEmployeeData);
            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, null, _memoryCache);

            var vacationService = new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);

            // Act
            var result = await vacationService.GetManagersList();

            // Assert
            result.Should().NotBeNull();
            result.Should().HaveCount(expectedManagers.Count);
            result.Should().BeEquivalentTo(expectedManagers, options => options.WithStrictOrdering());
        }

        [Fact]
        public async Task GetManagersList_ShouldBeEmptyWhenManagersDoNotExist()
        {
            var mockLoeobitDbContext = new Mock<LeobitContext>();

            mockLoeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(Enumerable.Empty<Employee>());

            var vacationService = new VacationService(mockLoeobitDbContext.Object, null, null, _memoryCache);

            // Act
            var result = await vacationService.GetManagersList();

            // Assert
            result.Should().BeEmpty();
        }

        [Fact]
        public async Task GetVacationInfo_ShouldReturnFromCache_WhenCalledMultipleTimes()
        {
            string userId = "bv";
            int year = 2024;

            var testEmployeeData = VacationMoqDataHelper.EmployeeFTEmployment;
            var testTimeReportData = VacationMoqDataHelper.EmployeeReserveTimeReport;
            var testCustomDayOffData = Enumerable.Empty<CustomDayOff>();
            var testTransferredVacationBalanceData = new List<TransferredVacationBalance>();
            var testEmployeeHistoryData = VacationMoqDataHelper.EmployeeHistoryWithTMLessThanSixMonth;

            var mockLeobitDbContext = new Mock<LeobitContext>();

            mockLeobitDbContext.Setup(db => db.Employee).ReturnsDbSet(new List<Employee> { testEmployeeData });
            mockLeobitDbContext.Setup(db => db.TimeReport).ReturnsDbSet(new List<TimeReport> { testTimeReportData });
            mockLeobitDbContext.Setup(db => db.TransferredVacationBalance).ReturnsDbSet(testTransferredVacationBalanceData);
            mockLeobitDbContext.Setup(db => db.EmployeeHistory).ReturnsDbSet(testEmployeeHistoryData);
            mockLeobitDbContext.Setup(db => db.CustomDayOff).ReturnsDbSet(Enumerable.Empty<CustomDayOff>());

            var operationalServiceMock = new Mock<IOperationalService>();
            operationalServiceMock.Setup(s => s.GetBaseLine(year, null))
                .ReturnsAsync(VacationMoqDataHelper.BaselineMonthHours.Where(baseLine => baseLine.Year == year).ToList());

            var vacationDataProvider = new VacationDataProvider(mockLeobitDbContext.Object, operationalServiceMock.Object, _memoryCache);

            var vacationService =
                new VacationService(mockLeobitDbContext.Object, null, vacationDataProvider, _memoryCache);

            await vacationService.GetVacationInformation(userId, year);
            await vacationService.GetVacationInformation(userId, year);

            mockLeobitDbContext.Verify(c => c.Employee, Times.AtMostOnce);
            mockLeobitDbContext.Verify(c => c.TransferredVacationBalance, Times.AtMostOnce);
            mockLeobitDbContext.Verify(c => c.EmployeeHistory, Times.AtMost(2));
            mockLeobitDbContext.Verify(c => c.CustomDayOff, Times.AtMostOnce);
        }
    }
}