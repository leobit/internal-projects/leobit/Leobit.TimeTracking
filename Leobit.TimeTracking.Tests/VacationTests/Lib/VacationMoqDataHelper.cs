﻿using Leobit.TimeTracking.BL;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using Employee = Leobit.TimeTracking.DAL.Entities.Employee;

namespace Leobit.TimeTracking.Tests.VacationTests.Lib
{
    public static class VacationMoqDataHelper
    {
        public static List<MonthHoursDTO> BaselineMonthHours = new List<MonthHoursDTO>
        {
            new MonthHoursDTO { Year = 2024, Month = 1, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 2, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 3, Hours = 160 },
            new MonthHoursDTO { Year = 2024, Month = 4, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 5, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 6, Hours = 144 },
            new MonthHoursDTO { Year = 2024, Month = 7, Hours = 184 },
            new MonthHoursDTO { Year = 2024, Month = 8, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 9, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 10, Hours = 176 },
            new MonthHoursDTO { Year = 2024, Month = 11, Hours = 168 },
            new MonthHoursDTO { Year = 2024, Month = 12, Hours = 168 }
        };

        #region Employee

        public static Employee EmployeeFTEmployment = new Employee 
        { 
            Id = "bv", 
            Employment = Constants.EmploymentTypeFT, 
            FullTimeStartDate = DateTime.Parse("2020-10-07 23:02:46.213"), 
            StartDate = new DateTime(year: 2020, month: 10, day: 06),
            Active = true
        };

        public static Employee EmployeeTMEmployment = new Employee
        {
            Id = "bv",
            Employment = Constants.EmploymentTypeTM,
            FullTimeStartDate = null,
            StartDate = new DateTime(year: 2020, month: 10, day: 06),
            Active = true
        };

        public static TimeReport EmployeeReserveTimeReport = new TimeReport
        {
            Id = 1,
            UserId = "bv",
            ReportingDate = new DateTime(year: 2020, month: 10, day: 06),
            Project = new Project { ProjectPriority = (int)ProjectPriority.Low }
        };

        public static TimeReport EmployeeBillableTimeReport = new TimeReport
        {
            Id = 1,
            UserId = "bv",
            ReportingDate = new DateTime(year: 2020, month: 10, day: 06),
            Project = new Project { ProjectPriority = (int)ProjectPriority.High }
        };

        #endregion


        #region EmployeeHistory

        public static List<EmployeeHistory> EmployeeHistoryOnlyFT = new List<EmployeeHistory>
        {
            new() { UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2020-10-07 23:02:46.213") },
            new() { Id = 21727, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-01-06 00:02:46.717") },
            new() { Id = 21728, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = null, Timestamp = DateTime.Parse("2021-03-19 00:02:35.480") },
            new() { Id = 21729, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-03-19 17:30:33.100") },
            new() { Id = 21730, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-04-27 23:46:54.263") },
            new() { Id = 21731, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-11 23:46:54.870") },
            new() { Id = 21732, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-12 23:46:55.313") },
            new() { Id = 21733, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-19 23:46:53.750") },
            new() { Id = 21734, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-01 23:47:00.207") },
            new() { Id = 21735, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-10 23:46:59.870") },
            new() { Id = 21736, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-07-01 23:46:56.703") },
            new() { Id = 21737, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-03 23:46:46.887") },
        };

        public static List<EmployeeHistory> EmployeeHistoryOnlyTM = new List<EmployeeHistory>
        {
            new() { Id = 21726, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2020-10-07 23:02:46.213") },
            new() { Id = 21727, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-01-06 00:02:46.717") },
            new() { Id = 21728, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-03-19 00:02:35.480") },
            new() { Id = 21729, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-03-19 17:30:33.100") },
            new() { Id = 21730, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-04-27 23:46:54.263") },
            new() { Id = 21731, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-11 23:46:54.870") },
            new() { Id = 21732, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-12 23:46:55.313") },
            new() { Id = 21733, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-19 23:46:53.750") },
            new() { Id = 21734, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-01 23:47:00.207") },
            new() { Id = 21735, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-10 23:46:59.870") },
            new() { Id = 21736, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-07-01 23:46:56.703") },
            new() { Id = 21737, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-03 23:46:46.887") },
            new() { Id = 21738, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-09 16:45:23.483") },
            new() { Id = 21739, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-08-31 23:46:58.313") },
            new() { Id = 21740, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-09-01 23:46:49.257") },
            new() { Id = 21741, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-09-30 23:46:56.607") },
            new() { Id = 21742, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2021-09-01"), Timestamp = DateTime.Parse("2021-10-01 23:46:57.530") },
            new() { Id = 21743, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-10-04 23:46:59.987") },
            new() { Id = 21744, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-01-20 00:46:48.370") },
            new() { Id = 21745, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2022-02-10 00:46:48.570") },
            new() { Id = 21746, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-02-10 08:36:43.680") },
            new() { Id = 21747, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-08 00:23:59.070") },
            new() { Id = 21748, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-09 00:24:02.493") },
            new() { Id = 21749, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-10 00:23:51.253") },
            new() { Id = 21750, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-11 00:23:58.423") },
            new() { Id = 21751, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-11 15:51:01.060") },
            new() { Id = 21752, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-12 00:23:58.473") },
            new() { Id = 21753, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-13 00:23:59.593") },
            new() { Id = 21754, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-14 00:24:06.373") },
            new() { Id = 21755, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-15 00:23:45.493") },
            new() { Id = 21756, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-15 10:46:06.860") },
            new() { Id = 21757, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-16 00:24:00.510") }
        };

        public static List<EmployeeHistory> EmployeeHistoryWithTMLessThanSixMonth = new List<EmployeeHistory>
        {
            new() { Id = 21726, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2020-10-07 23:02:46.213") },
            new() { Id = 21727, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-01-06 00:02:46.717") },
            new() { Id = 21728, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = null, Timestamp = DateTime.Parse("2021-03-19 00:02:35.480") },
            new() { Id = 21729, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-03-19 17:30:33.100") },
            new() { Id = 21730, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-04-27 23:46:54.263") },
            new() { Id = 21731, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-11 23:46:54.870") },
            new() { Id = 21732, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-12 23:46:55.313") },
            new() { Id = 21733, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-19 23:46:53.750") },
            new() { Id = 21734, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-01 23:47:00.207") },
            new() { Id = 21735, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-10 23:46:59.870") },
            new() { Id = 21736, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-07-01 23:46:56.703") },
            new() { Id = 21737, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-03 23:46:46.887") },
            new() { Id = 21738, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-09 16:45:23.483") },
            new() { Id = 21739, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-08-31 23:46:58.313") },
            new() { Id = 21740, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-09-01 23:46:49.257") },
            new() { Id = 21741, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = null, Timestamp = DateTime.Parse("2021-09-30 23:46:56.607") },
            new() { Id = 21742, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2021-09-01"), Timestamp = DateTime.Parse("2021-10-01 23:46:57.530") },
            new() { Id = 21743, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-10-04 23:46:59.987") },
            new() { Id = 21744, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-01-20 00:46:48.370") },
            new() { Id = 21745, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = null, Timestamp = DateTime.Parse("2022-02-10 00:46:48.570") },
            new() { Id = 21746, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-02-10 08:36:43.680") },
            new() { Id = 21747, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-08 00:23:59.070") },
            new() { Id = 21748, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-09 00:24:02.493") },
            new() { Id = 21749, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-10 00:23:51.253") },
            new() { Id = 21750, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-11 00:23:58.423") },
            new() { Id = 21751, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-11 15:51:01.060") },
            new() { Id = 21752, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-12 00:23:58.473") },
            new() { Id = 21753, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-13 00:23:59.593") },
            new() { Id = 21754, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-14 00:24:06.373") },
            new() { Id = 21755, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-15 00:23:45.493") },
            new() { Id = 21756, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-15 10:46:06.860") },
            new() { Id = 21757, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-16 00:24:00.510") }
        };

        public static List<EmployeeHistory> EmployeeHistoryWithTMMoreThanSixMonth = new List<EmployeeHistory>
        {
            new() { Id = 21726, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2020-10-07 23:02:46.213") },
            new() { Id = 21727, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-01-06 00:02:46.717") },
            new() { Id = 21728, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-03-19 00:02:35.480") },
            new() { Id = 21729, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-03-19 17:30:33.100") },
            new() { Id = 21730, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-04-27 23:46:54.263") },
            new() { Id = 21731, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-11 23:46:54.870") },
            new() { Id = 21732, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-12 23:46:55.313") },
            new() { Id = 21733, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-05-19 23:46:53.750") },
            new() { Id = 21734, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-01 23:47:00.207") },
            new() { Id = 21735, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-06-10 23:46:59.870") },
            new() { Id = 21736, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-07-01 23:46:56.703") },
            new() { Id = 21737, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-03 23:46:46.887") },
            new() { Id = 21738, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-08-09 16:45:23.483") },
            new() { Id = 21739, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-08-31 23:46:58.313") },
            new() { Id = 21740, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-09-01 23:46:49.257") },
            new() { Id = 21741, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = null, Timestamp = DateTime.Parse("2021-09-30 23:46:56.607") },
            new() { Id = 21742, UserId = "bv", Employment = Constants.EmploymentTypeTM, StartDate = DateTime.Parse("2021-09-01"), Timestamp = DateTime.Parse("2021-10-01 23:46:57.530") },
            new() { Id = 21743, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2021-10-04 23:46:59.987") },
            new() { Id = 21744, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-01-20 00:46:48.370") },
            new() { Id = 21745, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = null, Timestamp = DateTime.Parse("2022-02-10 00:46:48.570") },
            new() { Id = 21746, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-02-10 08:36:43.680") },
            new() { Id = 21747, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-08 00:23:59.070") },
            new() { Id = 21748, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-09 00:24:02.493") },
            new() { Id = 21749, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-10 00:23:51.253") },
            new() { Id = 21750, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-11 00:23:58.423") },
            new() { Id = 21751, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-11 15:51:01.060") },
            new() { Id = 21752, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-12 00:23:58.473") },
            new() { Id = 21753, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-13 00:23:59.593") },
            new() { Id = 21754, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-14 00:24:06.373") },
            new() { Id = 21755, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-15 00:23:45.493") },
            new() { Id = 21756, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-15 10:46:06.860") },
            new() { Id = 21757, UserId = "bv", Employment = Constants.EmploymentTypeFT, StartDate = DateTime.Parse("2020-10-06"), Timestamp = DateTime.Parse("2022-04-16 00:24:00.510") }
        };

        #endregion
    }
}
