﻿using AutoMapper;
using Leobit.TimeTracking.BL.Infrastructure.AutoMapperProfiles;
using Leobit.TimeTracking.DAL.Entities;
using Microsoft.EntityFrameworkCore;
using System;

namespace Leobit.TimeTracking.Tests.VacationTests.Lib
{
    public class VacationServiceTestsBase : IDisposable
    {
        protected LeobitContext Context;
        protected readonly IMapper Mapper;

        public VacationServiceTestsBase(bool useInMemoryDb = false)
        {
            if (useInMemoryDb)
            {
                var options = new DbContextOptionsBuilder<LeobitContext>()
                    .UseInMemoryDatabase(databaseName: Guid.NewGuid().ToString())
                    .Options;

                Context = new LeobitContext(options);
            }

            var mappingConfig = new MapperConfiguration(cfg =>
            {
                cfg.AddProfile(new TimeOffProfile());
                cfg.AddProfile(new UserProfile());
                cfg.AddProfile<OperationalProfile>();
            });
            Mapper = mappingConfig.CreateMapper();
        }

        public void Dispose()
        {
            Context?.Dispose();
        }
    }
}
