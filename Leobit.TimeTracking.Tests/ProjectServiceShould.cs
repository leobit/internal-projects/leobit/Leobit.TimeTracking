﻿using FluentAssertions;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General;
using System;
using Xunit;

namespace Leobit.TimeTracking.Tests
{
    public class ProjectServiceShould
    {
        [Fact]
        public void CalculateReopenDateRange_When_Time_Now_Daily()
        {
            var time = DateTime.UtcNow;
            var project = GetProject(true, 3, 3, ClosingType.Daily);
            (var beforeClosing, var afterClosing) = ProjectService.CalculateReopenDateRange(
                project, new Settings(), time, time, time);
            beforeClosing.Should().Be(3);
            afterClosing.Should().Be(3);
        }

        [Fact]
        public void CalculateReopenDateRange_When_Monthly()
        {
            var currentTime = new DateTime(2023, 05, 23);
            var time = new DateTime(2023, 04, 16);
            var endTime = new DateTime(2023, 06, 16);
            var project = GetProject(true, 10, 10, ClosingType.Monthly);
            (var beforeClosing, var afterClosing) = ProjectService.CalculateReopenDateRange(
                project, new Settings(), time, endTime, currentTime);
            beforeClosing.Should().Be(15);
            afterClosing.Should().Be(15);
        }

        [Fact]
        public void CalculateReopenDateRange_When_Weekly()
        {
            var currentTime = new DateTime(2023, 05, 23);
            var time = new DateTime(2023, 05, 07);
            var endTime = new DateTime(2023, 05, 16);
            var project = GetProject(true, 3, 3, ClosingType.Weekly);
            (var beforeClosing, var afterClosing) = ProjectService.CalculateReopenDateRange(
                project, new Settings(), time, endTime, currentTime);
            beforeClosing.Should().Be(15);
            afterClosing.Should().Be(3);
        }

        [Fact]
        public void CalculateReopenDateRange_When_Time_Now_Daily_If_Account()
        {
            var time = DateTime.UtcNow;
            var account = GetAccount(3, 3, ClosingType.Daily);
            var project = GetProject(false, 0, 0, ClosingType.Daily, account);
            (var beforeClosing, var afterClosing) = ProjectService.CalculateReopenDateRange(
                project, new Settings(), time, time, time);
            beforeClosing.Should().Be(3);
            afterClosing.Should().Be(3);
        }

        private static Project GetProject(bool overrideClosingSettingsint, int beforeClosing, int afterClosing, ClosingType closingType, Account account = null)
        {
            account ??= new();

            return new Project
            {
                Id = 1,
                OverrideClosingSettings = overrideClosingSettingsint,
                BeforeClosing = beforeClosing,
                AfterClosing = afterClosing,
                ClosingType = (short)closingType,
                Account = account
            };
        }
        private static Account GetAccount(int beforeClosing, int afterClosing, ClosingType closingType)
        {
            return new Account
            {
                Id = 1,
                OverrideClosingSettings = true,
                BeforeClosing = beforeClosing,
                AfterClosing = afterClosing,
                ClosingType = (short)closingType
            };
        }
    }
}