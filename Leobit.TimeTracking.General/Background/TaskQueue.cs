﻿using System;
using System.Collections.Concurrent;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.General.Background
{
    public class TaskQueue
    {
        private readonly BlockingCollection<Func<IServiceProvider, CancellationToken, Task>> _tasks = new();

        public void Enqueue(Func<IServiceProvider, CancellationToken, Task> task)
        {
            if (task == null)
            {
                throw new ArgumentNullException(nameof(task));
            }

            _tasks.Add(task);
        }

        public Func<IServiceProvider, CancellationToken, Task> Dequeue()
        {
            return _tasks.Take();
        }
    }
}
