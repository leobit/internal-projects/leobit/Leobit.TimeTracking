﻿using System;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Leobit.TimeTracking.General.Background
{
    public class TaskBackgroundService : BackgroundService
    {
        private readonly TaskQueue _taskQueue;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly ILogger<TaskBackgroundService> _logger;

        public TaskBackgroundService(TaskQueue taskQueue, IServiceScopeFactory serviceScopeFactory, ILogger<TaskBackgroundService> logger)
        {
            _taskQueue = taskQueue;
            _serviceScopeFactory = serviceScopeFactory;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken cancellationToken)
        {
            await Task.Run(async () =>
            {
                while (!cancellationToken.IsCancellationRequested)
                {
                    var task = _taskQueue.Dequeue();

                    await using var scope = _serviceScopeFactory.CreateAsyncScope();

                    try
                    {
                        await task(scope.ServiceProvider, cancellationToken);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex.Message);
                    }
                }
            }, cancellationToken);
        }
    }
}
