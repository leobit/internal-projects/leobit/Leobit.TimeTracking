﻿namespace Leobit.TimeTracking.General
{
    public enum ClosingType : short
    {
        Monthly = 0,
        Weekly = 1,
        Daily = 2,
        Monday = 3,
        MondayThursday = 4
    }
}
