﻿namespace Leobit.TimeTracking.General.AmazonSQS.Messages
{
    public class UserTerminatedMessage
    {
        public string UserId { get; set; }
    }
}
