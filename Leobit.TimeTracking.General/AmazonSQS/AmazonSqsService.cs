﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.General.AmazonSQS
{
    public class AmazonSqsService
    {
        private static readonly ILog _log = LogManager.GetLog(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private readonly AmazonSqsSettings _options;
        private string _sqsQueueUrl { get; set; }
        private IAmazonSQS _amazonSQS { get; set; }

        public AmazonSqsService(IAmazonSQS amazonSQS, IOptions<AmazonSqsSettings> options)
        {
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            if (!_options.EnableSqsTimeReportsSynch)
            {
                return;
            }

            _amazonSQS = amazonSQS;

            var request = new GetQueueUrlRequest
            {
                QueueName = _options.QueueName,
                QueueOwnerAWSAccountId = _options.QueueOwnerAWSAccountId,
            };

            try
            {
                var response = _amazonSQS.GetQueueUrlAsync(request)
                    .GetAwaiter()
                    .GetResult();
                _sqsQueueUrl = response.QueueUrl;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        public async Task<bool> SendSqsMessage(string message, string userId)
        {
            if (!_options.EnableSqsTimeReportsSynch)
            {
                return true;
            }

            var guid = Guid.NewGuid().ToString("N");

            var sendMessageRequest = new SendMessageRequest()
            {
                QueueUrl = _sqsQueueUrl,
                MessageGroupId = userId,
                MessageBody = message,
                MessageDeduplicationId = guid
            };

            var sendMessageResponse = await _amazonSQS.SendMessageAsync(sendMessageRequest);

            return sendMessageResponse.HttpStatusCode == System.Net.HttpStatusCode.OK;
        }

        public async Task<bool> SendSqsMessages(List<(string message, string userId)> messages)
        {
            if (!_options.EnableSqsTimeReportsSynch)
            {
                return true;
            }

            // AWS batch message only supports up to 10 entries per batch
            var batchRequests = messages.Select((message, index) => (message, index))
                .GroupBy(pair => pair.index / 10)
                .Select(group => new SendMessageBatchRequest()
                {
                    QueueUrl = _sqsQueueUrl,
                    Entries = group.Select(pair => new SendMessageBatchRequestEntry
                    {
                        Id = pair.index.ToString(),
                        MessageBody = pair.message.message,
                        MessageGroupId = pair.message.userId,
                        MessageDeduplicationId = Guid.NewGuid().ToString("N")
                    }).ToList()
                });

            foreach (var request in batchRequests)
            {
                var response = await _amazonSQS.SendMessageBatchAsync(request);

                if (response.HttpStatusCode != System.Net.HttpStatusCode.OK)
                    return false;
            }

            return true;
        }
    }
}
