﻿namespace Leobit.TimeTracking.General.AmazonSQS
{
    public class AmazonSqsSettings
    {
        public string QueueOwnerAWSAccountId { get; set; }
        public bool EnableSqsTimeReportsSynch { get; set; }
        public string QueueName { get; set; }
        public bool EnableEmployeeTerminationHandling { get; set; }
        public string EmployeeTerminationQueueName { get; set; }
    }
}
