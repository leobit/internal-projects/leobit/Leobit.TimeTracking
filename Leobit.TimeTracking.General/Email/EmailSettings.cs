﻿namespace Leobit.TimeTracking.General.Email
{
    public class SmtpSettings
    {
        public string Host { get; set; }
        public int Port { get; set; }
    }

    public class Credentials
    {
        public string Username { get; set; }
        public string Password { get; set; }
    }

    public class EmailSettings
    {
        public string From { get; set; }
        public SmtpSettings SmtpSettings { get; set; }
        public Credentials Credentials { get; set; }
    }
}
