﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.Extensions.Options;


namespace Leobit.TimeTracking.General.Email
{
    public class EmailService
    {
        private static readonly ILog _log = LogManager.GetLog(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        private EmailSettings _settings;

        public EmailService(IOptions<EmailSettings> settings)
        {
            _settings = settings.Value;
        }

        public async Task SendEmailAsync(string to, string subject, string body)
        {
            try
            {
                using (var smtpClient = new SmtpClient() { Host = _settings.SmtpSettings.Host, Port = _settings.SmtpSettings.Port })
                {
                    smtpClient.EnableSsl = true;
                    if (_settings.Credentials != null)
                    {
                        smtpClient.Credentials = new NetworkCredential(_settings.Credentials.Username, _settings.Credentials.Password);
                    }

                    var mailMessage = new MailMessage(_settings.From, to, subject, body)
                    {
                        IsBodyHtml = true
                    };

                    await smtpClient.SendMailAsync(mailMessage);
                }
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }
    }
}
