using System;

namespace Leobit.TimeTracking.General.Infratstructure
{
    public class ConflictException : Exception
    {
        public ConflictException() : base() { }
        public ConflictException(string message) : base(message) { }
    }
}
