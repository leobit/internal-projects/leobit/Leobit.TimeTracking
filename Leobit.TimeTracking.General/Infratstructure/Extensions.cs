﻿using System;
using System.Text;

namespace Leobit.TimeTracking.General.Infratstructure
{
    /// <summary>
    /// Class to hold all extension methods that should be accessible through all solution
    /// </summary>
    public static class Extensions
    {
        private const int DaysInWeek = 7;

        /// <summary>
        /// Presentation Excetpion as readable text
        /// </summary>
        /// <param name="e">Exception to parse</param>
        /// <returns></returns>
        public static string ToFullString(this Exception e)
        {
            var sb = new StringBuilder();
            if (e != null)
            {
                CreateExceptionString(sb, e, string.Empty);
            }
            return sb.ToString();
        }

        private static void CreateExceptionString(StringBuilder sb, Exception e, string indent = "")
        {
            while (true)
            {
                if (indent.Length > 0)
                {
                    sb.Append($"{indent}Inner ");
                }

                sb.Append($"Exception Found:\n{indent}Type: {e.GetType().FullName}");
                sb.Append($"\n{indent}Message: {e.Message}");
                sb.Append($"\n{indent}Source: {e.Source}");
                sb.Append($"\n{indent}Stacktrace: {e.StackTrace}");

                if (e.InnerException == null)
                {
                    break;
                }

                sb.Append("\n");
                e = e.InnerException;
                indent = indent + "  ";
            }
        }

        public static DateTime FirstDateOnlyOfYear(this int year)
        {
            return new DateTime(year, 1, 1);
        }

        public static DateTime FirstDateTimeOfMonth(this DateTime dateTime)
        {
            return new DateTime(dateTime.Year, dateTime.Month, 1, 0, 0, 0, dateTime.Kind);
        }

        public static DateTime FirstDateOfWeek(this DateTime dateTime)
        {
            var startOfWeek = DayOfWeek.Monday;
            int diff = (7 + (dateTime.DayOfWeek - startOfWeek)) % 7;

            return dateTime.AddDays(-diff).Date;
        }

        public static DateTime LastDateOnlyOfYear(this int year)
        {
            return year == DateTime.Today.Year ? DateTime.Today : new DateTime(year, 12, 31);
        }
        public static DateTime LastDateTimeOfYear(this int year)
        {
            return new DateTime(year, 12, 31, 23, 59, 59, DateTimeKind.Utc);
        }

        public static DateTime LastDateOfYear(this int year)
        {
            return year == DateTime.Today.Year ? DateTime.Today : new DateTime(year, 12, 31);
        }

        public static DateTime LastDateOfMonth(this DateTime dateTime)
        {
            var daysInMonth = DateTime.DaysInMonth(dateTime.Year, dateTime.Month);

            return dateTime.FirstDateTimeOfMonth().AddDays(daysInMonth - 1);
        }

        public static DateTime LastDateOfWeek(this DateTime dateTime)
        {
            return dateTime.FirstDateOfWeek().AddDays(DaysInWeek - 1);
        }

        public static DateTime AddWeeks(this DateTime dateTime, int weeks)
        {
            return dateTime.AddDays(weeks * 7);
        }

        public static DateTime AddBusinessDays(this DateTime current, int days)
        {
            var sign = Math.Sign(days);
            var unsignedDays = Math.Abs(days);
            for (var i = 0; i < unsignedDays; i++)
            {
                do
                {
                    current = current.AddDays(sign);
                }
                while (current.DayOfWeek == DayOfWeek.Saturday ||
                       current.DayOfWeek == DayOfWeek.Sunday);
            }
            return current;
        }
    }
}
