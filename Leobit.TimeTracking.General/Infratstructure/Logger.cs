﻿using System;
using System.IO;
using System.Reflection;
using System.Runtime.CompilerServices;
using log4net.Appender;
using log4net.Core;
using log4net.Layout;
using log4net.Repository.Hierarchy;

namespace Leobit.TimeTracking.General.Infratstructure
{
    /// <summary>
    /// Custom configuration and wrapper above lo4net Logger
    /// </summary>
    /// <remarks>
    /// Log File Path need to be modified
    /// </remarks>
    internal sealed class Logger : ILog
    {
        #region ctor
        static Logger()
        {
            PatternLayout.ActivateOptions();

            var h = (Hierarchy)log4net.LogManager.GetRepository(Assembly.GetEntryAssembly());

            h.Root.Level = Level.Debug;
            h.Root.AddAppender(CreateTraceAppender());
            h.Root.AddAppender(CreateRollingFileAppender());
            h.Root.AddAppender(CreateConsoleAppender());

            h.Configured = true;
        }
        internal Logger(Type type)
        {
            _log = log4net.LogManager.GetLogger(type);
        }
        #endregion ctor

        #region PrivateProperties
        private readonly log4net.ILog _log;
        private const string FileName = "Log.txt";
        private static readonly PatternLayout PatternLayout = new PatternLayout { ConversionPattern = "%d [%-5p] [%c{2}] %m%n" };
        private static readonly string FilePath = Path.Combine(Directory.GetCurrentDirectory(), FileName); //TODO
        #endregion PrivateProperties 

        #region Appenders
        /// <summary>
        /// Create FileAppender that will be limited to size and number of backups
        /// </summary>
        /// <returns>log4net.RollingFileAppender</returns>
        private static IAppender CreateRollingFileAppender()
        {
            var controlAppender = new RollingFileAppender
            {
                File = FilePath,
                AppendToFile = true,
                MaxSizeRollBackups = 10,
                MaximumFileSize = "100MB",
                RollingStyle = RollingFileAppender.RollingMode.Size,
                StaticLogFileName = true,
                Layout = PatternLayout,
                Threshold = Level.Debug
            };
            controlAppender.ActivateOptions();
            return controlAppender;
        }

        /// <summary>
        /// Create TraceAppender to track messages in VisualStudio Trace Window (Output)
        /// </summary>
        /// <returns>log4net.TraceAppender</returns>
        private static IAppender CreateTraceAppender()
        {
            var traceAppender = new TraceAppender { Layout = PatternLayout };
            traceAppender.ActivateOptions();
            return traceAppender;
        }

        /// <summary>
        /// Create ConsoleAppender to show messages in Console (if using Kestrel)
        /// </summary>
        /// <returns>log4net.ConsoleAppender</returns>
        private static IAppender CreateConsoleAppender()
        {
            var consoleAppender = new ConsoleAppender { Layout = PatternLayout };
            consoleAppender.ActivateOptions();
            return consoleAppender;
        }
        #endregion Appenders

        #region ILogImplementation
        public void Debug(string message = "", [CallerMemberName] string className = "") => _log.Debug($"[{className}] {message}");

        public void Error(string message = "", [CallerMemberName] string className = "") => _log.Error($"[{className}] {message}");

        public void Info(string message = "", [CallerMemberName] string className = "") => _log.Info($"[{className}] {message}");

        public void Warn(string message = "", [CallerMemberName] string className = "") => _log.Warn($"[{className}] {message}");

        public void Fatal(string message = "", [CallerMemberName] string className = "") => _log.Fatal($"[{className}] {message}");

        public void Debug(string message, Exception e, [CallerMemberName] string className = "") => _log.Debug($"[{className}] {message} {e.ToFullString()}");

        public void Error(string message, Exception e, [CallerMemberName] string className = "") => _log.Error($"[{className}] {message} {e.ToFullString()}");

        public void Info(string message, Exception e, [CallerMemberName] string className = "") => _log.Info($"[{className}] {message} {e.ToFullString()}");

        public void Warn(string message, Exception e, [CallerMemberName] string className = "") => _log.Warn($"[{className}] {message} {e.ToFullString()}");

        public void Fatal(string message, Exception e, [CallerMemberName] string className = "") => _log.Fatal($"[{className}] {message} {e.ToFullString()}");

        public void Debug(Exception e, [CallerMemberName] string className = "") => _log.Debug($"[{className}] {e.ToFullString()}");

        public void Error(Exception e, [CallerMemberName] string className = "") => _log.Error($"[{className}] {e.ToFullString()}");

        public void Info(Exception e, [CallerMemberName] string className = "") => _log.Info($"[{className}] {e.ToFullString()}");

        public void Warn(Exception e, [CallerMemberName] string className = "") => _log.Warn($"[{className}] {e.ToFullString()}");

        public void Fatal(Exception e, [CallerMemberName] string className = "") => _log.Fatal($"[{className}] {e.ToFullString()}");
        #endregion #region ILogImplementation
    }
}
