﻿using System;
using System.Runtime.CompilerServices;

namespace Leobit.TimeTracking.General.Infratstructure
{
    /// <summary>
    /// More user-friendly interface Wrapper above log4net Log
    /// </summary>
    public interface ILog
    {
        void Debug(string message = "", [CallerMemberName] string className = "");
        void Error(string message = "", [CallerMemberName] string className = "");
        void Info(string message = "", [CallerMemberName] string className = "");
        void Warn(string message = "", [CallerMemberName] string className = "");
        void Fatal(string message = "", [CallerMemberName] string className = "");
        
        void Debug(string message, Exception e, [CallerMemberName] string className = "");
        void Error(string message, Exception e, [CallerMemberName] string className = "");
        void Info(string message, Exception e, [CallerMemberName] string className = "");
        void Warn(string message, Exception e, [CallerMemberName] string className = "");
        void Fatal(string message, Exception e, [CallerMemberName] string className = "");
        
        void Debug(Exception e, [CallerMemberName] string className = "");
        void Error(Exception e, [CallerMemberName] string className = "");
        void Info(Exception e, [CallerMemberName] string className = "");
        void Warn(Exception e, [CallerMemberName] string className = "");
        void Fatal(Exception e, [CallerMemberName] string className = "");
    }
}
