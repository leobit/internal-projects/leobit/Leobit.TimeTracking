﻿using System;
using System.Collections.Concurrent;

namespace Leobit.TimeTracking.General.Infratstructure
{
    /// <inheritdoc />
    /// <summary>
    /// LogManager provides and holds logger instances.
    /// </summary>
    public sealed class LogManager : IDisposable
    {
        private static readonly ConcurrentDictionary<Type, ILog> Logs = new ConcurrentDictionary<Type, ILog>();
        public static ILog GetLog(Type type)
        {
            return Logs.GetOrAdd(type, new Logger(type));
        }

        public void Dispose()
        {
            Logs.Clear();
        }
    }
}
