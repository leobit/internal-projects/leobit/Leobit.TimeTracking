using System;

namespace Leobit.TimeTracking.General.Infratstructure
{
    [Serializable]
    public sealed class NotFoundException : Exception
    {
        public NotFoundException() : base() { }

        public NotFoundException(string message) : base(message) { }

        public NotFoundException(string message, Exception inner) : base(message, inner) { }
    }
}
