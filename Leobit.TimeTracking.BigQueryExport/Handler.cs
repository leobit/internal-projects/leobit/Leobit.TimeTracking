﻿using Leobit.TimeTracking.BL.Services;
using static Leobit.TimeTracking.BL.Services.ExportService;

namespace Leobit.TimeTracking.BigQueryExport
{
    public static class Handler
    {
        public static async Task HandleTimeReports(ExportService exportService, ExportConfig config, BigQueryConfiguration configuration)
        {
            var data = exportService.GetTimeReportsToExportBigQuery(config);
            if (data.Any())
            {
                var tempFileName = $"{Guid.NewGuid()}.csv";
                CSV.SaveToCsv(data, tempFileName);
                await BigQuery.Export(configuration, tempFileName);
                File.Delete(tempFileName);
            }
        }
    }
}
