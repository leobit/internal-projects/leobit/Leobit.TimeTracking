﻿using AutoMapper;
using HealthChecks.UI.Client;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.Consumer;
using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.General.Background;
using Leobit.TimeTracking.General.Email;
using Leobit.TimeTracking.General.Infratstructure;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Amazon;
using Leobit.TimeTracking.WEB.Infrastructure.HostedServices;
using Leobit.TimeTracking.WEB.Infrastructure.Schemes;
using Leobit.TimeTracking.WEB.Infrastructure.SwashbuckleFilters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Diagnostics.HealthChecks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;
using OfficeOpenXml;
using Serilog;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;
using Microsoft.AspNetCore.Rewrite;
using Leobit.TimeTracking.BL.Infrastructure.Slack;
using Leobit.BigQueryExport;
using Leobit.TimeTracking.BL.Helpers;
using Leobit.TimeTracking.BL.Infrastructure.Settings;
using Leobit.TimeTracking.BL.Interfaces;

namespace Leobit.TimeTracking.WEB
{
    public class Startup
    {
        private readonly ILog _log = LogManager.GetLog(MethodBase.GetCurrentMethod().DeclaringType);
        private readonly IConfiguration _configuration;

        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        public void ConfigureServices(IServiceCollection services)
        {
            #region DbContext
            services.AddDbContext<LeobitContext>(options =>
            {
                options.UseSqlServer(
                    _configuration.GetConnectionString("DefaultConnection"),
                    options => options.EnableRetryOnFailure(3)).EnableSensitiveDataLogging();
            });
            #endregion DbContext

            #region Application Insights
            services.AddApplicationInsightsTelemetry();
            #endregion Application Insights

            #region Core
            services.AddMvcCore(options => { options.Filters.Add<CustomExceptionFilterAttribute>(); })
                .AddRazorViewEngine()
                .AddDataAnnotations()
                .AddApiExplorer()
                .AddAuthorization()
                .AddJsonOptions(opt =>
                {
                    opt.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                    opt.JsonSerializerOptions.IgnoreNullValues = true;
                })
                .AddNewtonsoftJson(opt =>
                {
                    opt.SerializerSettings.Converters.Add(new Newtonsoft.Json.Converters.StringEnumConverter());
                    opt.SerializerSettings.NullValueHandling = Newtonsoft.Json.NullValueHandling.Ignore;
                });
            #endregion Core

            #region Authentication
            services.AddAuthentication(SchemeName.IdentityServer)
                 .AddIdentityServerAuthentication(SchemeName.IdentityServer, options =>
                 {
                     options.Authority = _configuration.GetValue<string>("IdentityServer:Authority");
                     options.RequireHttpsMetadata = _configuration.GetValue<bool>("IdentityServer:RequireHttpsMetadata");

                     options.ApiName = "TimeTracking";
                 });
            #endregion Authentication

            #region AutoMapper
            services.AddAutoMapper();
            #endregion AutoMapper

            #region ApiVersioning
            services.AddApiVersioning(options =>
            {
                options.ReportApiVersions = true;
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.DefaultApiVersion = new ApiVersion(1, 0);
            });
            #endregion

            #region Swagger
            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = $"Leobit TimeTracking API - {Program.ProductVersion}",
                    Version = "v1"
                });

                options.DescribeAllEnumsAsStrings();

                options.CustomOperationIds(apiDescription =>
                {
                    return apiDescription.TryGetMethodInfo(out var methodInfo) ? methodInfo.Name : null;
                });

                options.ResolveConflictingActions(apiDescriptions =>
                {
                    return apiDescriptions.Single(ad => !ad.ActionDescriptor.DisplayName.Contains("_"));
                });

                // version handling
                options.OperationFilter<RemoveVersionParameter>();

                options.DocumentFilter<RemoveVersionFromPath>();

                options.DocInclusionPredicate((name, apiDescription) =>
                {
                    apiDescription.TryGetMethodInfo(out var methodinfo);

                    var versions = methodinfo.DeclaringType
                        .GetCustomAttributes<ApiVersionAttribute>()
                        .SelectMany(attr => attr.Versions);

                    return versions.Any(v => $"v{v.ToString()}" == name);
                });

                options.OperationFilter<ExcelExportOperations>();
                options.OperationFilter<WordExportOperations>();

                // XML documentation
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Leobit.TimeTracking.WEB.xml"));
                options.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "Leobit.TimeTracking.BL.xml"));

                // Authorization
                options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Type = SecuritySchemeType.OAuth2,
                    Flows = new OpenApiOAuthFlows
                    {
                        AuthorizationCode = new OpenApiOAuthFlow
                        {
                            AuthorizationUrl = new Uri(_configuration.GetValue<string>("IdentityServer:Authority") + "/connect/authorize"),
                            TokenUrl = new Uri(_configuration.GetValue<string>("IdentityServer:Authority") + "/connect/token"),
                            Scopes = new Dictionary<string, string>
                            {
                                { _configuration.GetValue<string>("Swagger:Scope"), "Default scope" }
                            },
                        },
                    },
                });

                options.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {
                        new OpenApiSecurityScheme
                        {
                            Reference = new OpenApiReference { Type = ReferenceType.SecurityScheme, Id = "oauth2" }
                        },
                        new[] { "TimeTracking" }
                    }
                });
            });
            #endregion Swagger

            #region Health
            services.AddLeobitHealthChecks(_configuration);
            #endregion

            #region Services

            services.AddTransient<DivisionService>();
            services.AddTransient<OrganizationUnitService>();
            services.AddTransient<CategoryService>();
            services.AddTransient<AccountService>();
            services.AddTransient<ProjectService>();
            services.AddTransient<ProjectReportingReopenRequestService>();
            services.AddTransient<TimeReportService>();
            services.AddTransient<UserProjectService>();
            services.AddTransient<ProjectManagerService>();
            services.AddTransient<ReserveService>();
            services.AddTransient<RoleService>();
            services.AddTransient<PermissionService>();
            services.AddTransient<UserService>();
            services.AddTransient<SettingsService>();
            services.AddTransient<ExportService>();
            services.AddTransient<IVacationService, VacationService>();
            services.AddTransient<SickLeaveService>();
            services.AddTransient<IOperationalService, OperationalService>();
            services.AddTransient<ITimeOffService, TimeOffService>();
            services.AddTransient<IEmployeeService, EmployeeService>();
            services.AddTransient<TimeReportService>();
            services.AddTransient<FiltersPresetService>();
            services.AddTransient<WordReportGeneratorService>();
            services.AddScoped<AnalyticService>();

            #endregion Services

            #region Email
            services.Configure<EmailSettings>(_configuration.GetSection("EmailSettings"));
            services.AddScoped<EmailService>();
            services.AddScoped<ViewRenderService>();
            #endregion

            #region BigQuery
            services.Configure<BigQueryExport.BigQueryConfigurations>(_configuration.GetSection("BigQueryConfigurations"));
            services.AddSingleton<TaskQueue>();
            services.AddHostedService<TaskBackgroundService>();
            #endregion

            #region Export
            services.AddScoped<BigQueryExportCoordinator>();
            #endregion

            services.AddTransient<UkrainianDateTimeProvider>();

            services.AddTransient<IVacationDataProvider, VacationDataProvider>();

            services.ConfigureAmazonSqs(_configuration);

            services.AddSlackServices(_configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("Default", policy =>
                {
                    var allowedOrigins = _configuration.GetSection("AllowedOrigins").Get<string[]>();
                    if (allowedOrigins != null && allowedOrigins.Length > 0)
                    {
                        policy.WithOrigins(allowedOrigins)
                            .AllowAnyHeader()
                            .AllowAnyMethod();
                    }
                });
            });

            services.Configure<ProjectSettingsBackupSettings>(_configuration.GetSection("ProjectSettingsBackup"));
            services.Configure<ProjectReportingReopenRequestSettings>(_configuration.GetSection("ProjectReportingReopenRequest"));
            services.Configure<ProjectCleanupSettings>(_configuration.GetSection("ProjectCleanup"));
            services.Configure<SlackReopenRequestMessagesSettings>(_configuration.GetSection("SlackReopenRequestMessages"));

            services.AddHostedService<EmployeeTerminationSqsConsumer>();
            services.AddHostedService<UserProjectsCleanupHostedService>();
            services.AddHostedService<ProjectSettingsBackupCleanupHostedService>();
            services.AddHostedService<ProjectReportingReopenRequestCleanupHostedService>();
            services.AddHostedService<ProjectCleanupHostedService>();
            services.AddHostedService<UpdateSlackMessagesHostedService>();
            services.AddHttpContextAccessor();

            services.AddMemoryCache();
        }

        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
#if DEBUG
            _log.Debug("Starting Configuration");
#endif

            // https://real_url  <-> http://localhost:5XXX proxy handling
            if (!env.IsDevelopment())
            {
                app.Use((context, next) =>
                {
                    context.Request.Scheme = "https";
                    return next();
                });

                app.UseForwardedHeaders(new ForwardedHeadersOptions
                {
                    ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
                });
            }

            if (env.IsDevelopment() || env.IsEnvironment("Test") || env.IsStaging())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseDefaultFiles();
            app.UseStaticFiles();
            app.UseAuthentication();
            app.UseRouting();
            app.UseAuthorization();

            app.UseCors("Default");

            app.UseApiExceptionHandler();

            app.UseSerilogRequestLogging();

            app.UseSwagger(options => options.PreSerializeFilters.Add((swaggerDocument, httpRequest) =>
            {
                swaggerDocument.Servers = new List<OpenApiServer>
                {
                    new OpenApiServer { Url = $"{httpRequest.Scheme}://{httpRequest.Host.Value}/{swaggerDocument.Info.Version}" }
                };
            }));
            app.UseSwaggerUI(options =>
            {
                options.SwaggerEndpoint("/swagger/v1/swagger.json", "V1");
                options.RoutePrefix = "developers";

                options.OAuthClientId(_configuration.GetValue<string>("Swagger:ClientId"));
                options.OAuthUsePkce();
                options.OAuthScopes(_configuration.GetValue<string>("Swagger:Scope"));
                options.InjectStylesheet("/swagger-ui/custom.css");
                options.InjectJavascript("/swagger-ui/custom.js");
            });

            ExcelPackage.LicenseContext = LicenseContext.NonCommercial;

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHealthChecks("/health-api", new HealthCheckOptions
                {
                    Predicate = _ => true,
                    ResponseWriter = UIResponseWriter.WriteHealthCheckUIResponse
                });
                endpoints.MapControllers();
            });

            // Used to fallback all requests to wwwroot/index.html in production
            if (!env.IsDevelopment())
            {
                app.Run(async (context) =>
                {
                    context.Response.ContentType = "text/html";
                    await context.Response.SendFileAsync(Path.Combine(env.WebRootPath, "index.html"));
                });
            }

            // Used to redirect "/" to "/developers" in development (for convenience) 
            if (env.IsDevelopment())
            {
                app.UseRewriter(new RewriteOptions()
                    .AddRedirect("^$", "developers"));
            }
        }
    }
}
