using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Text.Json;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    internal class ClaimsAuthorizeAttribute : Attribute, IAuthorizationFilter
    {
        private readonly string _claimType;
        private readonly ClaimValue _claimValue;

        public ClaimsAuthorizeAttribute(string claimType, ClaimValue claimValue)
        {
            _claimType = claimType;
            _claimValue = claimValue;
        }

        public void OnAuthorization(AuthorizationFilterContext context)
        {
            if (context.HttpContext.User.Identity.IsAuthenticated)
            {
                var claim = context.HttpContext.User.FindFirst(_claimType);

                var authorized =
                    claim != null &&
                    Enum.TryParse(claim.Value, out ClaimValue value) &&
                    (value & _claimValue) == _claimValue;

                if (!authorized)
                {
                    context.Result = new ApplicationForbidResult("You do not have the required permission to access this resource.");
                }
            }
            else
            {
                context.Result = new UnauthorizedResult();
            }
        }
    }
}
