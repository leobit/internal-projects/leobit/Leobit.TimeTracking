namespace Leobit.TimeTracking.WEB.Infrastructure
{
    internal class Routes
    {
        public const string ApiVersion = "v{version:apiVersion}/";
        public const string Id = "{id}";
        public const string Default = "Default";

        public const string Me = "me";
        public const string User = "User";
        public const string Division = "Division";
        public const string OrganizationUnit = "OrganizationUnit";
        public const string Category = "Category";
        public const string Account = "Account";
        public const string Project = "Project";
        public const string TimeReport = "TimeReport";
        public const string UserProject = "UserProject";
        public const string ProjectManager = "ProjectManager";
        public const string Settings = "Settings";
        public const string Vacation = "Vacation";
        public const string CustomDayOff = "Custom";
        public const string SickLeave = "SickLeave";
        public const string Employee = "Employee";
        public const string Operational = "Operational";
        public const string Role = "Role";
        public const string Permission = "Permission";
        public const string Analytic = "Analytic";
        public const string Slack = "Slack";
        public const string FiltersPreset = "FiltersPreset";
        public const string Reserve = "Reserve";
        public const string TimeOff = "TimeOff";
    }
}