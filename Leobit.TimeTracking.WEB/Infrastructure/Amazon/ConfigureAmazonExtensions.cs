﻿using Amazon.Runtime;
using Amazon.SQS;
using Leobit.TimeTracking.General.AmazonSQS;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Leobit.TimeTracking.WEB.Infrastructure.Amazon
{
    public static class ConfigureAmazonExtensions
    {
        public static void ConfigureAmazonSqs(this IServiceCollection services, IConfiguration configuration)
        {
            var awsOptions = configuration.GetAWSOptions();

            var key = configuration.GetValue<string>("AWS:Credentials:AccessKey");
            var secret = configuration.GetValue<string>("AWS:Credentials:SecretKey");
            //var key = "AKIAVUEQZQQKLZSUQLOD";
            //var secret = "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";

            var awsCredetials = new BasicAWSCredentials(key, secret);
            awsOptions.Credentials = awsCredetials;
            
            services.AddDefaultAWSOptions(awsOptions);
            services.AddAWSService<IAmazonSQS>();
            services.Configure<AmazonSqsSettings>(configuration.GetSection("AWS:SqsSettings"));
            services.AddSingleton<AmazonSqsService>();
        }
    }
}
