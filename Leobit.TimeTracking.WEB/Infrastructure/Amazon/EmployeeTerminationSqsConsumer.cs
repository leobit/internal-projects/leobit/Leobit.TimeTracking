﻿using Amazon.SQS;
using Amazon.SQS.Model;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.General.AmazonSQS;
using Leobit.TimeTracking.General.AmazonSQS.Messages;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Options;
using System;
using System.Reflection;
using System.Text.Json;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.Consumer
{
    public class EmployeeTerminationSqsConsumer : BackgroundService
    {
        private static readonly ILog _log = LogManager.GetLog(MethodBase.GetCurrentMethod().DeclaringType);

        private readonly AmazonSqsSettings _options;
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IAmazonSQS _amazonSqs;
        private readonly string sqsQueueUrl;

        public EmployeeTerminationSqsConsumer(
            IServiceScopeFactory serviceScopeFactory,
            IAmazonSQS amazonSQS,
            IOptions<AmazonSqsSettings> options)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _amazonSqs = amazonSQS;
            _options = options?.Value ?? throw new ArgumentNullException(nameof(options));

            if (!_options.EnableEmployeeTerminationHandling)
            {
                return;
            }

            var request = new GetQueueUrlRequest
            {
                QueueName = _options.EmployeeTerminationQueueName,
                QueueOwnerAWSAccountId = _options.QueueOwnerAWSAccountId,
                
            };

            try
            {
                var response = _amazonSqs.GetQueueUrlAsync(request)
                    .GetAwaiter()
                    .GetResult();
                sqsQueueUrl = response.QueueUrl;
            }
            catch (Exception ex)
            {
                _log.Error(ex);
            }
        }

        protected async override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            if (!_options.EnableEmployeeTerminationHandling)
            {
                return;
            }

            using var scope = _serviceScopeFactory.CreateScope();

            var userService = scope.ServiceProvider.GetRequiredService<UserService>();

            while (true)
            {
                try
                {
                    var request = new ReceiveMessageRequest
                    {
                        QueueUrl = sqsQueueUrl,
                        WaitTimeSeconds = 5,
                    };

                    var result = await _amazonSqs.ReceiveMessageAsync(request, stoppingToken);

                    foreach (var message in result.Messages)
                    {
                        await userService.UpdateTerminatedUser(
                            JsonSerializer.Deserialize<UserTerminatedMessage>(message.Body).UserId);

                        await _amazonSqs.DeleteMessageAsync(sqsQueueUrl, message.ReceiptHandle, stoppingToken);
                    }

                    await Task.Delay(30 * 1000, stoppingToken);
                }
                catch (Exception ex)
                {
                    _log.Error(ex);
                }
            }
        }
    }
}
