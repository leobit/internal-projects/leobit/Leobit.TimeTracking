using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using System;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    internal class ClaimsRouteAttribute : RouteAttribute, IActionConstraintFactory
    {
        private readonly IActionConstraint _constraint;

        public bool IsReusable => true;

        public ClaimsRouteAttribute(string claimType, ClaimValue claimValue) : this(string.Empty, claimType, claimValue) { }

        public ClaimsRouteAttribute(string template, string claimType, ClaimValue claimValue) : base(template)
        {
            Order = -10;
            _constraint = new ClaimsActionConstraint(claimType, claimValue);
        }

        public IActionConstraint CreateInstance(IServiceProvider services)
        {
            return _constraint;
        }
    }
}
