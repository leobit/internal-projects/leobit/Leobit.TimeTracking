﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class FiltersPresetProfile : Profile
    {
        public FiltersPresetProfile() 
        {
            CreateMap<FiltersPresetDTO, FiltersPreset>().ReverseMap();
            CreateMap<EmployeeReportingFiltersDTO, EmployeeReportingFilters>().ReverseMap();
            CreateMap<DateRangeDTO, DateRange>().ReverseMap();
            CreateMap<SelectedItemListDTO<int>, SelectedItemList<int>>().ReverseMap();
            CreateMap<SelectedItemListDTO<string>, SelectedItemList<string>>().ReverseMap();
        }
    }
}
