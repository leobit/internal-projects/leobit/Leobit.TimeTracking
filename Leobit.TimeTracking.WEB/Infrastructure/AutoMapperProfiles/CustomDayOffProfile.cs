using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class CustomDayOffProfile : Profile
    {
        public CustomDayOffProfile()
        {
            CreateMap<CustomDayOffDTO, CustomDayOff>()
                .ForMember(d => d.LastEditorFullName,
                    o => o.MapFrom(s => s.LastEditor.FirstName + " " + s.LastEditor.LastName))
                .ForMember(d => d.UserFullName,
                    o => o.MapFrom(s => s.User.FirstName + " " + s.User.LastName));
            CreateMap<CustomDayOff, CustomDayOffDTO>()
                .ForMember(d => d.User, o => o.MapFrom(s => new UserShortDTO { Id = s.UserId }))
                .ForMember(d => d.LastEditor,
                    o => o.MapFrom(s => new UserShortDTO { Id = s.LastEditorId }));

            CreateMap<CreateCustomDayOff, CustomDayOffDTO>()
                .ForMember(d => d.User,
                    o => o.MapFrom(s => new UserShortDTO { Id = s.UserId }));
            CreateMap<EditCustomDayOff, CustomDayOffDTO>()
                .ForMember(d => d.User,
                    o => o.MapFrom(s => new UserShortDTO { Id = s.UserId }));
        }
    }
}
