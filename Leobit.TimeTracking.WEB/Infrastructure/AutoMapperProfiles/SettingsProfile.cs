﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;
namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class SettingsProfile : Profile
    {
        public SettingsProfile()
        {
            CreateMap<SettingsDTO, Settings>();
            CreateMap<Settings, SettingsDTO>();  
        }
    }
}
