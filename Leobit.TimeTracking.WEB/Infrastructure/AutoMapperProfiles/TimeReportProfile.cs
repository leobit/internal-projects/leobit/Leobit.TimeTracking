using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;
using System;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class TimeReportProfile : Profile
    {
        public TimeReportProfile()
        {
            CreateMap<TimeReportDTO, UserTimeReport>();
            CreateMap<UserTimeReport, TimeReportDTO>();
            CreateMap<TimeReportReminderDTO, TimeReportReminder>();

            CreateMap<CreateTimeReport, TimeReportDTO>()
                .ForMember(d => d.StartTime, o => o.MapFrom(s => s.StartTime.HasValue ? s.ReportingDate.Date.Add(s.StartTime.Value.TimeOfDay) : s.StartTime))
                .ForMember(d => d.Project, o => o.MapFrom(s => new ProjectShortDTO { Id = s.ProjectId }));
            CreateMap<UpdateTimeReport, TimeReportDTO>()
                .ForMember(d => d.StartTime, o => o.MapFrom(s => s.StartTime.HasValue ? s.ReportingDate.Date.Add(s.StartTime.Value.TimeOfDay) : s.StartTime))
                .ForMember(d => d.Project, o => o.MapFrom(s => new ProjectShortDTO { Id = s.ProjectId }));

            CreateMap<TimeReportDTO, TimeReport>();
            CreateMap<TimeReport, TimeReportDTO>();
        }
    }
}
