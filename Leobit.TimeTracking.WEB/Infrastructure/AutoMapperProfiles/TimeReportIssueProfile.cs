﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class TimeReportIssueProfile: Profile
    {
        public TimeReportIssueProfile()
        {
            CreateMap<TimeReportIssueDTO, TimeReportIssue>();
        }
    }
}
