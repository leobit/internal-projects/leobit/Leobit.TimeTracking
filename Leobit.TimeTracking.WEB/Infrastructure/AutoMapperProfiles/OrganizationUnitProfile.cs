﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class OrganizationUnitProfile : Profile
    {
        public OrganizationUnitProfile()
        {
            CreateMap<OrganizationUnitDTO, OrganizationUnit>();
            CreateMap<OrganizationUnit, OrganizationUnitDTO>();

            CreateMap<OrganizationUnitShortDTO, OrganizationUnitShort>();
            CreateMap<OrganizationUnitShort, OrganizationUnitShortDTO>();

            CreateMap<OrganizationUnitShortDTO, OrganizationUnit>();
        }
    }
}
