﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class UserDefaultProfile : Profile
    {
        public UserDefaultProfile()
        {
            CreateMap<UserDefaultDTO, UserDefault>();

            CreateMap<UserDefaultsDTO, UserDefaults>();
        }
    }
}
