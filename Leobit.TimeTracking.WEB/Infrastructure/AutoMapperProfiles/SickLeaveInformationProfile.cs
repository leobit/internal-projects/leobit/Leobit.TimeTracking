﻿using System.Linq;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class SickLeaveInformationProfile : Profile
    {
        public SickLeaveInformationProfile()
        {
            CreateMap<SickLeaveMonthDataDTO, SickLeaveMonthData>();

            CreateMap<SickLeaveMonthDataDTO[], SickLeaveYearData>()
                .ForMember(y => y.January, options => options.MapFrom(s => s.Any() ? s[0] : new()))
                .ForMember(y => y.February, options => options.MapFrom(s => s.Any() ? s[1] : new()))
                .ForMember(y => y.March, options => options.MapFrom(s => s.Any() ? s[2] : new()))
                .ForMember(y => y.April, options => options.MapFrom(s => s.Any() ? s[3] : new()))
                .ForMember(y => y.May, options => options.MapFrom(s => s.Any() ? s[4] : new()))
                .ForMember(y => y.June, options => options.MapFrom(s => s.Any() ? s[5] : new()))
                .ForMember(y => y.July, options => options.MapFrom(s => s.Any() ? s[6] : new()))
                .ForMember(y => y.August, options => options.MapFrom(s => s.Any() ? s[7] : new()))
                .ForMember(y => y.September, options => options.MapFrom(s => s.Any() ? s[8] : new()))
                .ForMember(y => y.October, options => options.MapFrom(s => s.Any() ? s[9] : new()))
                .ForMember(y => y.November, options => options.MapFrom(s => s.Any() ? s[10] : new()))
                .ForMember(y => y.December, options => options.MapFrom(s => s.Any() ? s[11] : new()));

            CreateMap<SickLeaveInformationDTO, SickLeaveInformation>();
        }
    }
}
