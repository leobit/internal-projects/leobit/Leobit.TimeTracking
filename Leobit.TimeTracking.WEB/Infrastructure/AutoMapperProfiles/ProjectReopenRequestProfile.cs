using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class ProjectReopenRequestProfile : Profile
    {
        public ProjectReopenRequestProfile()
        {
            CreateMap<ProjectReopenRequest, ProjectReopenRequestDTO>()
                .ForMember(d => d.CreationTimestamp, opts => opts.Ignore());
            
            CreateMap<ProjectReopenRequestDetailsDTO, ProjectReopenRequestDetails>();

            CreateMap<ReopenReportingApprovalAuthorityDTO, ReopenReportingApprovalAuthority>()
                .ForMember(d => d.UserFullName, opts => opts.MapFrom(s => $"{s.FirstName} {s.LastName}"));
        }
    }
}
