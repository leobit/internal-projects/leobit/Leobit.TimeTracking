﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class DivisionProfile : Profile
    {
        public DivisionProfile()
        {
            CreateMap<DivisionDTO, Division>();
            CreateMap<Division, DivisionDTO>();
        }
    }
}
