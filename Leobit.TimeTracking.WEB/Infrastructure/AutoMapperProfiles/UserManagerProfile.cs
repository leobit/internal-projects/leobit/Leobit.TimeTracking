﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class UserManagerProfile : Profile
    {
        public UserManagerProfile()
        {
            CreateMap<ManagerDTO, Manager>();
            CreateMap<Manager, ManagerDTO>();

            CreateMap<UserManagerDTO, UserManager>();
            CreateMap<UserManager, UserManagerDTO>();
        }
    }
}
