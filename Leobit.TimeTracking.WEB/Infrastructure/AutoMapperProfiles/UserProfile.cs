﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class UserProfile : Profile
    {
        public UserProfile()
        {
            CreateMap<UserShortDTO, UserShort>();
            CreateMap<UserShort, UserShortDTO>();
            CreateMap<UserShortDTO, ReopenReportingApprovalAuthority>();
        }
    }
}
