using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;
using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class ProjectProfile : Profile
    {
        public ProjectProfile()
        {
            CreateMap<ProjectDTO, Project>()
                .ForMember(p => p.ProjectSettingsBackup, options => options.MapFrom((pDTO, p, i, context) =>
                    context.Mapper.Map<ProjectSettingsBackup>(pDTO.ProjectSettingsBackup)))
                .ForMember(p => p.ProjectReopenRequests, options => options.MapFrom((pDTO, p, i, context) =>
                    context.Mapper.Map<List<ProjectReopenRequestDetails>>(pDTO.ProjectReopenRequests)));
            CreateMap<Project, ProjectDTO>()
                .ForMember(p => p.ProjectSettingsBackup, options => options.Ignore())
                .ForMember(p => p.ProjectReopenRequests, options => options.Ignore());

            CreateMap<ProjectShortDTO, ProjectShort>().ReverseMap();

            CreateMap<Restriction, RestrictionDTO>().ReverseMap();

            CreateMap<AllowedDateRange, AllowedDateRangeDTO>().ReverseMap();
        }
    }
}
