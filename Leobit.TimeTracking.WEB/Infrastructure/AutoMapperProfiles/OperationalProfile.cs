﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class OperationalProfile : Profile
    {
        public OperationalProfile()
        {
            CreateMap<HolidayDTO, Holiday>();
            CreateMap<Holiday, HolidayDTO>();

            CreateMap<MonthHours, MonthHoursDTO>();
            CreateMap<MonthHoursDTO, MonthHours>();
        }
    }
}
