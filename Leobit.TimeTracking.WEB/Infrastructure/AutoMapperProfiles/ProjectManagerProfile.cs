using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class ProjectManagerProfile : Profile
    {
        public ProjectManagerProfile()
        {
            CreateMap<ProjectManagerDTO, ProjectManager>();
            CreateMap<ProjectManager, ProjectManagerDTO>();
        }
    }
}
