using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class UserProjectProfile : Profile
    {
        public UserProjectProfile()
        {
            CreateMap<UserProjectDTO, UserProject>();
            CreateMap<UserProject, UserProjectDTO>();
            CreateMap<ProjectDTO, ProjectShort>();
        }
    }
}
