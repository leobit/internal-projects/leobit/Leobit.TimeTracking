using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class AccountProfile : Profile
    {
        public AccountProfile()
        {
            CreateMap<AccountDTO, Account>();
            CreateMap<Account, AccountDTO>();

            CreateMap<AccountShortDTO, AccountShort>();
            CreateMap<AccountShort, AccountShortDTO>();

            CreateMap<AccountEligibleBudgetOwnerDTO, AccountEligibleBudgetOwner>();
        }
    }
}
