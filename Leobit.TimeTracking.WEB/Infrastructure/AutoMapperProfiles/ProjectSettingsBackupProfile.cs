using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;
using System;

namespace Leobit.TimeTracking.WEB.Infrastructure.AutoMapperProfiles
{
    public class ProjectSettingsBackupProfile : Profile
    {
        public ProjectSettingsBackupProfile()
        {
            CreateMap<ProjectSettingsBackupDTO, ProjectSettingsBackup>()
                .ForMember(
                    p => p.ExpirationTime,
                    options => options.MapFrom(pDTO =>
                        new DateTime(pDTO.ExpirationTime.Year, pDTO.ExpirationTime.Month, pDTO.ExpirationTime.Day, pDTO.ExpirationTime.Hour, pDTO.ExpirationTime.Minute, 0, DateTimeKind.Utc)));
            CreateMap<ProjectSettingsBackup, ProjectSettingsBackupDTO>();
        }
    }
}
