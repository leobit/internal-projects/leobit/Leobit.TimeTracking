﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Infrastructure.SwashbuckleFilters
{
    public class WordExportOperations : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.OperationId != null && operation.OperationId.Contains("Word"))
            {
                operation.Responses["200"].Content = new Dictionary<string, OpenApiMediaType>
            {
                {
                    "application/octet-stream",
                    new OpenApiMediaType
                    {
                        Schema = new OpenApiSchema
                        {
                            Format = "binary",
                            Type = "string",
                            Description = "Download file"
                        }
                    }
                }
            };
            }
        }
    }
}
