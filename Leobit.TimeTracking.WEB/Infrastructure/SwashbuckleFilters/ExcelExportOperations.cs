﻿using Microsoft.OpenApi.Models;
using Swashbuckle.AspNetCore.SwaggerGen;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Infrastructure.SwashbuckleFilters
{
    public class ExcelExportOperations : IOperationFilter
    {
        public void Apply(OpenApiOperation operation, OperationFilterContext context)
        {
            if (operation.OperationId != null && operation.OperationId.Contains("Excel"))
            {
                operation.Responses["200"].Content = new Dictionary<string, OpenApiMediaType>
                {
                    {
                        "application/octet-stream",
                        new OpenApiMediaType
                        {
                            Schema = new OpenApiSchema
                            {
                                Format = "binary",
                                Type = "string",
                                Description = "Download file"
                            }
                        }
                    }
                };
            }
        }
    }
}

