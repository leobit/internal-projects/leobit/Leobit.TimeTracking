﻿using IdentityModel;
using Microsoft.AspNetCore.Mvc.Controllers;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    public class LogActionAttribute : Attribute, IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            var userId = context.HttpContext.User.FindFirst(JwtClaimTypes.Subject)?.Value ?? "Unknown";
            var controller = string.Empty;
            var action = string.Empty;
            var displayName = context.ActionDescriptor.DisplayName;
            var arguments = context.ActionArguments;

            if (context.ActionDescriptor is ControllerActionDescriptor controllerActionDescriptor)
            {
                controller = controllerActionDescriptor.ControllerName;
                action = controllerActionDescriptor.ActionName;
            }

            Serilog.Log.Information("userId={@userId}, controller={@controller}, action={@action}, displayName={@displayName}, arguments={@arguments}",
                userId, controller, action, displayName, arguments);
        }
    }
}
