﻿using IdentityModel;
using Microsoft.AspNetCore.Mvc;
using System.Linq;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    public static class ControllerExtentions
    {
        public static string UserId(this ControllerBase controller)
        {
            return controller.User.FindFirst(JwtClaimTypes.Subject).Value;
        }
    }
}
