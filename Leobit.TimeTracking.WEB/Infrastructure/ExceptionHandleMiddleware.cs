﻿using System;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    /// <summary>
    /// Handle Exceptions (mostly unhandled) on Middleware level
    /// </summary>
    internal class ExceptionHandleMiddleware
    {
        private static readonly ILog Log = LogManager.GetLog(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        private readonly RequestDelegate _next;

        public ExceptionHandleMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception ex)
            {
                HandleException(context, ex);
                throw;
            }
        }

        private void HandleException(HttpContext context, Exception exception)
        {
            var errorCode = GetErrorCode(context.TraceIdentifier);
            Log.Error($"An unhandled exception occurred; please contact the help desk with the following error code: '{errorCode}'  [{context.TraceIdentifier}]", exception);
        }

        private static string GetErrorCode(string traceIdentifier)
        {
            const int errorCodeLength = 6;
            const string codeValues = "BCDFGHJKLMNPQRSTVWXYZ";
            var hasher = MD5.Create();
            var sb = new StringBuilder(10);
            var traceBytes = hasher.ComputeHash(Encoding.UTF8.GetBytes(traceIdentifier));
            var codeValuesLength = codeValues.Length;
            for (var i = 0; i < errorCodeLength; i++)
                sb.Append(codeValues[traceBytes[i] % codeValuesLength]);
            return sb.ToString();
        }
    }

    /// <summary>
    /// Provide extension method to include Unhandled Exception handler to Application builder during Configuration
    /// </summary>
    public static class ExceptionHandleMiddlewareExtensions
    {
        public static IApplicationBuilder UseApiExceptionHandler(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<ExceptionHandleMiddleware>();
        }
    }
}
