﻿using Leobit.TimeTracking.DAL.Entities;
using Leobit.TimeTracking.WEB.Controllers;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
	public static class HealthExtensions
	{
		public static void AddLeobitHealthChecks(this IServiceCollection services, IConfiguration configuration)
		{
			services
				.AddHealthChecks()
				.AddCheck<ApiHealthCheck>("Api", tags: new string[]
				{
					"timetracking",
					"api"
				})
				.AddDbContextCheck<LeobitContext>("DB Context", tags: new string[]
				{
					"timetracking",
					"db",
				});
		}
	}
}
