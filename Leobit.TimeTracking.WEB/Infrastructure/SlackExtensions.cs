﻿using System;
using System.Net.Http.Headers;
using Leobit.TimeTracking.BL.Infrastructure.Slack;
using Leobit.TimeTracking.BL.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    public static class SlackExtensions
    {
        public static IServiceCollection AddSlackServices(this IServiceCollection services,
            IConfiguration configuration)
        {
            services.Configure<SlackSettings>(configuration.GetSection("Slack"));
            services.AddScoped<SlackService>();

            services.AddHttpClient("Slack", httpClient =>
            {
                httpClient.BaseAddress = new Uri(configuration.GetValue<string>("Slack:SlackHostUrl"));

                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer",
                    configuration.GetValue<string>("Slack:SlackBotToken"));
            });

            return services;
        }
    }
}