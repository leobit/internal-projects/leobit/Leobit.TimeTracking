using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Microsoft.AspNetCore.Mvc.ActionConstraints;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    internal class ClaimsActionConstraint : IActionConstraint
    {
        private readonly string _claimType;
        private readonly ClaimValue _claimValue;

        public int Order => 0;

        public ClaimsActionConstraint(string claimType, ClaimValue claimValue)
        {
            _claimType = claimType;
            _claimValue = claimValue;
        }

        public bool Accept(ActionConstraintContext context)
        {
            var claim = context.RouteContext.HttpContext.User.FindFirst(_claimType);

            return
                claim != null &&
                Enum.TryParse(claim.Value, out ClaimValue value) &&
                (value & _claimValue) == _claimValue;
        }
    }
}
