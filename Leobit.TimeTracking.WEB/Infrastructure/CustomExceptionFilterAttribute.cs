using Leobit.TimeTracking.General.Infratstructure;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    public class CustomExceptionFilterAttribute : ExceptionFilterAttribute
    {
        public override void OnException(ExceptionContext context)
        {
            switch (context.Exception)
            {
                case NotFoundException _:
                    context.Result = new NotFoundResult();
                    break;
                case ForbidException _:
                    context.Result = new ForbidResult();
                    break;
                case ConflictException ex:
                    context.Result = new ConflictObjectResult(ex.Message);
                    break;
            }
            base.OnException(context);
        }
    }
}
