﻿using Leobit.TimeTracking.BL.Infrastructure.Settings;
using Leobit.TimeTracking.BL.Services;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System.Threading.Tasks;
using System.Threading;
using System;
using Microsoft.Extensions.DependencyInjection;

namespace Leobit.TimeTracking.WEB.Infrastructure.HostedServices
{
    public class UpdateSlackMessagesHostedService : BackgroundService
    {
        private readonly ILogger<ProjectCleanupHostedService> _logger;
        private readonly SlackReopenRequestMessagesSettings _slackReopenRequestMessagesSettings;

        public UpdateSlackMessagesHostedService(
            IServiceProvider services,
            ILogger<ProjectCleanupHostedService> logger,
            IOptions<SlackReopenRequestMessagesSettings> slackReopenRequestMessagesSettingsOptions)
        {
            Services = services;
            _logger = logger;
            _slackReopenRequestMessagesSettings = slackReopenRequestMessagesSettingsOptions.Value;
        }

        public IServiceProvider Services { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Service 'Update Slack messages regarding reopen request' running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Service 'Update Slack messages regarding reopen request' is working.");

            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = Services.CreateScope())
                {
                    var service =
                        scope.ServiceProvider
                            .GetRequiredService<ProjectReportingReopenRequestService>();

                    var expirationTime = DateTime.UtcNow;
                    await service.UpdateProjectReopenRequestSentMessages(expirationTime);
                }

                await Task.Delay(TimeSpan.FromMinutes(_slackReopenRequestMessagesSettings.UpdateMessageIntervalInMinutes), stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Service 'Update Slack messages regarding reopen request' is stopping.");

            await base.StopAsync(stoppingToken);
        }
    }
}