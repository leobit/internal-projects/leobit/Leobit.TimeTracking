﻿using Leobit.TimeTracking.BL.Infrastructure.Slack;
using Leobit.TimeTracking.BL.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Infrastructure.HostedServices
{
	public class ProjectSettingsBackupCleanupHostedService : BackgroundService
	{
        private readonly ILogger<ProjectSettingsBackupCleanupHostedService> _logger;
        private readonly ProjectSettingsBackupSettings _projectSettingsBackupSettings;

        public ProjectSettingsBackupCleanupHostedService(
			IServiceProvider services,
            ILogger<ProjectSettingsBackupCleanupHostedService> logger,
            IOptions<ProjectSettingsBackupSettings> projectSettingsBackupSettingsOptions)
		{
			Services = services;
            _logger = logger;
            _projectSettingsBackupSettings = projectSettingsBackupSettingsOptions.Value;
		}

		public IServiceProvider Services { get; }

		protected override async Task ExecuteAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("Project Settings Backup Cleanup Service running.");

			await DoWork(stoppingToken);
		}

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Settings Backup Cleanup Service is working.");

            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = Services.CreateScope())
                {
                    var projectReportingReopenRequestService =
                        scope.ServiceProvider
                            .GetRequiredService<ProjectReportingReopenRequestService>();

                    var expirationTime = DateTime.UtcNow;
                    await projectReportingReopenRequestService.CleanUpExpiredProjectSettingsBackups(expirationTime);
                }

                await Task.Delay(TimeSpan.FromMinutes(_projectSettingsBackupSettings.CleanupIntervalInMinutes), stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("Project Settings Backup Cleanup Service is stopping.");

			await base.StopAsync(stoppingToken);
		}
	}
}
