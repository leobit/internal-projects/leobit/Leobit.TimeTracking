﻿using Leobit.TimeTracking.BL.Services;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Infrastructure.HostedServices
{
	public class UserProjectsCleanupHostedService : BackgroundService
	{
		private readonly ILogger<UserProjectsCleanupHostedService> _logger;
		private readonly IWebHostEnvironment _environment;

		public UserProjectsCleanupHostedService(
			IWebHostEnvironment environment,
			IServiceProvider services,
			ILogger<UserProjectsCleanupHostedService> logger)
		{
			Services = services;
			_logger = logger;
			_environment = environment;
		}

		public IServiceProvider Services { get; }

		protected override async Task ExecuteAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("User Cleanup Service running.");

			await DoWork(stoppingToken);
		}

		private async Task DoWork(CancellationToken stoppingToken)
		{
			_logger.LogInformation("User Cleanup Service is working.");

			if (_environment.IsProduction())
			{
				while (!stoppingToken.IsCancellationRequested)
				{
					using (var scope = Services.CreateScope())
					{
						var userService =
							scope.ServiceProvider
								.GetRequiredService<UserProjectService>();

						await userService.CleanupProjectAssigments();
					}

					await Task.Delay(TimeSpan.FromDays(1));
				}
			}
		}

		public override async Task StopAsync(CancellationToken stoppingToken)
		{
			_logger.LogInformation("Use Cleanup Service is stopping.");

			await base.StopAsync(stoppingToken);
		}
	}
}
