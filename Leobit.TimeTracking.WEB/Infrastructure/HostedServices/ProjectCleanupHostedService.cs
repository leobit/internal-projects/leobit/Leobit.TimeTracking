﻿using Leobit.TimeTracking.BL.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Infrastructure.Settings;

namespace Leobit.TimeTracking.WEB.Infrastructure.HostedServices
{
    public class ProjectCleanupHostedService : BackgroundService
    {
        private readonly ILogger<ProjectCleanupHostedService> _logger;
        private readonly ProjectCleanupSettings _projectCleanupSettings;

        public ProjectCleanupHostedService(
            IServiceProvider services,
            ILogger<ProjectCleanupHostedService> logger,
            IOptions<ProjectCleanupSettings> projectCleanupSettingsOptions)
        {
            Services = services;
            _logger = logger;
            _projectCleanupSettings = projectCleanupSettingsOptions.Value;
        }

        public IServiceProvider Services { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Cleanup Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Cleanup Service is working.");

            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = Services.CreateScope())
                {
                    var projectService =
                        scope.ServiceProvider
                            .GetRequiredService<ProjectService>();

                    var expirationTime = DateTime.UtcNow;
                    await projectService.CleanUpProjects(expirationTime);
                }

                await Task.Delay(TimeSpan.FromDays(_projectCleanupSettings.CleanupIntervalInDays), stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Cleanup Service is stopping.");

            await base.StopAsync(stoppingToken);
        }
    }
}
