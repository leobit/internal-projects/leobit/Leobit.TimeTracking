﻿using Leobit.TimeTracking.BL.Infrastructure.Slack;
using Leobit.TimeTracking.BL.Services;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Infrastructure.HostedServices
{
    public class ProjectReportingReopenRequestCleanupHostedService : BackgroundService
    {
        private readonly ILogger<ProjectReportingReopenRequestCleanupHostedService> _logger;
        private readonly ProjectReportingReopenRequestSettings _projectReportingReopenRequestSettings;

        public ProjectReportingReopenRequestCleanupHostedService(
            IServiceProvider services,
            ILogger<ProjectReportingReopenRequestCleanupHostedService> logger,
            IOptions<ProjectReportingReopenRequestSettings> projectReportingReopenRequestSettingsOptions)
        {
            Services = services;
            _logger = logger;
            _projectReportingReopenRequestSettings = projectReportingReopenRequestSettingsOptions.Value;
        }

        public IServiceProvider Services { get; }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Reporting Reopen Request Cleanup Service running.");

            await DoWork(stoppingToken);
        }

        private async Task DoWork(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Reporting Reopen Request Cleanup Service is working.");

            while (!stoppingToken.IsCancellationRequested)
            {
                using (var scope = Services.CreateScope())
                {
                    var projectReportingReopenRequestService =
                        scope.ServiceProvider
                            .GetRequiredService<ProjectReportingReopenRequestService>();

                    var expirationTime = DateTime.UtcNow;
                    await projectReportingReopenRequestService.CleanUpExpiredProjectReopenRequests(expirationTime);
                }

                await Task.Delay(TimeSpan.FromMinutes(_projectReportingReopenRequestSettings.CleanupIntervalInMinutes), stoppingToken);
            }
        }

        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            _logger.LogInformation("Project Reporting Reopen Request Cleanup Service is stopping.");

            await base.StopAsync(stoppingToken);
        }
    }
}
