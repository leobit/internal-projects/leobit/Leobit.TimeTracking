﻿using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Leobit.TimeTracking.WEB.Infrastructure
{
    public class ApplicationForbidResult : IActionResult
    {
        private readonly string _message;

        public ApplicationForbidResult(string message)
        {
            _message = message;
        }

        public async Task ExecuteResultAsync(ActionContext context)
        {
            context.HttpContext.Response.StatusCode = StatusCodes.Status403Forbidden;
            context.HttpContext.Response.ContentType = "application/json";
            var jsonResponse = JsonSerializer.Serialize(_message);
            await context.HttpContext.Response.WriteAsync(jsonResponse);
        }
    }
}
