using System;

namespace Leobit.TimeTracking.WEB.Infrastructure.Claims
{
    [Flags]
    internal enum ClaimValue
    {
        Read = 1,
        Write = 2
    }
}