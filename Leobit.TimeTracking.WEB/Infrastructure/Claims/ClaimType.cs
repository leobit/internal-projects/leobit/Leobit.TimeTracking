namespace Leobit.TimeTracking.WEB.Infrastructure.Claims
{
    internal static class ClaimType
    {
        public const string Division = "Division";
        public const string OrganizationUnit = "OrganizationUnit";
        public const string Category = "Category";
        public const string Account = "Account";
        public const string Project = "Project";
        public const string TimeReport = "TimeReport";
        public const string TeamTimeReport = "TeamTimeReport";
        public const string ProjectAssignment = "ProjectAssignment";
        public const string Role = "Role";
        public const string Settings = "Settings";
        public const string EmployeeVacationAndSickLeave = "EmployeeVacationAndSickLeave";
        public const string TimeReportHistory = "TimeReportHistory";
        public const string TransferVacationBalance = "TransferVacationBalance";
        public const string Export = "Export";
        public const string Holiday = nameof(Holiday);
	}
}