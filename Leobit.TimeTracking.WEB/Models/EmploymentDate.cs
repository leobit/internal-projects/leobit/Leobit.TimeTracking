﻿using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class EmploymentDate
    {
        public string UserId { get; set; }
        public string Date { get; set; }
    }
}
