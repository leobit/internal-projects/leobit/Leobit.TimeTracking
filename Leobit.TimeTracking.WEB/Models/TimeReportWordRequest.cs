﻿using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Models
{
    public class TimeReportWordRequest
    {
        public int[] Ids { get; set; }
        public ICollection<string> UserIds { get; set; }
        public DateTime DateFrom { get; set; }
        public DateTime DateTo { get; set; }
    }
}
