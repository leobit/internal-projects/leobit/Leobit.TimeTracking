﻿using Newtonsoft.Json;
using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class VacationMonthData
    {
        [JsonRequired]
        public double Accrual { get; set; }
        public double? Used { get; set; }
        public double? UsedUnpaid { get; set; }
        public bool ExcludeFromYearlyTotal { get; set; }
    }

    public class VacationYearData
    {
        public VacationMonthData January { get; set; }
        public VacationMonthData February { get; set; }
        public VacationMonthData March { get; set; }
        public VacationMonthData April { get; set; }
        public VacationMonthData May { get; set; }
        public VacationMonthData June { get; set; }
        public VacationMonthData July { get; set; }
        public VacationMonthData August { get; set; }
        public VacationMonthData September { get; set; }
        public VacationMonthData October { get; set; }
        public VacationMonthData November { get; set; }
        public VacationMonthData December { get; set; }
    }

    public class VacationInformation
    {
        [JsonRequired]
        public DateTime? EmploymentDate { get; set; }
        [JsonRequired]
        public bool IsTMEmployee { get; set; }
        [JsonRequired]
        public DateTime? TerminationDate{ get; set; }
        [JsonRequired]
        public DateTime? FirstReportedDate { get; set; }
        [JsonRequired]
        public double StandingYears { get; set; }
        [JsonRequired]
        public double TransferredBalance { get; set; }
        [JsonRequired]
        public int MinimumAllowedBalance { get; set; }
        [JsonRequired]
        public double CurrentVacationBalance { get; set; }
        [JsonRequired]
        public double ExpectedEoyVacationBalance { get; set; }
        [JsonRequired]
        public double CompensatedVacationLeftovers { get; set; }
        [JsonRequired]
        public double ExtraDaysOff { get; set; }
        [JsonRequired]
        public double ReportedReserveDays { get; set; }
        [JsonRequired]
        public double ReportedReserveDaysForLastSixMonths { get; set; }
        [JsonRequired]
        public VacationYearData YearData { get; set; }

        [JsonRequired]
        public double ProvidedSickLeaveDaysWithoutCertificate { get; set; }
        [JsonRequired]
        public double ProvidedSickLeaveDaysWithCertificate { get; set; }
        [JsonRequired]
        public double ProvidedSickLeaveDaysToBeCompensated100 { get; set; }
        [JsonRequired]
        public double ProvidedSickLeaveDaysToBeCompensated75 { get; set; }
        [JsonRequired]
        public double CurrentSickLeaveBalance { get; set; }
        [JsonRequired]
        public double OverusedSickLeavesFine { get; set; }
        [JsonRequired]
        public SickLeaveYearData SickLeaveYearData { get; set; }
    }
}
