﻿using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class DateRange
    {
        public DateRangeType Type { get; set; }

        public DateTime? DateFrom { get; set; }

        public DateTime? DateTo { get; set; }

        public TimeUnit? RelativeTimeUnit { get; set; }

        public TimeUnitState? RelativeTimeUnitState { get; set; }
    }

    public enum DateRangeType
    {
        Absolute,
        Relative
    }

    public enum TimeUnit
    {
        Day,
        Week,
        Month,
        Year,
    }

    public enum TimeUnitState
    {
        Current,
        Last,
    }
}
