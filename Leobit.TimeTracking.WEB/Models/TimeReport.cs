using Newtonsoft.Json;
using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class UserTimeReport
    {
        [JsonRequired]
        public int Id { get; set; }
        public AccountShort Account { get; set; }
        [JsonRequired]
        public ProjectShort Project { get; set; }
        [JsonRequired]
        public string WorkDescription { get; set; }
        [JsonRequired]
        public DateTime ReportingDate { get; set; }
        [JsonRequired]
        public double InternalHours { get; set; }
        [JsonRequired]
        public double ExternalHours { get; set; }
        [JsonRequired]
        public bool IsOverhead { get; set; }
        [JsonRequired]
        public bool SendEmail { get; set; }
        public DateTime? StartTime { get; set; }
        [JsonRequired]
        public bool IsEditable { get; set; }
    }

    public class UpdateTimeReport
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public int ProjectId { get; set; }
        [JsonRequired]
        public string WorkDescription { get; set; }
        [JsonRequired]
        public DateTime ReportingDate { get; set; }
        [JsonRequired]
        public double InternalHours { get; set; }
        [JsonRequired]
        public double ExternalHours { get; set; }
        [JsonRequired]
        public bool IsOverhead { get; set; }
        public DateTime? StartTime { get; set; }
    }

    public class CreateTimeReport
    {
        [JsonRequired]
        public int ProjectId { get; set; }
        [JsonRequired]
        public string WorkDescription { get; set; }
        [JsonRequired]
        public DateTime ReportingDate { get; set; }
        [JsonRequired]
        public double InternalHours { get; set; }
        [JsonRequired]
        public double ExternalHours { get; set; }
        [JsonRequired]
        public bool IsOverhead { get; set; }
        public bool SendEmail { get; set; }
        public DateTime? StartTime { get; set; }
    }

    public class TimeReport : UserTimeReport
    {
        [JsonRequired]
        public UserShort User { get; set; }
    }

    public class TimeReportReminder
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public string SlackUserId { get; set; }
        public double ExpectedHours { get; set; }
        public double ReportedHours { get; set; }
        public DateTime? ReminderDay { get; set; }
    }
}
