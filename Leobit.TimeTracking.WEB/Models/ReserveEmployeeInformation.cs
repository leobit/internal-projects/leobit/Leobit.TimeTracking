﻿using Newtonsoft.Json;
using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class ReserveEmployeeInformation
    {
        [JsonRequired]
        public string EmployeeId { get; set; }
        [JsonRequired]
        public string EmployeeFullName { get; set; }
        [JsonRequired]
        public DateTime LastReportTimestamp { get; set; }
        [JsonRequired]
        public double ReportedDaysForCurrentYear { get; set; }
        [JsonRequired]
        public double CurrentVacationBalance { get; set; }
        [JsonRequired]
        public double PenaltyAmountForCurrentYear { get; set; }
        [JsonRequired]
        public CustomDayOff VacationPenalty { get; set; }
        public double AfterVacationTaken { get; set; }
    }
}
