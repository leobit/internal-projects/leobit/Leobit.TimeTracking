﻿using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class Holiday
    {
        public DateTime Date { get; set; }
        public string Name { get; set; }
    }
}
