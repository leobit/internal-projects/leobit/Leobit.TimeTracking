﻿using Newtonsoft.Json;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Leobit.TimeTracking.WEB.Models
{
    public class RoleShort
    {
        [JsonRequired]
        public int Id { get; set; }

        [JsonRequired]
        public string Name { get; set; }
    }

    public class Role
    {
        [JsonRequired]
        public int Id { get; set; }

        [JsonRequired]
        [MaxLength(50)]
        public string Name { get; set; }
        
        [JsonRequired]
        public bool IsCustom { get; set; }        

        public bool IsSystemRole { get; set; }        
        public bool IsAssignedToUser { get; set; }

        [JsonRequired]
        public ICollection<RolePermission> RolePermission { get; set; }
    }
}
