﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Models
{
    public class TimeReportExcelRequest
    {
        public int[] Ids { get; set; }
        public ICollection<string> UserIds { get; set; }
    }
}
