﻿using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class FiltersPreset
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
        [JsonRequired]
        public EmployeeReportingFilters EmployeeReportingFilters { get; set; }
    }

    public class EmployeeReportingFilters
    {
        [JsonRequired]
        public DateRange DateRange { get; set; }
        [JsonRequired]
        public SelectedItemList<int> ProjectIds { get; set; }
        [JsonRequired]
        public SelectedItemList<string> UserIds { get; set; }
    }
}
