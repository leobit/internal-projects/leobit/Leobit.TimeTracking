﻿using System;
using System.Collections.Generic;
using System.Linq;
using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class UserDefaults
    {
        public int? AccountId { get; set; }
        public int? ProjectId { get; set; }
        [JsonRequired]
        public DateTime ReportingDate { get; set; }
        [JsonRequired]
        public double InternalHours { get; set; }
        [JsonRequired]
        public double ExternalHours { get; set; }

        public IEnumerable<UserDefault> Defaults { get; set; } = Enumerable.Empty<UserDefault>();
    }

    public class UserDefault
    {
        public int? AccountId { get; set; }
        public int? ProjectId { get; set; }
        [JsonRequired]
        public DateTime ReportingDate { get; set; }
        [JsonRequired]
        public double InternalHours { get; set; }
        [JsonRequired]
        public double ExternalHours { get; set; }
    }
}
