﻿using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Models
{
    public class UserManager
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public string SlackId { get; set; }
        public List<Manager> Managers { get; set; } = new List<Manager>();
    }

    public class Manager
    {
        public string UserId { get; set; }
        public string FullName { get; set; }
        public bool IsPrimary { get; set; }
        public string ProjectName { get; set; }
        public string SlackId { get; set; }
    }
}
