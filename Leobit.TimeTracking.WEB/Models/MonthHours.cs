﻿namespace Leobit.TimeTracking.WEB.Models
{
    public class MonthHours
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Hours { get; set; }
    }
}
