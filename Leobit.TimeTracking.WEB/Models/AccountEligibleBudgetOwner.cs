﻿using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class AccountEligibleBudgetOwner
    {
        public string DefaultBudgetOwnerId { get; set; }
        public UserShort[] EligibleBudgetOwners { get; set; } = Array.Empty<UserShort>();
    }
}
