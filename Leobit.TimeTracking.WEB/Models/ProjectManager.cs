using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class ProjectManager
    {
        [JsonRequired]
        public UserShort User { get; set; }
        [JsonRequired]
        public ProjectShort Project { get; set; }
    }
}
