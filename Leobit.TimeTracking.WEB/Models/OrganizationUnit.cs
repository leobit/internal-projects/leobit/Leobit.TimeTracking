﻿using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class OrganizationUnit
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
        [JsonRequired]
        public Division Division { get; set; }
    }

    public class OrganizationUnitShort
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
    }
}
