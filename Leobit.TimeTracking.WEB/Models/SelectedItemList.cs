﻿using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Models
{
    public class SelectedItemList<T>
    {
        public ICollection<T> SelectedItems { get; set; }
        public bool? AllSelected { get; set; }
    }
}
