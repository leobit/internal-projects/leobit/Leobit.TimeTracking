using Newtonsoft.Json;
using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class ProjectSettingsBackup
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public DateTime ExpirationTime { get; set; }
        [JsonRequired]
        public string ConfigurationSnapshot { get; set; }
    }
}
