﻿using Leobit.TimeTracking.General;
using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class Settings
    {
        [JsonRequired]
        public decimal MinInternal { get; set; }
        [JsonRequired]
        public decimal MaxInternal { get; set; }
        [JsonRequired]
        public decimal MinExternal { get; set; }
        [JsonRequired]
        public decimal MaxExternal { get; set; }
        [JsonRequired]
        public ClosingType ClosingType { get; set; }
        [JsonRequired]
        public int BeforeClosing { get; set; }
        [JsonRequired]
        public int AfterClosing { get; set; }
        [JsonRequired]
        public string DefaultBo { get; set; }
    }
}
