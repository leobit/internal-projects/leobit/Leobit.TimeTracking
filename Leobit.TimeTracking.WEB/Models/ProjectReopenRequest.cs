﻿using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class ProjectReopenRequest
    {
        public string SenderId { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
    }

    public class ProjectReopenRequestDetails
    {
        public int Id { get; set; }
        public string SenderFullName { get; set; }
        public string ProjectManagerApprovedFullName { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime ExpirationTime { get; set; }
        public bool CanApprove { get; set; }
    }
}
