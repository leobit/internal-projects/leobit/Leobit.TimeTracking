using Newtonsoft.Json;
using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class UserProject
    {
        [JsonRequired]
        public UserShort User { get; set; }
        [JsonRequired]
        public ProjectShort Project { get; set; }
        public DateTime? AssignmentDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Hours { get; set; }
        public decimal TotalInternalHours { get; set; }
    }
}
