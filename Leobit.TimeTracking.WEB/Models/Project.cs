using Leobit.TimeTracking.BL;
using Leobit.TimeTracking.General;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Models
{
    public class ProjectShort
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public AccountShort Account { get; set; }
        [JsonRequired]
        public string Name { get; set; }
        public bool IsClosed { get; set; }
        public Restriction Restriction { get; set; }
        public int ProjectType { get; set; }
        public ProjectPriority ProjectPriority { get; set; }
        public bool IsInternal { get; set; }
    }

    public class Project
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public CategoryShort Category { get; set; }
        [JsonRequired]
        public AccountShort Account { get; set; }
        [JsonRequired]
        public string Name { get; set; }
        [JsonRequired]
        public bool OverrideHoursSettings { get; set; }
        [JsonRequired]
        public decimal MinInternal { get; set; }
        [JsonRequired]
        public decimal MaxInternal { get; set; }
        [JsonRequired]
        public decimal MinExternal { get; set; }
        [JsonRequired]
        public decimal MaxExternal { get; set; }
        [JsonRequired]
        public bool OverrideClosingSettings { get; set; }
        [JsonRequired]
        public ClosingType ClosingType { get; set; }
        [JsonRequired]
        public int BeforeClosing { get; set; }
        [JsonRequired]
        public int AfterClosing { get; set; }
        [JsonRequired]
        public bool IsClosed { get; set; }
        [JsonRequired]
        public bool AutoAssignNewUsers { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public bool OverrideTimeReportPriorities { get; set; }
        public int ProjectType { get; set; }
        public ProjectPriority ProjectPriority { get; set; }
        public ProjectSettingsBackup ProjectSettingsBackup { get; set; }
        public ICollection<ProjectReopenRequestDetails> ProjectReopenRequests { get; set; }
    }
}
