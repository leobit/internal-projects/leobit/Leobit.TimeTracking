﻿using System;

namespace Leobit.TimeTracking.WEB.Models
{
    public class ReopenReportingApprovalAuthority
    {
        public string UserFullName { get; set; }
        public DateTime ReopenAllowedFromDate { get; set; }
        public DateTime? ReopenAllowedEndDate { get; set; }
    }
}
