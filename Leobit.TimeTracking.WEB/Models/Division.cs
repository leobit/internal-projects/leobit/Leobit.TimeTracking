﻿using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class Division
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
    }
}
