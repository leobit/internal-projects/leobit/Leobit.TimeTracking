﻿using Leobit.TimeTracking.BL.Services;
using System.Collections.Generic;

namespace Leobit.TimeTracking.WEB.Models
{
    public class TimeReportIssue
    {
        public List<TimeReport> Reports { get; set; }
        public UserShort User { get; set; }
        public TimeReportIssueType IssueType { get; set; }
        public string Description { get; set; }
    }
}
