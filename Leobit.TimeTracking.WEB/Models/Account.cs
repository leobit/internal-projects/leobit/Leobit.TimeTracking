using Leobit.TimeTracking.BL;
using Leobit.TimeTracking.General;
using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class AccountShort
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
    }

    public class Account
    {
        [JsonRequired]
        public int Id { get; set; }

        [JsonRequired]
        public string Name { get; set; }

        [JsonRequired]
        public string BudgetOwnerId { get; set; }

        [JsonRequired]
        public bool OverrideHoursSettings { get; set; }
        [JsonRequired]
        public decimal MinInternal { get; set; }
        [JsonRequired]
        public decimal MaxInternal { get; set; }
        [JsonRequired]
        public decimal MinExternal { get; set; }
        [JsonRequired]
        public decimal MaxExternal { get; set; }
        [JsonRequired]
        public bool OverrideClosingSettings { get; set; }
        [JsonRequired]
        public ClosingType ClosingType { get; set; }
        [JsonRequired]
        public int BeforeClosing { get; set; }
        [JsonRequired]
        public int AfterClosing { get; set; }
        [JsonRequired]
        public AccountType AccountType { get; set; }
        public bool IsActive { get; set; }
    }
}
