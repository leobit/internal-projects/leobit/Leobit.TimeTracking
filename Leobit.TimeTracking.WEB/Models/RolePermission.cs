﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Models
{
    public class RolePermission
    {
        [JsonRequired]
        public byte Value { get; set; }
        [JsonRequired]
        public Permission Permission { get; set; }
    }
}
