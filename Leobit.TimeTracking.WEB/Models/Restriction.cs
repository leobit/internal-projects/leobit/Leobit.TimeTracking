﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Models
{
    public class Restriction
    {
        [JsonRequired]
        public decimal MinInternal { get; set; }
        [JsonRequired]
        public decimal MaxInternal { get; set; }
        [JsonRequired]
        public decimal MinExternal { get; set; }
        [JsonRequired]
        public decimal MaxExternal { get; set; }
        [JsonRequired]
        public ICollection<AllowedDateRange> AllowedDateRanges { get; set; }
    }

    public class AllowedDateRange
    {
        [JsonRequired]
        public DateTime StartDate { get; set; }
        [JsonRequired]
        public DateTime EndDate { get; set; }
    }
}
