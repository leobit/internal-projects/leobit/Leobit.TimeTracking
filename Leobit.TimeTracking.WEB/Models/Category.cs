﻿using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class Category
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
        [JsonRequired]
        public OrganizationUnit OrganizationUnit { get; set; }
    }

    public class CategoryInput
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
        [JsonRequired]
        public OrganizationUnitShort OrganizationUnit { get; set; }
    }

    public class CategoryShort
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public string Name { get; set; }
    }
}
