using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class UserShort
    {
        [JsonRequired]
        public string Id { get; set; }
        [JsonRequired]
        public string FirstName { get; set; }
        [JsonRequired]
        public string LastName { get; set; }
        [JsonRequired]
        public int RoleId { get; set; }
    }
}
