using System;
using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class CustomDayOff
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public int Year { get; set; }
        [JsonRequired]
        public int Month { get; set; }
        [JsonRequired]
        public string Description { get; set; }
        [JsonRequired]
        public double ExtraDayOff { get; set; }
        [JsonRequired]
        public DateTime LastEditTimestamp { get; set; }
        [JsonRequired]
        public string UserId { get; set; }
        [JsonRequired]
        public string UserFullName { get; set; }
        [JsonRequired]
        public string LastEditorId { get; set; }
        [JsonRequired]
        public string LastEditorFullName { get; set; }
    }

    public class CreateCustomDayOff
    {
        [JsonRequired]
        public int Year { get; set; }
        [JsonRequired]
        public int Month { get; set; }
        [JsonRequired]
        public string Description { get; set; }
        [JsonRequired]
        public double ExtraDayOff { get; set; }
        [JsonRequired]
        public string UserId { get; set; }
    }

    public class EditCustomDayOff
    {
        [JsonRequired]
        public int Id { get; set; }
        [JsonRequired]
        public int Year { get; set; }
        [JsonRequired]
        public int Month { get; set; }
        [JsonRequired]
        public string Description { get; set; }
        [JsonRequired]
        public double ExtraDayOff { get; set; }
        [JsonRequired]
        public string UserId { get; set; }
    }
}
