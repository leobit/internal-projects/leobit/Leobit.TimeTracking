﻿using Newtonsoft.Json;

namespace Leobit.TimeTracking.WEB.Models
{
    public class SickLeaveMonthData
    {
        [JsonRequired]
        public double WithoutCertificate { get; set; }
        [JsonRequired]
        public double WithCertificate { get; set; }
        [JsonRequired]
        public double FullyPaid { get; set; }
        [JsonRequired]
        public double PartiallyPaid { get; set; }
    }

    public class SickLeaveYearData
    {
        public SickLeaveMonthData January { get; set; }
        public SickLeaveMonthData February { get; set; }
        public SickLeaveMonthData March { get; set; }
        public SickLeaveMonthData April { get; set; }
        public SickLeaveMonthData May { get; set; }
        public SickLeaveMonthData June { get; set; }
        public SickLeaveMonthData July { get; set; }
        public SickLeaveMonthData August { get; set; }
        public SickLeaveMonthData September { get; set; }
        public SickLeaveMonthData October { get; set; }
        public SickLeaveMonthData November { get; set; }
        public SickLeaveMonthData December { get; set; }
    }

    public class SickLeaveInformation
    {
        [JsonRequired]
        public double ProvidedSickLeaveDaysWithoutCertificate { get; set; }
        [JsonRequired]
        public double ProvidedSickLeaveDaysWithCertificate { get; set; }
        [JsonRequired]
        public double ProvidedSickLeaveDaysToBeCompensated100 { get; set; }
        [JsonRequired]
        public double ProvidedSickLeaveDaysToBeCompensated75 { get; set; }
        [JsonRequired]
        public double CurrentSickLeaveBalance { get; set; }
        [JsonRequired]
        public double OverusedSickLeavesFine { get; set; }
        [JsonRequired]
        public SickLeaveYearData YearData { get; set; }
    }
}
