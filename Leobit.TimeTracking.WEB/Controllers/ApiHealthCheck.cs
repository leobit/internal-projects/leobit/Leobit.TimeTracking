﻿using Microsoft.Extensions.Diagnostics.HealthChecks;
using System.Threading.Tasks;
using System.Threading;

namespace Leobit.TimeTracking.WEB.Controllers
{
	public class ApiHealthCheck : IHealthCheck
	{
		public Task<HealthCheckResult> CheckHealthAsync(HealthCheckContext context, CancellationToken cancellationToken = default)
		{
			return Task.FromResult(
				HealthCheckResult.Healthy("API is healthy"));
		}
	}
}
