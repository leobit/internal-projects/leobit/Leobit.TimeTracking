﻿using AutoMapper;
using Leobit.TimeTracking.BL.Interfaces;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Vacation)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class VacationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IVacationService _vacationService;

        public VacationController(IMapper mapper, IVacationService vacationService)
        {
            _mapper = mapper;
            _vacationService = vacationService;
        }

        [HttpGet]
        public async Task<VacationInformation> GetVacationInformation(int? year = null)
        {
            return _mapper.Map<VacationInformation>(
                await _vacationService.GetVacationInformation(this.UserId(), year ?? DateTime.UtcNow.Year));
        }

        [HttpGet]
        [Route(R.Employee)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Read)]
        public async Task<VacationInformation> GetEmployeeVacationInformation(string employeeId, int? year = null)
        {
            return _mapper.Map<VacationInformation>(
                await _vacationService.GetVacationInformation(employeeId, year ?? DateTime.UtcNow.Year));
        }

        [HttpPost]
        [Route(R.Employee)]
        [ClaimsAuthorize(ClaimType.TransferVacationBalance, ClaimValue.Write)]
        public async Task<IActionResult> TransferVacationBalance(int year)
        {
            await _vacationService.TransferVacationBalance(year);

            return NoContent();
        }

        [HttpGet]
        [Route("All")]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Read)]
        public async Task<IEnumerable<VacationStatisticModel>> GetAllEmployeesBallance(int? year, string managerId, int? projectId)
        {
            year ??= DateTime.UtcNow.Year;

            var allEmployeesBalance = await _vacationService.GetAllEmployeesBalance(year.Value, managerId, projectId);

            return allEmployeesBalance;
        }

        [HttpGet]
        [Route("Managers")]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Read)]
        public async Task<IEnumerable<EmployeeManagerShortDTO>> GetManagersList()
        {
            var managers = await _vacationService.GetManagersList();

            return managers;
        }

        /// <summary>
        /// Get list of CustomDayOff for specific year and month
        /// </summary>
        /// <param name="year">Year</param>
        /// <param name="month">Month</param>
        /// <returns>List of CustomDayOff</returns>
        [HttpGet]
        [Route(R.CustomDayOff)]
        public async Task<IEnumerable<CustomDayOff>> GetCustomDayOffs(int? year = null, int? month = null)
        {
            var customDayOffs = await _vacationService.GetCustomDayOffs(this.UserId(), year, month);

            return _mapper.Map<IEnumerable<CustomDayOff>>(customDayOffs);
        }

        /// <summary>
        /// Get list of CustomDayOff of all employees for specific year
        /// </summary>
        /// <param name="year">Year</param>
        /// <returns>List of CustomDayOff</returns>
        [LogAction]
        [HttpGet]
        [Route(R.CustomDayOff + "/All")]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Write)]
        public async Task<IEnumerable<CustomDayOff>> GetAllCustomDayOffsByYear(int year)
        {
            var customDayOffs = await _vacationService.GetAllCustomDayOffsByYear(year);

            return _mapper.Map<IEnumerable<CustomDayOff>>(customDayOffs);
        }

        /// <summary>
        /// Get list of CustomDayOff by employee id for specific year and month
        /// </summary>
        /// <param name="employeeId">Employee id</param>
        /// <param name="year">Year</param>
        /// <param name="month">Month</param>
        /// <returns>List of CustomDayOff</returns>
        [LogAction]
        [HttpGet]
        [Route(R.CustomDayOff + "/" + R.Employee)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Read)]
        public async Task<IEnumerable<CustomDayOff>> GetEmployeeCustomDayOffs(string employeeId, int? year = null, int? month = null)
        {
            var customDayOffs = await _vacationService.GetCustomDayOffs(employeeId, year, month);

            return _mapper.Map<IEnumerable<CustomDayOff>>(customDayOffs);
        }

        /// <summary>
        /// Create new CustomDayOff
        /// </summary>
        /// <param name="customDayOff">CustomDayOff</param>
        /// <returns>Created CustomDayOff</returns>
        [LogAction]
        [HttpPost]
        [Route(R.CustomDayOff)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Write)]
        public async Task<CustomDayOff> CreateCustomDayOff(CreateCustomDayOff customDayOff)
        {
            var customDayOffDTO = _mapper.Map<CustomDayOffDTO>(customDayOff);

            var createdCustomDayOff = await _vacationService.CreateCustomDayOff(this.UserId(), customDayOffDTO);

            return _mapper.Map<CustomDayOff>(createdCustomDayOff);
        }

        /// <summary>
        /// Update existing CustomDayOff
        /// </summary>
        /// <param name="customDayOff">CustomDayOff</param>
        /// <returns>Updated CustomDayOff</returns>
        [LogAction]
        [HttpPut]
        [Route(R.CustomDayOff)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Write)]
        public async Task<CustomDayOff> EditCustomDayOff(EditCustomDayOff customDayOff)
        {
            var customDayOffDTO = _mapper.Map<CustomDayOffDTO>(customDayOff);
            customDayOffDTO = await _vacationService.EditCustomDayOff(this.UserId(), customDayOffDTO);
            return _mapper.Map<CustomDayOff>(customDayOffDTO);
        }

        /// <summary>
        /// Delete CustomDayOff by id
        /// </summary>
        /// <param name="id">CustomDayOff id</param>
        /// <returns>No content</returns>
        [LogAction]
        [HttpDelete]
        [Route(R.CustomDayOff + "/" + R.Id)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Write)]
        public async Task<IActionResult> DeleteCustomDayOff(int id)
        {
            await _vacationService.DeleteCustomDayOff(id);

            return NoContent();
        }

        /// <summary>
        /// Edit Employment Date by id
        /// </summary>
        /// <param name="employmentDate">New employment date</param>
        /// <returns>No content</returns>
        [LogAction]
        [HttpPut]
        [Route(R.Employee + "/employmentDate")]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Write)]
        public async Task<IActionResult> EditEmploymentDate(EmploymentDate employmentDate)
        {
            await _vacationService.EditEmploymentDate(employmentDate.UserId, employmentDate.Date);

            return NoContent();
        }
    }
}
