﻿using AutoMapper;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
	[Route(R.ApiVersion + R.Analytic)]
	[Produces("application/json")]
	[ApiController]
	public class AnalyticController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly AnalyticService _analyticService;

		public AnalyticController(IMapper mapper, AnalyticService accountService)
		{
			_mapper = mapper;
			_analyticService = accountService;
		}

		[HttpGet("all")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetAllIssues(int year, int month, CancellationToken cancellationToken)
		{
			var type =
				 TimeReportIssueType.DuplicateContent
				 | TimeReportIssueType.ShortContent
				 | TimeReportIssueType.SickleaveAbuse
				 | TimeReportIssueType.SelfEducation
				 | TimeReportIssueType.KE
				 | TimeReportIssueType.Holidays
				 | TimeReportIssueType.Inactive;

			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, type, cancellationToken));
		}

		[HttpGet("duplicate")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetDuplicatesIssues(int year, int month, CancellationToken cancellationToken)
		{
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.DuplicateContent, cancellationToken));
		}

		[HttpGet("holidays")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetHolidaysIssues(int year, int month, CancellationToken cancellationToken)
		{
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.Holidays, cancellationToken));
		}

		[HttpGet("ke")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetKeIssues(int year, int month, CancellationToken cancellationToken)
		{
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.KE, cancellationToken));
		}

		[HttpGet("self-education")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetSelfEducationIssues(int year, int month, CancellationToken cancellationToken)
		{
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.SelfEducation, cancellationToken));
		}

		[HttpGet("sick-abuse")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetSickLeaveAbuseIssues(int year, int month, CancellationToken cancellationToken)
		{
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.SickleaveAbuse, cancellationToken));
		}

		[HttpGet("short")]
		[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
		public async Task<List<TimeReportIssue>> GetShortDescriptionIssues(int year, int month, CancellationToken cancellationToken)
		{
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.ShortContent, cancellationToken));
		}

        [HttpGet("inactive")]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
        public async Task<List<TimeReportIssue>> GetInactiveIssues(int year, int month, CancellationToken cancellationToken)
        {
			return _mapper.Map<List<TimeReportIssue>>(
				await _analyticService.GetTimeReportsIssues(year, month, TimeReportIssueType.Inactive, cancellationToken));
        }
    }
}
