﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Role)]
    [Produces("application/json")]
    [ApiController]
    public class RoleController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly RoleService _roleService;
        private readonly PermissionService _permissionService;
        private readonly UserService _userService;

        public RoleController(
            IMapper mapper,
            RoleService roleService,
            PermissionService permissionService,
            UserService userService)
        {
            _mapper = mapper;
            _roleService = roleService;
            _permissionService = permissionService;
            _userService = userService;
        }

        /// <summary>
        /// Get list of short roles info
        /// </summary>
        /// <returns>List of short roles info</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Read)]
        public async Task<IEnumerable<RoleShort>> GetRoles()
        {
            return _mapper.Map<List<RoleShort>>(await _roleService.GetRoles());
        }

        /// <summary>
        /// Get list of permissions info
        /// </summary>
        /// <returns>List of permissions info</returns>
        [HttpGet(R.Permission)]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Read)]
        public async Task<IEnumerable<Permission>> GetPermissions()
        {
            return _mapper.Map<List<Permission>>(await _permissionService.GetPermissions());
        }

        /// <summary>
        /// Get full role info by id
        /// </summary>
        /// <param name="id">Role id</param>
        /// <returns>Full role info</returns>
        [HttpGet(R.Id)]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Read)]
        public async Task<Role> GetFullRoleById(int id)
        {
            var role = _mapper.Map<Role>(await _roleService.GetFullRoleById(id));

            if (!role.IsSystemRole)
            {
                role.IsAssignedToUser = await _userService.HasRoleAssigned(role.Id);
            }

            return role;
        }

        /// <summary>
        /// Create new role
        /// </summary>
        /// <param name="role">Role</param>
        /// <returns>Created role</returns>
        [LogAction]
        [HttpPost]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Write)]
        public async Task<RoleShort> CreateRole(Role role)
        {
            var roleDTO = _mapper.Map<RoleDTO>(role);

            return _mapper.Map<RoleShort>(
                await _roleService.CreateRole(roleDTO));
        }

        /// <summary>
        /// Update existing role
        /// </summary>
        /// <param name="role">Role</param>
        /// <returns>Updated role</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Write)]
        public async Task<RoleShort> EditRole(Role role)
        {
            var roleDTO = _mapper.Map<RoleDTO>(role);

            return _mapper.Map<RoleShort>(
                await _roleService.EditRole(roleDTO));
        }

        /// <summary>
        /// Delete role
        /// </summary>
        /// <param name="id">Role ID to delete</param>
        /// <param name="fallbackRoleId">Fallback role ID to reassign users from deleted</param>
        [LogAction]
        [HttpDelete(R.Id)]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Write)]
        public async Task<IActionResult> DeleteRole(int id, [FromQuery] int? fallbackRoleId = null)
        {
            await _roleService.DeleteRole(id, fallbackRoleId);

            return NoContent();
        }
    }
}
