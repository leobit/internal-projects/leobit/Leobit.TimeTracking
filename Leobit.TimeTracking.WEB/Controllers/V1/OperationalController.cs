﻿using AutoMapper;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using Leobit.TimeTracking.WEB.Infrastructure;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Interfaces;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Operational)]
    [Produces("application/json")]
    [ApiController]
    public class OperationalController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IOperationalService _oprationalService;

        public OperationalController(IMapper mapper, IOperationalService operationalService)
        {
            _mapper = mapper ?? throw new ArgumentNullException(nameof(mapper));
            _oprationalService = operationalService ?? throw new ArgumentNullException(nameof(operationalService));
        }

        [HttpGet]
        [Route("Holidays")]
        public async Task<IEnumerable<Holiday>> GetHolidays()
        {
            return _mapper.Map<IEnumerable<Holiday>>(await _oprationalService.GetHolidays());
        }

        [HttpGet]
        [Route("Holidays/{year}")]
        public async Task<IEnumerable<Holiday>> GetHolidaysByYear(int year)
        {
            return _mapper.Map<IEnumerable<Holiday>>(await _oprationalService.GetHolidays(year));
        }

        [HttpGet]
        [Route("Holidays/{year}/{month}")]
        public async Task<IEnumerable<Holiday>> GetHolidaysByYearAndMonth(int year, int month)
        {
            return _mapper.Map<IEnumerable<Holiday>>(await _oprationalService.GetHolidays(year, month));
        }

        [HttpPost]
        [Route("Holidays")]
		[ClaimsAuthorize(ClaimType.Holiday, ClaimValue.Write)]
		[ApiExplorerSettings(IgnoreApi = true)]
        public async Task<IEnumerable<Holiday>> SetHolidays(IEnumerable<Holiday> holidays)
        {
            var mapped = _mapper.Map<IEnumerable<HolidayDTO>>(holidays);

            return _mapper.Map<IEnumerable<Holiday>>(
                await _oprationalService.UpdateHolidays(mapped));
        }

        [HttpGet]
        [Route("Baseline")]
        public async Task<IEnumerable<MonthHours>> GetBaseLine()
        {
            var baseline = await _oprationalService.GetBaseLine();
            return _mapper.Map<IEnumerable<MonthHours>>(baseline);
        }

        [HttpGet]
        [Route("Baseline/{year}")]
        public async Task<IEnumerable<MonthHours>> GetBaseLineByYear(int year)
        {
            var baseline = await _oprationalService.GetBaseLine(year);
            return _mapper.Map<IEnumerable<MonthHours>>(baseline);
        }

        [HttpGet]
        [Route("Baseline/{year}/{month}")]
        public async Task<IEnumerable<MonthHours>> GetBaseLineByYearANdMonth(int year, int month)
        {
            var baseline = await _oprationalService.GetBaseLine(year, month);
            return _mapper.Map<IEnumerable<MonthHours>>(baseline);
        }
    }
}
