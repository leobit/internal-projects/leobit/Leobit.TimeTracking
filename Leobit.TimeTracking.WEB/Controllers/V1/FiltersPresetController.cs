﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;


namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.FiltersPreset)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class FiltersPresetController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly FiltersPresetService _filtersPresetService;

        public FiltersPresetController(IMapper mapper, FiltersPresetService filtersPresetService)
        {
            _filtersPresetService = filtersPresetService;
            _mapper = mapper;
        }

        /// <summary>
        /// Get filter preset by filter preset id and user id
        /// </summary>
        /// <param name="filtersPresetId">Filters preset id</param>
        /// <returns></returns>
        [HttpGet]
        [Route("{filtersPresetId}")]
        public async Task<FiltersPreset> GetFiltersPreset(int filtersPresetId)
        {
            var filtersPresetDto = await _filtersPresetService.GetFiltersPresetAsync(this.UserId(), filtersPresetId);
            return _mapper.Map<FiltersPreset>(filtersPresetDto);
        }

        /// <summary>
        /// Get list of filter presets by user id
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<FiltersPreset>> GetFiltersPresets()
        {
            var filtersPresetDtos = await _filtersPresetService.GetFiltersPresetsAsync(this.UserId());
            return _mapper.Map<List<FiltersPreset>>(filtersPresetDtos);
        }

        /// <summary>
        /// Create new filters preset
        /// </summary>
        /// <param name="filtersPresetDTO">Filters preset</param>
        /// <returns></returns>
        [HttpPost]
        public async Task<FiltersPreset> CreateFiltersPreset([FromBody] FiltersPreset filtersPresetDTO)
        {
            var filtersPresetDto = _mapper.Map<FiltersPresetDTO>(filtersPresetDTO);
            filtersPresetDto.UserId = this.UserId();

            var result = await _filtersPresetService.CreateFiltersPresetAsync(filtersPresetDto);
            return _mapper.Map<FiltersPreset>(result);
        }

        /// <summary>
        /// Edit employee reporting filter by id
        /// </summary>
        /// <param name="filtersPresetId">Filters preset id</param>
        /// <param name="employeeReportingFilters">Employee Reporting Filters</param>
        /// <returns></returns>
        [HttpPut]
        [Route("EmployeeReportingFilters")]
        public async Task<IActionResult> EditEmployeeReportingFilters(int filtersPresetId, EmployeeReportingFilters employeeReportingFilters)
        {
            var employeeReportingFiltersDto = _mapper.Map<EmployeeReportingFiltersDTO>(employeeReportingFilters);
            await _filtersPresetService.EditEmployeeReportingFiltersAsync(filtersPresetId, employeeReportingFiltersDto);
            return NoContent();
        }

        /// <summary>
        /// Edit filter preset name by id
        /// </summary>
        /// <param name="filtersPresetId">Filters preset id</param>
        /// <param name="filtersPresetName">Filters preset name</param>
        /// <returns></returns>
        [HttpPut]
        [Route("Name")]
        public async Task<IActionResult> EditFiltersPresetName(int filtersPresetId, string filtersPresetName)
        {
            await _filtersPresetService.EditFiltersPresetNameAsync(filtersPresetId, userId: this.UserId(), filtersPresetName);
            return NoContent();
        }

        /// <summary>
        /// Delete fitlers preset by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete]
        [Route(R.Id)]
        public async Task<IActionResult> DeleteFiltersPreset(int id)
        {
            await _filtersPresetService.DeleteFiltersPresetAsync(id);
            return NoContent();
        }
    }
}
