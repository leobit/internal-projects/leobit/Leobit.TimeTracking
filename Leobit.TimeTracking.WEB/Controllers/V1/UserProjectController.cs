using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.UserProject)]
    [Produces("application/json")]
    [ApiController]
    public class UserProjectController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly ProjectService _projectService;
        private readonly UserProjectService _userProjectService;
        private readonly ProjectReportingReopenRequestService _projectReportingReopenRequestService;

        public UserProjectController(
            IMapper mapper, 
            ProjectService projectService, 
            UserProjectService userProjectService,
            ProjectReportingReopenRequestService projectReportingReopenRequestService)
        {
            _mapper = mapper;
            _projectService = projectService;
            _userProjectService = userProjectService;
            _projectReportingReopenRequestService = projectReportingReopenRequestService;
        }

        /// <summary>
        /// Get list of short projects info for further UserProject creating
        /// </summary>
        /// <returns>List of short projects info</returns>
        [HttpGet]
        [ClaimsRoute(R.Project, ClaimType.ProjectAssignment, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Read)]
        public async Task<IEnumerable<ProjectShort>> GetShortProjects()
        {
            return _mapper.Map<IEnumerable<ProjectShort>>(await _projectService.GetProjects(null,null));
        }

        /// <summary>
        /// Get list of UserProject
        /// </summary>
        /// <returns>List of UserProject</returns>
        [HttpGet]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Read)]
        public async Task<IEnumerable<UserProject>> GetUserProjects([FromQuery] ICollection<string> userIds, [FromQuery] ICollection<int> projectIds)
        {
            return _mapper.Map<IEnumerable<UserProject>>(await _userProjectService.GetUserProjects(userIds, projectIds));
        }

        /// <summary>
        /// Create new UserProject
        /// </summary>
        /// <param name="userProject">UserProject</param>
        /// <returns>Created UserProject</returns>
        [LogAction]
        [HttpPost]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<UserProject> CreateUserProject(UserProject userProject)
        {
            var userProjectDTO = _mapper.Map<UserProjectDTO>(userProject);

            return _mapper.Map<UserProject>(
                await _userProjectService.CreateUserProject(userProjectDTO));
        }

        /// <summary>
        /// Update UserProject
        /// </summary>
        /// <param name="userProject"></param>
        /// <returns>Updated UserProject</returns>
        [HttpPut]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<UserProject> UpdateUserProject(UserProject userProject)
        {
            var userProjectDTO = _mapper.Map<UserProjectDTO>(userProject);

            return _mapper.Map<UserProject>(
                               await _userProjectService.UpdateUserProject(userProjectDTO));
        }


        [HttpPost("Batch")]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<IActionResult> CreateUserProjects(string userId, List<int> projectIds)
        {
            await _userProjectService.CreateUserProjects(userId, projectIds);

            return Ok();
        }

        /// <summary>
        /// Delete UserProject
        /// </summary>
        /// <param name="userProject">UserProject</param>
        /// <returns>No content</returns>
        [LogAction]
        [HttpDelete]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<IActionResult> DeleteUserProject(UserProject userProject)
        {
            var userProjectDTO = _mapper.Map<UserProjectDTO>(userProject);
            await _userProjectService.DeleteUserProject(userProjectDTO);

            return NoContent();
        }

        [HttpGet("cleanuptest")]
        public async Task<IActionResult> RunProjectCleanupJob()
        {
            await _userProjectService.CleanupProjectAssigments();

            var expirationTime = DateTime.UtcNow;
            await _projectReportingReopenRequestService.CleanUpExpiredProjectSettingsBackups(expirationTime);
            await _projectReportingReopenRequestService.CleanUpExpiredProjectReopenRequests(expirationTime);

            return Ok();
        }

    }
}