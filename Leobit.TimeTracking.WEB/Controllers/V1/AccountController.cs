using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Account)]
    [Produces("application/json")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly AccountService _accountService;

        private readonly ProjectService _projectService;

        public AccountController(IMapper mapper, AccountService accountService, ProjectService projectService)
        {
            _mapper = mapper;
            _accountService = accountService;
            _projectService = projectService;
        }

        /// <summary>
        /// Get list of accounts
        /// </summary>
        /// <returns>List of accounts</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.Account, ClaimValue.Read)]
        public async Task<IEnumerable<Account>> GetAccounts()
        {
            // Retrieves accounts related to the current user based on the projects they are associated with.
            var projects = await _projectService.GetProjects(null,null);

            return _mapper.Map<List<Account>>(await _accountService.GetAccountsFromProjects(projects.Select(x=>x.Id).ToList()));
        }

        [HttpGet("budgetOwners")]
        [ClaimsAuthorize(ClaimType.Account, ClaimValue.Read)]
        public async Task<AccountEligibleBudgetOwner> GetEligibleBudgetOwners(CancellationToken cancellation)
        {
            return _mapper.Map<AccountEligibleBudgetOwner>(
                await _accountService.GetEligibleBudgetOwners(cancellation));
        }

        /// <summary>
        /// Create new account
        /// </summary>
        /// <param name="account">Account</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Created account</returns>
        [LogAction]
        [HttpPost]
        [ClaimsAuthorize(ClaimType.Account, ClaimValue.Write)]
        public async Task<Account> CreateAccount(Account account, CancellationToken cancellationToken)
        {
            var accountDto = _mapper.Map<AccountDTO>(account);
            accountDto = await _accountService.CreateAccount(accountDto, cancellationToken);

            return _mapper.Map<Account>(accountDto);
        }

        /// <summary>
        /// Update existing account
        /// </summary>
        /// <param name="account">Account</param>
        /// <param name="cancellationToken">Cancellation Token</param>
        /// <returns>Updated account</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Account, ClaimValue.Write)]
        public async Task<Account> EditAccount(Account account, CancellationToken cancellationToken)
        {
            var accountDto = _mapper.Map<AccountDTO>(account);
            accountDto = await _accountService.EditAccount(accountDto, cancellationToken);

            return _mapper.Map<Account>(accountDto);
        }
    }
}