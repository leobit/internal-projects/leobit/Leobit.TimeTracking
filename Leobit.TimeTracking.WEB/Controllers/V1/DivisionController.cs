﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Division)]
    [Produces("application/json")]
    [ApiController]
    public class DivisionController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly DivisionService _divisionService;

        public DivisionController(IMapper mapper, DivisionService divisionService)
        {
            _mapper = mapper;
            _divisionService = divisionService;
        }

        /// <summary>
        /// Get list of divisions
        /// </summary>
        /// <param name="id">Division id</param>
        /// <returns>List of divisions</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.Division, ClaimValue.Read)]
        public async Task<IEnumerable<Division>> GetDivisions(int? id)
        {
            return _mapper.Map<List<Division>>(await _divisionService.GetDivisions(id));
        }

        /// <summary>
        /// Create new division
        /// </summary>
        /// <param name="divisionDto">Division</param>
        /// <returns>Created division</returns>
        [LogAction]
        [HttpPost]
        [ClaimsAuthorize(ClaimType.Division, ClaimValue.Write)]
        public async Task<DivisionDTO> CreateDivision(DivisionDTO divisionDto)
        {
            return await _divisionService.CreateDivision(divisionDto);
        }

        /// <summary>
        /// Update existing division
        /// </summary>
        /// <param name="divisionDto">Division</param>
        /// <returns>Updated division</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Division, ClaimValue.Write)]
        public async Task<DivisionDTO> EditDivision(DivisionDTO divisionDto)
        {
            return await _divisionService.EditDivision(divisionDto);
        }
    }
}
