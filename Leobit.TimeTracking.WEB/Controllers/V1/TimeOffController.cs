﻿using System.Threading.Tasks;
using Leobit.TimeTracking.BL.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;
using System;
using Leobit.TimeTracking.WEB.Models;
using AutoMapper;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.TimeOff)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class TimeOffController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly ITimeOffService _timeOffService;

        public TimeOffController(ITimeOffService timeOffService,
            IMapper mapper)
        {
            _mapper = mapper;
            _timeOffService = timeOffService;
        }


        [HttpGet]
        public async Task<VacationInformation> GetTimeOffsInformation(int? year = null)
        {
            return _mapper.Map<VacationInformation>(
                await _timeOffService.GetTimeOffInformation(this.UserId(), year ?? DateTime.UtcNow.Year));
        }

        [HttpGet]
        [Route(R.Employee)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Read)]
        public async Task<VacationInformation> GetEmployeeTimeOffs(string employeeId, int? year = null)
        {
            return _mapper.Map<VacationInformation>(
                await _timeOffService.GetTimeOffInformation(employeeId, year ?? DateTime.UtcNow.Year));
        }
    }
}
