using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.ProjectManager)]
    [Produces("application/json")]
    [ApiController]
    public class ProjectManagerController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly ProjectService _projectService;
        private readonly ProjectManagerService _projectManagerService;
        private readonly UserService _userService;
        public ProjectManagerController(IMapper mapper, ProjectService projectService, ProjectManagerService projectManagerService, UserService userService)
        {
            _mapper = mapper;
            _projectService = projectService;
            _projectManagerService = projectManagerService;
            _userService = userService;
        }

        /// <summary>
        /// Get list of short projects info for further ProjectManager creating
        /// </summary>
        /// <returns>List of short projects info</returns>
        [HttpGet]
        [ClaimsRoute(R.Project, ClaimType.ProjectAssignment, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Read)]
        public async Task<IEnumerable<ProjectShort>> GetProjectManagerProjects()
        {
            return _mapper.Map<List<ProjectShort>>(await _projectService.GetProjectShorts());
        }

        /// <summary>
        /// Get list of ProjectManager
        /// </summary>
        /// <returns>List of ProjectManager</returns>
        [HttpGet]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Read)]
        public async Task<IEnumerable<ProjectManager>> GetProjectManagers([FromQuery] ICollection<string> userIds, [FromQuery] ICollection<int> projectIds)
        {
            return _mapper.Map<List<ProjectManager>>(
                await _userService.GetProjectManagers(userIds, projectIds));
        }

        /// <summary>
        /// Get list of ProjectManager
        /// </summary>
        /// <returns>List of ProjectManager</returns>
        [HttpGet]
        [ClaimsRoute("User", ClaimType.ProjectAssignment, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Read)]
        public async Task<UserManager> GetEmployeeManagers([FromQuery] string userId)
        {
            return _mapper.Map<UserManager>(await _projectManagerService.GetEmployeeManagers(userId));
        }

        /// <summary>
        /// Create new ProjectManager
        /// </summary>
        /// <param name="projectManager">ProjectManager</param>
        /// <returns>Created ProjectManager</returns>
        [LogAction]
        [HttpPost]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<ProjectManager> CreateProjectManager(ProjectManager projectManager)
        {
            var projectManagerDTO = _mapper.Map<ProjectManagerDTO>(projectManager);

            return _mapper.Map<ProjectManager>(
                await _projectManagerService.CreateProjectManager(projectManagerDTO));
        }

        /// <summary>
        /// Delete ProjectManager
        /// </summary>
        /// <param name="projectManager">ProjectManager</param>
        /// <returns>No content</returns>
        [LogAction]
        [HttpDelete]
        [ClaimsRoute(ClaimType.ProjectAssignment, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<IActionResult> DeleteProjectManager(ProjectManager projectManager)
        {
            var projectManagerDTO = _mapper.Map<ProjectManagerDTO>(projectManager);
            await _projectManagerService.DeleteProjectManager(projectManagerDTO);

            return NoContent();
        }
    }
}