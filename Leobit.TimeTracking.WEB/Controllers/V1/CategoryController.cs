﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Category)]
    [Produces("application/json")]
    [ApiController]
    public class CategoryController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly CategoryService _categoryService;
        private readonly OrganizationUnitService _organizationUnitService;

        public CategoryController(IMapper mapper, CategoryService categoryService, OrganizationUnitService organizationUnitService)
        {
            _mapper = mapper;
            _categoryService = categoryService;
            _organizationUnitService = organizationUnitService;
        }

        /// <summary>
        /// Get list of short organization units info for further category creating
        /// </summary>
        /// <returns>List of short organization units info</returns>
        [HttpGet(R.OrganizationUnit)]
        [ClaimsAuthorize(ClaimType.Category, ClaimValue.Read)]
        public async Task<IEnumerable<OrganizationUnitShort>> GetShortOrganizationUnits()
        {
            return _mapper.Map<IEnumerable<OrganizationUnitShort>>(await _organizationUnitService.GetOrganizationUnitsShort());
        }

        /// <summary>
        /// Get list of categories
        /// </summary>
        /// <param name="id">Category id</param>
        /// <param name="organizationUnitId">Organization unit id</param>
        /// <returns>List of categories</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.Category, ClaimValue.Read)]
        public async Task<IEnumerable<CategoryShort>> GetCategories(int? id, int? organizationUnitId)
        {
            return _mapper.Map<List<CategoryShort>>(await _categoryService.GetCategoriesShort(id, organizationUnitId));
        }

        /// <summary>
        /// Create new category
        /// </summary>
        /// <param name="category">Category</param>
        /// <returns>Created category</returns>
        [LogAction]
        [HttpPost]
        [ClaimsAuthorize(ClaimType.Category, ClaimValue.Write)]
        public async Task<Category> CreateCategory(CategoryInput category)
        {
            var categoryDTO = _mapper.Map<CategoryInputDTO>(category);

            return _mapper.Map<Category>(await _categoryService.CreateCategory(categoryDTO));
        }

        /// <summary>
        /// Update existing category
        /// </summary>
        /// <param name="category">Category</param>
        /// <returns>Updated category</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Category, ClaimValue.Write)]
        public async Task<Category> EditCategory(CategoryInput category)
        {
            var categoryDTO = _mapper.Map<CategoryInputDTO>(category);

            return _mapper.Map<Category>(await _categoryService.EditCategory(categoryDTO));
        }

        /// <summary>
        /// Get projects that are assigned to given category
        /// </summary>
        /// <param name="id">Category Id</param>
        /// <returns>List of projects</returns>
        [HttpGet]
        [Route("{id}/" + R.Project)]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Read)]
        public async Task<IEnumerable<ProjectShort>> GetCategoryProjects(int id)
        {
            return _mapper.Map<IEnumerable<ProjectShort>>(
                await _categoryService.GetProjectsByCategoryId(id));
        }

        /// <summary>
        /// Assign new category to a project
        /// </summary>
        /// <param name="categoryId">Category Id</param>
        /// <param name="projectId">Poject Id</param>
        /// <returns>Object representing new project</returns>
        [HttpPost]
        [Route("{categoryId}/" + R.Project + "/{projectId}")]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Write)]
        public async Task<ProjectShort> EditCategoryProject(int categoryId, int projectId)
        {
            return _mapper.Map<ProjectShort>(await _categoryService.EditProjectCategory(projectId, categoryId));
        }
    }
}
