﻿using System.Collections.Generic;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.OrganizationUnit)]
    [Produces("application/json")]
    [ApiController]
    public class OrganizationUnitController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly OrganizationUnitService _organizationUnitService;
        private readonly DivisionService _divisionService;

        public OrganizationUnitController(IMapper mapper, OrganizationUnitService organizationUnitService, DivisionService divisionService)
        {
            _mapper = mapper;
            _organizationUnitService = organizationUnitService;
            _divisionService = divisionService;
        }

        /// <summary>
        /// Get list of divisions for further organization unit creating
        /// </summary>
        /// <returns>List of divisions</returns>
        [HttpGet(R.Division)]
        [ClaimsAuthorize(ClaimType.OrganizationUnit, ClaimValue.Read)]
        public async Task<IEnumerable<Division>> GetShortDivisions()
        {
            return _mapper.Map<List<Division>>(await _divisionService.GetDivisions());
        }

        /// <summary>
        /// Get list of organization units
        /// </summary>
        /// <param name="id">Organization unit id</param>
        /// <param name="divisionId">Division id</param>
        /// <returns>List of organization units</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.OrganizationUnit, ClaimValue.Read)]
        public async Task<IEnumerable<OrganizationUnitShort>> GetOrganizationUnits(int? id, int? divisionId)
        {
            return _mapper.Map<List<OrganizationUnitShort>>(
                await _organizationUnitService.GetOrganizationUnitsShort(id, divisionId));
        }

        /// <summary>
        /// Create new organization unit
        /// </summary>
        /// <param name="organizationUnit">Organization unit</param>
        /// <returns>Created organization unit</returns>
        [LogAction]
        [HttpPost]
        [ClaimsAuthorize(ClaimType.OrganizationUnit, ClaimValue.Write)]
        public async Task<OrganizationUnit> CreateOrganizationUnit(OrganizationUnit organizationUnit)
        {
            var organizationUnitDTO = _mapper.Map<OrganizationUnitDTO>(organizationUnit);

            return _mapper.Map<OrganizationUnit>(
                await _organizationUnitService.CreateOrganizationUnit(organizationUnitDTO));
        }

        /// <summary>
        /// Update existing organization unit
        /// </summary>
        /// <param name="organizationUnit">Organization unit</param>
        /// <returns>Updated organization unit</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.OrganizationUnit, ClaimValue.Write)]
        public async Task<OrganizationUnit> EditOrganizationUnit(OrganizationUnit organizationUnit)
        {
            var organizationUnitDTO = _mapper.Map<OrganizationUnitDTO>(organizationUnit);

            return _mapper.Map<OrganizationUnit>(
                await _organizationUnitService.EditOrganizationUnit(organizationUnitDTO));
        }
    }
}
