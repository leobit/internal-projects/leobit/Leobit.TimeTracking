using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.General.Email;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Me)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class MeController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ProjectService _projectService;
        private readonly UserProjectService _userProjectService;
        private readonly TimeReportService _timeReportService;
        private readonly UserService _userService;

        private readonly EmailService _emailService;
        private readonly ViewRenderService _viewRenderService;

        private readonly ProjectReportingReopenRequestService _projectReportingReopenRequestService;

        public MeController(
            IMapper mapper,
            ProjectService projectService,
            UserProjectService userProjectService,
            TimeReportService timeReportService,
            UserService userService,
            EmailService emailService,
            ViewRenderService viewRenderService,
            ProjectReportingReopenRequestService projectReportingReopenRequestService)
        {
            _mapper = mapper;
            _projectService = projectService;
            _timeReportService = timeReportService;
            _userService = userService;
            _userProjectService = userProjectService;

            _emailService = emailService;
            _viewRenderService = viewRenderService;
            _projectReportingReopenRequestService = projectReportingReopenRequestService;
        }

        /// <summary>
        /// Get list of short projects info by account id for further UserTimeReport creating
        /// </summary>
        /// <returns>List of short projects info</returns>
        [HttpGet(R.Project)]
        public async Task<IEnumerable<ProjectShort>> GetMyProjects()
        {
            return _mapper.Map<IEnumerable<ProjectShort>>(
                await _projectService.GetAssignedProjectShortsWithRestriction(this.UserId()));
        }

        /// <summary>
        /// Get list of users
        /// </summary>
        /// <returns>List of short users info</returns>
        [HttpGet(R.User)]
        [ClaimsAuthorize(ClaimType.ProjectAssignment, ClaimValue.Write)]
        public async Task<IEnumerable<UserShort>> GetUsersForAssignment()
        {
            return _mapper.Map<List<UserShort>>(await _userService.GetActiveUsers());
        }

        /// <summary>
        /// Retrieves a list of short project information that have time reported by the current user within a specified date range.
        /// This list can be used to filter projects for further operations. Additionally, it returns a list of project IDs that are assigned to the user.
        /// </summary>
        /// <param name="dateFrom">The start date for the time reporting period. Can be null to indicate no start date constraint.</param>
        /// <param name="dateTo">The end date for the time reporting period. Can be null to indicate no end date constraint.</param>
        /// <returns>A response containing a list of short project details (ProjectShort) and a list of project IDs assigned to the user (AssignedProjectsIds).</returns>

        [HttpGet(R.Project + "/filter")]
        public async Task<IEnumerable<ProjectShort>> GetFilterProjects(DateTime? dateFrom, DateTime? dateTo)
        {
            return _mapper.Map<IEnumerable<ProjectShort>>(await _projectService.GetFilterProjectShortsByPeriod(this.UserId(), dateFrom, dateTo));
        }

        [HttpGet(R.Project + "/get-assigned-user-projects")]
        public async Task<IEnumerable<int>> GetAssignedUserProjects()
        {
            return await _projectService.GetAssignedUserProjectIds(this.UserId());
        }

        /// <summary>
        /// Get default values for UserTimeReport
        /// </summary>
        /// <returns></returns>
        [HttpGet(R.Default)]
        public async Task<UserDefaults> GetDefault()
        {
            return _mapper.Map<UserDefaults>(await _userService.GetUserDefaults(this.UserId()));
        }

        /// <summary>
        /// Export time reports to excel
        /// </summary>
        /// <returns>Excel file</returns>
        [HttpPost(R.TimeReport + "/Excel")]
        [Produces("application/octet-stream")]
        public async Task<FileResult> ExportExcel(TimeReportExcelRequest request)
        {
            var timeReports = await _timeReportService.GetTimeReports(request.Ids, new[] { this.UserId() });

            var toExport = timeReports.Select(tr => new
            {
                tr.Id,
                Timestamp = tr.Timestamp.ToString(@"MM/dd/yyyy hh\:mm\:ss"),
                User = $"{tr.User.FirstName} {tr.User.LastName}",
                Account = tr.Account.Name,
                Project = tr.Project.Name,
                tr.WorkDescription,
                Int = tr.InternalHours,
                Ext = tr.ExternalHours,
                tr.IsOverhead,
                ReportingDate = tr.ReportingDate.ToString("MM/dd/yyyy")
            });

            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(toExport, PrintHeaders: true);

                await package.SaveAsync();
            }

            stream.Position = 0;

            var excelName = $"TimeReports-{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }


        /// <summary>
        /// Get list of UserTimeReport
        /// </summary>
        /// <param name="id">UserTimeReport id</param>
        /// <param name="dateFrom">Date from inclusively. By default - first day of current month</param>
        /// <param name="dateTo">Date to inclusively. By default - last day of current month</param>
        /// <param name="projectIds">Projects ids</param>
        /// <param name="isOverhead">Is Overhead</param>
        /// <returns>List of UserTimeReport</returns>
        [HttpGet(R.TimeReport)]
        public async Task<IEnumerable<UserTimeReport>> GetMyTimeReports(int? id, DateTime? dateFrom, DateTime? dateTo, [FromQuery] ICollection<int> projectIds, bool? isOverhead)
        {
            dateFrom = dateFrom?.ToUniversalTime() ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Utc);

            dateTo = dateTo?.ToUniversalTime() ?? new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1, 0, 0, 0, DateTimeKind.Utc).AddMonths(1).AddDays(-1);

            return _mapper.Map<IEnumerable<UserTimeReport>>(
                await _timeReportService.GetTimeReports(id, dateFrom.Value, dateTo.Value, projectIds, new[] { this.UserId() }, isOverhead));
        }

        /// <summary>
        /// Create new UserTimeReport
        /// </summary>
        /// <param name="userTimeReport">UserTimeReport</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Created UserTimeReport</returns>
        [HttpPost(R.TimeReport)]
        public async Task<ActionResult<UserTimeReport>> CreateMyTimeReport(CreateTimeReport userTimeReport, CancellationToken cancellationToken)
        {
            var timeReportDTO = _mapper.Map<TimeReportDTO>(userTimeReport);
            timeReportDTO.User = new UserShortDTO { Id = this.UserId() };

            var totalHours = await _timeReportService.GetTotalInternalTimeReported(timeReportDTO.User.Id, timeReportDTO.Project.Id);
            var userProject = await _userProjectService.GetUserProject(timeReportDTO.User.Id, timeReportDTO.Project.Id);

            if (userProject is null)
            {
                return BadRequest("You are no longer assigned to this project");
            }

            if (userProject.Hours is not null && totalHours + (decimal)userTimeReport.InternalHours >= userProject.Hours)
            {
                return BadRequest($"You can't report more hours, you already reported {totalHours}, your limit is {userProject.Hours}");
            }

            timeReportDTO = await _timeReportService.CreateTimeReport(timeReportDTO, cancellationToken: cancellationToken);
            var result = _mapper.Map<UserTimeReport>(timeReportDTO);

            if (userTimeReport.SendEmail)
            {
                var timeReportEmail = await _viewRenderService.RenderToStringAsync("EmailTemplates/TimeReport.cshtml", result);
                await _emailService.SendEmailAsync($"{this.UserId()}@leobit.com", "Time Tracking Report", timeReportEmail);
            }

            return Ok(result);
        }

        /// <summary>
        /// Create new UserTimeReports
        /// </summary>
        /// <param name="userTimeReport">UserTimeReport</param>
        /// <param name="cancellationToken">Cancellation token</param>
        /// <returns>Created UserTimeReports</returns>
        [HttpPost(R.TimeReport + "/Batch")]
        public async Task<List<UserTimeReport>> CreateMyTimeReports([FromBody] List<CreateTimeReport> userTimeReport, CancellationToken cancellationToken)
        {
            var userShortDTO = new UserShortDTO { Id = this.UserId() };
            var timeReportDTOs = _mapper.Map<List<TimeReportDTO>>(userTimeReport);
            timeReportDTOs.ForEach(trd => trd.User = userShortDTO);

            return _mapper.Map<List<UserTimeReport>>(
                await _timeReportService.CreateTimeReports(timeReportDTOs, cancellationToken: cancellationToken));
        }

        /// <summary>
        /// Update existing UserTimeReport
        /// </summary>
        /// <param name="userTimeReport">UserTimeReport</param>
        /// <returns>Updated UserTimeReport</returns>
        [HttpPut(R.TimeReport)]
        public async Task<ActionResult<UserTimeReport>> EditMyTimeReport(UpdateTimeReport userTimeReport)
        {
            try
            {
                var timeReportDTO = _mapper.Map<TimeReportDTO>(userTimeReport);
                timeReportDTO.User = new UserShortDTO { Id = this.UserId() };

                var updatedReportDTO = await _timeReportService.UpdateTimeReport(timeReportDTO, (decimal)userTimeReport.InternalHours);

                return Ok(_mapper.Map<UserTimeReport>(updatedReportDTO));
            }
            catch (InvalidOperationException ex)
            {
                return BadRequest(ex.Message);
            }

        }

        /// <summary>
        /// Delete UserTimeReport by id
        /// </summary>
        /// <param name="id">UserTimeReport Id</param>
        /// <returns>No content</returns>
        [HttpDelete(R.TimeReport + "/" + R.Id)]
        public async Task<IActionResult> DeleteMyTimeReport(int id)
        {
            await _timeReportService.DeleteOwnTimeReport(this.UserId(), id);
            return NoContent();
        }


        /// <summary>
        /// Get reopen request notification recipients 
        /// </summary>
        /// <param name="projectReopenRequest">Project Reopen Request</param>
        /// <returns></returns>
        [HttpGet(R.Project + "/Reopen/Recipients")]
        public Task<IEnumerable<string>> GetReopenRequestNotificationRecipients([FromQuery] ProjectReopenRequest projectReopenRequest)
        {
            return _projectReportingReopenRequestService.GetReopenRequestNotificationRecipients(
                _mapper.Map<ProjectReopenRequestDTO>(projectReopenRequest));

        }

        /// <summary>
        /// Send requests to Project Managers in Slack in order to reopen project reporting 
        /// </summary>
        /// <param name="projectReopenRequest">Project Reopen Request</param>
        /// <returns></returns>
        [HttpPost(R.Project + "/Reopen")]
        public async Task<IActionResult> SendSlackMessageReopenProjectReporting([FromBody] ProjectReopenRequest projectReopenRequest)
        {
            var isSuccessful = await _projectReportingReopenRequestService.RequestReopenProjectReporting(
                _mapper.Map<ProjectReopenRequestDTO>(projectReopenRequest));

            if (!isSuccessful)
            {
                return BadRequest("Failed to send a request. Please, try again, or contact your PM");
            }

            return Ok("Request Sent");
        }
    }
}