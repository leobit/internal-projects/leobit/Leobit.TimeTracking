using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Mvc;
using OfficeOpenXml;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.TimeReport)]
    [Produces("application/json")]
    [ApiController]
    public class TimeReportController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly ProjectService _projectService;
        private readonly TimeReportService _timeReportService;
        private readonly WordReportGeneratorService _wordReportGeneratorService;

        public TimeReportController(
            IMapper mapper,
            ProjectService projectService,
            TimeReportService timeReportService,
            WordReportGeneratorService wordReportGeneratorService)
        {
            _mapper = mapper;
            _projectService = projectService;
            _timeReportService = timeReportService;
            _wordReportGeneratorService = wordReportGeneratorService;
        }

        /// <summary>
        /// Get list of short projects info by account id for further TimeReport creating
        /// </summary>
        /// <returns>List of short projects info</returns>
        [HttpGet]
        [ClaimsRoute(R.Project, ClaimType.TeamTimeReport, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.TeamTimeReport, ClaimValue.Read)]
        public async Task<IEnumerable<ProjectShort>> GetTimeReportProjects()
        {
            return _mapper.Map<IEnumerable<ProjectShort>>(
                await _projectService.GetAssignedProjectShorts(this.UserId()));
        }

        [HttpGet]
        [ClaimsRoute(R.Project, ClaimType.TimeReport, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
        public async Task<IEnumerable<ProjectShort>> GetProjects_All()
        {
            return _mapper.Map<IEnumerable<ProjectShort>>(
                await _projectService.GetProjectShorts());
        }

        /// <summary>
        /// Get list of TimeReport
        /// </summary>
        /// <param name="id">TimeReport id</param>
        /// <param name="dateFrom">Date from inclusively. By default - first day of current month</param>
        /// <param name="dateTo">Date to inclusively. By default - last day of current month</param>
        /// <param name="projectIds">Projects ids</param>
        /// <param name="userIds">User ids</param>
        /// <param name="isOverhead">Is Overhead</param>
        /// <returns>List of TimeReport</returns>
        [HttpGet]
        [ClaimsRoute(ClaimType.TeamTimeReport, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.TeamTimeReport, ClaimValue.Read)]
        public async Task<IEnumerable<TimeReport>> GetTeamTimeReports(int? id, DateTime? dateFrom, DateTime? dateTo, [FromQuery] ICollection<int> projectIds, [FromQuery] ICollection<string> userIds, bool? isOverhead)
        {
            dateFrom ??= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dateTo ??= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);

            return _mapper.Map<IEnumerable<TimeReport>>(
                await _timeReportService.GetTeamTimeReports(this.UserId(), id, dateFrom.Value, dateTo.Value, projectIds, userIds, isOverhead));
        }

        [HttpGet]
        [ClaimsRoute(ClaimType.TimeReport, ClaimValue.Read)]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
        public async Task<IEnumerable<TimeReport>> GetTimeReports_All(int? id, DateTime? dateFrom, DateTime? dateTo, [FromQuery] ICollection<int> projectIds, [FromQuery] ICollection<string> userIds, bool? isOverhead)
        {
            dateFrom ??= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1);
            dateTo ??= new DateTime(DateTime.Now.Year, DateTime.Now.Month, 1).AddMonths(1).AddDays(-1);

            return _mapper.Map<IEnumerable<TimeReport>>(
                await _timeReportService.GetTimeReports(id, dateFrom.Value, dateTo.Value, projectIds, userIds, isOverhead));
        }


        /// <summary>
        /// Export time reports to excel
        /// </summary>
        /// <returns>Excel file</returns>
        [HttpPost]
        [Route("Excel")]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
        public async Task<FileResult> ExportTimeReportsToExcel(TimeReportExcelRequest request)
        {
            var timeReports = await _timeReportService.GetTimeReports(request.Ids, request.UserIds);
            
            var timeReportsExportItems = timeReports
                .Select(tr => new
                {
                    tr.Id,
                    Timestamp = tr.Timestamp.ToString(@"MM/dd/yyyy hh\:mm\:ss"),
                    User = $"{tr.User.FirstName} {tr.User.LastName}",
                    Account = tr.Account.Name,
                    Project = tr.Project.Name,
                    tr.WorkDescription,
                    Int = tr.InternalHours,
                    Ext = tr.ExternalHours,
                    tr.IsOverhead,
                    ReportingDate = tr.ReportingDate.ToString("MM/dd/yyyy")
                });

            var stream = new MemoryStream();

            using (var package = new ExcelPackage(stream))
            {
                var workSheet = package.Workbook.Worksheets.Add("Sheet1");
                workSheet.Cells.LoadFromCollection(timeReportsExportItems, true);
                workSheet.Row(1).Style.Font.Bold = true;

                await package.SaveAsync();
            }

            stream.Position = 0;

            var excelName = $"TimeReports-{DateTime.Now:yyyyMMddHHmmssfff}.xlsx";

            return File(stream, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", excelName);
        }

        /// <summary>
        /// Export time reports to word
        /// </summary>
        /// <returns>Word file</returns>
        [HttpPost]
        [Route("Word")]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
        public async Task<IActionResult> ExportTimeReportsToWord(TimeReportWordRequest request)
        {
            try
            {
                var timeReports = await _timeReportService.GetTimeReports(request.Ids, request.UserIds);
                var stream =
                    await _wordReportGeneratorService.CreateTimeReportsWordDocumentAsync(timeReports, request.DateFrom,
                        request.DateTo);

                stream.Position = 0;

                var wordName = $"TimeReports-{DateTime.Now:yyyyMMddHHmmssfff}.docx";
                return File(stream, "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                    wordName);
            }
            catch(Exception ex)
            {
                return BadRequest(ex.Message);
            }
        }

        /// <summary>
        /// Create new TimeReport
        /// </summary>
        /// <param name="timeReport">TimeReport</param>
        /// <returns>Created TimeReport</returns>
        [HttpPost]
        [ClaimsAuthorize(ClaimType.TeamTimeReport, ClaimValue.Write)]
        public TimeReport CreateTimeReport(TimeReport timeReport)
        {
            var timeReportDTO = _mapper.Map<TimeReportDTO>(timeReport);
            timeReportDTO = _timeReportService.CreateTeamTimeReport(this.UserId(), timeReportDTO);

            return _mapper.Map<TimeReport>(timeReportDTO);
        }

        [HttpPost]
        [ClaimsRoute(ClaimType.TimeReport, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Write)]
        public async Task<TimeReport> CreateTimeReport_All(TimeReport timeReport, CancellationToken cancellationToken)
        {
            var timeReportDTO = _mapper.Map<TimeReportDTO>(timeReport);
            timeReportDTO = await _timeReportService.CreateTimeReport(timeReportDTO, true, cancellationToken);
            
            return _mapper.Map<TimeReport>(timeReportDTO);
        }

        /// <summary>
        /// Update existing UserTimeReport
        /// </summary>
        /// <param name="timeReport">TimeReport</param>
        /// <returns>Updated TimeReport</returns>
        [HttpPut]
        [ClaimsAuthorize(ClaimType.TeamTimeReport, ClaimValue.Write)]
        public async Task<TimeReport> EditTimeReport(TimeReport timeReport)
        {
            var timeReportDTO = _mapper.Map<TimeReportDTO>(timeReport);
            timeReportDTO = await _timeReportService.EditTeamTimeReport(this.UserId(), timeReportDTO);
            return _mapper.Map<TimeReport>(timeReportDTO);
        }

        [HttpPut]
        [ClaimsRoute(ClaimType.TimeReport, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Write)]
        public async Task<TimeReport> EditTimeReport_All(TimeReport timeReport)
        {
            var timeReportDTO = _mapper.Map<TimeReportDTO>(timeReport);
            timeReportDTO = await _timeReportService.EditTimeReport(timeReportDTO);

            return _mapper.Map<TimeReport>(timeReportDTO);
        }

        [HttpPut("Batch")]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Write)]
        public async Task<IEnumerable<TimeReport>> EditTimeReports([FromBody]List<TimeReport> timeReports)
        {
            var timeReportsDTOs = _mapper.Map<List<TimeReportDTO>>(timeReports);
            timeReportsDTOs = await _timeReportService.EditTimeReports(this.UserId(), timeReportsDTOs);

            return _mapper.Map<List<TimeReport>>(timeReportsDTOs);
        }

        [HttpGet]
        [Route("Batch")]
        [ClaimsAuthorize(ClaimType.TimeReportHistory, ClaimValue.Read)]
        public async Task<IEnumerable<TimeReportBatchCommitDTO>> GetTimeReportBatchCommits()
        {
            return await _timeReportService.GetTimeReportCommits();
        }

        [HttpGet]
        [Route("Batch/" + R.Id)]
        [ClaimsAuthorize(ClaimType.TimeReportHistory, ClaimValue.Read)]
        public async Task<IEnumerable<TimeReportService.TimeReportCommitDetails>> GetTimeReportBatchCommitDetails(string id)
        {
            return await _timeReportService.GetTimeReportCommitDetails(id);
        }

        [HttpDelete]
        [Route("Batch/" + R.Id)]
        [ClaimsAuthorize(ClaimType.TimeReportHistory, ClaimValue.Write)]
        public async Task<IActionResult> RevertTimeReportBatchCommit(string id)
        {
            await _timeReportService.RevertTimeReportCommit(id);

            return NoContent();
        }

        [HttpGet]
        [Route("reminders/{type}")]
        //[ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Read)]
        public async Task<IActionResult> GetTimeReportReminders(string type, string token)
        {
            if (string.IsNullOrEmpty(token) || token != "tt.service.tmp.token")
            {
                return Forbid();
            }

            var reminders = await _timeReportService.GetTimeReportReminders(type);

            return Ok(_mapper.Map<IEnumerable<TimeReportReminder>>(reminders));
        }

        /// <summary>
        /// Delete TimeReport by id
        /// </summary>
        /// <param name="id">TimeReport id</param>
        /// <returns>No content</returns>
        [HttpDelete]
        [Route(R.Id)]
        [ClaimsAuthorize(ClaimType.TeamTimeReport, ClaimValue.Write)]
        public async Task<IActionResult> DeleteTimeReport(int id)
        {
            await _timeReportService.DeleteTeamTimeReport(this.UserId(), id);

            return NoContent();
        }

        [HttpDelete]
        [ClaimsRoute(R.Id, ClaimType.TimeReport, ClaimValue.Write)]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Write)]
        public async Task<IActionResult> DeleteTimeReport_All(int id)
        {
            await _timeReportService.DeleteTimeReport(this.UserId(), id);

            return NoContent();
        }

        [HttpDelete]
        [ClaimsAuthorize(ClaimType.TimeReport, ClaimValue.Write)]
        public async Task<IActionResult> DeleteTimeReports([FromQuery] int[] ids)
        {
            await _timeReportService.DeleteTimeReports(this.UserId(), ids);

            return NoContent();
        }
    }
}