using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Settings)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class SettingsController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly SettingsService _settingsService;

        public SettingsController(IMapper mapper, SettingsService settingsService)
        {
            _mapper = mapper;

            _settingsService = settingsService;
        }

        /// <summary>
        /// Get reporting settings
        /// </summary>
        /// <returns>Settings</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.Settings, ClaimValue.Read)]
        public async Task<Settings> GetSettings()
        {
            return _mapper.Map<Settings>(await _settingsService.GetSettings());
        }

        /// <summary>
        /// Update reporting settings
        /// </summary>
        /// <param name="settings">Settings</param>
        /// <returns>Updated Settings</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Settings, ClaimValue.Write)]
        public async Task<Settings> EditSettings(Settings settings)
        {
            var settingsDTO = _mapper.Map<SettingsDTO>(settings);
            settingsDTO = await _settingsService.EditSettings(settingsDTO);

            return _mapper.Map<Settings>(settingsDTO);
        }
    }
}