﻿using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.User)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class UserController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly UserService _userService;
        private readonly RoleService _roleService;

        public UserController(IMapper mapper, UserService userService, RoleService roleService)
        {
            _mapper = mapper;

            _userService = userService;
            _roleService = roleService;
        }

        /// <summary>
        /// Get list of active users
        /// </summary>
        /// <param name="roleId">Role id</param>
        /// <returns>List of users</returns>
        [HttpGet("active")]
        public async Task<IEnumerable<UserShort>> GetActiveUsers(int? roleId)
        {
            return _mapper.Map<List<UserShort>>(await _userService.GetActiveUsers(roleId));
        }

        /// <summary>
        /// Get list of inactive users
        /// </summary>
        /// <param name="roleId">Role id</param>
        /// <returns>List of inactive users</returns>
        [HttpGet("inactive")]
        public async Task<IEnumerable<UserShort>> GetInactiveUsers(int? roleId)
        {
            return _mapper.Map<List<UserShort>>(await _userService.GetInactiveUsers(roleId, this.UserId()));
        }

        /// <summary>
        /// Get user role
        /// </summary>
        /// <param name="id">User id</param>
        /// <returns>User role</returns>
        [HttpGet(R.Id + "/" + R.Role)]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Read)]
        public async Task<RoleShort> GetUserRole(string id)
        {
            return _mapper.Map<RoleShort>(await _roleService.GetUserRole(id));
        }

        /// <summary>
        /// Update existing user
        /// </summary>
        /// <param name="user">User</param>
        /// <returns>Updated user</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Role, ClaimValue.Write)]
        public async Task<UserShort> EditUser(UserShort user)
        {
            var userShortDTO = _mapper.Map<UserShortDTO>(user);

            return _mapper.Map<UserShort>(await _userService.EditUser(userShortDTO));
        }
    }
}
