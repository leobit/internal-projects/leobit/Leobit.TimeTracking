﻿using AutoMapper;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Reserve)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class ReserveController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ReserveService _reserveService;

        public ReserveController(IMapper mapper, ReserveService reserveService)
        {
            _mapper = mapper;
            _reserveService = reserveService;
        }

        [HttpGet(R.Employee)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Write)]
        public async Task<IEnumerable<ReserveEmployeeInformation>> GetReserveEmployeesInformation(int? year = null)
        {
            var reserveEmployeeInformationDtos = await _reserveService.GetReserveEmployeesInformation(year ?? DateTime.Now.Year);
            return _mapper.Map<List<ReserveEmployeeInformation>>(reserveEmployeeInformationDtos);
        }
    }
}
