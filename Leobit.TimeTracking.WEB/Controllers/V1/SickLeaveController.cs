﻿using AutoMapper;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.SickLeave)]
    [Produces("application/json")]
    [ApiController]
    [Authorize]
    public class SickLeaveController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly SickLeaveService _sickLeaveService;

        public SickLeaveController(IMapper mapper, SickLeaveService sickLeaveService)
        {
            _mapper = mapper;
            _sickLeaveService = sickLeaveService;
        }

        [HttpGet]
        public async Task<SickLeaveInformation> GetSickLeaveInformation(int? year = null)
        {
            return _mapper.Map<SickLeaveInformation>(
                await _sickLeaveService.GetSickLeaveInformation(this.UserId(), year ?? DateTime.UtcNow.Year));
        }

        [HttpGet]
        [Route(R.Employee)]
        [ClaimsAuthorize(ClaimType.EmployeeVacationAndSickLeave, ClaimValue.Read)]
        public async Task<SickLeaveInformation> GetEmployeeSickLeaveInformation(string employeeId, int? year = null)
        {
            return _mapper.Map<SickLeaveInformation>(
                await _sickLeaveService.GetSickLeaveInformation(employeeId, year ?? DateTime.UtcNow.Year));
        }
    }
}
