using AutoMapper;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Leobit.TimeTracking.WEB.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using R = Leobit.TimeTracking.WEB.Infrastructure.Routes;

namespace Leobit.TimeTracking.WEB.Controllers.V1
{
    [ApiVersion("1")]
    [Route(R.ApiVersion + R.Project)]
    [Produces("application/json")]
    [ApiController]
    public class ProjectController : ControllerBase
    {
        private readonly IMapper _mapper;

        private readonly ProjectService _projectService;
        private readonly ProjectReportingReopenRequestService _projectReportingReopenRequestService;
        private readonly AccountService _accountService;
        private readonly CategoryService _categoryService;

        public ProjectController(
            IMapper mapper, 
            ProjectService projectService,
            ProjectReportingReopenRequestService projectReportingReopenRequestService,
            AccountService accountService, 
            CategoryService categoryService)
        {
            _mapper = mapper;
            _projectService = projectService;
            _accountService = accountService;
            _categoryService = categoryService;
            _projectReportingReopenRequestService = projectReportingReopenRequestService;
        }

        /// <summary>
        /// Get list of short accounts info for further project creating
        /// </summary>
        /// <returns>List of short accounts info</returns>
        [HttpGet]
        [Route(R.Account)]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Read)]
        public async Task<IEnumerable<AccountShort>> GetShortAccounts()
        {
            return _mapper.Map<List<AccountShort>>(await _accountService.GetAccountShorts());
        }

        /// <summary>
        /// Get list of short categories info for further project creating
        /// </summary>
        /// <returns>List of short categories info</returns>
        [HttpGet]
        [Route(R.Category)]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Read)]
        public async Task<IEnumerable<Category>> GetShortCategories()
        {
            return _mapper.Map<List<Category>>(await _categoryService.GetCategories());
        }

        /// <summary>
        /// Get list of projects
        /// </summary>
        /// <param name="id">Project id</param>
        /// <param name="accountIds">Accounts ids</param>
        /// <returns>List of projects</returns>
        [HttpGet]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Read)]
        public async Task<IEnumerable<Project>> GetProjects(int? id, [FromQuery] ICollection<int> accountIds)
        {
            return _mapper.Map<List<Project>>(await _projectService.GetProjects(id, accountIds));
        }

        /// <summary>
        /// Create new project
        /// </summary>
        /// <param name="project">Project</param>
        /// <returns>Created project</returns>
        [LogAction]
        [HttpPost]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Write)]
        public async Task<Project> CreateProject(Project project)
        {
            var projectDTO = _mapper.Map<ProjectDTO>(project);

            return _mapper.Map<Project>(await _projectService.CreateProject(projectDTO, this.UserId()));
        }

        /// <summary>
        /// Update existing project
        /// </summary>
        /// <param name="project">Project</param>
        /// <returns>Updated project</returns>
        [LogAction]
        [HttpPut]
        [ClaimsAuthorize(ClaimType.Project, ClaimValue.Write)]
        public async Task<Project> EditProject(Project project)
        {
            var projectDTO = _mapper.Map<ProjectDTO>(project);
            projectDTO.RestoreClosingSettingsLater = true;

            return _mapper.Map<Project>(await _projectService.EditProject(projectDTO));
        }

        /// <summary>
        /// Approve request for reopening project reporting
        /// </summary>
        /// <param name="id">Project Reporting Reopen Request id</param>
        /// <returns></returns>
        [HttpPost("Reopen/Approve/{id}")]
        [Authorize] // Anyone can approve request assigned to them
        public async Task<IActionResult> ApproveRequestReopenProjectReporting(int id)
        {
            if (!await _projectReportingReopenRequestService.ProjectReportingReopenRequestExists(id))
            {
                return BadRequest("Request has been expired or cancelled");
            }
            if (await _projectReportingReopenRequestService.IsProjectReportingReopenRequestApproved(id))
            {
                return Ok("Request has already been approved");
            }

            var projectManagerId = HttpContext.User.FindFirstValue("sub");
            
            var isSuccessful = await _projectReportingReopenRequestService.ApproveRequestReopenProjectReporting(id, projectManagerId);

            if (!isSuccessful)
            {
                return BadRequest("Failed to send a request. Please, try again");
            }

            return Ok("Request is approved");
        }

        /// <summary>
        /// Decline request for reopening project reporting
        /// </summary>
        /// <param name="id">Project Reporting Reopen Request id</param>
        /// <returns></returns>
        [HttpDelete("Reopen/Approve/{id}")]
        [Authorize]
        public async Task<IActionResult> DeclineRequestReopenProjectReporting(int id)
        {
            var notExpired = await _projectReportingReopenRequestService.ProjectReportingReopenRequestExists(id);
            if (!notExpired)
            {
                return BadRequest("Request has been expired or cancelled");
            }

            var alreadyApproved = await _projectReportingReopenRequestService.IsProjectReportingReopenRequestApproved(id);
            if (alreadyApproved)
            {
                return Ok("Request has already been approved");
            }
            
            var isSuccessful = await _projectReportingReopenRequestService.DeclineRequestReopenProjectReporting(id);

            if (!isSuccessful)
            {
                return BadRequest("Failed to send a request. Please, try again");
            }
            
            return Ok("Request is declined");
        }

        [HttpGet("Reopen/{id}/Authority")]
        [Authorize] // Anyone can see who can approve the request
        public async Task<IEnumerable<ReopenReportingApprovalAuthority>> GetReopenReportingAuthorities(int id)
        {
            return _mapper.Map<IEnumerable<ReopenReportingApprovalAuthority>>(
                await _projectReportingReopenRequestService.GetReopenReportingAllowance(id, this.UserId()));
        }

        /// <summary>
        /// Delete project by id
        /// </summary>
        /// <param name="id">Project id</param>
        /// <returns>No content</returns>
        //[HttpDelete]
        //[Route(R.Id)]
        //[ClaimsAuthorize(ClaimType.Project, ClaimValue.Write)]
        //public IActionResult DeleteProject(int id)
        //{
        //    _projectService.DeleteProject(id);
        //    return NoContent();
        //}
    }
}