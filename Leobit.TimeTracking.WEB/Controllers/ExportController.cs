﻿using Leobit.BigQueryExport;
using Leobit.TimeTracking.BL.Models;
using Leobit.TimeTracking.BL.Services;
using Leobit.TimeTracking.General.Background;
using Leobit.TimeTracking.WEB.Infrastructure;
using Leobit.TimeTracking.WEB.Infrastructure.Claims;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Leobit.TimeTracking.WEB.Controllers
{
    [ApiExplorerSettings(IgnoreApi = true)]
    public class ExportController : ControllerBase
    {
        private readonly ExportService _exportService;
        private readonly TaskQueue _taskQueue;
        private readonly BigQueryConfigurations _bigQueryConfigurations;

        public ExportController(
            ExportService exportService,
            TaskQueue taskQueue,
            IOptions<BigQueryConfigurations> bigQueryConfigurations)
        {
            _exportService = exportService ?? throw new ArgumentNullException(nameof(exportService));
            _taskQueue = taskQueue ?? throw new ArgumentNullException(nameof(taskQueue));
            _bigQueryConfigurations = bigQueryConfigurations?.Value ?? throw new ArgumentNullException(nameof(bigQueryConfigurations));
        }

        // expect to receive date in ISO 8601 format ("2019-02-16")
        [HttpGet("Export")]
        [ClaimsAuthorize(ClaimType.Export, ClaimValue.Read)]
        public async Task<string[][]> Index(DateTime? from, CancellationToken cancellationToken)
        {
            return await _exportService.GetTimeReportsToExport(ExportService.ExportConfig.DefaultConfig(from), cancellationToken);
        }

        [HttpGet("Export/bq")]
        [ClaimsAuthorize(ClaimType.Export, ClaimValue.Read)]
        public void ExportBigQuery()
        {
            _taskQueue.Enqueue(
                CreateExportTask(ExportService.ExportConfig.DefaultConfig(), _bigQueryConfigurations.DefaultExport));
        }

        [HttpGet("Export/bq/monthly")]
        [ClaimsAuthorize(ClaimType.Export, ClaimValue.Read)]
        public void ExportBigQueryMonthly()
        {
            _taskQueue.Enqueue(
                CreateExportTask(ExportService.ExportConfig.MonthlyReport(), _bigQueryConfigurations.MonthlyExport));
        }

        [HttpGet("Export/resource")]
        [ClaimsAuthorize(ClaimType.Export, ClaimValue.Read)]
        public async Task<IEnumerable<SpreadSheetTimeReportDTO>> TimeReportsToExportResource(DateTime? from, CancellationToken cancellationToken)
        {
            return await _exportService.GetTimeReportsToExportResource(ExportService.ExportConfig.DefaultConfig(from), cancellationToken);
        }

        private static Func<IServiceProvider, CancellationToken, Task> CreateExportTask(ExportService.ExportConfig exportConfig, BigQueryConfiguration bigQueryConfig)
        {
            return async (serviceProvider, cancellationToken) =>
            {
                var exportCoordinator = serviceProvider.GetService<BigQueryExportCoordinator>();

                await exportCoordinator.ExecuteAsync(exportConfig, bigQueryConfig, cancellationToken);
            };
        }
    }
}