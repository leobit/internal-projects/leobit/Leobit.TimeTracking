﻿using Leobit.TimeTracking.DAL.Interfaces;
using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Project : IHoursRestrictionRange
    {
        public Project()
        {
            ProjectManager = new HashSet<ProjectManager>();
            TimeReport = new HashSet<TimeReport>();
            TimeReportHistory = new HashSet<TimeReportHistory>();
            UserProject = new HashSet<UserProject>();
        }

        public int Id { get; set; }
        public int AccountId { get; set; }
        public int? CategoryId { get; set; }
        public string Name { get; set; }
        public bool OverrideHoursSettings { get; set; }
        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public decimal? MaxTotal { get; set; }
        public bool OverrideClosingSettings { get; set; }
        public short ClosingType { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
        public bool IsClosed { get; set; }
        public bool AutoAssignNewUsers { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int ProjectType { get; set; }
        public int ProjectPriority { get; set; }

        public virtual Account Account { get; set; }
        public virtual Category Category { get; set; }
        public virtual ProjectSettingsBackup ProjectSettingsBackup { get; set; }
        public virtual ICollection<ProjectManager> ProjectManager { get; set; }
        public virtual ICollection<TimeReport> TimeReport { get; set; }
        public virtual ICollection<TimeReportHistory> TimeReportHistory { get; set; }
        public virtual ICollection<UserProject> UserProject { get; set; }
        public virtual ICollection<ProjectReportingReopenRequest> ProjectReportingReopenRequests { get; set; }
    }
}
