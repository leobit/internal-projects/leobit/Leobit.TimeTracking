﻿using Microsoft.EntityFrameworkCore;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class LeobitContext : DbContext
    {
        public LeobitContext()
        {
        }

        public LeobitContext(DbContextOptions<LeobitContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Account> Account { get; set; }
        public virtual DbSet<Category> Category { get; set; }
        public virtual DbSet<CustomDayOff> CustomDayOff { get; set; }
        public virtual DbSet<Division> Division { get; set; }
        public virtual DbSet<Employee> Employee { get; set; }
        public virtual DbSet<Holiday> Holiday { get; set; }
        public virtual DbSet<OrganizationUnit> OrganizationUnit { get; set; }
        public virtual DbSet<Permission> Permission { get; set; }
        public virtual DbSet<Project> Project { get; set; }
        public virtual DbSet<ProjectManager> ProjectManager { get; set; }
        public virtual DbSet<Role> Role { get; set; }
        public virtual DbSet<RolePermission> RolePermission { get; set; }
        public virtual DbSet<Settings> Settings { get; set; }
        public virtual DbSet<TimeReport> TimeReport { get; set; }
        public virtual DbSet<TimeReportHistory> TimeReportHistory { get; set; }
        public virtual DbSet<TransferredVacationBalance> TransferredVacationBalance { get; set; }
        public virtual DbSet<User> User { get; set; }
        public virtual DbSet<UserProject> UserProject { get; set; }
        public virtual DbSet<ProjectSettingsBackup> ProjectSettingsBackup { get; set; }
        public virtual DbSet<ProjectReportingReopenRequest> ProjectReportingReopenRequest { get; set; }
        public virtual DbSet<FiltersPreset> FiltersPreset { get; set; }
        public virtual DbSet<EmployeeHistory> EmployeeHistory { get; set; }
        public virtual DbSet<EmployeeMonthlyActivityView> EmployeeMonthlyActivityView { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Account>(entity =>
            {
                entity.Property(e => e.MaxExternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MaxInternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MinExternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MinInternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Category>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.OrganizationUnit)
                    .WithMany(p => p.Category)
                    .HasForeignKey(d => d.OrganizationUnitId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Category_OrganizationUnit");
            });

            modelBuilder.Entity<CustomDayOff>(entity =>
            {
                entity.Property(e => e.ExtraDayOff).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.LastEditTimestamp).HasPrecision(0);

                entity.Property(e => e.LastEditorId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.LastEditor)
                    .WithMany(p => p.CustomDayOffLastEditor)
                    .HasForeignKey(d => d.LastEditorId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomDayOff_LastEditor");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.CustomDayOffUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_CustomDayOff_User");
            });

            modelBuilder.Entity<Division>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Employee>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(50);

                entity.Property(e => e.AccountType).HasMaxLength(50);

                entity.Property(e => e.Birthday).HasColumnType("date");

                entity.Property(e => e.CareerStart).HasColumnType("date");

                entity.Property(e => e.ContractDate).HasColumnType("date");

                entity.Property(e => e.ContractId)
                    .HasMaxLength(64)
                    .HasColumnName("ContractID");

                entity.Property(e => e.Dismissal).HasMaxLength(20);

                entity.Property(e => e.Employment)
                    .IsRequired()
                    .HasMaxLength(5);

                entity.Property(e => e.EnglishLevel).HasMaxLength(50);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.FullTimeStartDate).HasColumnType("date");

                entity.Property(e => e.IntHours).HasColumnType("decimal(10, 2)");

                entity.Property(e => e.Keinfo).HasColumnName("KEInfo");

                entity.Property(e => e.Location).HasMaxLength(10);

                entity.Property(e => e.Manager).HasMaxLength(50);

                entity.Property(e => e.Mentor).HasMaxLength(50);

                entity.Property(e => e.Oblast).HasMaxLength(50);

                entity.Property(e => e.PaidVacationFrom).HasColumnType("date");

                entity.Property(e => e.PaymentMode).HasMaxLength(15);

                entity.Property(e => e.Pemanagement)
                    .HasMaxLength(10)
                    .HasColumnName("PEManagement");

                entity.Property(e => e.Phone).HasMaxLength(15);

                entity.Property(e => e.Position).HasMaxLength(50);

                entity.Property(e => e.Profile).HasMaxLength(50);

                entity.Property(e => e.Seniority).HasMaxLength(50);

                entity.Property(e => e.SenioritySubLevel).HasMaxLength(50);

                entity.Property(e => e.Skype).HasMaxLength(25);

                entity.Property(e => e.SlackUserId).HasMaxLength(16);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.Property(e => e.TerminationDate).HasColumnType("date");
            });

            modelBuilder.Entity<Holiday>(entity =>
            {
                entity.HasKey(e => new { e.Year, e.Month, e.Day, e.Name });

                entity.Property(e => e.Name).HasMaxLength(64);
            });

            modelBuilder.Entity<OrganizationUnit>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.HasOne(d => d.Division)
                    .WithMany(p => p.OrganizationUnit)
                    .HasForeignKey(d => d.DivisionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_OrganizationUnit_Division");
            });

            modelBuilder.Entity<Permission>(entity =>
            {
                entity.Property(e => e.Claim).HasMaxLength(50);

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<Project>(entity =>
            {
                entity.Property(e => e.EndDate).HasColumnType("date");

                entity.Property(e => e.MaxExternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MaxInternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MaxTotal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MinExternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MinInternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(100)
                    .IsUnicode(false);

                entity.Property(e => e.StartDate).HasColumnType("date");

                entity.HasOne(d => d.Account)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.AccountId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_Project_Account");

                entity.HasOne(d => d.Category)
                    .WithMany(p => p.Project)
                    .HasForeignKey(d => d.CategoryId)
                    .HasConstraintName("FK_Project_Category");
            });

            modelBuilder.Entity<ProjectManager>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ProjectId });

                entity.Property(e => e.UserId).HasMaxLength(50);

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.ProjectManager)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProjectManager_Project");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.ProjectManager)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_ProjectManager_User");
            });

            modelBuilder.Entity<Role>(entity =>
            {
                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasMaxLength(50)
                    .IsUnicode(false);
            });

            modelBuilder.Entity<RolePermission>(entity =>
            {
                entity.HasKey(e => new { e.RoleId, e.PermissionId });

                entity.HasOne(d => d.Permission)
                    .WithMany(p => p.RolePermission)
                    .HasForeignKey(d => d.PermissionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RolePermission_Permission");

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.RolePermission)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_RolePermission_Role");
            });

            modelBuilder.Entity<Settings>(entity =>
            {
                entity.HasKey(e => e.Uniqueness);

                entity.Property(e => e.Uniqueness).HasColumnName("_Uniqueness");

                entity.Property(e => e.MaxExternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MaxInternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MinExternal).HasColumnType("decimal(4, 2)");

                entity.Property(e => e.MinInternal).HasColumnType("decimal(4, 2)");
            });

            modelBuilder.Entity<TimeReport>(entity =>
            {
                entity.HasIndex(e => e.ReportingDate, "ReportingDateIndex");

                entity.Property(e => e.ExternalHours).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.InternalHours).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.ReportingDate).HasColumnType("date");

                entity.Property(e => e.StartTime).HasPrecision(0);

                entity.Property(e => e.Timestamp).HasPrecision(0);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.WorkDescription).IsRequired();

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.TimeReport)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TimeReport_Project");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TimeReport)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TimeReport_User");
            });

            modelBuilder.Entity<TimeReportHistory>(entity =>
            {
                entity.Property(e => e.EditGroup)
                    .HasMaxLength(50)
                    .IsUnicode(false);

                entity.Property(e => e.EditTimestamp).HasPrecision(0);

                entity.Property(e => e.EditorId).HasMaxLength(50);

                entity.Property(e => e.ExternalHours).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.InternalHours).HasColumnType("decimal(5, 2)");

                entity.Property(e => e.ReportingDate).HasColumnType("date");

                entity.Property(e => e.StartTime).HasPrecision(0);

                entity.Property(e => e.Timestamp).HasPrecision(0);

                entity.Property(e => e.UserId)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.WorkDescription).IsRequired();

                entity.HasOne(d => d.Editor)
                    .WithMany(p => p.TimeReportHistoryEditor)
                    .HasForeignKey(d => d.EditorId)
                    .HasConstraintName("FK_TimeReportHistory_User_Edit");

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.TimeReportHistory)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TimeReportHistory_Project");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.TimeReportHistoryUser)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_TimeReportHistory_User");
            });

            modelBuilder.Entity<TransferredVacationBalance>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.Year });

                entity.Property(e => e.UserId).HasMaxLength(50);

                entity.Property(e => e.Transferred).HasColumnType("decimal(5, 2)");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.Property(e => e.Id).HasMaxLength(50);

                entity.Property(e => e.FirstName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.Property(e => e.LastName)
                    .IsRequired()
                    .HasMaxLength(50);

                entity.HasOne(d => d.Role)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RoleId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_User_Role");
            });

            modelBuilder.Entity<UserProject>(entity =>
            {
                entity.HasKey(e => new { e.UserId, e.ProjectId });

                entity.Property(e => e.UserId).HasMaxLength(50);

                entity.HasOne(d => d.Project)
                    .WithMany(p => p.UserProject)
                    .HasForeignKey(d => d.ProjectId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserProject_Project");

                entity.HasOne(d => d.User)
                    .WithMany(p => p.UserProject)
                    .HasForeignKey(d => d.UserId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("FK_UserProject_User");
            });

            modelBuilder.Entity<EmployeeMonthlyActivityView>(entity =>
            {
                entity.HasNoKey();
                entity.ToView("EmployeeMonthlyActivityView");
            });

            modelBuilder.Entity<EmployeeHistory>().HasNoKey();

            OnModelCreatingPartial(modelBuilder);
        }

        partial void OnModelCreatingPartial(ModelBuilder modelBuilder);
    }
}
