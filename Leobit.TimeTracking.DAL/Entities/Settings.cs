﻿using Leobit.TimeTracking.DAL.Interfaces;
using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Settings : IHoursRestrictionRange
    {
        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public short ClosingType { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
        public bool Uniqueness { get; set; }
        public string DefaultBo { get; set; }
    }
}
