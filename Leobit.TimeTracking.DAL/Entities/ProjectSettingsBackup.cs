﻿using System;

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class ProjectSettingsBackup
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public DateTime TimeCreated { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string ConfigurationSnapshot { get; set; }
        public virtual Project Project { get; set; }
    }
}
