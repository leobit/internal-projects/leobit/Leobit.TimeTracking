﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class TimeReportHistory
    {
        public int Id { get; set; }
        public DateTime Timestamp { get; set; }
        public string UserId { get; set; }
        public int ProjectId { get; set; }
        public int ProjectPrioritySnapshot { get; set; }
        public string WorkDescription { get; set; }
        public DateTime ReportingDate { get; set; }
        public decimal InternalHours { get; set; }
        public decimal ExternalHours { get; set; }
        public DateTime? StartTime { get; set; }
        public bool IsOverhead { get; set; }
        public int TimeReportId { get; set; }
        public string EditorId { get; set; }
        public DateTime EditTimestamp { get; set; }
        public short EditAction { get; set; }
        public string EditGroup { get; set; }

        public virtual User Editor { get; set; }
        public virtual Project Project { get; set; }
        public virtual User User { get; set; }
    }
}
