﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Employee
    {
        public string Id { get; set; }
        public string FullName { get; set; }
        public bool Active { get; set; }
        public string Employment { get; set; }
        public DateTime? FullTimeStartDate { get; set; }
        public string PaymentMode { get; set; }
        public string Location { get; set; }
        public DateTime? TerminationDate { get; set; }
        public string Seniority { get; set; }
        public string SenioritySubLevel { get; set; }
        public string Profile { get; set; }
        public string Position { get; set; }
        public string Phone { get; set; }
        public string Skype { get; set; }
        public string Photo { get; set; }
        public string Manager { get; set; }
        public string Mentor { get; set; }
        public int? RoomNumber { get; set; }
        public string EnglishLevel { get; set; }
        public string Certificates { get; set; }
        public string Keinfo { get; set; }
        public string Pemanagement { get; set; }
        public DateTime? PaidVacationFrom { get; set; }
        public DateTime? StartDate { get; set; }
        public decimal IntHours { get; set; }
        public string Dismissal { get; set; }
        public DateTime? Birthday { get; set; }
        public int? YearHired { get; set; }
        public int? YearDismissed { get; set; }
        public DateTime? CareerStart { get; set; }
        public string ExpectedPromotion { get; set; }
        public string ContractId { get; set; }
        public DateTime? ContractDate { get; set; }
        public bool? PreferablyBillable { get; set; }
        public string Oblast { get; set; }
        public string SlackUserId { get; set; }
        public string AccountType { get; set; }
        public byte? Level { get; set; }
    }
}
