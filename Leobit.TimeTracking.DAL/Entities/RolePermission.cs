﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class RolePermission
    {
        public byte Value { get; set; }
        public int RoleId { get; set; }
        public int PermissionId { get; set; }

        public virtual Permission Permission { get; set; }
        public virtual Role Role { get; set; }
    }
}
