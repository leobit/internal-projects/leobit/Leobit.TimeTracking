﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class CustomDayOff
    {
        public int Id { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal ExtraDayOff { get; set; }
        public string Description { get; set; }
        public DateTime LastEditTimestamp { get; set; }
        public string UserId { get; set; }
        public string LastEditorId { get; set; }

        public virtual User LastEditor { get; set; }
        public virtual User User { get; set; }
    }
}
