﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Holiday
    {
        public int Year { get; set; }
        public int Month { get; set; }
        public int Day { get; set; }
        public string Name { get; set; }
    }
}
