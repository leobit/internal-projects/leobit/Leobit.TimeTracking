﻿namespace Leobit.TimeTracking.DAL.Entities
{
    public class EmployeeMonthlyActivityView
    {
        public string EmployeeId { get; set; }
        public int Year { get; set; }
        public int Month { get; set; }
        public decimal DaysOffUsed { get; set; }
        public decimal UnpaidDaysOffUsed { get; set; }
        public decimal SickLeaveWithoutCertUsed { get; set; }
        public decimal SickLeaveWithCertUsed { get; set; }
        public decimal DaysInReserve { get; set; }
        public decimal TotalReportedDays { get; set; } // SUM(ProjectType != UnpaidVacation)
        public long TotalRows { get; set; }
    }
}
