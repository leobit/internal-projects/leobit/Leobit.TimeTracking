﻿using System;

namespace Leobit.TimeTracking.DAL.Entities
{
    public class EmployeeHistory
    {
        public int Id { get; set; }
        public string UserId { get; set; } 
        public string Employment { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime Timestamp { get; set; }
    }
}
