﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Permission
    {
        public Permission()
        {
            RolePermission = new HashSet<RolePermission>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Claim { get; set; }
        public bool IsProtected { get; set; }

        public virtual ICollection<RolePermission> RolePermission { get; set; }
    }
}
