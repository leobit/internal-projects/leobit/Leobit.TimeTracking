﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class UserProject
    {
        public string UserId { get; set; }
        public int ProjectId { get; set; }
        public DateTime? AssignmentDate { get; set; }
        public DateTime? StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public int? Hours { get; set; }

        public virtual Project Project { get; set; }
        public virtual User User { get; set; }
    }
}
