﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class User
    {
        public User()
        {
            CustomDayOffLastEditor = new HashSet<CustomDayOff>();
            CustomDayOffUser = new HashSet<CustomDayOff>();
            ProjectManager = new HashSet<ProjectManager>();
            TimeReport = new HashSet<TimeReport>();
            TimeReportHistoryEditor = new HashSet<TimeReportHistory>();
            TimeReportHistoryUser = new HashSet<TimeReportHistory>();
            UserProject = new HashSet<UserProject>();
        }

        public string Id { get; set; }
        public int RoleId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public virtual Role Role { get; set; }
        public virtual ICollection<CustomDayOff> CustomDayOffLastEditor { get; set; }
        public virtual ICollection<CustomDayOff> CustomDayOffUser { get; set; }
        public virtual ICollection<ProjectManager> ProjectManager { get; set; }
        public virtual ICollection<TimeReport> TimeReport { get; set; }
        public virtual ICollection<TimeReportHistory> TimeReportHistoryEditor { get; set; }
        public virtual ICollection<TimeReportHistory> TimeReportHistoryUser { get; set; }
        public virtual ICollection<UserProject> UserProject { get; set; }
    }
}
