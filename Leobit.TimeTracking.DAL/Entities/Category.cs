﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Category
    {
        public Category()
        {
            Project = new HashSet<Project>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int OrganizationUnitId { get; set; }

        public virtual OrganizationUnit OrganizationUnit { get; set; }
        public virtual ICollection<Project> Project { get; set; }
    }
}
