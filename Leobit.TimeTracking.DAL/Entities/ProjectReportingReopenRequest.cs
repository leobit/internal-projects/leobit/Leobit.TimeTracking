﻿using System;

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class ProjectReportingReopenRequest
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime CreationTimestamp { get; set; }
        public DateTime ExpirationTime { get; set; }
        public string SenderId { get; set; }
        public string ProjectManagerApprovedId { get; set; }
        public DateTime? ApprovalTimestamp { get; set; }
        public string SentMessages { get; set; }
        public bool IsDeleted { get; set; }
        public DateTime? DeletionTimestamp { get; set; }
        public virtual Project Project { get; set; }
        public virtual User Sender { get; set; }
        public virtual User ProjectManagerApproved { get; set; }
    }
}
