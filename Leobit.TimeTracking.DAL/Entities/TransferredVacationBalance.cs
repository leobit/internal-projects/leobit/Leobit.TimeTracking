﻿namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class TransferredVacationBalance
    {
        public string UserId { get; set; }
        public int Year { get; set; }
        public decimal Transferred { get; set; }
        public int? CustomCredit { get; set; }
    }
}
