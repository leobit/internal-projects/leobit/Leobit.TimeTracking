﻿#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public class FiltersPreset
    {
        public int Id { get; set; }
        public string UserId { get; set; }
        public string Name { get; set; }
        public string EmployeeReportingFilters { get; set; }

        public virtual User User { get; set; }
    }
}
