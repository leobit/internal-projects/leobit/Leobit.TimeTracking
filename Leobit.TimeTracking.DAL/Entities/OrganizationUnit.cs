﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class OrganizationUnit
    {
        public OrganizationUnit()
        {
            Category = new HashSet<Category>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public int DivisionId { get; set; }

        public virtual Division Division { get; set; }
        public virtual ICollection<Category> Category { get; set; }
    }
}
