﻿using System;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Division
    {
        public Division()
        {
            OrganizationUnit = new HashSet<OrganizationUnit>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<OrganizationUnit> OrganizationUnit { get; set; }
    }
}
