﻿using Leobit.TimeTracking.DAL.Interfaces;
using System.Collections.Generic;

#nullable disable

namespace Leobit.TimeTracking.DAL.Entities
{
    public partial class Account : IHoursRestrictionRange
    {
        public Account()
        {
            Project = new HashSet<Project>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string BudgetOwnerId { get; set; }
        public bool OverrideHoursSettings { get; set; }
        public decimal MinInternal { get; set; }
        public decimal MaxInternal { get; set; }
        public decimal MinExternal { get; set; }
        public decimal MaxExternal { get; set; }
        public bool OverrideClosingSettings { get; set; }
        public short ClosingType { get; set; }
        public int BeforeClosing { get; set; }
        public int AfterClosing { get; set; }
        public short? AccountType { get; set; }

        public virtual User BudgetOwner {  get; set; }
        public virtual ICollection<Project> Project { get; set; }
    }
}
