﻿using Leobit.TimeTracking.DAL.Entities;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class RoleExtensions
    {
        public static IQueryable<Role> ById(this IQueryable<Role> roles, int? id)
        {
            return id.HasValue ?
                roles.Where(x => x.Id == id.Value) :
                roles;
        }

        public static IQueryable<Role> ByUserId(this IQueryable<Role> roles, string userId)
        {
            return !string.IsNullOrWhiteSpace(userId) ?
                roles.Where(r => r.User.Select(u => u.Id).Contains(userId)) :
                roles;
        }
    }
}
