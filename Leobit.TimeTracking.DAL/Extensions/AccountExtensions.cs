using Leobit.TimeTracking.DAL.Entities;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class AccountExtensions
    {
        public static IQueryable<Account> ByUserId(this IQueryable<Account> accounts, string userId)
        {
            return accounts.Where(a => a.Project
                                        .SelectMany(p => p.UserProject)
                                        .Select(up => up.UserId)
                                        .Contains(userId));
        }

        public static IQueryable<Account> ById(this IQueryable<Account> accounts, int? id)
        {
            return id.HasValue ?
                accounts.Where(x => x.Id == id.Value) :
                accounts;
        }
    }
}
