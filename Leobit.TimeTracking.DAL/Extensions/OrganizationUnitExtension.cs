﻿using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class OrganizationUnitExtension
    {
        public static IQueryable<OrganizationUnit> ById(this IQueryable<OrganizationUnit> organizationUnits, int? id)
        {
            return id.HasValue ?
                organizationUnits.Where(x => x.Id == id.Value) :
                organizationUnits;
        }

        public static IQueryable<OrganizationUnit> ByDivisionId(this IQueryable<OrganizationUnit> organizationUnits, int? divisionId)
        {
            return divisionId.HasValue ?
                organizationUnits.Where(x => x.DivisionId == divisionId.Value) :
                organizationUnits;
        }
    }
}
