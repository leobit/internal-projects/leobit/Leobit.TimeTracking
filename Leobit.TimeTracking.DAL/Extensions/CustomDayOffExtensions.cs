using Leobit.TimeTracking.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class CystomDayOffExtensions
    {
        public static IQueryable<CustomDayOff> ById(this IQueryable<CustomDayOff> customDayOffs, int id)
        {
            return customDayOffs.Where(cdo => cdo.Id == id);
        }

        public static IQueryable<CustomDayOff> ByUserId(this IQueryable<CustomDayOff> customDayOffs, string userId)
        {
            return customDayOffs.Where(cdo => cdo.UserId == userId);
        }

        public static IQueryable<CustomDayOff> ByUserIds(this IQueryable<CustomDayOff> customDayOffs, ICollection<string> userIds)
        {
            return (userIds != null && userIds.Count != 0) ?
                customDayOffs.Where(tr => userIds.Contains(tr.UserId)) :
                customDayOffs;
        }

        public static IQueryable<CustomDayOff> ByYear(this IQueryable<CustomDayOff> customDayOffs, int year)
        {
            return customDayOffs.Where(cdo => cdo.Year == year);
        }

        public static IQueryable<CustomDayOff> ByYearAndMonth(this IQueryable<CustomDayOff> customDayOffs, int? year, int? month)
        {
            return year.HasValue && month.HasValue ?
                customDayOffs.Where(x => x.Year == year.Value && x.Month == month) :
                year.HasValue ?
                    customDayOffs.Where(x => x.Year == year.Value) :
                    customDayOffs;
        }

        public static IQueryable<CustomDayOff> StartFrom(this IQueryable<CustomDayOff> customDayOffs, int year)
        {
            return customDayOffs.Where(cdo => cdo.Year >= year);
        }

        public static IQueryable<CustomDayOff> ByMonth(this IQueryable<CustomDayOff> customDayOffs, int month)
        {
            return customDayOffs.Where(cdo => cdo.Month == month);
        }

        public static IQueryable<CustomDayOff> ByMonthRange(this IQueryable<CustomDayOff> customDayOffs, int firstMonth, int lastMonth)
        {
            return customDayOffs.Where(cdo => cdo.Month >= firstMonth &&
                                              cdo.Month <= lastMonth);
        }
    }
}
