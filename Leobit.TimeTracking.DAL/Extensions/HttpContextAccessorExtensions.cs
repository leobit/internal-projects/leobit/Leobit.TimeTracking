﻿using IdentityModel;
using Microsoft.AspNetCore.Http;
using System.Linq;
using Microsoft.IdentityModel.Protocols;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class HttpContextAccessorExtensions
    {
        public static string GetUserId(this IHttpContextAccessor httpContextAccessor)
        {
            return httpContextAccessor.HttpContext?.User?.FindFirst(JwtClaimTypes.Subject)?.Value;
        }

        public static bool IsAdmin(this IHttpContextAccessor httpContextAccessor)
        {
            return httpContextAccessor.HttpContext?.User?.Claims.Any(c => c.Type == "TimeTrackingAdmin") ?? false;
        }
    }
}
