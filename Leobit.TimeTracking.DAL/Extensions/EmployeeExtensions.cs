﻿using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class EmployeeExtensions
    {
        public static IQueryable<Employee> ById(this IQueryable<Employee> employees, string userId)
        {
            return !string.IsNullOrWhiteSpace(userId) ?
                employees.Where(u => u.Id == userId) :
                employees;
        }

        public static IQueryable<Employee> ByIds(this IQueryable<Employee> employees, ICollection<string> ids)
        {
            return (ids != null && ids.Count != 0) ?
                employees.Where(e => ids.Contains(e.Id)) :
                employees;
        }

        public static DateTime? GetTrialEndDate(this Employee employee)
        {
            if (employee?.FullTimeStartDate == null)
            {
                return null;
            }

            return new[] {
                employee?.FullTimeStartDate.Value,
                employee.StartDate.Value
            }
            .Where(x => x.HasValue)
            .Select(x => x.Value)
            .Min()
            .AddMonths(3);
        }
    }
}