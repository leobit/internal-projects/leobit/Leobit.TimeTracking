using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class UserProjectExtensions
    {
        public static IQueryable<UserProject> ByTeamUserId(this IQueryable<UserProject> userProjects,string userId)
        {
            return userProjects.Where(up => up.UserId == userId)
                .Select(up => up.Project)
                .SelectMany(p => p.UserProject);
        }

        public static IQueryable<UserProject> ByUserIds(this IQueryable<UserProject> userProjects, ICollection<string> userIds)
        {
            return (userIds != null && userIds.Count != 0) ?
                userProjects.Where(up => userIds.Contains(up.UserId)) :
                userProjects;
        }

        public static IQueryable<UserProject> ByUserId(this IQueryable<UserProject> userProjects, string userId)
        {
            return userProjects.Where(x => x.UserId == userId);
        }

        public static IQueryable<UserProject> ByProjectIds(this IQueryable<UserProject> userProjects, ICollection<int> projectIds)
        {
            return (projectIds != null && projectIds.Count != 0) ?
                userProjects.Where(up => projectIds.Contains(up.ProjectId)) :
                userProjects;
        }
    }
}
