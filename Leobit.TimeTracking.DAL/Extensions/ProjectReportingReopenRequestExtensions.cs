﻿using Leobit.TimeTracking.DAL.Entities;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class ProjectReportingReopenRequestExtensions
    {
        public static IQueryable<ProjectReportingReopenRequest> NotDeleted(this IQueryable<ProjectReportingReopenRequest> projectReportingReopenRequests)
        {
            return projectReportingReopenRequests.Where(p => !p.IsDeleted);
        }
    }
}
