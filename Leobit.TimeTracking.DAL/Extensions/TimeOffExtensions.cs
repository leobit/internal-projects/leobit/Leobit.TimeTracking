﻿using Leobit.TimeTracking.DAL.Entities;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class TimeOffExtensions
    {
        public static IQueryable<EmployeeMonthlyActivityView> ByYear(this IQueryable<EmployeeMonthlyActivityView> timeOffs, int? year)
        {
            return year.HasValue ?
                timeOffs.Where(x => x.Year == year.Value) :
                timeOffs;
        }

        public static IQueryable<EmployeeMonthlyActivityView> ByYearAndMonth(this IQueryable<EmployeeMonthlyActivityView> timeOffs, int? year, int? month)
        {
            return year.HasValue && month.HasValue ?
                timeOffs.Where(x => x.Year == year && x.Month == month) :
                year.HasValue ?
                    timeOffs.Where(x => x.Year == year) :
                    timeOffs;
        }

        public static IQueryable<EmployeeMonthlyActivityView> ByEmployeeId(this IQueryable<EmployeeMonthlyActivityView> timeOffs, string employeeId)
        {
            return !string.IsNullOrEmpty(employeeId) ?
                timeOffs.Where(x => x.EmployeeId == employeeId) :
                timeOffs;
        }

        public static IQueryable<TransferredVacationBalance> ByEmployeeId(this IQueryable<TransferredVacationBalance> tvb, string employeeId)
        {
            return !string.IsNullOrEmpty(employeeId) ?
                tvb.Where(x => x.UserId == employeeId) :
                tvb;
        }

        public static IQueryable<TransferredVacationBalance> ByYear(this IQueryable<TransferredVacationBalance> tvb, int? year)
        {
            return year.HasValue ?
                tvb.Where(x => x.Year == year.Value) :
                tvb;
        }
    }
}