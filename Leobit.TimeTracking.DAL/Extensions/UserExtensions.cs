using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class UserExtensions
    {
        public static IQueryable<User> ById(this IQueryable<User> users, string userId)
        {
            return !string.IsNullOrWhiteSpace(userId) ?
                users.Where(u => u.Id == userId) :
                users;
        }

        public static IQueryable<User> ByRoleId(this IQueryable<User> users, int? roleId)
        {
            return roleId.HasValue ?
                users.Where(x => x.RoleId == roleId.Value) :
                users;
        }
    }
}
