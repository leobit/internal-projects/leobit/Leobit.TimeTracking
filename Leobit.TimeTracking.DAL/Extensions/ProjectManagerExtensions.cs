﻿using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class ProjectManagerExtensions
    {
        public static IQueryable<ProjectManager> ByUserIds(this IQueryable<ProjectManager> projectManagers, ICollection<string> userIds)
        {
            return (userIds != null && userIds.Count != 0) ?
                projectManagers.Where(pm => userIds.Contains(pm.UserId)) :
                projectManagers;
        }

        public static IQueryable<ProjectManager> ByProjectIds(this IQueryable<ProjectManager> projectManagers, ICollection<int> projectIds)
        {
            return (projectIds != null && projectIds.Count != 0) ?
                projectManagers.Where(pm => projectIds.Contains(pm.ProjectId)) :
                projectManagers;
        }
    }
}
