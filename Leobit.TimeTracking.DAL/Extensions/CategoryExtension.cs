﻿using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class CategoryExtension
    {
        public static IQueryable<Category> ById(this IQueryable<Category> categories, int? id)
        {
            return id.HasValue ?
                categories.Where(x => x.Id == id.Value) :
                categories;
        }

        public static IQueryable<Category> ByOrganizationUnitId(this IQueryable<Category> categories, int? organizationUnitId)
        {
            return organizationUnitId.HasValue ?
                categories.Where(x => x.OrganizationUnitId == organizationUnitId.Value) :
                categories;
        }
    }
}
