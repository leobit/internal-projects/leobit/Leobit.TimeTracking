﻿using Leobit.TimeTracking.DAL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class DivisionExtension
    {
        public static IQueryable<Division> ById(this IQueryable<Division> divisions, int? id)
        {
            return id.HasValue ?
                divisions.Where(x => x.Id == id.Value) :
                divisions;
        }
    }
}
