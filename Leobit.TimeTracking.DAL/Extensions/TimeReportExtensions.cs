using System;
using System.Collections.Generic;
using System.Linq;
using Leobit.TimeTracking.DAL.Common;
using Leobit.TimeTracking.DAL.Entities;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class TimeReportExtensions
    {
        public static IQueryable<TimeReport> ById(this IQueryable<TimeReport> timeReports, int? id)
        {
            return id.HasValue ?
                timeReports.Where(tr => tr.Id == id.Value) :
                timeReports;
        }

        public static IQueryable<TimeReport> ByIds(this IQueryable<TimeReport> timeReports, int[] ids)
        {
            return timeReports.Where(tr => ids.Contains(tr.Id));
        }

        public static IQueryable<TimeReport> ByUserIds(this IQueryable<TimeReport> timeReports, ICollection<string> userIds)
        {
            return (userIds != null && userIds.Count != 0) ?
                timeReports.Where(tr => userIds.Contains(tr.UserId)) :
                timeReports;
        }

        public static IQueryable<TimeReport> ByUserId(this IQueryable<TimeReport> timeReports, string userId)
        {
            return (!string.IsNullOrEmpty(userId)) ?
                timeReports.Where(tr => tr.UserId == userId) :
                timeReports;
        }

        public static IQueryable<TimeReport> ByProjectIds(this IQueryable<TimeReport> timeReports, ICollection<int> projectIds)
        {
            return (projectIds != null && projectIds.Count != 0) ?
                timeReports.Where(tr => projectIds.Contains(tr.ProjectId)) :
                timeReports;
        }

        public static IQueryable<TimeReport> ByProjectTypes(this IQueryable<TimeReport> timeReports, ICollection<int> projectTypes)
        {
            return (projectTypes != null && projectTypes.Count != 0) ?
                timeReports.Where(tr => projectTypes.Contains(tr.Project.ProjectType)) :
                timeReports;
        }

        public static IQueryable<TimeReport> ByDate(this IQueryable<TimeReport> timeReports, DateTime date)
        {
            return timeReports.Where(tr => tr.ReportingDate == date.Date);
        }

        public static IQueryable<TimeReport> ByDateRange(this IQueryable<TimeReport> timeReports, DateTime dateFromInclusively, DateTime dateToInclusively)
        {
            return timeReports.Where(tr => tr.ReportingDate >= dateFromInclusively.Date &&
                                           tr.ReportingDate <= dateToInclusively.Date);
        }

        public static IQueryable<TimeReport> ByOverhead(this IQueryable<TimeReport> timeReports, bool? isOverhead)
        {
            return isOverhead.HasValue ?
                timeReports.Where(tr => tr.IsOverhead == isOverhead) :
                timeReports;
        }

        public static IOrderedQueryable<TimeReport> OrderByDate(this IQueryable<TimeReport> timeReports)
        {
            return timeReports.OrderBy(tr => tr.ReportingDate);
        }

        public static IOrderedQueryable<TimeReport> OrderByDateDescending(this IQueryable<TimeReport> timeReports)
        {
            return timeReports.OrderByDescending(tr => tr.ReportingDate);
        }

        public static TimeReportHistory ToHistory(this TimeReport report, BackupOptions opt)
        {
            return new TimeReportHistory
            {
                TimeReportId = report.Id,
                ExternalHours = report.ExternalHours,
                InternalHours = report.InternalHours,
                IsOverhead = report.IsOverhead,
                ProjectId = report.ProjectId,
                ProjectPrioritySnapshot = report.ProjectPrioritySnapshot,
                ReportingDate = report.ReportingDate,
                Timestamp = report.Timestamp,
                UserId = report.UserId,
                WorkDescription = report.WorkDescription,

                EditorId = opt.EditorId,
                EditTimestamp = opt.EditTimestamp,
                EditAction = (short)opt.EditAction,
                EditGroup = opt.EditGroup,

                User = report.User,
                Project = report.Project
            };
        }

        public static TimeReport FromHistory(this TimeReportHistory report)
        {
            return new TimeReport
            {
                Id = report.TimeReportId,
                Timestamp = report.Timestamp,
                InternalHours = report.InternalHours,
                ExternalHours = report.ExternalHours,
                IsOverhead = report.IsOverhead,
                WorkDescription = report.WorkDescription,
                UserId = report.UserId,
                ProjectId = report.ProjectId,
                ReportingDate = report.ReportingDate,

                User = report.User,
                Project = report.Project
            };
        }
    }
}
