using Leobit.TimeTracking.DAL.Entities;
using System.Collections.Generic;
using System.Linq;

namespace Leobit.TimeTracking.DAL.Extensions
{
    public static class ProjectExtensions
    {
        public static IQueryable<Project> ByUserId(this IQueryable<Project> projects, string userId)
        {
            return projects.Where(p => p.UserProject
                                        .Select(up => up.UserId)
                                        .Contains(userId));
        }

        public static IQueryable<Project> ByAccountIds(this IQueryable<Project> projects, ICollection<int> accountIds)
        {
            return (accountIds != null  && accountIds.Count != 0) ?
                projects.Where(p => accountIds.Contains(p.AccountId)) :
                projects;
        }

        public static IQueryable<Project> ById(this IQueryable<Project> projects, int? id)
        {
            return id.HasValue ?
                projects.Where(x => x.Id == id.Value) :
                projects;
        }

        public static IQueryable<Project> ByIds(this IQueryable<Project> projects, ICollection<int> projectIds)
        {
            return (projectIds != null && projectIds.Count != 0) ?
                projects.Where(p => projectIds.Contains(p.Id)) :
                projects;
        }

        public static IQueryable<Project> NotClosed(this IQueryable<Project> projects)
        {
            return projects.Where(p => !p.IsClosed);
        }
    }
}