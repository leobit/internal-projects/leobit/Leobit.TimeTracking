﻿namespace Leobit.TimeTracking.DAL.Interfaces
{
    public interface IHoursRestrictionRange
    {
        decimal MinInternal { get; set; }
        decimal MaxInternal { get; set; }
        decimal MinExternal { get; set; }
        decimal MaxExternal { get; set; }
    }
}
