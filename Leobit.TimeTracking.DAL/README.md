﻿# DB Scaffold guide
* Set Leobit.Timetracking.DAL as default project
* Run following shell command
```
    Scaffold-DbContext "CONNECTION_STRING" Microsoft.EntityFrameworkCore.SqlServer -OutputDir Entities -t Division, OrganizationUnit, Category, Account, Project, ProjectManager, Role, Permission, RolePermission, Settings, TimeReport, TimeReportHistory, TransferredVacationBalance, User, UserProject, Employee, Holiday, CustomDayOff -f -NoPluralize -NoOnConfiguring -Context LeobitContext
```