﻿using System;

namespace Leobit.TimeTracking.DAL.Common
{
    public enum EditAction
    {
        Edit = 0,
        Delete = 1
    }

    public class BackupOptions
    {
        public string EditorId { get; set; }

        public string EditGroup { get; set; }

        public DateTime EditTimestamp { get; set; }

        public EditAction EditAction { get; set; }
    }
}
