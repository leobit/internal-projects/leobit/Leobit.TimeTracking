﻿using Leobit.TimeTracking.BL.Services;
using System;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using static Leobit.TimeTracking.BL.Services.ExportService;

namespace Leobit.BigQueryExport
{
    public class BigQueryExportCoordinator
    {
        private readonly ExportService _exportService;

        public BigQueryExportCoordinator(ExportService exportService)
        {
            _exportService = exportService ?? throw new ArgumentNullException(nameof(exportService));
        }

        public async Task ExecuteAsync(ExportConfig config, BigQueryConfiguration configuration, CancellationToken cancellationToken = default)
        {
            var data = await _exportService.GetTimeReportsToExportBigQuery(config, cancellationToken);
            if (data.Any())
            {
                var tempFileName = $"{Guid.NewGuid()}.csv";
                CSV.SaveToCsv(data, tempFileName);
                await BigQuery.Export(configuration, tempFileName);
                File.Delete(tempFileName);
            }
        }
    }
}
