﻿namespace Leobit.BigQueryExport
{
    public class BigQueryConfigurations
    {
        public BigQueryConfiguration DefaultExport { get; set; }
        public BigQueryConfiguration MonthlyExport { get; set; }
    }

    public class BigQueryConfiguration
    {
        public string Credential { get; set; }
        public string ProjectId { get; set; }
        public string DatasetId { get; set; }
        public string TableId { get; set; }
    }
}