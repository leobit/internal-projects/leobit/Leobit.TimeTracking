﻿using CsvHelper;
using Google.Apis.Auth.OAuth2;
using Google.Cloud.BigQuery.V2;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Threading.Tasks;

namespace Leobit.BigQueryExport
{
    public static class CSV
    {
        public static void SaveToCsv<T>(IEnumerable<T> data, string path)
        {
            using var writer = new StreamWriter(path);
            using var csv = new CsvWriter(writer, CultureInfo.InvariantCulture);
            csv.WriteRecords(data);
        }
    }

    public static class BigQuery
    {
        public static async Task Export(BigQueryConfiguration configuration, string path)
        {
            using var fileStream = new FileStream(path, FileMode.Open, FileAccess.Read);
            using var client = BigQueryClient.Create(configuration.ProjectId, GoogleCredential.FromJson(configuration.Credential));

            await client.UploadCsv(
                datasetId: configuration.DatasetId,
                tableId: configuration.TableId,
                schema: null, // auto detect schema
                fileStream,
                new UploadCsvOptions()
                {
                    Autodetect = true,
                    AllowQuotedNewlines = true,
                    WriteDisposition = WriteDisposition.WriteTruncate
                }).PollUntilCompletedAsync();
        }
    }
}
